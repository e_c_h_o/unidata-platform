/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.io.Serializable;

import org.unidata.mdm.rest.system.ro.SimpleDataType;

public abstract class AbstractSimpleAttributeRO extends AbstractAttributeRO implements Serializable {

    private static final long serialVersionUID = -5284413340344918080L;
    /**
     * Can be null
     */
    private boolean nullable = true;

    /**
     * Should be unique
     */
    private boolean unique = false;

    /**
     * Attribute is generally searchable.
     */
    private boolean searchable = false;
    /**
     * data type.
     */
    private SimpleDataType simpleDataType;

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public SimpleDataType getSimpleDataType() {
        return simpleDataType;
    }

    public void setSimpleDataType(SimpleDataType simpleDataType) {
        this.simpleDataType = simpleDataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractSimpleAttributeRO)) return false;
        if (!super.equals(o)) return false;

        AbstractSimpleAttributeRO that = (AbstractSimpleAttributeRO) o;

        return simpleDataType == that.simpleDataType;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (simpleDataType != null ? simpleDataType.hashCode() : 0);
        return result;
    }
}
