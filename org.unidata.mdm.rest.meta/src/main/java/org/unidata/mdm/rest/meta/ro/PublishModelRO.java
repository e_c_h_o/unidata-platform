package org.unidata.mdm.rest.meta.ro;

import java.util.HashMap;
import java.util.Map;

import org.unidata.mdm.rest.system.ro.RestInputSource;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Mikhail Mikhailov on Oct 26, 2020
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublishModelRO implements RestInputSource {

    private long draftId;

    private boolean force;

    private boolean delete;

    private final Map<String, JsonNode> any = new HashMap<>();
    /**
     * Constructor.
     */
    public PublishModelRO() {
        super();
    }

    @Override
    @JsonAnyGetter
    public Map<String, JsonNode> getAny() {
        return any;
    }

    /**
     * @return the draftId
     */
    public long getDraftId() {
        return draftId;
    }

    /**
     * @param draftId the draftId to set
     */
    public void setDraftId(long draftId) {
        this.draftId = draftId;
    }

    /**
     * @return the force
     */
    public boolean isForce() {
        return force;
    }

    /**
     * @param force the force to set
     */
    public void setForce(boolean force) {
        this.force = force;
    }

    /**
     * @return the delete
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * @param delete the delete to set
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
