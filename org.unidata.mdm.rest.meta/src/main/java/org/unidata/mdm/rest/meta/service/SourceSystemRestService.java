/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import static java.util.Objects.isNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.ModelChangeContext.ModelChangeType;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.UpsertSourceSystemsContext;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.instance.SourceSystemsInstance;
import org.unidata.mdm.meta.type.model.sourcesystem.SourceSystemsModel;
import org.unidata.mdm.rest.meta.converter.SourceSystemConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.SourceSystemRO;
import org.unidata.mdm.rest.meta.ro.SourceSystemsRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.serialization.xml.XmlObjectSerializer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * Source system service.
 *
 * @author Michael Yashin. Created on 19.05.2015.
 */
@Path(SourceSystemRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class SourceSystemRestService extends AbstractRestService {
    /**
     * Path.
     */
    public static final String SERVICE_PATH = "source-systems";
    /**
     * The metamodel service.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Load and return list of all source systems.
     *
     * @return list of source systems. {@see SourceSystemsRO}.
     */
    @GET
    @Operation(
        description = "Get all source systems.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = SourceSystemsRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll() {
        return ok(SourceSystemConverter.to(metaModelService.instance(Descriptors.SOURCE_SYSTEMS)));
    }
    /**
     * Creates new source system.
     *
     * @param req Source system definition.
     * @return HTTP response.
     */
    @POST
    @Operation(
        description = "Creates a new source system.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SourceSystemRO.class)), description = "Insert request."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response create(final SourceSystemRO req) {

        boolean exists = metaModelService.instance(Descriptors.SOURCE_SYSTEMS)
            .getSourceSystem(req.getName()) != null;

        if (exists) {
            throw new PlatformBusinessException(
                    "Source system with this name already exists!",
                    MetaRestExceptionIds.EX_META_SOURCE_SYSTEMS_ALREADY_EXISTS);
        }

        metaModelService.upsert(
            UpsertSourceSystemsContext.builder()
                .update(SourceSystemConverter.from(req))
                .build());

        return ok(new UpdateResponse(true, req.getName()));
    }
    /**
     * Delete existing source system.
     *
     * @param sourceSystemName the source system name
     * @return HTTP response.
     */
    @DELETE
    @Path("{sourceSystemName}")
    @Operation(
        description = "Removes an existing source system.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(@Parameter(description = "Source system id.", in = ParameterIn.PATH) @PathParam(value = "sourceSystemName") String sourceSystemName) {

        boolean absent = metaModelService.instance(Descriptors.SOURCE_SYSTEMS)
            .getSourceSystem(sourceSystemName) == null;

        if (absent) {
            throw new PlatformBusinessException(
                    "Source system with this name not found!",
                    MetaRestExceptionIds.EX_META_SOURCE_SYSTEMS_NOT_FOUND);
        }

        metaModelService.upsert(
            UpsertSourceSystemsContext.builder()
                .delete(sourceSystemName)
                .build());

        return ok(new RestResponse<>(null));
    }
    /**
     * Updates existing source system.
     *
     * @param sourceSystemName the source system name
     * @param req              Updated source system. {@see SourceSystemRO}
     * @return HTTP response.
     */
    @PUT
    @Path("{sourceSystemName}")
    @Operation(
        description = "Update or rename an existing source system. If source system name in the path and the name in the body differ, the request is interpreted as renaming action.",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = SourceSystemRO.class)), description = "Update request"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(
            @Parameter(description = "Source system name.", in = ParameterIn.PATH) @PathParam("sourceSystemName") String sourceSystemName,
            SourceSystemRO req) {

        metaModelService.upsert(
            UpsertSourceSystemsContext.builder()
                .update(SourceSystemConverter.from(req))
                .delete(!StringUtils.equals(sourceSystemName, req.getName()) ? sourceSystemName : null)
                .build());

        // needed for sencha
        return ok(new UpdateResponse(true, req.getName()));
    }
    /**
     * Gets existing source system by name.
     *
     * @param sourceSystemName the source system name
     * @return Source system definition. {@see SourceSystemRO}
     */
    @GET
    @Path("{sourceSystemName}")
    @Operation(
        description = "Get an existing source system by name",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = SourceSystemRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response get(@Parameter(description = "Source system name.", in = ParameterIn.PATH) @PathParam("sourceSystemName") String sourceSystemName) {
        return ok(SourceSystemConverter.to(metaModelService
                .instance(Descriptors.SOURCE_SYSTEMS)
                .getSourceSystem(sourceSystemName)));
    }

    @POST
    @Path("/import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(
        description = "Import full source systems model. Only xml is supported.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA), description = "Source request."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response source(@Multipart(required = true, value = "file") Attachment attachment) throws IOException {

        if (isNull(attachment)) {
            return okOrNotFound(null);
        }

        if (!MediaType.TEXT_XML_TYPE.equals(attachment.getContentType())) {
            throw new PlatformBusinessException("Import of source systems failed. Invalid media type [{}]. XML is expected.",
                    MetaRestExceptionIds.EX_META_SOURCE_SYSTEMS_IMPORT_UNSUPPORTED, attachment.getContentType().toString());
        }

        SourceSystemsModel values = XmlObjectSerializer.getInstance()
                .fromXmlInputStream(SourceSystemsModel.class, attachment.getObject(InputStream.class));

        if (CollectionUtils.isEmpty(values.getValues())) {
            throw new PlatformBusinessException("Import of source systems failed. Empty definition.",
                    MetaRestExceptionIds.EX_META_SOURCE_SYSTEMS_IMPORT_EMPTY);
        }

        metaModelService.upsert(UpsertSourceSystemsContext.builder()
            .upsertType(ModelChangeType.FULL)
            .update(values.getValues())
            .storageId(values.getStorageId())
            .name(values.getName())
            .build());

        return ok(new RestResponse<>());
    }

    @GET
    @Path("/export")
    @Produces(MediaType.TEXT_XML)
    @Operation(
        description = "Dump measurement units model.",
        method = HttpMethod.GET,
        parameters = { @Parameter(description = "Storage ID. Optional", in = ParameterIn.QUERY, name = "storageId") },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StreamingOutput.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response dump(@QueryParam("storageId") String storageId) throws UnsupportedEncodingException {

        SourceSystemsInstance current = StringUtils.isBlank(storageId)
                ? metaModelService.instance(Descriptors.SOURCE_SYSTEMS)
                : metaModelService.instance(Descriptors.SOURCE_SYSTEMS, storageId, null);

        if (Objects.isNull(current) || current.isEmpty()) {
            return okOrNotFound(null);
        }

        SourceSystemsModel source = current.toSource();
        final String encodedFilename = URLEncoder.encode(
                "source-systems-" + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss") + ".xml",
                StandardCharsets.UTF_8.name());

        StreamingOutput result = output -> {
            try {
                output.write(
                    XmlObjectSerializer.getInstance()
                        .toXmlString(source, true)
                        .getBytes(StandardCharsets.UTF_8));
            } catch (Exception e) {
                throw new PlatformBusinessException("Source systems marshaling failed.",
                        MetaRestExceptionIds.EX_META_SOURCE_SYSTEMS_MARSHALING_FAILED);
            }
        };

        return Response.ok(result)
                .encoding(StandardCharsets.UTF_8.name())
                .header("Content-Disposition", "attachment; filename=" + encodedFilename + "; filename*=UTF-8''" + encodedFilename)
                .header("Content-Type", MediaType.TEXT_XML)
                .build();
    }
}
