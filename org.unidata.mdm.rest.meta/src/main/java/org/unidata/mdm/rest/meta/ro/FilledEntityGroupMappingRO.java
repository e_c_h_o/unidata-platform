/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.ro;

import java.util.Collection;

public class FilledEntityGroupMappingRO {

    private Collection<FilledEntityGroupRO> groupNodes;

    public FilledEntityGroupMappingRO(Collection<FilledEntityGroupRO> groupNodes) {
        this.groupNodes = groupNodes;
    }

    public FilledEntityGroupMappingRO() {
    }

    public Collection<FilledEntityGroupRO> getGroupNodes() {
        return groupNodes;
    }

    public void setGroupNodes(Collection<FilledEntityGroupRO> groupNodes) {
        this.groupNodes = groupNodes;
    }
}
