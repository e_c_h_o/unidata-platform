/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.unidata.mdm.rest.meta.ro.CustomExternalIdGenerationStrategyRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * @author Alexander Malyshev
 */
@Path(ExternalIdStrategyRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class ExternalIdStrategyRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "external-id-strategy";

    @GET
    @Operation(
        description = "Вернуть список всех доступных пользовательских стратегий генерации внешнего ключа",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = List.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response getAvailableExternalIdStrategies() {
        // TODO @Modules
//        List<CustomExternalIdGenerationStrategy> definitions = configurationService.getExternalIdGeneratorDefinitions();
        List<CustomExternalIdGenerationStrategyRO> result = new ArrayList<>();
        // TODO @Modules
//        if (CollectionUtils.isEmpty(definitions)) {
//            result = Collections.emptyList();
//        } else {
//            result = definitions.stream()
//                    .map(dto -> {
//                        CustomExternalIdGenerationStrategyRO ro = new CustomExternalIdGenerationStrategyRO();
//                        ro.setStrategyId(dto.getId());
//                        ro.setName(dto.getName());
//                        ro.setDescription(dto.getDescription());
//                        return ro;
//                    })
//                    .collect(Collectors.toList());
//        }
        return ok(new RestResponse<>(result));
    }

}
