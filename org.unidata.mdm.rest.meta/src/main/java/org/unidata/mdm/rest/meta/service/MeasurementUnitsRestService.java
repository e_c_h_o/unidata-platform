/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.service;

import static java.lang.String.CASE_INSENSITIVE_ORDER;
import static java.util.Objects.isNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.ModelChangeContext.ModelChangeType;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.context.UpsertMeasurementUnitsContext;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.instance.MeasurementUnitsInstance;
import org.unidata.mdm.meta.type.model.measurement.MeasurementUnitsModel;
import org.unidata.mdm.rest.meta.converter.MeasurementUnitsConverter;
import org.unidata.mdm.rest.meta.exception.MetaRestExceptionIds;
import org.unidata.mdm.rest.meta.ro.MeasurementCategoryRO;
import org.unidata.mdm.rest.system.ro.ErrorResponse;
import org.unidata.mdm.rest.system.ro.RestResponse;
import org.unidata.mdm.rest.system.ro.UpdateResponse;
import org.unidata.mdm.rest.system.service.AbstractRestService;
import org.unidata.mdm.system.exception.PlatformBusinessException;
import org.unidata.mdm.system.serialization.xml.XmlObjectSerializer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path(MeasurementUnitsRestService.SERVICE_PATH)
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class MeasurementUnitsRestService extends AbstractRestService {

    public static final String SERVICE_PATH = "measurement-units";

    @Autowired
    private MetaModelService metaModelService;

    private static final Comparator<MeasurementCategoryRO> CATEGORIES_COMPARATOR = (o1, o2) -> CASE_INSENSITIVE_ORDER
            .compare(o1.getName(), o2.getName());

    @GET
    @Operation(
        description = "Gets all measurement units, registered for user's storage ID.",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = RestResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response findAll() {

        Collection<MeasurementCategoryRO> result = metaModelService.instance(Descriptors.MEASUREMENT_UNITS).getCategories().stream()
                .map(MeasurementUnitsConverter::to)
                .sorted(CATEGORIES_COMPARATOR)
                .collect(Collectors.toList());

        return ok(new RestResponse<>(result));
    }

    /**
     * Creates new MUC.
     *
     * @param req MUC definition.
     * @return HTTP response.
     */
    @POST
    @Operation(
        description = "Creates an measurement units category.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = MeasurementCategoryRO.class)), description = "Insert request."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response create(final MeasurementCategoryRO req) {

        boolean exists = metaModelService.instance(Descriptors.MEASUREMENT_UNITS)
            .getCategory(req.getName()) != null;

        if (exists) {
            throw new PlatformBusinessException(
                    "Measurement units category with this name already exists!",
                    MetaRestExceptionIds.EX_META_MEASUREMENT_CATEGORY_ALREADY_EXISTS);
        }

        metaModelService.upsert(
            UpsertMeasurementUnitsContext.builder()
                .update(MeasurementUnitsConverter.from(req))
                .build());

        return ok(new UpdateResponse(true, req.getName()));
    }
    /**
     * Updates existing MUC.
     *
     * @param measurementCategoryName the MUC name
     * @param req              Updated MUC.
     * @return HTTP response.
     */
    @PUT
    @Path("{measurementCategoryName}")
    @Operation(
        description = "Update or rename an existing measurement units category. If name in the path and the name in the body differ, the request is interpreted as renaming action.",
        method = HttpMethod.PUT,
        requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = MeasurementCategoryRO.class)), description = "Update request"),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response update(
            @Parameter(description = "Measurement units category name.", in = ParameterIn.PATH) @PathParam("measurementCategoryName") String measurementCategoryName,
            MeasurementCategoryRO req) {

        metaModelService.upsert(
            UpsertMeasurementUnitsContext.builder()
                .update(MeasurementUnitsConverter.from(req))
                .delete(!StringUtils.equals(measurementCategoryName, req.getName()) ? measurementCategoryName : null)
                .build());

        // needed for sencha
        return ok(new UpdateResponse(true, req.getName()));
    }
    /**
     * Delete existing MUC.
     *
     * @param measurementCategoryName the MUC name
     * @return HTTP response.
     */
    @DELETE
    @Path("{measurementCategoryName}")
    @Operation(
        description = "Removes an existing measurement units category.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = String.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response delete(@Parameter(description = "MUC id.", in = ParameterIn.PATH) @PathParam(value = "measurementCategoryName") String measurementCategoryName) {

        boolean absent = metaModelService.instance(Descriptors.MEASUREMENT_UNITS)
            .getCategory(measurementCategoryName) == null;

        if (absent) {
            throw new PlatformBusinessException(
                    "Measurement units category with the name [{}] not found!",
                    MetaRestExceptionIds.EX_META_MEASUREMENT_CATEGORY_NOT_FOUND,
                    measurementCategoryName);
        }

        metaModelService.upsert(
            UpsertMeasurementUnitsContext.builder()
                .delete(measurementCategoryName)
                .build());

        return ok(new RestResponse<>(null));
    }
    /**
     * Gets existing MUC by name.
     *
     * @param measurementCategoryName the MUC name
     * @return MUC definition.
     */
    @GET
    @Path("{measurementCategoryName}")
    @Operation(
        description = "Get an existing MUC by name",
        method = HttpMethod.GET,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = MeasurementCategoryRO.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response get(@Parameter(description = "MUC name.", in = ParameterIn.PATH) @PathParam("measurementCategoryName") String measurementCategoryName) {
        return ok(MeasurementUnitsConverter.to(metaModelService.instance(Descriptors.MEASUREMENT_UNITS).getCategory(measurementCategoryName)));
    }

    @POST
    @Path("/import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(
        description = "Import full measurement units model. Only xml is supported.",
        method = HttpMethod.POST,
        requestBody = @RequestBody(content = @Content(mediaType = MediaType.MULTIPART_FORM_DATA), description = "Source request."),
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = UpdateResponse.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response source(@Multipart(required = true, value = "file") Attachment attachment) throws IOException {

        if (isNull(attachment)) {
            return okOrNotFound(null);
        }

        if (!MediaType.TEXT_XML_TYPE.equals(attachment.getContentType())) {
            throw new PlatformBusinessException("Import of measurement units impossible. Invalid media type [{}]. XML is expected.",
                    MetaRestExceptionIds.EX_META_MEASUREMENT_IMPORT_UNSUPPORTED, attachment.getContentType().toString());
        }

        MeasurementUnitsModel values = XmlObjectSerializer.getInstance()
                .fromXmlInputStream(MeasurementUnitsModel.class, attachment.getObject(InputStream.class));

        if (CollectionUtils.isEmpty(values.getValues())) {
            throw new PlatformBusinessException("Import of measurement units failed. Empty definition.",
                    MetaRestExceptionIds.EX_META_MEASUREMENT_IMPORT_EMPTY);
        }

        metaModelService.upsert(UpsertMeasurementUnitsContext.builder()
            .upsertType(ModelChangeType.FULL)
            .update(values.getValues())
            .storageId(values.getStorageId())
            .name(values.getName())
            .build());

        return ok(new RestResponse<>());
    }

    @GET
    @Path("/export")
    @Produces(MediaType.TEXT_XML)
    @Operation(
        description = "Dump measurement units model.",
        method = HttpMethod.GET,
        parameters = { @Parameter(description = "Storage ID. Optional", in = ParameterIn.QUERY, name = "storageId") },
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = StreamingOutput.class)), responseCode = "200"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = ErrorResponse.class)), responseCode = "500")
        }
    )
    public Response dump(@QueryParam("storageId") String storageId) throws UnsupportedEncodingException {

        MeasurementUnitsInstance current = StringUtils.isBlank(storageId)
                ? metaModelService.instance(Descriptors.MEASUREMENT_UNITS)
                : metaModelService.instance(Descriptors.MEASUREMENT_UNITS, storageId, null);

        if (Objects.isNull(current) || current.isEmpty()) {
            return okOrNotFound(null);
        }

        MeasurementUnitsModel source = current.toSource();
        final String encodedFilename = URLEncoder.encode(
                "measurement-units-" + DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd_HH-mm-ss") + ".xml",
                StandardCharsets.UTF_8.name());

        StreamingOutput result = output -> {
            try {
                output.write(
                    XmlObjectSerializer.getInstance()
                        .toXmlString(source, true)
                        .getBytes(StandardCharsets.UTF_8));
            } catch (Exception e) {
                throw new PlatformBusinessException("Marshaling is failed",
                        MetaRestExceptionIds.EX_META_MEASUREMENT_MARSHALING_FAILED);
            }
        };

        return Response.ok(result)
                .encoding(StandardCharsets.UTF_8.name())
                .header("Content-Disposition", "attachment; filename=" + encodedFilename + "; filename*=UTF-8''" + encodedFilename)
                .header("Content-Type", MediaType.TEXT_XML)
                .build();
    }
}
