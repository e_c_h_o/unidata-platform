/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.meta.converter;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.meta.type.model.MetaModelAttribute;
import org.unidata.mdm.meta.type.model.SimpleDataType;
import org.unidata.mdm.meta.type.model.attributes.AttributeMeasurementSettings;
import org.unidata.mdm.meta.type.model.attributes.SimpleMetaModelAttribute;
import org.unidata.mdm.rest.meta.ro.AbstractAttributeRO;
import org.unidata.mdm.rest.meta.ro.SimpleAttributeRO;

public final class SimpleAttributeDefConverter {

    /**
     * Constructor.
     */
    private SimpleAttributeDefConverter() {
        super();
    }

    /**
     * Copies simple attributes from list to list.
     *
     * @param source
     *            the source
     * @param target
     *            the target
     */
    public static void copySimpleAttributeDataList(List<SimpleAttributeRO> source,
                                                   List<SimpleMetaModelAttribute> target) {
        if (source == null) {
            return;
        }

        for (SimpleAttributeRO sourceAttr : source) {
            SimpleMetaModelAttribute targetAttr = new SimpleMetaModelAttribute();
            copySimpleAttributeData(sourceAttr, targetAttr);
            target.add(targetAttr);
        }
    }

    /**
     * Copy simple attributes data from REST to internal.
     *
     * @param source
     *            REST source
     * @param target
     *            internal
     */
    private static void copySimpleAttributeData(SimpleAttributeRO source, SimpleMetaModelAttribute target) {
        copyAbstractAttributeData(source, target);
        target.setMask(source.getMask());
        target.setEnumDataType(source.getEnumDataType());
        target.setLookupEntityType(source.getLookupEntityType());

        if (StringUtils.isNotEmpty(source.getLookupEntityType())) {
            if (CollectionUtils.isNotEmpty(source.getLookupEntityDisplayAttributes())) {
                target.withLookupEntityDisplayAttributes(source.getLookupEntityDisplayAttributes());
            }
            if (CollectionUtils.isNotEmpty(source.getLookupEntitySearchAttributes())) {
                target.withLookupEntitySearchAttributes(source.getLookupEntitySearchAttributes());
            }
        }
        if (StringUtils.isNotEmpty(source.getDictionaryDataType())) {
            target.withDictionaryDataType(source.getDictionaryDataType());
        }

        target.setUseAttributeNameForDisplay(source.isUseAttributeNameForDisplay());
        target.setNullable(source.isNullable());
        target.setSimpleDataType(source.getSimpleDataType() == null ? null : SimpleDataType.fromValue(source
                .getSimpleDataType().value()));
        target.setUnique(source.isUnique());
        target.setOrder(source.getOrder());
        target.setSearchable(source.isSearchable());
        target.setSearchMorphologically(source.isSearchMorphologically());
        target.setSearchCaseInsensitive(source.isSearchCaseInsensitive());
        target.setDisplayable(source.isDisplayable());
        target.setMainDisplayable(source.isMainDisplayable());
        target.setLinkDataType(source.getLinkDataType());

        String categoryId = source.getValueId();
        String unitId = source.getDefaultUnitId();
        if (target.getSimpleDataType() == SimpleDataType.NUMBER && !isBlank(categoryId) && !isBlank(unitId)) {
            AttributeMeasurementSettings measurement = new AttributeMeasurementSettings();
            measurement.withCategoryId(categoryId).withDefaultUnitId(unitId);
            target.withMeasureSettings(measurement);
            target.setSimpleDataType(SimpleDataType.MEASURED);
        }
    }

    /**
     * Copy abstract attribute data from REST to internal.
     *
     * @param source
     *            REST source
     * @param target
     *            internal
     */
    public static void copyAbstractAttributeData(AbstractAttributeRO source, MetaModelAttribute target) {
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setHidden(source.isHidden());
        target.setReadOnly(source.isReadOnly());
        target.setCustomProperties(CustomPropertiesConverter.from(source.getCustomProperties()));
    }
}
