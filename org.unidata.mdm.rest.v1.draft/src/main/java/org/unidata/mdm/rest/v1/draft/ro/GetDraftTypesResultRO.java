package org.unidata.mdm.rest.v1.draft.ro;

import java.util.Map;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get draft types response object
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
public class GetDraftTypesResultRO extends DetailedOutputRO {

    private Map<String, String> types;

    public Map<String, String> getTypes() {
        return types;
    }

    public void setTypes(Map<String, String> types) {
        this.types = types;
    }
}
