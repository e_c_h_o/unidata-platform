package org.unidata.mdm.rest.v1.draft.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Remove draft result
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
public class RemoveDraftResultRO extends DetailedOutputRO {

    private int removed;

    public int getRemoved() {
        return removed;
    }

    public void setRemoved(int removed) {
        this.removed = removed;
    }
}
