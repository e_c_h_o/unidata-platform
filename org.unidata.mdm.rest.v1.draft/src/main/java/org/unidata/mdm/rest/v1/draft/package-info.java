/**
 * Draft REST service module
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
package org.unidata.mdm.rest.v1.draft;