package org.unidata.mdm.rest.v1.draft.ro;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert draft result
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
public class UpsertDraftResultRO extends DetailedOutputRO {

    private DraftRO draft;

    public DraftRO getDraft() {
        return draft;
    }

    public void setDraft(DraftRO draft) {
        this.draft = draft;
    }
}
