package org.unidata.mdm.rest.v1.draft.ro;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * TODO:
 *
 * @author Alexandr Serov
 * @since 01.12.2020
 **/
public class GetDraftResultRO extends DetailedOutputRO {

    private List<DraftRO> drafts;
    private long totalCount;

    public List<DraftRO> getDrafts() {
        return drafts;
    }

    public void setDrafts(List<DraftRO> drafts) {
        this.drafts = drafts;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
