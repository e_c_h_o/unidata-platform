/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.draft.configuration;

import java.util.Arrays;
import java.util.Collections;
import javax.ws.rs.core.Application;
import javax.ws.rs.ext.RuntimeDelegate;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.unidata.mdm.rest.system.exception.DetailedRestExceptionMapper;
import org.unidata.mdm.rest.system.service.ReceiveInInterceptor;
import org.unidata.mdm.rest.system.util.OpenApiMetadataFactory;
import org.unidata.mdm.rest.v1.draft.converter.DraftConverter;
import org.unidata.mdm.rest.v1.draft.service.DraftRestApplication;
import org.unidata.mdm.rest.v1.draft.service.DraftRestService;

/**
 * @author Alexander Malyshev
 */
@Configuration
public class DraftRestConfiguration {

    @Bean
    public Application draftRestApplication() {
        return new DraftRestApplication();
    }

    @Bean
    public DraftRestService draftRestService() {
        return new DraftRestService();
    }

    @Bean
    public DraftConverter draftConverter() {
        return new DraftConverter();
    }

    @Bean
    public DetailedRestExceptionMapper detailedRestExceptionMapper() {
        return new DetailedRestExceptionMapper(() -> "DRAFT_REST_SERVICE");
    }

    @Bean
    public Server server(
            final Bus cxf,
            final Application draftRestApplication,
            final JacksonJaxbJsonProvider jacksonJaxbJsonProvider,
            final DetailedRestExceptionMapper restExceptionMapper,
            final ReceiveInInterceptor receiveInInterceptor,
            final DraftRestService draftRestService) {

        final JAXRSServerFactoryBean jaxrsServerFactoryBean = RuntimeDelegate.getInstance()
                .createEndpoint(draftRestApplication, JAXRSServerFactoryBean.class);

        final OpenApiFeature draftOpenApiFeature = OpenApiMetadataFactory.openApiFeature(
                "Unidata draft API",
                "Unidata draft REST API operations",
                draftRestApplication, cxf,
                "org.unidata.mdm.rest.v1.draft.service");

        jaxrsServerFactoryBean.setFeatures(Collections.singletonList(draftOpenApiFeature));
        jaxrsServerFactoryBean.setProviders(Arrays.asList(jacksonJaxbJsonProvider, restExceptionMapper));
        jaxrsServerFactoryBean.setInInterceptors(Collections.singletonList(receiveInInterceptor));
        jaxrsServerFactoryBean.setServiceBeans(Collections.singletonList(draftRestService));

        return jaxrsServerFactoryBean.create();
    }

}
