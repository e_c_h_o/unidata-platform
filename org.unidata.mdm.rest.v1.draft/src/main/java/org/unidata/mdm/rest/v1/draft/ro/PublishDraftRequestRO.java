package org.unidata.mdm.rest.v1.draft.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PublishDraftRequestRO {

    private Long draftId;
    private boolean force;
    private boolean delete;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean forse) {
        this.force = forse;
    }

    /**
     * @return the delete
     */
    public boolean isDelete() {
        return delete;
    }
    /**
     * @param delete the delete to set
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
