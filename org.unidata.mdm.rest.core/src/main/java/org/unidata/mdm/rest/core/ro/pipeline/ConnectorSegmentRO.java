package org.unidata.mdm.rest.core.ro.pipeline;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class ConnectorSegmentRO extends SegmentRO {
    /**
     * Connected start id.
     */
    private String startId;
    /**
     * Connected subject id.
     */
    private String subjectId;
    /**
     * Constructor.
     */
    public ConnectorSegmentRO() {
        super();
    }
    /**
     * Constructor.
     */
    public ConnectorSegmentRO(String startId, String subjectId) {
        super();
        this.startId = startId;
        this.subjectId = subjectId;
    }
    /**
     * @return the start
     */
    public String getStartId() {
        return startId;
    }
    /**
     * @param start the start to set
     */
    public void setStartId(String connectedId) {
        this.startId = connectedId;
    }
    /**
     * @return the subjectId
     */
    public String getSubjectId() {
        return subjectId;
    }
    /**
     * @param subjectId the subjectId to set
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }
}
