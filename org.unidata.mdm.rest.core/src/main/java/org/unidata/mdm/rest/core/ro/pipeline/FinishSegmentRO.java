package org.unidata.mdm.rest.core.ro.pipeline;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class FinishSegmentRO extends SegmentRO {
    /**
     * Constructor.
     */
    public FinishSegmentRO() {
        super();
    }
}
