/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.core.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.rest.core.ro.pipeline.ConnectorSegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.FallbackSegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.FinishSegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.PipelineRO;
import org.unidata.mdm.rest.core.ro.pipeline.PointSegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.SegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.SplitterSegmentRO;
import org.unidata.mdm.rest.core.ro.pipeline.StartSegmentRO;
import org.unidata.mdm.system.exception.PipelineException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.type.pipeline.Connector;
import org.unidata.mdm.system.type.pipeline.Fallback;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Outcome;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.type.pipeline.SegmentType;
import org.unidata.mdm.system.type.pipeline.Splitter;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.connection.OutcomesPipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.PipelineConnection;
import org.unidata.mdm.system.type.pipeline.connection.SinglePipelineConnection;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on Nov 25, 2019
 */
public class PipelinesConverter {

    private PipelinesConverter() {
        super();
    }

    public static List<PipelineRO> toPipelines(Collection<Pipeline> pipelines) {

        if (CollectionUtils.isEmpty(pipelines)) {
            return Collections.emptyList();
        }

        List<PipelineRO> result = new ArrayList<>(pipelines.size());
        for (Pipeline p : pipelines) {

            PipelineRO pro = to(p);
            if (Objects.isNull(pro)) {
                continue;
            }

            result.add(pro);
        }

        return result;
    }

    public static PipelineRO to (Pipeline p) {

        if (Objects.isNull(p)) {
            return null;
        }

        // Order segments according to expectations from UI
        List<Segment> view = new ArrayList<>(p.getSegments().size() + p.getFallbacks().size());
        view.addAll(p.getSegments().subList(0, p.getSegments().size() - 1));
        view.addAll(p.getFallbacks());
        view.add(p.getSegments().get(p.getSegments().size() - 1));

        PipelineRO result = new PipelineRO();
        result.setStartId(p.getStartId());
        result.setSubjectId(p.getSubjectId());
        result.setDescription(p.getDescription());
        result.setSegments(toSegments(view, p));

        return result;
    }

    public static List<SegmentRO> toSegments(Collection<Segment> segments, Pipeline p) {

        if (CollectionUtils.isEmpty(segments)) {
            return Collections.emptyList();
        }

        List<SegmentRO> result = new ArrayList<>(segments.size());
        for (Segment s : segments) {

            SegmentRO sro = to(s, p);
            if (Objects.isNull(sro)) {
                continue;
            }

            result.add(sro);
        }

        return result;
    }

    public static SegmentRO to(Segment s, Pipeline p) {

        if (Objects.isNull(s)) {
            return null;
        }

        SegmentRO result = null;
        switch (s.getType()) {
        case CONNECTOR:
            SinglePipelineConnection spc = p.getConnection(s);
            if (Objects.nonNull(spc)) {
                result = new ConnectorSegmentRO(spc.getPipeline().getStartId(), spc.getPipeline().getSubjectId());
            } else {
                result = new ConnectorSegmentRO();
            }
            break;
        case FALLBACK:
            result = new FallbackSegmentRO();
            break;
        case FINISH:
            result = new FinishSegmentRO();
            break;
        case POINT:
            result = new PointSegmentRO();
            break;
        case SPLITTER:
            OutcomesPipelineConnection opc = p.getConnection(s);
            if (Objects.nonNull(opc)) {
                result = new SplitterSegmentRO(opc.serialize());
            } else {
                result = new SplitterSegmentRO();
            }

            ((SplitterSegmentRO) result).setOutcomeNames(Arrays.stream(((Splitter<?, ?>) s).getOutcomes())
                    .map(Outcome::getName)
                    .collect(Collectors.toList()));
            break;
        case START:
            result = new StartSegmentRO();
            break;
        default:
            break;
        }

        Objects.requireNonNull(result, "Trying to marshal segment of unknown type.");

        result.setId(s.getId());
        result.setDescription(s.getDescription());
        result.setSegmentType(s.getType().name());

        return result;
    }

    public static Pipeline from(PipelineRO ro) {

        if (Objects.isNull(ro)) {
            return null;
        }

        // Gather segments
        List<Pair<Segment, PipelineConnection>> gathered = fromSegments(ro.getSegments());

        throwOnWrongPipelineBounds(gathered);

        Start<?> s = (Start<?>) gathered.get(0).getKey();

        Pipeline p = Pipeline.start(s, StringUtils.isBlank(ro.getSubjectId()) ? StringUtils.EMPTY : ro.getSubjectId(), ro.getDescription());
        for (int i = 1; i < gathered.size(); i++) {

            Pair<Segment, PipelineConnection> segment = gathered.get(i);
            if (segment.getKey().getType() == SegmentType.POINT) {
                p.with((Point<?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.CONNECTOR) {
                if (Objects.nonNull(segment.getValue())) {
                    p.with((Connector<?, ?>) segment.getKey(), segment.getValue());
                } else {
                    p.with((Connector<?, ?>) segment.getKey());
                }
            } else if (segment.getKey().getType() == SegmentType.FALLBACK) {
                p.fallback((Fallback<?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.FINISH) {
                p.end((Finish<?, ?>) segment.getKey());
            } else if (segment.getKey().getType() == SegmentType.SPLITTER) {
                p.split((Splitter<?, ?>) segment.getKey(), segment.getValue());
            }
        }

        return p;
    }

    public static List<Pair<Segment, PipelineConnection>> fromSegments(List<SegmentRO> ros) {

        if (CollectionUtils.isEmpty(ros)) {
            return Collections.emptyList();
        }

        List<Pair<Segment, PipelineConnection>> result = new ArrayList<>(ros.size());
        for (SegmentRO ro : ros) {
            result.add(from(ro));
        }

        return result;
    }

    public static Pair<Segment, PipelineConnection> from(SegmentRO ro) {

        if (Objects.isNull(ro)) {
            return null;
        }

        Segment hit = PipelineUtils.findSegment(ro.getId());
        if (Objects.isNull(hit) || !hit.getType().name().equals(ro.getSegmentType())) {
            throw new PipelineException("Segment not found by id or is of wrong type [{}].",
                    SystemExceptionIds.EX_PIPELINE_SEGMENT_NOT_FOUND_BY_ID,
                    ro.getId());
        }

        if (hit.getType() == SegmentType.CONNECTOR) {
            ConnectorSegmentRO csro = (ConnectorSegmentRO) ro;
            if (StringUtils.isNotBlank(csro.getStartId())) {
                return Pair.of(hit, PipelineConnection.of(csro.getStartId(), csro.getSubjectId()));
            }
        } else if (hit.getType() == SegmentType.SPLITTER) {
            SplitterSegmentRO ssro = (SplitterSegmentRO) ro;
            if (MapUtils.isNotEmpty(ssro.getOutcomes())) {
                return Pair.of(hit, PipelineConnection.of(ssro.getOutcomes()));
            }
        }

        return Pair.of(hit, null);
    }

    private static void throwOnWrongPipelineBounds(List<Pair<Segment, PipelineConnection>> gathered) {
        if (gathered.size() < 2) {
            throw new PipelineException("Invalid number of segments. A pipeline must contain at least 2 points of type 'start' and 'finish' or 'splitter'.",
                    SystemExceptionIds.EX_PIPELINE_INVALID_NUMBER_OF_SEGMENTS);
        } else if (gathered.get(0).getKey().getType() != SegmentType.START
                || (gathered.get(gathered.size() - 1).getKey().getType() != SegmentType.FINISH
                 && gathered.get(gathered.size() - 1).getKey().getType() != SegmentType.SPLITTER)) {
            throw new PipelineException("Invalid pipeline layout. A pipeline must start with a point of type 'start' and end with a point of type 'finish' or 'splitter'.",
                    SystemExceptionIds.EX_PIPELINE_HAS_NO_START_OR_FINISH_OR_BOTH);
        }
    }
}
