package org.unidata.mdm.rest.core.ro.pipeline;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class SplitterSegmentRO extends SegmentRO {

    private Map<String, String> outcomes = new HashMap<>();

    private List<String> outcomeNames;
    /**
     * Constructor.
     */
    public SplitterSegmentRO() {
        super();
    }
    /**
     * Constructor.
     */
    public SplitterSegmentRO(Map<String, String> outcomes) {
        super();
        this.outcomes.putAll(outcomes);
    }
    /**
     * @return the outcomes
     */
    public Map<String, String> getOutcomes() {
        return outcomes;
    }
    /**
     * @param outcomes the outcomes to set
     */
    public void setOutcomes(Map<String, String> outcomes) {
        this.outcomes = outcomes;
    }
    /**
     * @return the outcomeNames
     */
    public List<String> getOutcomeNames() {
        return outcomeNames;
    }
    /**
     * @param outcomeNames the outcomeNames to set
     */
    public void setOutcomeNames(List<String> outcomeNames) {
        this.outcomeNames = outcomeNames;
    }
}
