/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;
import org.unidata.mdm.core.type.data.ArrayAttribute;
import org.unidata.mdm.core.type.data.ArrayValue;
import org.unidata.mdm.core.type.data.CodeLinkValue;
import org.unidata.mdm.core.type.data.extended.WinnerInformationArrayAttribute;
import org.unidata.mdm.core.type.data.impl.DateArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.DateArrayValue;
import org.unidata.mdm.core.type.data.impl.DictionaryArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.DictionaryArrayValue;
import org.unidata.mdm.core.type.data.impl.IntegerArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.IntegerArrayValue;
import org.unidata.mdm.core.type.data.impl.NumberArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.NumberArrayValue;
import org.unidata.mdm.core.type.data.impl.StringArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.StringArrayValue;
import org.unidata.mdm.core.type.data.impl.TimeArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.TimeArrayValue;
import org.unidata.mdm.core.type.data.impl.TimestampArrayAttributeImpl;
import org.unidata.mdm.core.type.data.impl.TimestampArrayValue;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.meta.ro.ArrayDataType;
import org.unidata.mdm.rest.v1.data.ro.attributes.ArrayAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.ArrayObjectRO;
import org.unidata.mdm.rest.v1.data.ro.extended.ExtendedArrayAttributeRO;

/**
 * @author Mikhail Mikhailov
 *
 */
public class ArrayAttributeConverter {

    /**
     * Constructor.
     */
    private ArrayAttributeConverter() {
        super();
    }

    public static ArrayAttribute<?> from(ArrayAttributeRO source) {

        if (Objects.isNull(source)) {
            return null;
        }

        ArrayAttribute<?> result;
        org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType type
            = org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType.valueOf(source.getType().name());

        switch (type) {
        case DATE:
            result = new DateArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new DateArrayValue((LocalDate) v))
                        .collect(Collectors.toList()));
            break;
        case INTEGER:
            result = new IntegerArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new IntegerArrayValue((Long) v))
                        .collect(Collectors.toList()));
            break;
        case NUMBER:
            result = new NumberArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new NumberArrayValue((Double) v))
                        .collect(Collectors.toList()));
            break;
        case STRING:
            result = new StringArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new StringArrayValue((String) v))
                        .collect(Collectors.toList()));
            break;
            case DICTIONARY:
                result = new DictionaryArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                        ? null
                        : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new DictionaryArrayValue((String) v))
                        .collect(Collectors.toList()));
                break;

            case TIME:
            result = new TimeArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new TimeArrayValue((LocalTime) v))
                        .collect(Collectors.toList()));
            break;
        case TIMESTAMP:
            result = new TimestampArrayAttributeImpl(source.getName(), CollectionUtils.isEmpty(source.getValue())
                    ? null
                    : source.getValue().stream()
                        .map(ArrayObjectRO::getValue)
                        .map(v -> new TimestampArrayValue((LocalDateTime) v))
                        .collect(Collectors.toList()));
            break;
        default:
            return null;
        }

        return result;
    }

    public static List<ArrayAttribute<?>> from(Collection<ArrayAttributeRO> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<ArrayAttribute<?>> result = new ArrayList<>(source.size());
        for (ArrayAttributeRO val : source) {
            ArrayAttribute<?> converted = from(val);
            if (Objects.isNull(converted)) {
                continue;
            }

            result.add(converted);
        }

        return result;
    }

    public static ArrayAttributeRO to(ArrayAttribute<?> source) {
        if (Objects.isNull(source)) {
            return null;
        }

        ArrayAttributeRO target = new ArrayAttributeRO();
        populate(source, target);
        return target;
    }

    public static List<ArrayAttributeRO> to(Collection<ArrayAttribute<?>> source) {
        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<ArrayAttributeRO> result = new ArrayList<>(source.size());
        for (ArrayAttribute<?> attr : source) {
            ArrayAttributeRO converted = to(attr);
            if (Objects.isNull(converted)) {
                continue;
            }

            result.add(converted);
        }
        return result;
    }

    public static List<ArrayAttributeRO> to(Collection<ArrayAttribute<?>> source, EtalonRecord etalonRecord, RecordOriginKey originKey) {
        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<ArrayAttributeRO> result = new ArrayList<>(source.size());
        for (ArrayAttribute<?> sourceAttribute : source) {
            if(sourceAttribute != null){
                ExtendedArrayAttributeRO targetAttribute = new ExtendedArrayAttributeRO();
                populate(sourceAttribute, targetAttribute);

                ArrayAttribute<?> winnerAttribute = etalonRecord.getArrayAttribute(sourceAttribute.getName());
                targetAttribute.setWinner(winnerAttribute instanceof WinnerInformationArrayAttribute
                        && originKey.getExternalId().equals(((WinnerInformationArrayAttribute<?>) winnerAttribute).getWinnerExternalId())
                        && originKey.getSourceSystem().equals(((WinnerInformationArrayAttribute<?>) winnerAttribute).getWinnerSourceSystem()));

                result.add(targetAttribute);
            }
        }
        return result;
    }

    protected static void populate(ArrayAttribute<?> source, ArrayAttributeRO target) {
        List<ArrayObjectRO> value = null;

        if (!source.isEmpty()) {

            value = new ArrayList<>(source.getValue().size());
            for (int i = 0; i < source.getValue().size(); i++) {
                ArrayValue<?> entry = source.getValue().get(i);
                ArrayObjectRO obj = new ArrayObjectRO();

                obj.setValue(entry.getValue());
                obj.setDisplayValue(entry.getDisplayValue());

                if (source.getDataType() == org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType.STRING
                        || source.getDataType() == org.unidata.mdm.core.type.data.ArrayAttribute.ArrayDataType.INTEGER) {
                    obj.setTargetEtalonId(((CodeLinkValue) entry).getLinkEtalonId());
                }

                value.add(obj);
            }
        }

        target.setName(source.getName());
        target.setType(ArrayDataType.fromValue(source.getDataType().name()));
        target.setValue(value);
    }
}
