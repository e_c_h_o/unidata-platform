package org.unidata.mdm.rest.v1.data.service.rendering;

import java.util.Collection;
import java.util.Collections;
import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.v1.data.service.atomic.AtomicDataUpsertRequestRenderer;
import org.unidata.mdm.rest.v1.data.service.atomic.AtomicDataUpsertResultRenderer;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestInputRenderingAction;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.system.type.rendering.InputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.InputRenderingAction;
import org.unidata.mdm.system.type.rendering.OutputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.OutputRenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

/**
 * @author Mikhail Mikhailov on Jan 16, 2020
 */
@Component
public class DataRenderingProvider implements RenderingProvider {

    @Autowired
    private MetaModelService metaModelService;

    @Override
    public Collection<InputFragmentRenderer> get(@Nonnull InputRenderingAction action) {
        if (DataRestInputRenderingAction.ATOMIC_UPSERT_INPUT == action) {
            return Collections.singletonList(new AtomicDataUpsertRequestRenderer(metaModelService));
        }
        return Collections.emptyList();
    }

    @Override
    public Collection<OutputFragmentRenderer> get(@Nonnull OutputRenderingAction action) {
        if (DataRestOutputRenderingAction.ATOMIC_UPSERT_OUTPUT == action) {
            return Collections.singletonList(new AtomicDataUpsertResultRenderer());
        }
        return Collections.emptyList();
    }

    public MetaModelService getMetaModelService() {
        return metaModelService;
    }

    public void setMetaModelService(MetaModelService metaModelService) {
        this.metaModelService = metaModelService;
    }
}
