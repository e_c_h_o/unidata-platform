package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.v1.data.ro.LsnRO;

/**
 * Detach origin request
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class DetachOriginRequestRO {

    private LsnRO lsn;
    private OriginKeyRO recordKey;

    public LsnRO getLsn() {
        return lsn;
    }

    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }

    public OriginKeyRO getRecordKey() {
        return recordKey;
    }

    public void setRecordKey(OriginKeyRO recordKey) {
        this.recordKey = recordKey;
    }
}
