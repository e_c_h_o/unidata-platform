/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.rest.v1.data.service.rendering.DataRenderingProvider;
import org.unidata.mdm.rest.v1.data.service.rendering.SearchDataRenderingProvider;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestInputRenderingAction;
import org.unidata.mdm.rest.v1.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestOutputRenderingAction;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.module.Module;
import org.unidata.mdm.system.type.rendering.RenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

/**
 * @author Alexander Malyshev
 */
public class DataRestModule implements Module {

    public static final String MODULE_ID = "org.unidata.mdm.rest.v1.data";

    private static final List<RenderingAction> RENDERING_ACTIONS = unionToList(
        Arrays.asList(DataRestInputRenderingAction.values()),
        Arrays.asList(DataRestOutputRenderingAction.values()),
        Collections.singletonList(SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT),
        Collections.singletonList(SearchRestOutputRenderingAction.SIMPLE_SEARCH_OUTPUT)
    );

    private static final List<Dependency> DEPENDENCIES = Arrays.asList(
        new Dependency("org.unidata.mdm.data", "6.0"),
        new Dependency("org.unidata.mdm.search", "6.0"),
        new Dependency("org.unidata.mdm.rest.core", "6.0")
    );

    @Autowired
    private DataRenderingProvider dataRenderingProvider;

    @Autowired
    private SearchDataRenderingProvider searchDataRenderingProvider;

    private static <T> List<T> unionToList(Collection<? extends T>... collections) {
        List<T> result = new ArrayList<>();
        for (Collection<? extends T> c : collections) {
            result.addAll(c);
        }
        return result;
    }


    @Override
    public String getId() {
        return MODULE_ID;
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "Data Rest Module";
    }

    @Override
    public String getDescription() {
        return "Data Rest Module";
    }

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingAction> getRenderingActions() {
        return RENDERING_ACTIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingProvider> getRenderingProviders() {
        return Arrays.asList(dataRenderingProvider, searchDataRenderingProvider);
    }

    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{"rest_data_messages"};
    }
}
