/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.OriginRecordInfoSection;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.v1.data.ro.records.OriginRecordRO;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 * Golden record to REST golden record converter.
 */
public class DataRecordOriginConverter {

    /**
     * Constructor.
     */
    private DataRecordOriginConverter() {
        super();
    }

    /**
     * Converts a golden record to a REST golden record.
     * @param source the record to convert
     * @return REST
     */
    public static OriginRecordRO to(OriginRecord source) {
        OriginRecordRO target = new OriginRecordRO();

        populateMainInformation(source, target);
        populateAttributeInformation(source, target);

        return target;
    }

    /**
     * Converts a golden record to a REST golden record with extended information.
     * @param source the record to convert
     * @return REST
     */
    public static OriginRecordRO to(OriginRecord source, EtalonRecord etalonRecord) {
        OriginRecordRO target = new OriginRecordRO();

        populateMainInformation(source, target);
        populateExtendedAttributeInformation(source, target, etalonRecord);

        return target;
    }

    /**
     * main populator for origin record ro
     * @param source source record to populate
     * @param target target record to populate
     */
    protected static void populateMainInformation(OriginRecord source, OriginRecordRO target) {

        OriginRecordInfoSection info = source.getInfoSection();

        if (info != null) {
            // Key
            if(info.getOriginKey() != null){
                target.setOriginId(info.getOriginKey().getId()); // System ID
                target.setExternalId(info.getOriginKey().getExternalId()); // Foreign ID
                target.setSourceSystem(info.getOriginKey().getSourceSystem()); // Source system
                target.setEntityName(info.getOriginKey().getEntityName()); // Entity name
                target.setEnriched(info.getOriginKey().isEnrichment());
            } else {
                target.setOriginId(null); // System ID
                target.setExternalId(null); // Foreign ID
                target.setSourceSystem(null); // Source system
                target.setEntityName(null); // Entity name
            }

            // Informational
            target.setValidFrom(ConvertUtils.date2LocalDateTime(info.getValidFrom()));
            target.setValidTo(ConvertUtils.date2LocalDateTime(info.getValidTo()));
            target.setCreateDate(info.getCreateDate());
            target.setUpdateDate(info.getUpdateDate());
            target.setCreatedBy(info.getCreatedBy());
            target.setUpdatedBy(info.getUpdatedBy());
            target.setRevision(info.getRevision());
            target.setStatus(info.getStatus() != null ? info.getStatus().name() : "");

        }
    }

    /**
     * attribute populator for origin record ro
     * @param source source record to populate
     * @param source target record to populate
     */
    protected static void populateAttributeInformation(OriginRecord source, OriginRecordRO target) {
        SimpleAttributeConverter.to(source.getSimpleAttributes(), target.getSimpleAttributes());
        ComplexAttributeConverter.to(source.getComplexAttributes(), target.getComplexAttributes());
        target.getCodeAttributes().addAll(CodeAttributeConverter.to(source.getCodeAttributes()));
        target.getArrayAttributes().addAll(ArrayAttributeConverter.to(source.getArrayAttributes()));
    }

    /**
     * attribute populator for origin record ro
     * @param source source record to populate
     * @param target target record to populate
     * @param etalonRecord etalon record
     */
    protected static void populateExtendedAttributeInformation(OriginRecord source, OriginRecordRO target, EtalonRecord etalonRecord) {
        if(etalonRecord != null &&
                source.getInfoSection().getOriginKey() != null &&
                StringUtils.isNotEmpty(source.getInfoSection().getOriginKey().getExternalId())){
            RecordOriginKey originKey = source.getInfoSection().getOriginKey();
            SimpleAttributeConverter.to(source.getSimpleAttributes(), target.getSimpleAttributes(), etalonRecord, originKey);
            ComplexAttributeConverter.to(source.getComplexAttributes(), target.getComplexAttributes(), etalonRecord, originKey);
            target.getCodeAttributes().addAll(CodeAttributeConverter.to(source.getCodeAttributes(), etalonRecord, originKey));
            target.getArrayAttributes().addAll(ArrayAttributeConverter.to(source.getArrayAttributes(), etalonRecord, originKey));
        } else {
            populateAttributeInformation(source, target);
        }
    }

    /**
     * Converts from REST to system.
     * @param record the record
     * @return record
     */
    public static DataRecord from(OriginRecordRO record) {

        if (Objects.isNull(record)) {
            return null;
        }

        SerializableDataRecord result = new SerializableDataRecord(
                record.getSimpleAttributes().size() +
                record.getCodeAttributes().size() +
                record.getArrayAttributes().size() +
                record.getComplexAttributes().size() + 1);

        result.addAll(SimpleAttributeConverter.from(record.getSimpleAttributes()));
        result.addAll(CodeAttributeConverter.from(record.getCodeAttributes()));
        result.addAll(ArrayAttributeConverter.from(record.getArrayAttributes()));
        result.addAll(ComplexAttributeConverter.from(record.getComplexAttributes()));

        return result;
    }
}
