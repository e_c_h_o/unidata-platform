/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.List;

/**
 * @author Mikhail Mikhailov
 * Keys RO.
 */
public class RecordKeysRO {
    /**
     * Etalon id.
     */
    private String etalonId;
    /**
     * Origin keys.
     */
    private List<OriginKeyRO> originKeys;
    /**
     * Constructor.
     */
    public RecordKeysRO() {
        super();
    }
    /**
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }
    /**
     * @param etalonId the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }
    /**
     * @return the originKeys
     */
    public List<OriginKeyRO> getOriginKeys() {
        return originKeys;
    }
    /**
     * @param originKeys the originKeys to set
     */
    public void setOriginKeys(List<OriginKeyRO> originKeys) {
        this.originKeys = originKeys;
    }


}
