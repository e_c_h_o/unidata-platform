package org.unidata.mdm.rest.v1.data.ro.records;


/**
 * Entity deletion request
 *
 * @author Alexandr Serov
 * @since 12.10.2020
 **/
public class DeleteRecordRequestRO extends AbstractRecordRequestRO {

    private Long draftId;
    private DataRecordRO dataRecord;
    private Boolean inactivateEtalon;
    private Boolean inactivateOrigin;
    private Boolean inactivatePeriod;
    private Boolean wipe;

    public DeleteRecordRequestRO(){}

    public DataRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(DataRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    public Boolean getInactivateEtalon() {
        return inactivateEtalon;
    }

    public void setInactivateEtalon(Boolean inactivateEtalon) {
        this.inactivateEtalon = inactivateEtalon;
    }

    public Boolean getInactivateOrigin() {
        return inactivateOrigin;
    }

    public void setInactivateOrigin(Boolean inactivateOrigin) {
        this.inactivateOrigin = inactivateOrigin;
    }

    public Boolean getInactivatePeriod() {
        return inactivatePeriod;
    }

    public void setInactivatePeriod(Boolean inactivatePeriod) {
        this.inactivatePeriod = inactivatePeriod;
    }

    public Boolean getWipe() {
        return wipe;
    }

    public void setWipe(Boolean wipe) {
        this.wipe = wipe;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

}
