/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.data.type.data.EtalonRelation;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.meta.type.search.RelationHeaderField;
import org.unidata.mdm.rest.v1.data.ro.attributes.ComplexAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.attributes.SimpleAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 * Converts {@link EtalonRelationToRO} type relations.
 */
public class RelationToEtalonConverter {

    /**
     * Constructor.
     */
    private RelationToEtalonConverter() {
        super();
    }

    /**
     * Converts to the REST type.
     * @param source the source
     * @return target
     */
    public static EtalonRelationToRO to(EtalonRelation source) {

        if (source == null) {
            return null;
        }

        EtalonRelationToRO target = new EtalonRelationToRO();

        List<ComplexAttributeRO> complexAttributes = new ArrayList<>();
        ComplexAttributeConverter.to(source.getComplexAttributes(), complexAttributes);

        List<SimpleAttributeRO> simpleAttributes = new ArrayList<>();
        SimpleAttributeConverter.to(source.getSimpleAttributes(), simpleAttributes);

        target.setComplexAttributes(complexAttributes);
        target.setSimpleAttributes(simpleAttributes);
        target.setEtalonIdTo(source.getInfoSection().getToEtalonKey() != null ? source.getInfoSection().getToEtalonKey().getId() : null);
        target.setRelName(source.getInfoSection().getRelationName());
        target.setCreateDate(source.getInfoSection() != null ? source.getInfoSection().getCreateDate() : null);
        target.setCreatedBy(source.getInfoSection() != null ? source.getInfoSection().getCreatedBy() : null);
        target.setUpdateDate(source.getInfoSection() != null ? source.getInfoSection().getUpdateDate() : null);
        target.setUpdatedBy(source.getInfoSection() != null ? source.getInfoSection().getUpdatedBy() : null);
        target.setStatus(source.getInfoSection() != null
                ? (source.getInfoSection().getStatus() != null ? source.getInfoSection().getStatus().value() : null)
                : null);
        target.setValidFrom(source.getInfoSection() != null ? ConvertUtils.date2LocalDateTime(source.getInfoSection().getValidFrom()) : null);
        target.setValidTo(source.getInfoSection() != null ? ConvertUtils.date2LocalDateTime(source.getInfoSection().getValidTo()) : null);

        target.setEtalonId(source.getInfoSection() != null ? source.getInfoSection().getRelationEtalonKey() : null);
        return target;
    }

    public static List<EtalonRelationToRO> to(SearchResultDTO searchResultDTO, Relation relationDef){
        List<EtalonRelationToRO> result = new ArrayList<>();
        Set<String> processedRels = new HashSet<>();
        if(CollectionUtils.isNotEmpty(searchResultDTO.getHits())){
            for(SearchResultHitDTO hit : searchResultDTO.getHits()){
                if(!processedRels.contains(hit.getInternalId())){
                    processedRels.add(hit.getInternalId());
                    EtalonRelationToRO target = new EtalonRelationToRO();
                    target.setRelName(relationDef.getName());
                    // right now only active relations from search system
                    target.setStatus(RecordStatus.ACTIVE.value());

                    target.setEtalonId(hit.getId());
                    target.setEtalonIdTo(hit.getFieldValue(RelationHeaderField.FIELD_TO_ETALON_ID.getName()).getFirstValue().toString());

                    target.setCreateDate(SearchUtils.parse(Objects.toString(
                            hit.getFieldValue(RelationHeaderField.FIELD_CREATED_AT.getName()).getFirstValue(), null)));
                    target.setUpdateDate(SearchUtils.parse(Objects.toString(
                            hit.getFieldValue(RelationHeaderField.FIELD_UPDATED_AT.getName()).getFirstValue(), null)));

                    target.setValidFrom(ConvertUtils.date2LocalDateTime(SearchUtils.parse(Objects.toString(
                            hit.getFieldValue(RelationHeaderField.FIELD_FROM.getName()).getFirstValue(), null))));
                    target.setValidTo(ConvertUtils.date2LocalDateTime(SearchUtils.parse(Objects.toString(
                            hit.getFieldValue(RelationHeaderField.FIELD_TO.getName()).getFirstValue(), null))));

                    target.setSimpleAttributes(SimpleAttributeConverter.to(hit, "", relationDef.getSimpleAttribute()));
                    result.add(target);
                }
            }
        }
        return result;
    }

    /**
     * Converts from the REST type.
     * @param source the source
     * @return target
     */
    public static DataRecord from(EtalonRelationToRO source) {

        if (source == null) {
            return null;
        }

        SerializableDataRecord result = new SerializableDataRecord(
                source.getComplexAttributes().size() +
                source.getSimpleAttributes().size());

        List<Attribute> attributes = new ArrayList<>(
                source.getComplexAttributes().size() +
                source.getSimpleAttributes().size());

        attributes.addAll(ComplexAttributeConverter.from(source.getComplexAttributes()));
        attributes.addAll(SimpleAttributeConverter.from(source.getSimpleAttributes()));

        result.addAll(attributes);
        return result;
    }

    /**
     * Converts a list of {@link RelationToRO} type relations.
     * @param source the source list
     * @param target the destination
     */
    public static List<EtalonRelationToRO> to(List<EtalonRelation> source) {

        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<EtalonRelationToRO> target = new ArrayList<>();
        for (EtalonRelation r : source) {
            target.add(to(r));
        }

        return target;
    }

    /**
     * Converts a list of {@link RelationToRO} type relations.
     * @param source the source list
     * @param target the destination
     * @param relName relation name
     */
    public static List<DataRecord> from(List<EtalonRelationToRO> source) {

        if (source == null || source.isEmpty()) {
            return Collections.emptyList();
        }

        List<DataRecord> target = new ArrayList<>();
        for (EtalonRelationToRO r : source) {
            target.add(from(r));
        }

        return target;
    }
}
