package org.unidata.mdm.rest.v1.data.ro.restore;

import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;

/**
 * Restore request request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class RestoreRecordRequestRO {

    private Long draftId;
    private DataRecordRO record;
    private boolean modified;

    public DataRecordRO getRecord() {
        return record;
    }

    public void setRecord(DataRecordRO record) {
        this.record = record;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
