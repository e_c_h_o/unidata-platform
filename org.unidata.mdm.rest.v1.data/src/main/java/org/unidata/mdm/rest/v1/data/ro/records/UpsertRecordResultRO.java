package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.List;

/**
 * Upsert record result
 *
 * @author Alexandr Serov
 * @since 06.10.2020
 **/
public class UpsertRecordResultRO extends RecordKeysSupportResultRO {

    /**
     * Data record
     */
    private EtalonRecordRO dataRecord;
    private List<String> duplicates;

    public EtalonRecordRO getDataRecord() {
        return dataRecord;
    }

    public void setDataRecord(EtalonRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    public List<String> getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(List<String> duplicates) {
        this.duplicates = duplicates;
    }
}
