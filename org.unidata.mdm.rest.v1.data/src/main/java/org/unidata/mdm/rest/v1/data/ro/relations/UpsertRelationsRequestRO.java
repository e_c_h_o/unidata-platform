package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;

/**
 * Upsert relations request
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class UpsertRelationsRequestRO {

    private String sourceSystem;
    private DataRecordRO from;
    private List<RecordRelationRO> to;

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }


    public List<RecordRelationRO> getTo() {
        return to;
    }

    public void setTo(List<RecordRelationRO> to) {
        this.to = to;
    }

    public DataRecordRO getFrom() {
        return from;
    }

    public void setFrom(DataRecordRO from) {
        this.from = from;
    }

}