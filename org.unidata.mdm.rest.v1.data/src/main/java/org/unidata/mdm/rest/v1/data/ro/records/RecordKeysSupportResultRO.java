package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Result with record keys
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public abstract class RecordKeysSupportResultRO extends DetailedOutputRO {

    private RecordKeysRO recordKeys;

    public RecordKeysRO getRecordKeys() {
        return recordKeys;
    }

    public void setRecordKeys(RecordKeysRO recordKeys) {
        this.recordKeys = recordKeys;
    }
}
