/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.v1.data.converter;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.unidata.mdm.core.type.keys.EtalonKey;
import org.unidata.mdm.core.type.keys.Keys;
import org.unidata.mdm.core.type.keys.OriginKey;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.rest.v1.data.ro.ContributorRO;
import org.unidata.mdm.rest.v1.data.ro.TimeIntervalRO;
import org.unidata.mdm.rest.v1.data.ro.TimelineRO;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * @author Mikhail Mikhailov
 * Time line converter.
 */
public class TimelineToTimelineROConverter {

    /**
     * Constructor.
     */
    private TimelineToTimelineROConverter() {
        super();
    }

    /**
     * Converts a list of sodurce timeline objects to target.
     * @param source the source
     * @param target the target
     */
    public static void convert(List<Timeline<?>> source, List<TimelineRO> target) {
        for (int i = 0; source != null && i < source.size(); i++) {
            target.add(convert(source.get(i)));
        }
    }

    /**
     * Convert method.
     * @param source the source
     * @return target
     */
    public static TimelineRO convert(Timeline<?> source) {

        if (source == null) {
            return null;
        }

        TimelineRO target = new TimelineRO();
        Keys<? extends EtalonKey, ? extends OriginKey> keys = source.getKeys();

        if (Objects.isNull(keys)) {
            return target;
        }

        target.setEtalonId(keys.getEtalonKey().getId());
        target.setTimeline(source.stream()
                .filter(TimeInterval::isNotEmpty)
                .map(i -> {

                    TimeIntervalRO iro = new TimeIntervalRO();
                    copyTimeInterval(i, iro);

                    return iro;
                })
                .collect(Collectors.toList()));

        return target;
    }

    /**
     * Copy individual interval.
     * @param source the source
     * @param target the target
     */
    private static void copyTimeInterval(TimeInterval<?> source, TimeIntervalRO target) {

        target.setDateFrom(ConvertUtils.date2LocalDateTime(source.getValidFrom()));
        target.setDateTo(ConvertUtils.date2LocalDateTime(source.getValidTo()));
        target.setActive(source.isActive());
        target.setContributors(source.stream()
                .map(h -> {

                    ContributorRO co = new ContributorRO();

                    co.setOriginId(h.getOriginKey().getId());
                    co.setSourceSystem(h.getSourceSystem());
                    co.setVersion(h.getRevision());
                    co.setStatus(Objects.nonNull(h.getStatus()) ? h.getStatus().name() : null);
                    co.setOwner(h.getTypeName());

                    return co;
                })
                .collect(Collectors.toList()));
    }
}
