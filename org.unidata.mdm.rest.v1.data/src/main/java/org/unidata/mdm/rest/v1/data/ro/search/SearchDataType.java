package org.unidata.mdm.rest.v1.data.ro.search;

/**
 * TODO:
 *
 * @author Alexandr Serov
 * @link http://jira.taskdata.com/browse/TODO:
 * @since 04.12.2020
 **/
public enum SearchDataType {

    ETALON,

    RECORD,

    RELATION

}
