package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;

/**
 * Record relation rest object
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RecordRelationRO {

    private String relationName;
    private String relationEtalonKey;
//    private EtalonRecordRO etalonRecord;
    private DataRecordRO record;

    public String getRelationEtalonKey() {
        return relationEtalonKey;
    }

    public void setRelationEtalonKey(String relationEtalonKey) {
        this.relationEtalonKey = relationEtalonKey;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public DataRecordRO getRecord() {
        return record;
    }

    public void setRecord(DataRecordRO record) {
        this.record = record;
    }

    //    public EtalonRecordRO getEtalonRecord() {
//        return etalonRecord;
//    }
//
//    public void setEtalonRecord(EtalonRecordRO etalonRecord) {
//        this.etalonRecord = etalonRecord;
//    }
}
