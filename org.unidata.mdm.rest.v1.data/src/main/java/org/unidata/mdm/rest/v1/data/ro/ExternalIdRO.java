package org.unidata.mdm.rest.v1.data.ro;


/**
 * ExternalId
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public class ExternalIdRO {

    /**
     * External id.
     */
    private String externalId;
    /**
     * Source system.
     */
    private String sourceSystem;

    public ExternalIdRO() {}

    public ExternalIdRO(String externalId, String sourceSystem) {
        this.externalId = externalId;
        this.sourceSystem = sourceSystem;
    }

    public ExternalIdRO(String externalId) {
        this(externalId, null);
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}
