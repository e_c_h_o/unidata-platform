package org.unidata.mdm.rest.v1.data.ro.lob;

import org.unidata.mdm.rest.v1.data.ro.LargeObjectRO;

/**
 * Upload large object result
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class UploadResultRO {

    private LargeObjectRO largeObjectRO;

    public LargeObjectRO getLargeObjectRO() {
        return largeObjectRO;
    }

    public void setLargeObjectRO(LargeObjectRO largeObjectRO) {
        this.largeObjectRO = largeObjectRO;
    }
}
