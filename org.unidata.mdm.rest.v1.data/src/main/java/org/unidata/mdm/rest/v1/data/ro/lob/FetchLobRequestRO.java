package org.unidata.mdm.rest.v1.data.ro.lob;

/**
 * Fetch large object request
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class FetchLobRequestRO {

    private String id;
    private boolean binary;

    public FetchLobRequestRO(String id, boolean binary) {
        this.id = id;
        this.binary = binary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBinary() {
        return binary;
    }

    public void setBinary(boolean binary) {
        this.binary = binary;
    }
}
