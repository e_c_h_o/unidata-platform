package org.unidata.mdm.rest.v1.data.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;
import org.unidata.mdm.rest.v1.data.ro.TimelineRO;

/**
 * Get relation timelines result
 *
 * @author Alexandr Serov
 * @since 23.10.2020
 **/
public class GetRelationTimelinesResultRO extends DetailedOutputRO {

    private List<TimelineRO> timelines;

    public List<TimelineRO> getTimelines() {
        return timelines;
    }

    public void setTimelines(List<TimelineRO> timelines) {
        this.timelines = timelines;
    }
}
