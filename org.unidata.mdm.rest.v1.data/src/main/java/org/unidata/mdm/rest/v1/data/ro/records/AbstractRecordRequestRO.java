package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.v1.data.ro.ExternalIdRO;
import org.unidata.mdm.rest.v1.data.ro.ExternalIdSupport;
import org.unidata.mdm.rest.v1.data.ro.LsnRO;

/**
 * Base record request class
 * contain
 * - etalonId
 * - entityName
 * - recordKey (External id)
 * - lsn
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public abstract class AbstractRecordRequestRO implements ExternalIdSupport {

    protected String etalonId;
    protected String entityName;
    protected ExternalIdRO externalId;
    protected LsnRO lsn;

    public String getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public ExternalIdRO getExternalId() {
        return externalId;
    }

    public void setExternalId(ExternalIdRO externalId) {
        this.externalId = externalId;
    }

    public LsnRO getLsn() {
        return lsn;
    }

    public void setLsn(LsnRO lsn) {
        this.lsn = lsn;
    }
}
