package org.unidata.mdm.rest.v1.data.ro.favorite;

/**
 * Get favorite etalons request
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class GetFavoriteRequestRO {

    private String entityName;


    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}
