package org.unidata.mdm.rest.v1.data.ro.relations;


import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Delete relation result
 *
 * @author Alexandr Serov
 * @since 19.10.2020
 **/
public class DeleteRelationResultRO extends DetailedOutputRO {

    private RelationKeysRO relationKeys;

    public void setRelationKeys(RelationKeysRO relationKeys) {
        this.relationKeys = relationKeys;
    }

    public RelationKeysRO getRelationKeys() {
        return relationKeys;
    }
}
