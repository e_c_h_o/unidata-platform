package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.v1.data.ro.TimelineRO;

/**
 * Get timeline result
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class GetTimelineResultRO {

    private TimelineRO timeline;

    public TimelineRO getTimeline() {
        return timeline;
    }

    public void setTimeline(TimelineRO timeline) {
        this.timeline = timeline;
    }
}
