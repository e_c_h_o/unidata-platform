package org.unidata.mdm.rest.v1.data.ro;

/**
 * Object with external id
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public interface ExternalIdSupport {

    ExternalIdRO getExternalId();

}
