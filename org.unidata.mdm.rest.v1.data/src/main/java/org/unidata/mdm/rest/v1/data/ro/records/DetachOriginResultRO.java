package org.unidata.mdm.rest.v1.data.ro.records;

import java.util.Map;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Result of origin detaching
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class DetachOriginResultRO extends DetailedOutputRO {

    private Map<String, String> etalonId;

    public Map<String, String> getEtalonId() {
        return etalonId;
    }

    public void setEtalonId(Map<String, String> etalonId) {
        this.etalonId = etalonId;
    }
}
