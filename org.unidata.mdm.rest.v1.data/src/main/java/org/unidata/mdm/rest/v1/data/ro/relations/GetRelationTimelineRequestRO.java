package org.unidata.mdm.rest.v1.data.ro.relations;

import java.time.LocalDateTime;

/**
 * Get relation timeline request
 *
 * @author Alexandr Serov
 * @since 23.10.2020
 **/
public class GetRelationTimelineRequestRO {

    private String relationEtalonKey;
    private LocalDateTime dateFor;
    private LocalDateTime validFrom;
    private LocalDateTime validTo;

    public String getRelationEtalonKey() {
        return relationEtalonKey;
    }

    public void setRelationEtalonKey(String relationEtalonKey) {
        this.relationEtalonKey = relationEtalonKey;
    }

    public LocalDateTime getDateFor() {
        return dateFor;
    }

    public void setDateFor(LocalDateTime dateFor) {
        this.dateFor = dateFor;
    }

    public LocalDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDateTime validTo) {
        this.validTo = validTo;
    }
}
