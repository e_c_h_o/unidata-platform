/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.extended;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.unidata.mdm.rest.v1.data.ro.attributes.SimpleAttributeRO;
import org.unidata.mdm.rest.v1.data.ro.serializer.SimpleAttributeDeserializer;
import org.unidata.mdm.rest.v1.data.ro.serializer.SimpleAttributeSerializer;

/**
 * @author Dmitry Kopin. Created on 21.06.2017.
 * Contains additional information about simple attribute rest object
 */
@JsonDeserialize(using = SimpleAttributeDeserializer.class)
@JsonSerialize(using = SimpleAttributeSerializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtendedSimpleAttributeRO extends SimpleAttributeRO {

    private boolean winner;

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }
}
