package org.unidata.mdm.rest.v1.data.ro;

/**
 * Local serial number
 *
 * @author Alexandr Serov
 * @since 20.11.2020
 **/
public class LsnRO {

    private Long lsn;
    private Integer shard;

    public LsnRO() {
        // default
    }

    public LsnRO(Long lsn, Integer shard) {
        this.lsn = lsn;
        this.shard = shard;
    }

    public Long getLsn() {
        return lsn;
    }

    public void setLsn(Long lsn) {
        this.lsn = lsn;
    }

    public Integer getShard() {
        return shard;
    }

    public void setShard(Integer shard) {
        this.shard = shard;
    }
}
