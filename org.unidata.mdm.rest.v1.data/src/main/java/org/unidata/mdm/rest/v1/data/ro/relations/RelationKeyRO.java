package org.unidata.mdm.rest.v1.data.ro.relations;

import org.unidata.mdm.rest.v1.data.ro.records.OriginKeyRO;

/**
 * Relation key rest object
 *
 * @author Alexandr Serov
 * @since 20.10.2020
 **/
public class RelationKeyRO {

    private OriginKeyRO from;
    private OriginKeyRO to;

    public OriginKeyRO getFrom() {
        return from;
    }

    public void setFrom(OriginKeyRO from) {
        this.from = from;
    }

    public OriginKeyRO getTo() {
        return to;
    }

    public void setTo(OriginKeyRO to) {
        this.to = to;
    }
}
