/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author ibykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WinnerRO {

    /** The etalon id. */
    private String etalonId;

    /** The paths. */
    private List<String> paths;

    /**
     * Gets the etalon id.
     *
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * Sets the etalon id.
     *
     * @param etalonId            the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    /**
     * Gets the paths.
     *
     * @return the paths
     */
    public List<String> getPaths() {
        if (this.paths == null) {
            this.paths = new ArrayList<>();
        }
        return paths;
    }

    /**
     * Sets the paths.
     *
     * @param paths            the paths to set
     */
    public void setPaths(List<String> paths) {
        this.paths = paths;
    }
}
