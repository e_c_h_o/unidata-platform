/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.data.ro.attributes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov
 * Array object.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArrayObjectRO {
    /**
     * The value.
     */
    private Object value;
    /**
     * Display value, in case, this is a enum link, or a link to a code attribute.
     */
    @JsonIgnoreProperties(allowGetters = true, allowSetters = false)
    private String displayValue;
    /**
     * The etalon id value of the lookup record, in case of a link to a code attribute.
     */
    @JsonIgnoreProperties(allowGetters = true, allowSetters = false)
    private String targetEtalonId;
    /**
     * Constructor.
     */
    public ArrayObjectRO() {
        super();
    }
    /**
     * Constructor.
     */
    public ArrayObjectRO(Object value) {
        super();
        this.value = value;
    }
    /**
     * Constructor.
     */
    public ArrayObjectRO(Object value, String displayValue, String targetEtalonId) {
        super();
        this.value = value;
        this.displayValue = displayValue;
        this.targetEtalonId = targetEtalonId;
    }
    /**
     * @return the value
     */
    public Object getValue() {
        return value;
    }
    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }
    /**
     * @return the displayValue
     */
    public String getDisplayValue() {
        return displayValue;
    }
    /**
     * @param displayValue the displayValue to set
     */
    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }
    /**
     * @return the targetEtalonId
     */
    public String getTargetEtalonId() {
        return targetEtalonId;
    }
    /**
     * @param targetEtalonId the targetEtalonId to set
     */
    public void setTargetEtalonId(String targetEtalonId) {
        this.targetEtalonId = targetEtalonId;
    }
}
