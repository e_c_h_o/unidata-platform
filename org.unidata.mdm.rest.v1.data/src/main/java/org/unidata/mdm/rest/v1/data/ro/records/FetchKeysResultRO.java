package org.unidata.mdm.rest.v1.data.ro.records;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Fetch keys result
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class FetchKeysResultRO extends DetailedOutputRO {

    private RecordKeysRO keys;

    public RecordKeysRO getKeys() {
        return keys;
    }

    public void setKeys(RecordKeysRO keys) {
        this.keys = keys;
    }
}
