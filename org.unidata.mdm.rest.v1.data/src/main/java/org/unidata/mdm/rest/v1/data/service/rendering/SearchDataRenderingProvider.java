package org.unidata.mdm.rest.v1.data.service.rendering;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.rest.v1.data.service.search.HierarchicalSearchOutputFinalizer;
import org.unidata.mdm.rest.v1.data.service.search.SearchDataResponseRenderer;
import org.unidata.mdm.rest.v1.data.service.search.SearchInputRequestRenderer;
import org.unidata.mdm.rest.v1.data.service.search.SearchResultHitModifier;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestInputRenderingAction;
import org.unidata.mdm.rest.v1.search.type.rendering.SearchRestOutputRenderingAction;
import org.unidata.mdm.search.configuration.SearchConfigurationConstants;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.rendering.InputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.InputRenderingAction;
import org.unidata.mdm.system.type.rendering.OutputFragmentRenderer;
import org.unidata.mdm.system.type.rendering.OutputRenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

/**
 * TODO:
 *
 * @author Alexandr Serov
 * @since 05.12.2020
 **/
@Component
public class SearchDataRenderingProvider implements RenderingProvider {

    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private SearchResultHitModifier searchResultHitModifier;

    @ConfigurationRef(SearchConfigurationConstants.PROPERTY_CALCULATE_SCORE)
    private ConfigurationValue<Boolean> calculateScore;

    @Override
    public Collection<InputFragmentRenderer> get(@Nonnull InputRenderingAction action) {
        if (SearchRestInputRenderingAction.SIMPLE_SEARCH_INPUT == action) {
            return Collections.singletonList(new SearchInputRequestRenderer(metaModelService, calculateScore));
        }
        return Collections.emptyList();
    }

    @Override
    public Collection<OutputFragmentRenderer> get(@Nonnull OutputRenderingAction action) {
        if (SearchRestOutputRenderingAction.SIMPLE_SEARCH_OUTPUT == action) {
            return Arrays.asList(
                new SearchDataResponseRenderer(searchResultHitModifier),
                new HierarchicalSearchOutputFinalizer(searchResultHitModifier)
            );
        }
        return Collections.emptyList();
    }
}
