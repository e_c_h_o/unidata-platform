package org.unidata.mdm.rest.v1.data.ro.lob;

/**
 * Delete large object result
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
public class DeleteLobResultRO {

    private boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
