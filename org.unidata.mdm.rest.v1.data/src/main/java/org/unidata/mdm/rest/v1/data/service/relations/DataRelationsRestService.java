package org.unidata.mdm.rest.v1.data.service.relations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.DeleteRelationRequestContext;
import org.unidata.mdm.data.context.GetRelationRequestContext;
import org.unidata.mdm.data.context.GetRelationTimelineRequestContext;
import org.unidata.mdm.data.context.GetRelationsTimelineRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationsRequestContext;
import org.unidata.mdm.data.dto.DeleteRelationDTO;
import org.unidata.mdm.data.dto.GetRelationDTO;
import org.unidata.mdm.data.dto.RelationStateDTO;
import org.unidata.mdm.data.dto.UpsertRelationDTO;
import org.unidata.mdm.data.dto.UpsertRelationsDTO;
import org.unidata.mdm.data.service.DataRelationsService;
import org.unidata.mdm.data.service.impl.CommonRelationsComponent;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.rest.system.ro.DetailedErrorResponseRO;
import org.unidata.mdm.rest.system.ro.details.ResultDetailsRO;
import org.unidata.mdm.rest.system.util.RestConstants;
import org.unidata.mdm.rest.v1.data.converter.DataRecordEtalonConverter;
import org.unidata.mdm.rest.v1.data.converter.RelationKeysConverter;
import org.unidata.mdm.rest.v1.data.converter.RelationToEtalonConverter;
import org.unidata.mdm.rest.v1.data.converter.TimelineToTimelineROConverter;
import org.unidata.mdm.rest.v1.data.ro.ExternalIdRO;
import org.unidata.mdm.rest.v1.data.ro.TimelineRO;
import org.unidata.mdm.rest.v1.data.ro.records.DataRecordRO;
import org.unidata.mdm.rest.v1.data.ro.records.EtalonRelationToRO;
import org.unidata.mdm.rest.v1.data.ro.relations.DeleteRelationRequestRO;
import org.unidata.mdm.rest.v1.data.ro.relations.DeleteRelationResultRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationRequestRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationResultRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationTimelineRequestRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationTimelineResultRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationTimelinesRequestRO;
import org.unidata.mdm.rest.v1.data.ro.relations.GetRelationTimelinesResultRO;
import org.unidata.mdm.rest.v1.data.ro.relations.RecordRelationRO;
import org.unidata.mdm.rest.v1.data.ro.relations.UpsertRelationsRequestRO;
import org.unidata.mdm.rest.v1.data.ro.relations.UpsertRelationsResultRO;
import org.unidata.mdm.rest.v1.data.service.AbstractDataRestService;
import org.unidata.mdm.system.type.runtime.MeasurementContextName;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

import static org.unidata.mdm.system.util.ConvertUtils.localDateTime2Date;

/**
 * Controller that implements operations with relations
 *
 * @author Alexandr Serov
 * @since 14.10.2020
 **/
@Path("/relations")
@Tag(name = "relations")
public class DataRelationsRestService extends AbstractDataRestService {

    private static final String RELATIONS_TAGS = "relations";

    /**
     * Relation.
     */
    public static final String PATH_PARAM_RELATION = "relation";

    public static final String RELATION_ETALON_ID_PARAM = "relationEtalonId";

    public static final String ETALON_ID_PARAM = "etalonId";

    public static final String RELATION_NAME_PARAM = "relationName";

    /**
     * Data relation facade.
     */
    @Autowired
    private DataRelationsService dataRelationsService;

    @Autowired
    private CommonRelationsComponent commonRelationsComponent;

    /**
     * Gets etalon relation objects along with their time lines by record etalon ID.
     *
     * @param relationName relation name
     * @param relationEtalonKey relation etalon ID
     * @param dateFor relation date
     * @param includeDraft include drafts version
     * @return relation timeline
     */
    @GET
    @Path("timeline/{" + RELATION_ETALON_ID_PARAM + "}/{" + DATE_FOR_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Gets etalon relation objects along with their time lines by record etalon ID.", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = GetRelationTimelineResultRO.class)), responseCode = "200")
    }, tags = RELATIONS_TAGS)
    public GetRelationTimelineResultRO relationTimelineForDate(
        @Parameter(description = "Relation etalon ID.", in = ParameterIn.PATH)
        @PathParam(RELATION_ETALON_ID_PARAM) String relationEtalonKey,
        @Parameter(description = "Relation date.", in = ParameterIn.PATH)
        @PathParam(DATE_FOR_PARAM) String dateFor,
        @Parameter(description = "Relation name", in = ParameterIn.QUERY)
        @Nullable
        @QueryParam(RELATION_NAME_PARAM) String relationName) {

        GetRelationTimelineRequestRO req = new GetRelationTimelineRequestRO();
        req.setRelationEtalonKey(relationEtalonKey);
        req.setDateFor(parseDateTime("dateFor", dateFor));

        return executeGetRelationsTimelineRequest(req);

    }

    @GET
    @Path("timeline/{" + RELATION_ETALON_ID_PARAM + "}/{" + VALID_FROM_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}/{"
        + VALID_TO_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Gets etalon relation objects along with their time lines by record etalon ID.", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = GetRelationTimelineResultRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public GetRelationTimelineResultRO relationTimelineFromRange(
        @Parameter(description = "Relation etalon ID.", in = ParameterIn.PATH)
        @PathParam(RELATION_ETALON_ID_PARAM) String relationEtalonKey,
        @Parameter(description = "Period begin.", in = ParameterIn.PATH)
        @PathParam(VALID_FROM_PARAM) String validFrom,
        @Parameter(description = "Period end.", in = ParameterIn.PATH)
        @PathParam(VALID_FROM_PARAM) String validTo) {

        GetRelationTimelineRequestRO req = new GetRelationTimelineRequestRO();
        req.setRelationEtalonKey(relationEtalonKey);
        req.setValidFrom(parseDateTime("validFrom", validFrom));
        req.setValidFrom(parseDateTime("validTo", validTo));

        return executeGetRelationsTimelineRequest(req);
    }


    @GET
    @Path("timelines/{" + ETALON_ID_PARAM + "}/{" + DATE_FOR_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Gets etalon relation objects along with their time lines by record etalon ID.", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = GetRelationTimelinesResultRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public GetRelationTimelinesResultRO relationTimelinesForDate(
        @Parameter(description = "Etalon ID.", in = ParameterIn.PATH)
        @PathParam(ETALON_ID_PARAM) String etalonId,
        @Parameter(description = "Relation date.", in = ParameterIn.PATH)
        @PathParam(DATE_FOR_PARAM) String dateFor,
        @Parameter(description = "Relation name", in = ParameterIn.QUERY)
        @Nullable
        @QueryParam(RELATION_NAME_PARAM) String relationName) {

        GetRelationTimelinesRequestRO req = new GetRelationTimelinesRequestRO();
        req.setEtalonId(etalonId);
        req.setDateFor(parseDateTime("dateFor", dateFor));

        return executeGetRelationTimelinesRequest(req);

    }

    @GET
    @Path("timelines/{" + ETALON_ID_PARAM + "}/{" + VALID_FROM_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN +
        "}/{" + VALID_TO_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Gets etalon relation data object by relation etalon ID.", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = GetRelationTimelinesResultRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public GetRelationTimelinesResultRO relationTimelinesFromRange(
        @Parameter(description = "Etalon ID.", in = ParameterIn.PATH)
        @PathParam(ETALON_ID_PARAM) String etalonId,
        @Parameter(description = "Period begin.", in = ParameterIn.PATH)
        @PathParam(VALID_FROM_PARAM) String validFrom,
        @Parameter(description = "Period end.", in = ParameterIn.PATH)
        @PathParam(VALID_FROM_PARAM) String validTo) {

        GetRelationTimelinesRequestRO req = new GetRelationTimelinesRequestRO();
        req.setEtalonId(etalonId);
        req.setValidFrom(parseDateTime("validFrom", validFrom));
        req.setValidFrom(parseDateTime("validTo", validTo));

        return executeGetRelationTimelinesRequest(req);
    }

    @GET
    @Path("/{" + RELATION_ETALON_ID_PARAM + "}/{" + DATE_FOR_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Get relations content (either containment or relation to).", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = GetRelationResultRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public GetRelationResultRO getRelationByEtalonId(
        @Parameter(description = "Relation etalon ID.", in = ParameterIn.PATH)
        @PathParam(RELATION_ETALON_ID_PARAM) String relationEtalonKey,
        @Parameter(description = "Relation date.", in = ParameterIn.PATH)
        @PathParam(DATE_FOR_PARAM) String dateFor,
        @Parameter(description = "Operation ID", in = ParameterIn.QUERY)
        @DefaultValue("false")
        @QueryParam("operationId") String operationId,
        @Parameter(description = "Include draft versions.", in = ParameterIn.QUERY)
        @DefaultValue("false")
        @QueryParam(INCLUDE_DRAFT_PARAM) Boolean includeDraft,
        @Parameter(description = "Include inactive versions.", in = ParameterIn.QUERY)
        @DefaultValue("false")
        @QueryParam(INCLUDE_INACTIVE_PARAM) Boolean includeInactive) {
        GetRelationRequestRO req = new GetRelationRequestRO();
        req.setRelationEtalonKey(relationEtalonKey);
        req.setForDate(parseDateTime("dateFor", dateFor));
        req.setOperationId(operationId);
        req.setIncludeDrafts(BooleanUtils.toBoolean(includeDraft));
        req.setIncludeInactive(BooleanUtils.toBoolean(includeInactive));
        return executeGetRelationRequest(req);
    }


    @POST
    @Operation(description = "Upsert relations", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = UpsertRelationsResultRO.class)), responseCode = "200")
    }, tags = RELATIONS_TAGS)
    public UpsertRelationsResultRO upsertRelations(UpsertRelationsRequestRO req) {
        return executeUpsertRelationsRequest(req);
    }

    /**
     * Inactivation of an etalon relation.
     *
     * @param etalonId etalon id
     * @return response
     */
    @DELETE
    @Path("/" + PATH_PARAM_RELATION + "/{" + RestConstants.DATA_PARAM_ID + "}")
    @Operation(
        description = "Inactivation of an etalon relation.",
        method = HttpMethod.DELETE,
        responses = {
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
            @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteRelationResultRO.class)), responseCode = "200")
        }, tags = RELATIONS_TAGS
    )
    public DeleteRelationResultRO deactivateEtalonRelation(
        @Parameter(description = "Relation etalon ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String etalonId) {
        DeleteRelationRequestRO req = new DeleteRelationRequestRO();
        req.setInactivateEtalon(true);
        return executeDeleteRelations(req);
    }


    /**
     * Inactivation of an origin relation.
     *
     * @return response
     */
    @DELETE
    @Path("{" + ETALON_ID_PARAM + "}/{" + VALID_FROM_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN +
        "}/{" + VALID_TO_PARAM + ":" + RestConstants.DEFAULT_TIMESTAMP_PATTERN + "}")
    @Operation(description = "Inactivation of an origin relation.", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteRelationResultRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public DeleteRelationResultRO deactivateVersionRelation(
        @Parameter(description = "Relation etalon ID", in = ParameterIn.PATH) @PathParam(RestConstants.DATA_PARAM_ID) String relationEtalonKey,
        @Parameter(description = "Begin periond", in = ParameterIn.QUERY) @PathParam("validFrom") String validFrom,
        @Parameter(description = "End period", in = ParameterIn.QUERY) @PathParam("validTo") String validTo) {

        DeleteRelationRequestRO req = new DeleteRelationRequestRO();
        req.setInactivatePeriod(true);
        req.setValidFrom(parseDateTime("validFrom", validFrom));
        req.setValidTo(parseDateTime("validTo", validTo));
        return executeDeleteRelations(req);
    }

    @POST
    @Path("/delete")
    @Operation(description = "Delete relations", responses = {
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "400"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DetailedErrorResponseRO.class)), responseCode = "500"),
        @ApiResponse(content = @Content(schema = @Schema(implementation = DeleteRelationRequestRO.class)), responseCode = "200"),
    }, tags = RELATIONS_TAGS)
    public DeleteRelationResultRO deleteRelations(DeleteRelationRequestRO req) {
        return executeDeleteRelations(req);
    }

    // execs

    private GetRelationResultRO executeGetRelationRequest(GetRelationRequestRO req) {
        Objects.requireNonNull(req, "Get relation request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_RELATIONS_GET);
        MeasurementPoint.start();
        try {
            //getRelationByEtalonId
            GetRelationDTO relation = dataRelationsService.getRelation(GetRelationRequestContext.builder()
                .etalonKey(req.getEtalonId())
                .relationEtalonKey(req.getRelationEtalonKey())
                .operationId(req.getOperationId())
                .forDate(localDateTime2Date(req.getForDate()))
                .build());
            GetRelationResultRO result = new GetRelationResultRO();
            result.setEtalonRelation(RelationToEtalonConverter.to(relation.getEtalon()));
            result.setRelationKeys(RelationKeysConverter.to(relation.getRelationKeys()));
            return result;
        } finally {
            MeasurementPoint.stop();
        }
    }

    private GetRelationTimelineResultRO executeGetRelationsTimelineRequest(GetRelationTimelineRequestRO req) {
        Objects.requireNonNull(req, "Get relation request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_GET);
        MeasurementPoint.start();
        try {
            LocalDateTime validFrom = req.getValidFrom();
            LocalDateTime validTo = req.getValidTo();
            Pair<Date, Date> range = (validFrom != null && validTo != null) ? Pair.of(localDateTime2Date(validFrom), localDateTime2Date(validTo)) : null;
            Timeline<OriginRelation> timeline = commonRelationsComponent.loadTimeline(GetRelationTimelineRequestContext.builder()
                .relationEtalonKey(req.getRelationEtalonKey())
                .forDate(localDateTime2Date(req.getDateFor()))
                .forDatesFrame(range)
                .fetchData(false)
                .build());
            GetRelationTimelineResultRO result = new GetRelationTimelineResultRO();
            result.setTimeline(TimelineToTimelineROConverter.convert(timeline));
            return result;
        } finally {
            MeasurementPoint.stop();
        }
    }

    private GetRelationTimelinesResultRO executeGetRelationTimelinesRequest(GetRelationTimelinesRequestRO req) {
        Objects.requireNonNull(req, "Get relation request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_GET);
        MeasurementPoint.start();
        try {
            LocalDateTime validFrom = req.getValidFrom();
            LocalDateTime validTo = req.getValidTo();
            Pair<Date, Date> range = (validFrom != null && validTo != null) ? Pair.of(localDateTime2Date(validFrom), localDateTime2Date(validTo)) : null;
            Map<String, List<Timeline<OriginRelation>>> timelines = commonRelationsComponent.loadTimelines(GetRelationsTimelineRequestContext.builder()
                .etalonKey(req.getEtalonId())
                .relationNames(req.getRelationNames())
                .forDate(localDateTime2Date(req.getDateFor()))
                .forDatesFrame(range)
                .fetchData(false)
                .reduceReferences(true)
                .build());
            GetRelationTimelinesResultRO result = new GetRelationTimelinesResultRO();
            List<TimelineRO> converted = new ArrayList<>();
            TimelineToTimelineROConverter.convert(timelines.values().stream()
                .flatMap(Collection::stream).collect(Collectors.toList()), converted);
            result.setTimelines(converted);
            return result;
        } finally {
            MeasurementPoint.stop();
        }
    }

    private UpsertRelationsResultRO executeUpsertRelationsRequest(UpsertRelationsRequestRO req) {
        Objects.requireNonNull(req, "Upsert relation request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_RELATIONS_TO_UPSERT);
        MeasurementPoint.start();
        try {
            DataRecordRO from = req.getFrom();
            DataRecord fromRecord = DataRecordEtalonConverter.from(from);
            String sourceSystem = req.getSourceSystem();
            List<RecordRelationRO> relations = ObjectUtils.defaultIfNull(req.getTo(), Collections.emptyList());
            UpsertRelationsDTO upsertResult = dataRelationsService.upsertRelations(UpsertRelationsRequestContext.builder()
                .etalonKey(from.getEtalonId())
                .relationsFrom(relations.stream().map(it -> {
                    DataRecordRO to = Optional.ofNullable(it.getRecord()).orElseGet(DataRecordRO::new);
                    ExternalIdRO extId = Optional.ofNullable(to.getExternalId()).orElseGet(ExternalIdRO::new);
                    String relationName = it.getRelationName();
                    RelationElement el = StringUtils.isNotBlank(relationName) ? metaModelService.instance(Descriptors.DATA).getRelation(relationName) : null;
                    String entityByRelation = el != null ? el.getRight().getName() : null;
                    String entityName = StringUtils.isNotBlank(to.getEntityName()) ? to.getEntityName() : entityByRelation;
                    return UpsertRelationRequestContext.builder()
                        .record(fromRecord)
                        .relationName(relationName)
                        .sourceSystem(sourceSystem)
                        .etalonKey(to.getEtalonId())
                        .externalId(extId.getExternalId())
                        .relationEtalonKey(it.getRelationEtalonKey())
                        .entityName(entityName)
                        .validFrom(localDateTime2Date(to.getValidFrom()))
                        .validTo(localDateTime2Date(to.getValidTo()))
                        .build();
                }).collect(Collectors.groupingBy(UpsertRelationRequestContext::getRelationName))).build());
            UpsertRelationsResultRO result = new UpsertRelationsResultRO();
            final ResultDetailsRO details = new ResultDetailsRO();
            final List<EtalonRelationToRO> resultRelations = new ArrayList<>();
            Map<RelationStateDTO, List<UpsertRelationDTO>> rels = ObjectUtils.defaultIfNull(upsertResult.getRelations(), Collections.emptyMap());
            rels.values().stream().flatMap(List::stream).forEach(rel -> {
                EtalonRelationToRO etalonRelation = RelationToEtalonConverter.to(rel.getEtalon());
                if (etalonRelation != null) {
                    resultRelations.add(etalonRelation);
                }
                updateResultDetails(details, rel.getErrors());
            });
            result.setEtalonRelations(resultRelations);
            result.setDetails(details);
            return result;
        } finally {
            MeasurementPoint.stop();
        }
    }

    private DeleteRelationResultRO executeDeleteRelations(DeleteRelationRequestRO req) {
        Objects.requireNonNull(req, "Delete relation request can't be null");
        MeasurementPoint.init(MeasurementContextName.MEASURE_UI_RELATIONS_ETALON_DELETE);
        MeasurementPoint.start();
        try {
            DeleteRelationDTO deleteResult = dataRelationsService.deleteRelation(
                DeleteRelationRequestContext.builder()
                    .relationEtalonKey(req.getEtalonId())
                    .relationOriginKey(req.getOriginId())
                    .inactivateOrigin(req.isInactivateOrigin())
                    .inactivateEtalon(req.isInactivateEtalon())
                    .inactivatePeriod(req.isInactivatePeriod())
                    .validFrom(localDateTime2Date(req.getValidFrom()))
                    .validFrom(localDateTime2Date(req.getValidTo())).build());
            DeleteRelationResultRO result = new DeleteRelationResultRO();
            result.setRelationKeys(RelationKeysConverter.to(deleteResult.getRelationKeys()));
            result.setDetails(errorsToDetails(deleteResult.getErrors()));
            return result;
        } finally {
            MeasurementPoint.stop();
        }
    }

    public DataRelationsService getDataRelationsService() {
        return dataRelationsService;
    }

    public void setDataRelationsService(DataRelationsService dataRelationsService) {
        this.dataRelationsService = dataRelationsService;
    }

    public CommonRelationsComponent getCommonRelationsComponent() {
        return commonRelationsComponent;
    }

    public void setCommonRelationsComponent(CommonRelationsComponent commonRelationsComponent) {
        this.commonRelationsComponent = commonRelationsComponent;
    }
}