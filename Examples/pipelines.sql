INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_CREATE_DRAFT_START]', '', '{"startId":"org.unidata.mdm.meta[MODEL_CREATE_DRAFT_START]","subjectId":"","description":"org.unidata.mdm.meta.model.get.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.meta[MODEL_CREATE_DRAFT_START]"},{"segmentType":"FINISH","id":"org.unidata.mdm.meta[MODEL_CREATE_DRAFT_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_DELETE_START]', '', '{"startId":"org.unidata.mdm.meta[MODEL_DELETE_START]","subjectId":"","description":"org.unidata.mdm.meta.model.delete.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.meta[MODEL_DELETE_START]"},{"segmentType":"FINISH","id":"org.unidata.mdm.meta[MODEL_DELETE_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_DROP_DRAFT_START]', '', '{"startId":"org.unidata.mdm.meta[MODEL_DROP_DRAFT_START]","subjectId":"","description":"org.unidata.mdm.meta.model.draft.drop.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.meta[MODEL_DROP_DRAFT_START]"},{"segmentType":"FINISH","id":"org.unidata.mdm.meta[MODEL_DROP_DRAFT_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.draft[DRAFT_PUBLISH_START]', '', '{"startId":"org.unidata.mdm.draft[DRAFT_PUBLISH_START]","subjectId":"","description":"org.unidata.mdm.draft.draft.publish.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.draft[DRAFT_PUBLISH_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.draft[DRAFT_PUBLISH_POINT]"},{"segmentType":"FINISH","id":"org.unidata.mdm.draft[DRAFT_PUBLISH_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RELATION_GET_START]', '', '{"startId":"org.unidata.mdm.data[RELATION_GET_START]","subjectId":"","description":"org.unidata.mdm.data.relations.get.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.data[RELATION_GET_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_GET_SECURITY]"},{"segmentType":"FINISH","id":"org.unidata.mdm.data[RELATION_GET_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RELATION_UPSERT_START]', '', '{"startId":"org.unidata.mdm.data[RELATION_UPSERT_START]","subjectId":"","description":"org.unidata.mdm.data.relation.upsert.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.data[RELATION_UPSERT_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_SECURITY]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_VALIDATE]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_CONTAINMENT]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_MODBOX]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_TIMELINE]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_INDEXING]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_UPSERT_PERSISTENCE]"},{"segmentType":"FINISH","id":"org.unidata.mdm.data[RELATION_UPSERT_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RECORD_GET_START]', '', '{"startId":"org.unidata.mdm.data[RECORD_GET_START]","subjectId":"","description":"org.unidata.mdm.data.record.get.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.data[RECORD_GET_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_READ_ACCESS]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_GET_DIFF]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_GET_ATTRIBUTES_POSTPROCESSING]"},{"segmentType":"CONNECTOR","id":"org.unidata.mdm.data[RELATIONS_GET_CONNECTOR]"},{"segmentType":"FINISH","id":"org.unidata.mdm.data[RECORD_GET_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RECORD_DELETE_START]', '', '{"startId":"org.unidata.mdm.data[RECORD_DELETE_START]","subjectId":"","description":"org.unidata.mdm.data.record.delete.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.data[RECORD_DELETE_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_DELETE_ACCESS]"},{"segmentType":"CONNECTOR","id":"org.unidata.mdm.data[RELATIONS_DELETE_CONNECTOR]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_DELETE_PERIOD_CHECK]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_DELETE_DATA_CONSISTENCY]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_DELETE_INDEXING]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RECORD_DELETE_PERSISTENCE]"},{"segmentType":"FINISH","id":"org.unidata.mdm.data[RECORD_DELETE_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RELATION_DELETE_START]', '', '{"startId":"org.unidata.mdm.data[RELATION_DELETE_START]","subjectId":"","description":"org.unidata.mdm.data.relation.delete.start.description","segments":[{"segmentType":"START","id":"org.unidata.mdm.data[RELATION_DELETE_START]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_DELETE_SECURITY]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_COMMON_PERIOD_CHECK]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_DELETE_CONTAINMENT]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_DELETE_TIMELINE]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_DELETE_INDEXING]"},{"segmentType":"POINT","id":"org.unidata.mdm.data[RELATION_DELETE_PERSISTENCE]"},{"segmentType":"FINISH","id":"org.unidata.mdm.data[RELATION_DELETE_FINISH]"}]}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.data[RECORD_UPSERT_START]', '', '{"description": "org.unidata.mdm.data.record.upsert.start.description","segments": [{"id": "org.unidata.mdm.data[RECORD_UPSERT_START]","segmentType": "START"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_VALIDATE]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_ACCESS]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_PERIOD_CHECK]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_MODBOX]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_TIMELINE]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_INDEXING]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_PERSISTENCE]","segmentType": "POINT"},{"id": "org.unidata.mdm.data[RELATIONS_UPSERT_CONNECTOR]","segmentType": "CONNECTOR"},{"id": "org.unidata.mdm.data[RECORD_UPSERT_FINISH]","segmentType": "FINISH"}],"startId": "org.unidata.mdm.data[RECORD_UPSERT_START]","subjectId": ""}');
INSERT INTO org_unidata_mdm_system.pipelines_info
(start_id, subject, "content")
VALUES('org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_START]', '', '{
  "startId": "org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_START]",
  "subjectId": "",
  "description": "org.unidata.mdm.meta.model.get.start.description",
  "segments": [
    {
      "segmentType": "START",
      "id": "org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_START]"
    },
    {
      "segmentType": "POINT",
      "id": "org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_VALIDATION_POINT]"
    },
    {
      "segmentType": "POINT",
      "id": "org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_FILL_GRAPH_POINT]"
    },
    {
      "segmentType": "FINISH",
      "id": "org.unidata.mdm.meta[CACHE_UPLOAD_MODEL_PREVIEW_FINISH]"
    }
  ]
}');
-- MERGE
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.data[RELATION_MERGE_START]', '', 
'{
   "startId":"org.unidata.mdm.data[RELATION_MERGE_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.data.relations.merge.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"org.unidata.mdm.data[RELATION_MERGE_START]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_MERGE_SECURITY]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_MERGE_TIMELINE]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_MERGE_INDEXING]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_MERGE_PERSISTENCE]"
      },
      {
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.data[RELATION_MERGE_FINISH]"
      }
   ]
}');

INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.data[RECORD_MERGE_START]', '', 
'{
   "startId":"org.unidata.mdm.data[RECORD_MERGE_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.data.record.merge.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"org.unidata.mdm.data[RECORD_MERGE_START]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_MERGE_ACCESS]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_MERGE_TIMELINE]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_MERGE_INDEXING]"
      },
      {
         "segmentType":"CONNECTOR",
         "id":"org.unidata.mdm.data[RELATIONS_MERGE_CONNECTOR]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_MERGE_PERSISTENCE]"
      },
      {
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.data[RECORD_MERGE_FINISH]"
      }
   ]
}');
-- END OF MERGE
-- START OF RESTORE
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.data[RELATION_RESTORE_START]', '', 
'{
   "startId":"org.unidata.mdm.data[RELATION_RESTORE_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.data.relation.restore.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_START]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_SECURITY]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_COMMON_PERIOD_CHECK]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_TIMELINE]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_INDEXING]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_PERSISTENCE]"
      },
      {
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.data[RELATION_RESTORE_FINISH]"
      }
   ]
}');

INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_START]', '', 
'{
   "startId":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_START]",
   "subjectId":"",
   "description":"com.unidata.mdm.classifier.data.classifier.data.restore.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_START]"
      },
      {
         "segmentType":"POINT",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_SECURITY]"
      },
      {
         "segmentType":"POINT",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_TIMELINE]"
      },
      {
         "segmentType":"POINT",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_INDEXING]"
      },
      {
         "segmentType":"POINT",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_PERSISTENCE]"
      },
      {
         "segmentType":"FINISH",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_FINISH]"
      }
   ]
}');

INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.data[RECORD_RESTORE_START]', '', 
'{
   "startId":"org.unidata.mdm.data[RECORD_RESTORE_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.data.record.restore.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_START]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_ACCESS]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_MODBOX]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_TIMELINE]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_INDEXING]"
      },
      {
         "segmentType":"POINT",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_PERSISTENCE]"
      },
      {
         "segmentType":"CONNECTOR",
         "id":"org.unidata.mdm.data[RELATIONS_RESTORE_CONNECTOR]",
         "connectedId":null
      },
      {
         "segmentType":"CONNECTOR",
         "id":"com.unidata.mdm.classifier.data[CLASSIFIER_DATA_RESTORE_CONNECTOR]",
         "connectedId":null
      },
      {
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.data[RECORD_RESTORE_FINISH]"
      }
   ]
}');
-- END OF RESTORE

-- START OF MODEL
-- Model upsert
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_UPSERT_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_UPSERT_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.get.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_UPSERT_START]"
      },
      {
         "segmentType":"POINT",
         "segmentType":"POINT",
         "id":"org.unidata.mdm.meta[MODEL_UPSERT_POINT]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_UPSERT_FINISH]"
      }
   ]
}');

-- Model draft upsert
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_DRAFT_UPSERT_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_DRAFT_UPSERT_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.draft.upsert.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_UPSERT_START]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_UPSERT_FINISH]"
      }
   ]
}');

-- Model get
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_GET_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_GET_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.get.start.description",
   "segments":[
      {
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_GET_START]"
      },
      {
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_GET_FINISH]"
      }
   ]
}');

-- Model draft get
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_DRAFT_GET_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_DRAFT_GET_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.get.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_GET_START]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_GET_FINISH]"
      }
   ]
}');

-- Model publish
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_PUBLISH_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_PUBLISH_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.publish.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_PUBLISH_START]"
      },
      {
         "segmentType":"POINT",
         "segmentType":"POINT",
         "id":"org.unidata.mdm.meta[MODEL_PUBLISH_POINT]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_PUBLISH_FINISH]"
      }
   ]
}');

-- Model draft publish
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_DRAFT_PUBLISH_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_DRAFT_PUBLISH_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.draft.publish.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_PUBLISH_START]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_DRAFT_PUBLISH_FINISH]"
      }
   ]
}');

-- Model refresh
INSERT INTO org_unidata_mdm_system.pipelines_info (start_id, subject, "content")
VALUES('org.unidata.mdm.meta[MODEL_REFRESH_START]', '', 
'{
   "startId":"org.unidata.mdm.meta[MODEL_REFRESH_START]",
   "subjectId":"",
   "description":"org.unidata.mdm.meta.model.refresh.start.description",
   "segments":[
      {
         "segmentType":"START",
         "segmentType":"START",
         "id":"org.unidata.mdm.meta[MODEL_REFRESH_START]"
      },
      {
         "segmentType":"POINT",
         "segmentType":"POINT",
         "id":"org.unidata.mdm.meta[MODEL_REFRESH_POINT]"
      },
      {
         "segmentType":"FINISH",
         "segmentType":"FINISH",
         "id":"org.unidata.mdm.meta[MODEL_REFRESH_FINISH]"
      }
   ]
}');
-- END OF MODEL