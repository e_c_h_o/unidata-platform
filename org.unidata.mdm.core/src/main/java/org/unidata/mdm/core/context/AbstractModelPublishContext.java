package org.unidata.mdm.core.context;

import org.unidata.mdm.system.context.AbstractCompositeRequestContext;

/**
 * @author Mikhail Mikhailov on Oct 8, 2020
 */
public abstract class AbstractModelPublishContext extends AbstractCompositeRequestContext implements ModelPublishContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 8768851190380167183L;
    /**
     * Storage ID to apply the updates to.
     */
    private final String storageId;
    /**
     * The model ID being update.
     */
    private final String instanceId;
    /**
     * Force upon conflicts.
     */
    private final boolean force;
    /**
     * Delete draft object upon successful publishing.
     */
    private final boolean delete;
    /**
     * Wait until model is aplied and then return.
     */
    private final boolean waitForFinish;
    /**
     * Constructor.
     */
    protected AbstractModelPublishContext(AbstractModelPublishContextBuilder<?> b) {
        super(b);
        this.storageId = b.storageId;
        this.instanceId = b.instanceId;
        this.force = b.force;
        this.delete = b.delete;
        this.waitForFinish = b.waitForFinish;
    }
    /**
     * @return the storageId
     */
    @Override
    public String getStorageId() {
        return storageId;
    }
    /**
     * @return the modelId
     */
    @Override
    public String getInstanceId() {
        return instanceId;
    }
    /**
     * @return the force
     */
    @Override
    public boolean isForce() {
        return force;
    }
    /**
     * Tells the draft object should be deleted upon successful publishing.
     * @return the delete flag
     */
    @Override
    public boolean isDelete() {
        return delete;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean waitForFinish() {
        return waitForFinish;
    }
    /**
     * @author Mikhail Mikhailov on Oct 9, 2020
     */
    public abstract static class AbstractModelPublishContextBuilder<X extends AbstractModelPublishContextBuilder<X>>
        extends AbstractCompositeRequestContextBuilder<X> {
        /**
         * Storage ID to apply the updates to.
         */
        private String storageId;
        /**
         * The model ID being update.
         */
        private String instanceId;
        /**
         * Force upon conflicts.
         */
        private boolean force;
        /**
         * Delete draft object upon successful publishing.
         */
        private boolean delete;
        /**
         * Wait until model is aplied and then return.
         */
        private boolean waitForFinish;
        /**
         * Constructor.
         */
        protected AbstractModelPublishContextBuilder() {
            super();
        }
        /**
         * Sets storage ID.
         *
         * @param storageId the ID
         * @return self
         */
        public X storageId(String storageId) {
            this.storageId = storageId;
            return self();
        }
        /**
         * Sets model ID.
         *
         * @param instanceId the ID
         * @return self
         */
        public X instanceId(String instanceId) {
            this.instanceId = instanceId;
            return self();
        }
        /**
         * Sets force flag
         * @param force the froce flag
         * @return self
         */
        public X force(boolean force) {
            this.force = force;
            return self();
        }
        /**
         * Sets delete upon publishing flag.
         * @param delete the flag
         * @return self
         */
        public X delete(boolean delete){
            this.delete = delete;
            return self();
        }
        /**
         * Wait until model is aplied and then return.
         *
         * @param upsertType the type
         * @return self
         */
        public X waitForFinish(boolean waitForFinish) {
            this.waitForFinish = waitForFinish;
            return self();
        }
    }
}
