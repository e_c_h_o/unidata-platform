/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.core.type.model;

import java.util.Collection;

import javax.annotation.Nullable;

/**
 * @author Mikhail Mikhailov on Nov 5, 2020
 * The measurement category.
 */
public interface MeasurementCategoryElement extends IdentityElement, NamedDisplayableElement, CustomPropertiesElement {
    /**
     * Gets all unit values.
     * @return values
     */
    Collection<MeasurementUnitElement> getUnits();
    /**
     * Returns true, if category contains named element.
     * @return true, if category contains named element.
     */
    boolean exists(String name);
    /**
     * Gets unit value with given name.
     * @param name the name
     * @return value
     */
    @Nullable
    MeasurementUnitElement getUnit(String name);
    /**
     * Gets the base unit element.
     * @return base unit element
     */
    MeasurementUnitElement getBaseUnit();
}
