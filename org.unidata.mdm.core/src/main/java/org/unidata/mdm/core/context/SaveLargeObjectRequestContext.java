/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.core.context;

import java.io.InputStream;
import java.util.Objects;
import java.util.function.Supplier;

import javax.annotation.concurrent.NotThreadSafe;

import org.unidata.mdm.system.context.CommonRequestContext;

/**
 * @author Mikhail Mikhailov
 * TODO: Kill unneeded old fields!
 */
@NotThreadSafe
public class SaveLargeObjectRequestContext extends CommonRequestContext {

    /**
     * Generated SVUID.
     */
    private static final long serialVersionUID = -6915756538080064206L;
    /**
     * ID of the BLOB/CLOB record.
     */
    private final String largeObjectId;
    /**
     * Golden key.
     */
    private final String goldenKey;
    /**
     * Origin key.
     */
    private final String originKey;
    /**
     * Event key.
     */
    private final String eventKey;
    /**
     * Attribute name.
     */
    private final String attribute;
    /**
     * Binary or character data.
     */
    private final boolean binary;
    /**
     * Input stream.
     */
    private final transient Supplier<InputStream> input;
    /**
     * File name.
     */
    private final String filename;
    /**
     * MIME type.
     */
    private final String mimeType;
    /**
     * Constructor.
     */
    private SaveLargeObjectRequestContext(SaveLargeObjectRequestContextBuilder b) {
        super(b);
        this.largeObjectId = b.largeObjectId;
        this.goldenKey = b.goldenKey;
        this.originKey = b.originKey;
        this.eventKey = b.eventKey;
        this.attribute = b.attribute;
        this.binary = b.binary;
        this.input = Objects.nonNull(b.inputSupplier) ? b.inputSupplier : () -> b.inputStream;
        this.filename = b.filename;
        this.mimeType = b.mimeType;
    }

    /**
     * @return the largeObjectId
     */
    public String getLargeObjectId() {
        return largeObjectId;
    }

    /**
     * @return the goldenKey
     */
    public String getGoldenKey() {
        return goldenKey;
    }

    /**
     * @return the originKey
     */
    public String getOriginKey() {
        return originKey;
    }


    /**
     * @return the eventKey
     */
    public String getEventKey() {
        return eventKey;
    }

    /**
     * @return the attribute
     */
    public String getAttribute() {
        return attribute;
    }

    /**
     * @return the binary
     */
    public boolean isBinary() {
        return binary;
    }

    /**
     * @return the inputStream
     */
    public Supplier<InputStream> getInput() {
        return input;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @return the mimeType
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Is a origin key or not
     * @return true if so, false otherwise
     */
    public boolean isOrigin() {
        return goldenKey == null && originKey != null;
    }
    /**
     * Is a golden key or not
     * @return true if so, false otherwise
     */
    public boolean isGolden() {
        return goldenKey != null && originKey == null;
    }
    /**
     * Symmetrically to all other contexts.
     * @return builder instance
     */
    public static SaveLargeObjectRequestContextBuilder builder() {
        return new SaveLargeObjectRequestContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov
     * Builder class.
     */
    public static class SaveLargeObjectRequestContextBuilder extends CommonRequestContextBuilder<SaveLargeObjectRequestContextBuilder> {
        /**
         * ID of the BLOB/CLOB record.
         */
        private String largeObjectId;
        /**
         * Golden key.
         */
        private String goldenKey;
        /**
         * Origin key.
         */
        private String originKey;
        /**
         * Event key.
         */
        private String eventKey;
        /**
         * Attribute name.
         */
        private String attribute;
        /**
         * Binary or character data.
         */
        private boolean binary;
        /**
         * Input stream supplier.
         */
        private Supplier<InputStream> inputSupplier;
        /**
         * Input stream.
         */
        private InputStream inputStream;
        /**
         * File name.
         */
        private String filename;
        /**
         * Mime type.
         */
        private String mimeType;
        /**
         * Sets LOB object ID.
         * @param largeObjectId the key
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder largeObjectId(String largeObjectId) {
            this.largeObjectId = largeObjectId;
            return this;
        }
        /**
         * Sets classifier origin key.
         * @param classifierKey the key
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder classifierKey(String classifierKey) {
            this.goldenKey = classifierKey;
            return this;
        }
        /**
         * Sets origin key.
         * @param originKey the key
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder originKey(String originKey) {
            this.originKey = originKey;
            return this;
        }
        /**
         * Sets event key.
         * @param eventKey the key
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder eventKey(String eventKey) {
            this.eventKey = eventKey;
            return this;
        }
        /**
         * Sets attribute name.
         * @param attribute the attribute name
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder attribute(String attribute) {
            this.attribute = attribute;
            return this;
        }
        /**
         * Sets flag to return binary (or character) data.
         * @param binary the flag
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder binary(boolean binary) {
            this.binary = binary;
            return this;
        }
        /**
         * Sets the input stream.
         * @param inputStream the input stream
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder input(InputStream inputStream) {
            this.inputStream = inputStream;
            return this;
        }
        /**
         * Sets an input stream supplier,
         * which may do some recovery in case of failure or run other activities,
         * related to underlying input.
         * @param inputStream the input stream
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder input(Supplier<InputStream> inputStream) {
            this.inputSupplier = inputStream;
            return this;
        }
        /**
         * Sets the file name.
         * @param filename the file name
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder filename(String filename) {
            this.filename = filename;
            return this;
        }
        /**
         * Sets the MIME type.
         * @param mimeType the mime type
         * @return self
         */
        public SaveLargeObjectRequestContextBuilder mimeType(String mimeType) {
            this.mimeType = mimeType;
            return this;
        }
        /**
         * Builds the context.
         * @return new context
         */
        @Override
        public SaveLargeObjectRequestContext build() {
            return new SaveLargeObjectRequestContext(this);
        }
    }

}
