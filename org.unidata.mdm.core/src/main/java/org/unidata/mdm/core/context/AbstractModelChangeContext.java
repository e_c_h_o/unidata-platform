package org.unidata.mdm.core.context;

import org.unidata.mdm.system.context.AbstractCompositeRequestContext;

/**
 * @author Mikhail Mikhailov on Oct 8, 2020
 */
public abstract class AbstractModelChangeContext extends AbstractCompositeRequestContext implements ModelChangeContext {
    /**
     * Name set.
     */
    private static final int NAME_SET_MARK = 1 << 0;
    /**
     * Display name set.
     */
    private static final int DISPLAY_NAME_SET_MARK = 1 << 1;
    /**
     * Description set.
     */
    private static final int DESCRIPTION_SET_MARK = 1 << 2;
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -57473327638268493L;
    /**
     * Storage ID to apply the updates to.
     */
    private final String storageId;
    /**
     * The model ID being update.
     */
    private final String instanceId;
    /**
     * Drop existing model and create a new one (no merge).
     */
    private final ModelChangeType upsertType;
    /**
     * The model name.
     */
    private final String name;
    /**
     * The model display name.
     */
    private final String displayName;
    /**
     * The model description.
     */
    private final String description;
    /**
     * Wait until model is aplied and then return.
     */
    private final boolean waitForFinish;
    /**
     * Info fields state.
     */
    private final int state;
    /**
     * Constructor.
     */
    protected AbstractModelChangeContext(AbstractModelChangeContextBuilder<?> b) {
        super(b);
        this.storageId = b.storageId;
        this.instanceId = b.instanceId;
        this.upsertType = b.upsertType;
        this.name = b.name;
        this.displayName = b.displayName;
        this.description = b.description;
        this.state = b.state;
        this.waitForFinish = b.waitForFinish;
    }
    /**
     * @return the storageId
     */
    @Override
    public String getStorageId() {
        return storageId;
    }
    /**
     * @return the modelId
     */
    @Override
    public String getInstanceId() {
        return instanceId;
    }
    /**
     * @return flag responsible for cleaning current DB and cache state
     */
    public ModelChangeType getUpsertType() {
        return upsertType;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * Returns true, if name field has been set.
     * @return true, if name field has been set.
     */
    public boolean hasNameSet() {
        return (state & NAME_SET_MARK) != 0;
    }
    /**
     * @return display name
     */
    public String getDisplayName() {
        return displayName;
    }
    /**
     * Returns true, if display name field has been set.
     * @return true, if display name field has been set.
     */
    public boolean hasDisplayNameSet() {
        return (state & DISPLAY_NAME_SET_MARK) != 0;
    }
    /**
     * @return description
     */
    public String getDescription() {
        return description;
    }
    /**
     * Returns true, if description field has been set.
     * @return true, if description field has been set.
     */
    public boolean hasDescriptionSet() {
        return (state & DESCRIPTION_SET_MARK) != 0;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean waitForFinish() {
        return waitForFinish;
    }
    /**
     * @author Mikhail Mikhailov on Oct 9, 2020
     */
    public abstract static class AbstractModelChangeContextBuilder<X extends AbstractModelChangeContextBuilder<X>> extends AbstractCompositeRequestContextBuilder<X> {
        /**
         * Storage ID to apply the updates to.
         */
        private String storageId;
        /**
         * The model ID being update.
         */
        private String instanceId;
        /**
         * The model name.
         */
        private String name;
        /**
         * The model display name.
         */
        private String displayName;
        /**
         * The model description.
         */
        private String description;
        /**
         * Info fields state.
         */
        private int state = 0;
        /**
         * Wait until model is aplied and then return.
         */
        private boolean waitForFinish;
        /**
         * Update mode.
         */
        private ModelChangeType upsertType = ModelChangeType.PARTIAL;
        /**
         * Constructor.
         */
        protected AbstractModelChangeContextBuilder() {
            super();
        }
        /**
         * Sets storage ID.
         *
         * @param storageId the ID
         * @return self
         */
        public X storageId(String storageId) {
            this.storageId = storageId;
            return self();
        }
        /**
         * Sets model ID.
         *
         * @param instanceId the ID
         * @return self
         */
        public X instanceId(String instanceId) {
            this.instanceId = instanceId;
            return self();
        }
        /**
         * Sets model description.
         *
         * @param description the model description
         * @return self
         */
        public X description(String description) {
            this.description = description;
            this.state |= DESCRIPTION_SET_MARK;
            return self();
        }
        /**
         * Sets model display name.
         *
         * @param displayName the display name
         * @return self
         */
        public X displayName(String displayName) {
            this.displayName = displayName;
            this.state |= DISPLAY_NAME_SET_MARK;
            return self();
        }
        /**
         * Sets model name.
         *
         * @param name the name
         * @return self
         */
        public X name(String name) {
            this.name = name;
            this.state |= NAME_SET_MARK;
            return self();
        }
        /**
         * Sets upsert type.
         *
         * @param upsertType the type
         * @return self
         */
        public X upsertType(ModelChangeType upsertType) {
            this.upsertType = upsertType;
            return self();
        }
        /**
         * Wait until model is aplied and then return.
         *
         * @param upsertType the type
         * @return self
         */
        public X waitForFinish(boolean waitForFinish) {
            this.waitForFinish = waitForFinish;
            return self();
        }
    }
}
