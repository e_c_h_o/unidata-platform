/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.core.context;

/**
 * @author Mikhail Mikhailov on Oct 8, 2020
 * Model change marker, the model descriptor operates on.
 */
public interface ModelChangeContext extends ModelIdentityContext {
    /**
     * Wait, until changes applied and the return.
     * @return
     */
    boolean waitForFinish();
    /**
     * Model change type.
     */
    enum ModelChangeType {
        /**
         * One element update.
         * Former PARTIAL_UPDATE
         */
        PARTIAL,
        /**
         * Full replacement - model will be recreated.
         * Former FULLY_NEW.
         */
        FULL,
        /**
         * Current model and existed will be merged.
         * Former ADDITION
         */
        MERGE
    }
}
