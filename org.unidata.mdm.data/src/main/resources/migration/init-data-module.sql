-- Records
-- drop type record_origin_key cascade;
create type record_origin_key as (
  id uuid,
  initial_owner uuid,
  status record_status,
  enrichment boolean,
  revision integer,
  source_system varchar(256),
  create_date timestamptz,
  created_by varchar(256),
  update_date timestamptz,
  updated_by varchar(256),
  external_id varchar(512)
);

-- drop type record_key cascade;
create type record_key as (
  shard integer,
  lsn bigint,
  id uuid,
  name varchar(256),
  status record_status,
  approved boolean,
  create_date timestamptz,
  created_by varchar(256),
  update_date timestamptz,
  updated_by varchar(256),
  origin_keys record_origin_key[]
);

-- Relations
-- drop type relation_origin_key cascade;
create type relation_origin_key as (
  id uuid,
  initial_owner uuid,
  status record_status,
  enrichment boolean,
  revision integer,
  source_system varchar(256),
  create_date timestamptz,
  created_by varchar(256),
  update_date timestamptz,
  updated_by varchar(256),
  from_key uuid,
  to_key uuid
);

-- drop type relation_key cascade;
create type relation_key as (
  shard integer,
  lsn bigint,
  id uuid,
  name varchar(256),
  status record_status,
  approved boolean,
  create_date timestamptz,
  created_by varchar(256),
  update_date timestamptz,
  updated_by varchar(256),
  type relation_type,
  from_key uuid,
  to_key uuid,
  origin_keys relation_origin_key[]
);


create type record_vistory_data as (
  id uuid,
  origin_id uuid,
  shard int4,
  revision int4,
  valid_from timestamptz,
  valid_to timestamptz,
  create_date timestamptz,
  created_by varchar,
  status record_status,
  shift data_shift,
  operation_type operation_type,
  operation_id text,
  data_b bytea,
  major int4,
  minor int4
);

create type record_timeline as (
keys record_key,
vistory_data record_vistory_data[]
);
---------------------------------------------- Relations ---------------------------------------------
create type relation_vistory_data as (
id uuid,
origin_id uuid,
shard int4,
revision int4,
valid_from timestamptz,
valid_to timestamptz,
create_date timestamptz,
created_by varchar,
status record_status,
shift data_shift,
operation_type operation_type,
operation_id text,
data_b bytea,
major int4,
minor int4);

create type relation_timeline as (
keys relation_key,
vistory_data relation_vistory_data[]
);


create type update_mark as (update_date timestamptz, updated_by varchar(256));
