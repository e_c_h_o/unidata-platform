/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.relations.draft;

import java.util.Objects;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Alexey Tsarapkin
 */
@Component(RelationDraftGetStartExecutor.SEGMENT_ID)
public class RelationDraftGetStartExecutor extends Start<DraftGetContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_DRAFT_GET_START]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.draft.get.start.description";
    /**
     * The draft service.
     */
    @Autowired
    private DraftService draftService;
    /**
     * Constructor.
     */
    public RelationDraftGetStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetContext.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(@Nonnull DraftGetContext ctx) {
        setup(ctx);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(DraftGetContext ctx) {

        setup(ctx);

        Draft draft = ctx.currentDraft();
        Variables variables = draft.getVariables();
        return Objects.nonNull(variables) ? variables.valueGet(DataDraftConstants.RELATION_NAME) : null;
    }

    protected void setup(DraftGetContext ctx) {

        if (ctx.setUp()) {
            return;
        }

        Draft draft = ctx.currentDraft();
        ctx.currentEdition(draftService.current(draft.getDraftId(), true));

        ctx.setUp(true);
    }
}
