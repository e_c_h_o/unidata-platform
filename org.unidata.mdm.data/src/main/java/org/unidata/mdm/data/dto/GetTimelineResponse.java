package org.unidata.mdm.data.dto;

import org.unidata.mdm.core.type.calculables.Calculable;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.draft.type.DraftPayloadResponse;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 */
public class GetTimelineResponse<X extends Calculable> implements DraftPayloadResponse {
    /**
     * The timeline.
     */
    private Timeline<X> timeline;
    /**
     * Constructor.
     */
    public GetTimelineResponse(Timeline<X> timeline) {
        super();
        this.timeline = timeline;
    }
    /**
     * @return the timeline
     */
    public Timeline<X> getTimeline() {
        return timeline;
    }
    /**
     * @param timeline the timeline to set
     */
    public void setTimeline(Timeline<X> timeline) {
        this.timeline = timeline;
    }
}
