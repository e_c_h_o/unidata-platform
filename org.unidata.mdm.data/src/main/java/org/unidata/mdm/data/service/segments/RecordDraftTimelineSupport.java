package org.unidata.mdm.data.service.segments;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.data.DataShift;
import org.unidata.mdm.core.type.data.OperationType;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.core.type.formless.DataBundle;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.type.calculables.impl.DataRecordHolder;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.data.OriginRecordInfoSection;
import org.unidata.mdm.data.type.data.impl.OriginRecordImpl;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.keys.RecordEtalonKey;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.data.type.timeline.RecordTimeline;
import org.unidata.mdm.data.util.StorageUtils;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;

/**
 * @author Mikhail Mikhailov on Sep 26, 2020
 * Draft timeline and keys generation support.
 */
public interface RecordDraftTimelineSupport {
    /**
     * Gets common component, needed for key fetch.
     * @return component
     */
    CommonRecordsComponent getCommonRecordsComponent();
    /**
     * Generates timeline from record and edition draft.
     * @param draft the draft
     * @param edition the edition
     * @return timeline
     */
    default Timeline<OriginRecord> timeline(Draft draft, Edition edition) {

        // Just for case
        if (Objects.isNull(edition)) {
            return null;
        }

        boolean isNew = draft.getVariables().valueGet(DataDraftConstants.IS_NEW_RECORD);
        RecordKeys keys = isNew ? generate(draft, edition) : load(draft);

        BundlesArray bundles = edition.getContent();
        List<CalculableHolder<OriginRecord>> calculables = bundles.stream()
            .map(b -> origin(b, edition, keys.getOriginKey()))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        return new RecordTimeline(keys, calculables);
    }
    /**
     * Generates a calculable holder from a bundle.
     * @param bundle the bundle
     * @param edition the edition
     * @param ok the keys
     * @return holder
     */
    default CalculableHolder<OriginRecord> origin(DataBundle bundle, Edition edition, RecordOriginKey ok) {

        if (Objects.isNull(bundle)) {
            return null;
        }

        boolean isDraft = bundle.getVariables().valueGet(DataDraftConstants.IS_DRAFT_RECORD);
        Instant from = bundle.getVariables().valueGet(DataDraftConstants.VALID_FROM);
        Instant to = bundle.getVariables().valueGet(DataDraftConstants.VALID_TO);
        Instant ts = bundle.getVariables().valueGet(DataDraftConstants.CREATE_TIMESTAMP);
        OperationType type = bundle.getVariables().valueGet(DataDraftConstants.OPERATION_TYPE, OperationType.class);
        RecordStatus status = bundle.getVariables().valueGet(DataDraftConstants.PERIOD_STATUS, RecordStatus.class);

        Date timestamp = new Date(ts.toEpochMilli());

        OriginRecordImpl origin = new OriginRecordImpl()
            .withDataRecord(bundle.getRecord())
            .withInfoSection(new OriginRecordInfoSection()
                .withOriginKey(ok)
                .withValidFrom(from == null ? null : new Date(from.toEpochMilli()))
                .withValidTo(to == null ? null : new Date(to.toEpochMilli()))
                .withCreateDate(timestamp)
                .withUpdateDate(timestamp)
                .withCreatedBy(edition.getCreatedBy())
                .withUpdatedBy(edition.getCreatedBy())
                .withRevision(isDraft ? DataDraftConstants.USER_DRAFT_REVISION : DataDraftConstants.BASE_DRAFT_REVISION)
                .withStatus(status)
                .withOperationType(type)
                .withShift(DataShift.PRISTINE)
                .withMajor(0)
                .withMinor(0));

        return new DataRecordHolder(origin);
    }
    /**
     * Loads exisitng record's keys using draft object.
     * @param draft the draft object
     * @return keys
     */
    default RecordKeys load(Draft draft) {
        String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
        return getCommonRecordsComponent().identify(RecordEtalonKey.builder()
                    .id(etalonId)
                    .build());
    }
    /**
     * Generates phantom keys for a new record using draft object.
     * @param draft the draft object
     * @param edition the edition
     * @return keys
     */
    default RecordKeys generate(Draft draft, Edition edition) {

        String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
        RecordOriginKey rok = RecordOriginKey.builder()
                .createDate(draft.getCreateDate())
                .createdBy(draft.getCreatedBy())
                .updateDate(edition.getCreateDate())
                .updatedBy(edition.getCreatedBy())
                .enrichment(false)
                .entityName(draft.getVariables().valueGet(DataDraftConstants.ENTITY_NAME))
                .externalId(draft.getVariables().valueGet(DataDraftConstants.EXTERNAL_ID))
                .sourceSystem(draft.getVariables().valueGet(DataDraftConstants.SOURCE_SYSTEM))
                .id(StringUtils.EMPTY)
                .initialOwner(UUID.fromString(etalonId))
                .revision(0)
                .build();

        int shard = draft.getVariables().valueGet(DataDraftConstants.SHARD);
        long lsn = draft.getVariables().valueGet(DataDraftConstants.LSN);
        return RecordKeys.builder()
                .createDate(draft.getCreateDate())
                .createdBy(draft.getCreatedBy())
                .entityName(draft.getVariables().valueGet(DataDraftConstants.ENTITY_NAME))
                .etalonKey(RecordEtalonKey.builder()
                    .id(etalonId)
                    .lsn(lsn)
                    .status(RecordStatus.ACTIVE)
                    .build())
                .originKey(rok)
                .node(StorageUtils.node(shard))
                .published(false)
                .shard(shard)
                .supplementaryKeys(Collections.singletonList(rok))
                .updateDate(edition.getCreateDate())
                .updatedBy(edition.getCreatedBy())
                .build();
    }
}
