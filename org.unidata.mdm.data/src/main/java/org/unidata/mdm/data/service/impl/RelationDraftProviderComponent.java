package org.unidata.mdm.data.service.impl;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.serialization.CoreSerializer;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.RelationIdentityContext;
import org.unidata.mdm.data.service.segments.relations.draft.RelationDraftGetStartExecutor;
import org.unidata.mdm.data.service.segments.relations.draft.RelationDraftPublishStartExecutor;
import org.unidata.mdm.data.service.segments.relations.draft.RelationDraftUpsertStartExecutor;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.draft.context.DraftQueryContext;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftQueryResult;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.DraftOperation;
import org.unidata.mdm.draft.type.DraftProvider;
import org.unidata.mdm.system.context.DraftAwareContext;
import org.unidata.mdm.system.service.TextService;

/**
 * @author Mikhail Mikhailov on Sep 25, 2020
 */
@Component
public class RelationDraftProviderComponent implements DraftProvider<BundlesArray> {
    /**
     * This draft provider id.
     */
    public static final String ID = "relation";
    /**
     * This draft provider description.
     */
    private static final String DESCRIPTION = "app.data.draft.relation.provider.description";
    /**
     * The TS.
     */
    @Autowired
    private TextService textService;
    /**
     * The DS.
     */
    private DraftService draftService;
    /**
     * Constructor.
     */
    @Autowired
    public RelationDraftProviderComponent(DraftService draftService) {
        super();
        this.draftService = draftService;
        this.draftService.register(this);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return textService.getText(DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPipelineId(DraftOperation operation) {

        switch (operation) {
        case GET_DATA:
            return RelationDraftGetStartExecutor.SEGMENT_ID;
        case UPSERT_DATA:
            return RelationDraftUpsertStartExecutor.SEGMENT_ID;
        case PUBLISH_DATA:
            return RelationDraftPublishStartExecutor.SEGMENT_ID;
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public BundlesArray fromBytes(byte[] data) {
        return CoreSerializer.bundlesArrayFromProtostuff(data);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] toBytes(BundlesArray data) {
        return CoreSerializer.bundlesArrayToProtostuff(data);
    }
    /**
     * Tries to select draft id for the given context.
     * @param <C> the context type
     * @param ctx the context instance
     * @return draft id or null, if not found
     */
    public <C extends DraftAwareContext & RelationIdentityContext> Long selectDraftId(C ctx) {

        // Return DID
        if (Objects.nonNull(ctx) && ctx.hasDraftId()) {
            return ctx.getDraftId();
        }

        // Resolve draft id by parent draft id
        if (Objects.nonNull(ctx) && ctx.hasParentDraftId()) {

            String subjectId;
            RelationKeys keys = ctx.relationKeys();
            if (Objects.nonNull(keys)) {
                subjectId = keys.getEtalonKey().getId();
            } else {
                subjectId = ctx.isRelationEtalonKey() ? ctx.getRelationEtalonKey() : null;
            }

            if (Objects.nonNull(subjectId)) {

                DraftQueryResult result = draftService.drafts(DraftQueryContext.builder()
                        .parentDraftId(ctx.getParentDraftId())
                        .provider(ID)
                        .subjectId(subjectId)
                        .build());

                return result.getDrafts().size() == 1
                        ? result.getDrafts().get(0).getDraftId()
                        : null;
            }
        }

        return null;
    }
    /**
     * If draft id can be resolved using this context, it is used.
     * If not, it is CREATED and the returned.
     * @param <C> the context type
     * @param ctx the context
     * @return draft id
     */
    public <C extends DraftAwareContext & RelationIdentityContext> Long ensureDraftId(C ctx) {

        // Find
        Long existing = selectDraftId(ctx);
        if (Objects.nonNull(existing)) {
            return existing;
        }

        // Or create
        RelationKeys keys = ctx.relationKeys();
        DraftUpsertResult result = draftService.upsert(DraftUpsertContext.builder()
                .provider(ID)
                .subjectId(keys != null ? keys.getEtalonKey().getId() : StringUtils.EMPTY)
                .parentDraftId(ctx.getParentDraftId())
                .owner(SecurityUtils.getCurrentUserName())
                .build());

        return result.getDraft().getDraftId();
    }
}
