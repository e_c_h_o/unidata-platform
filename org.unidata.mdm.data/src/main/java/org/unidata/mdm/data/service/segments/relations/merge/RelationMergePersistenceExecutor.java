/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.merge;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.MergeFromRelationRequestContext;
import org.unidata.mdm.data.context.MergeToRelationRequestContext;
import org.unidata.mdm.data.context.RelationMergeContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.po.data.RelationEtalonPO;
import org.unidata.mdm.data.po.data.RelationEtalonRemapFromPO;
import org.unidata.mdm.data.po.data.RelationEtalonRemapToPO;
import org.unidata.mdm.data.po.data.RelationOriginRemapPO;
import org.unidata.mdm.data.po.keys.RelationExternalKeyPO;
import org.unidata.mdm.data.service.RelationChangeSetProcessor;
import org.unidata.mdm.data.type.apply.RelationMergeChangeSet;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.data.type.merge.MergeRelationMasterState;
import org.unidata.mdm.data.util.StorageUtils;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Dec 9, 2019
 */
@Component(RelationMergePersistenceExecutor.SEGMENT_ID)
public class RelationMergePersistenceExecutor extends Point<RelationMergeContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_MERGE_PERSISTENCE]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.merge.persistence.description";
    /**
     * Rels change set.
     */
    @Autowired
    private RelationChangeSetProcessor relationChangeSetProcessor;
    /**
     * Constructor.
     */
    public RelationMergePersistenceExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationMergeContext ctx) {

        MeasurementPoint.start();
        try {

            // 1. Prepare set
            prepareChangeSet(ctx);

            // 2. Apply changes
            applyChangeSet(ctx);

        } finally {
            MeasurementPoint.stop();
        }
    }

    private void applyChangeSet(RelationMergeContext ctx) {

        // Will be applied later in batched fashion.
        if (ctx.isBatchOperation()) {
            return;
        }

        // 2. Apply
        RelationMergeChangeSet set = ctx.changeSet();
        relationChangeSetProcessor.apply(set);
    }

    private void prepareChangeSet(RelationMergeContext ctx) {

        MeasurementPoint.start();
        try {

            if (ctx.getDirection() == RelativeDirection.FROM) {
                prepareFromChangeSet((MergeFromRelationRequestContext) ctx);
            } else {
                prepareToChangeSet((MergeToRelationRequestContext) ctx);
            }

        } finally {
            MeasurementPoint.stop();
        }
    }

    private void prepareFromChangeSet(MergeFromRelationRequestContext ctx) {

        String userName = SecurityUtils.getCurrentUserName();
        Date timestamp = ctx.timestamp();

        RelationKeys relationKeys = ctx.relationKeys();
        RecordKeys duplicate = relationKeys.getFromAsRecordKeys();
        RecordKeys master = ctx.masterKeys();

        MergeRelationMasterState state = ctx.masterState();
        RelationMergeChangeSet set = ctx.changeSet();

        // 1. Drop from ref keys tables
        RelationExternalKeyPO wek = new RelationExternalKeyPO();
        wek.setFromRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getFrom().getId()));
        wek.setFromShard(StorageUtils.shard(wek.getFromRecordEtalonId()));
        wek.setToRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getTo().getId()));
        wek.setToShard(StorageUtils.shard(wek.getToRecordEtalonId()));
        wek.setRelationName(relationKeys.getRelationName());

        set.getExternalKeyWipes().add(wek);

        // 2. Decide what to do with origins and etalon object
        RelationKeys existingMasterKeys = state.getMasterKeysByTypeAndToId(relationKeys);
        if (Objects.nonNull(existingMasterKeys)) {

            // 2.1. Drop this rel, that we cannot remap.
            // Master rel points already to the same 'to' side, so drop the original.
            RelationEtalonPO erpo = new RelationEtalonPO();
            erpo.setId(relationKeys.getEtalonKey().getId());
            erpo.setFromEtalonId(duplicate.getEtalonKey().getId());
            erpo.setOperationId(ctx.getOperationId());
            erpo.setStatus(RecordStatus.MERGED);
            erpo.setUpdateDate(timestamp);
            erpo.setUpdatedBy(userName);
            erpo.setName(relationKeys.getRelationName());

            set.getEtalonUpdates().add(erpo);

            // 2.2. Remap its origins to the new target
            set.getOriginRemaps().addAll(
                relationKeys.getSupplementaryKeys().stream()
                    .map(ok -> {

                        RelationOriginRemapPO po = new RelationOriginRemapPO();
                        po.setId(ok.getId());
                        po.setShard(relationKeys.getShard());
                        po.setNewEtalonId(UUID.fromString(existingMasterKeys.getEtalonKey().getId()));
                        po.setNewShard(existingMasterKeys.getShard());
                        po.setUpdateDate(timestamp);
                        po.setUpdatedBy(userName);
                        po.setCreateDate(ok.getCreateDate());
                        po.setCreatedBy(ok.getCreatedBy());
                        po.setEnrichment(ok.isEnrichment());
                        po.setEtalonId(relationKeys.getEtalonKey().getId());
                        po.setFromOriginId(ok.getFrom().getId());
                        po.setInitialOwner(ok.getInitialOwner());
                        po.setName(relationKeys.getRelationName());
                        po.setSourceSystem(ok.getSourceSystem());
                        po.setStatus(ok.getStatus());
                        po.setToOriginId(ok.getTo().getId());

                        return po;
                    })
                    .collect(Collectors.toList()));

            return;
        }

        // 3. Contribute a new relation to the master object
        RelationEtalonRemapFromPO remapFrom = new RelationEtalonRemapFromPO();

        remapFrom.setId(relationKeys.getEtalonKey().getId());
        remapFrom.setShard(relationKeys.getShard());
        remapFrom.setFromEtalonId(duplicate.getEtalonKey().getId());
        remapFrom.setNewEtalonIdFrom(master.getEtalonKey().getId());
        remapFrom.setName(relationKeys.getRelationName());
        remapFrom.setOperationId(ctx.getOperationId());
        remapFrom.setUpdateDate(timestamp);
        remapFrom.setUpdatedBy(userName);

        set.getEtalonFromRemaps().add(remapFrom);

        // 4. Add new ref keys
        RelationExternalKeyPO iek = new RelationExternalKeyPO();

        iek.setFromRecordEtalonId(UUID.fromString(master.getEtalonKey().getId()));
        iek.setFromShard(StorageUtils.shard(wek.getFromRecordEtalonId()));
        iek.setToRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getTo().getId()));
        iek.setToShard(StorageUtils.shard(wek.getToRecordEtalonId()));
        iek.setRelationName(relationKeys.getRelationName());
        iek.setRelationEtalonId(UUID.fromString(relationKeys.getEtalonKey().getId()));

        set.getExternalKeyInserts().add(iek);
    }

    private void prepareToChangeSet(MergeToRelationRequestContext ctx) {

        String userName = SecurityUtils.getCurrentUserName();
        Date timestamp = ctx.timestamp();

        RelationKeys relationKeys = ctx.relationKeys();
        RecordKeys duplicate = relationKeys.getToAsRecordKeys();
        RecordKeys master = ctx.masterKeys();

        MergeRelationMasterState state = ctx.masterState();
        RelationMergeChangeSet set = ctx.changeSet();

        // 1. Drop from ref keys tables
        RelationExternalKeyPO wek = new RelationExternalKeyPO();
        wek.setFromRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getFrom().getId()));
        wek.setFromShard(StorageUtils.shard(wek.getFromRecordEtalonId()));
        wek.setToRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getTo().getId()));
        wek.setToShard(StorageUtils.shard(wek.getToRecordEtalonId()));
        wek.setRelationName(relationKeys.getRelationName());

        set.getExternalKeyWipes().add(wek);

        // 2. Decide what to do with origins and etalon object
        RelationKeys existingMasterKeys = state.getMasterKeysByTypeAndFromId(relationKeys);
        if (Objects.nonNull(existingMasterKeys)) {

            // 3.1. Drop this rel, that we cannot remap.
            // Master rel points already to the same 'to' side, so drop the original.
            RelationEtalonPO erpo = new RelationEtalonPO();
            erpo.setId(relationKeys.getEtalonKey().getId());
            erpo.setToEtalonId(duplicate.getEtalonKey().getId());
            erpo.setOperationId(ctx.getOperationId());
            erpo.setStatus(RecordStatus.MERGED);
            erpo.setUpdateDate(timestamp);
            erpo.setUpdatedBy(userName);
            erpo.setName(relationKeys.getRelationName());

            set.getEtalonUpdates().add(erpo);

            // 2.2. Remap its origins to the new target
            set.getOriginRemaps().addAll(
                relationKeys.getSupplementaryKeys().stream()
                    .map(ok -> {

                        RelationOriginRemapPO po = new RelationOriginRemapPO();
                        po.setId(ok.getId());
                        po.setShard(relationKeys.getShard());
                        po.setNewEtalonId(UUID.fromString(existingMasterKeys.getEtalonKey().getId()));
                        po.setNewShard(existingMasterKeys.getShard());
                        po.setUpdateDate(timestamp);
                        po.setUpdatedBy(userName);
                        po.setCreateDate(ok.getCreateDate());
                        po.setCreatedBy(ok.getCreatedBy());
                        po.setEnrichment(ok.isEnrichment());
                        po.setEtalonId(relationKeys.getEtalonKey().getId());
                        po.setFromOriginId(ok.getFrom().getId());
                        po.setToOriginId(ok.getTo().getId());
                        po.setInitialOwner(ok.getInitialOwner());
                        po.setName(relationKeys.getRelationName());
                        po.setSourceSystem(ok.getSourceSystem());
                        po.setStatus(ok.getStatus());

                        return po;
                    })
                    .collect(Collectors.toList()));

            return;
        }

        // 4. Contribute a new relation to new master object
        RelationEtalonRemapToPO remapTo = new RelationEtalonRemapToPO();

        remapTo.setId(relationKeys.getEtalonKey().getId());
        remapTo.setShard(relationKeys.getShard());
        remapTo.setToEtalonId(duplicate.getEtalonKey().getId());
        remapTo.setNewEtalonIdTo(master.getEtalonKey().getId());
        remapTo.setName(relationKeys.getRelationName());
        remapTo.setOperationId(ctx.getOperationId());
        remapTo.setUpdateDate(timestamp);
        remapTo.setUpdatedBy(userName);

        set.getEtalonToRemaps().add(remapTo);

        RelationExternalKeyPO iek = new RelationExternalKeyPO();
        iek.setFromRecordEtalonId(UUID.fromString(relationKeys.getEtalonKey().getFrom().getId()));
        iek.setFromShard(StorageUtils.shard(wek.getFromRecordEtalonId()));
        iek.setToRecordEtalonId(UUID.fromString(master.getEtalonKey().getId()));
        iek.setToShard(StorageUtils.shard(wek.getToRecordEtalonId()));
        iek.setRelationName(relationKeys.getRelationName());
        iek.setRelationEtalonId(UUID.fromString(relationKeys.getEtalonKey().getId()));

        set.getExternalKeyInserts().add(iek);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationMergeContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
