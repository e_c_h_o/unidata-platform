/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.type.apply.batch.impl;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.DataContextFlags;
import org.unidata.mdm.data.context.RecordIdentityContext;
import org.unidata.mdm.data.context.RestoreFromRelationRequestContext;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.context.RestoreRelationsRequestContext;
import org.unidata.mdm.data.context.RestoreToRelationRequestContext;
import org.unidata.mdm.data.dto.RestoreRelationsDTO;
import org.unidata.mdm.data.po.data.RelationVistoryPO;
import org.unidata.mdm.data.service.segments.relations.batch.RelationsUpsertStartExecutor;
import org.unidata.mdm.data.type.apply.batch.BatchKeyReference;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.system.type.batch.BatchIterator;
import org.unidata.mdm.system.type.pipeline.fragment.FragmentId;
import org.unidata.mdm.system.type.pipeline.fragment.InputFragment;

/**
 * @author Mikhail Mikhailov
 * Relation batch set accumulator.
 */
public class RelationRestoreBatchSetAccumulator
    extends AbstractRelationBatchSetAccumulator<RestoreRelationsRequestContext, RestoreRelationsDTO>
    implements InputFragment<RelationRestoreBatchSetAccumulator> {
    /**
     * This accumulator ID.
     */
    public static final FragmentId<RelationRestoreBatchSetAccumulator> ID
        = new FragmentId<>("RELATION_RESTORE_BATCH_SET");
    /**
     * Containments accumulator.
     */
    private final RecordRestoreBatchSetAccumulator recordBatchSetAccumulator;
    /**
     * RelTo id cache for multiVersion relations.
     */
    private final Map<String, BatchKeyReference<RelationKeys>> ids;
    /**
     * This chunk is a containment relation
     */
    private final boolean containmentRelation;
    /**
     * Virtual timelines of type 'REFERENCE'.
     */
    private final Map<String, List<Timeline<OriginRelation>>> collectedReferenceTimelines = new HashMap<>();
    /**
     * Stats / results.
     */
    private final RelationRestoreBatchSetStatistics statistics;
    /**
     * Constructor.
     * @param commitSize chunk size
     * @param isMultiversion true for several updates from the same record id in the same job.
     *  If true, a simple id cache will be build.
     *  The accumulator must exist during the step then.
     * @param isContainment whether this accumulator processes a containment relation
     */
    public RelationRestoreBatchSetAccumulator(int commitSize, boolean isMultiversion, boolean isContainment) {
        super(commitSize);
        // Containments and keys cache
        containmentRelation = isContainment;
        recordBatchSetAccumulator = containmentRelation ? new RecordRestoreBatchSetAccumulator(commitSize) : null;
        statistics = new RelationRestoreBatchSetStatistics();

        if (isMultiversion) {
            ids = new HashMap<>();
        } else {
            ids = null;
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FragmentId<RelationRestoreBatchSetAccumulator> fragmentId() {
        return ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public BatchIterator<RestoreRelationsRequestContext> iterator() {
        return new RelationRestoreBatchIterator();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void discharge() {
        super.discharge();
        statistics.reset();
        // Containments
        if (Objects.nonNull(recordBatchSetAccumulator)) {
            recordBatchSetAccumulator.discharge();
        }
    }
    /**
     * @return the recordBatchSetAccumulator
     */
    public RecordRestoreBatchSetAccumulator getRecordBatchSetAccumulator() {
        return recordBatchSetAccumulator;
    }
    /**
     * Extracts cache string from contexts.
     * @param left left rel side context
     * @param right right rel side context
     * @return string or null
     */
    public static String toCacheString(RecordIdentityContext left, RecordIdentityContext right) {

        if (Objects.nonNull(left) && Objects.nonNull(right)) {

            String leftSide = null;
            String rightSide = null;

            if (left.isOriginExternalId()) {
                leftSide = RecordUpsertBatchSetAccumulator.toExternalIdCacheString(left.getExternalId(), left.getEntityName(), left.getSourceSystem());
            } else if (left.isEtalonRecordKey()) {
                leftSide = left.getEtalonKey();
            }

            if (right.isOriginExternalId()) {
                rightSide = RecordUpsertBatchSetAccumulator.toExternalIdCacheString(right.getExternalId(), right.getEntityName(), right.getSourceSystem());
            } else if (right.isEtalonRecordKey()) {
                rightSide = right.getEtalonKey();
            }

            if (StringUtils.isNoneBlank(leftSide, rightSide)) {
                return StringUtils.join(leftSide, "|", rightSide);
            }
        }

        return null;
    }
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public RelationRestoreBatchSetStatistics statistics() {
        return statistics;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return RelationsUpsertStartExecutor.SEGMENT_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void accumulate(RestoreRelationsRequestContext ctx) {

        for (Entry<String, List<RestoreFromRelationRequestContext>> entry : ctx.getRelationsFrom().entrySet()) {
            for (RestoreFromRelationRequestContext uCtx : entry.getValue()) {

                RelationRestoreBatchSet batchSet = uCtx.changeSet();
                if (Objects.isNull(batchSet)) {
                    return;
                }

                accumulateEtalonUpdates(batchSet.getEtalonRelationUpdatePOs());
                accumulateOriginUpdates(batchSet.getOriginRelationUpdatePOs());

                // COPY support, revision must be known beforehand.
                BatchKeyReference<RelationKeys> cachedKeys = findOrPutCachedKeys(uCtx.relationKeys(), ctx, uCtx);
                int currentRevision = cachedKeys.getRevision();
                for (RelationVistoryPO v : batchSet.getOriginsVistoryRelationsPOs()) {
                    v.setRevision(++currentRevision);
                    accumulateVistory(v);
                }

                if (cachedKeys.getRevision() != currentRevision) {
                    cachedKeys.setRevision(currentRevision);
                }

                // Containment
                if (containmentRelation) {
                    RestoreRecordRequestContext cCtx = uCtx.containmentContext();
                    if (Objects.nonNull(cCtx)) {
                        recordBatchSetAccumulator.accumulate(cCtx);
                    }
                }

                if (Objects.nonNull(batchSet.getIndexRequestContexts())) {
                    indexUpdates.addAll(batchSet.getIndexRequestContexts());
                }
            }
        }

        for (Entry<String, List<RestoreToRelationRequestContext>> entry : ctx.getRelationsTo().entrySet()) {
            for (RestoreToRelationRequestContext uCtx : entry.getValue()) {

                RelationRestoreBatchSet batchSet = uCtx.changeSet();
                if (Objects.isNull(batchSet)) {
                    return;
                }

                accumulateEtalonUpdates(batchSet.getEtalonRelationUpdatePOs());
                accumulateOriginUpdates(batchSet.getOriginRelationUpdatePOs());

                // COPY support, revision must be known beforehand.
                BatchKeyReference<RelationKeys> cachedKeys = findOrPutCachedKeys(uCtx.relationKeys(), ctx, uCtx);
                int currentRevision = cachedKeys.getRevision();
                for (RelationVistoryPO v : batchSet.getOriginsVistoryRelationsPOs()) {
                    v.setRevision(++currentRevision);
                    accumulateVistory(v);
                }

                if (cachedKeys.getRevision() != currentRevision) {
                    cachedKeys.setRevision(currentRevision);
                }

                // Containment
                if (containmentRelation) {
                    RestoreRecordRequestContext cCtx = uCtx.containmentContext();
                    if (Objects.nonNull(cCtx)) {
                        recordBatchSetAccumulator.accumulate(cCtx);
                    }
                }

                if (Objects.nonNull(batchSet.getIndexRequestContexts())) {
                    indexUpdates.addAll(batchSet.getIndexRequestContexts());
                }
            }
        }

    }

    private BatchKeyReference<RelationKeys> findOrPutCachedKeys(RelationKeys keys, RecordIdentityContext left, RecordIdentityContext right) {

        BatchKeyReference<RelationKeys> cachedKeys = null;
        if (Objects.nonNull(ids)) {

            final String cacheString = toCacheString(left, right);
            if (StringUtils.isNotBlank(cacheString)) {
                cachedKeys = ids.get(cacheString);
            }

            if (Objects.isNull(cachedKeys)) {
                cachedKeys = new RelationBatchKeyReference(keys);
                ids.put(cacheString, cachedKeys);
            }
        } else {
            cachedKeys = new RelationBatchKeyReference(keys);
        }

        return cachedKeys;
    }

    /**
     * @author Mikhail Mikhailov
     * Simple batch iterator.
     */
    private class RelationRestoreBatchIterator implements BatchIterator<RestoreRelationsRequestContext> {
        /**
         * The iterator.
         */
        private ListIterator<RestoreRelationsRequestContext> i = workingCopy.listIterator();
        /**
         * Currently processed contex.
         */
        private RestoreRelationsRequestContext current;
        /**
         * Constructor.
         */
        public RelationRestoreBatchIterator() {
            super();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean hasNext() {

            boolean hasNext = i.hasNext();
            if (!hasNext && current != null) {
                accumulate(current);
            }

            return hasNext;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public RestoreRelationsRequestContext next() {

            RestoreRelationsRequestContext next = i.next();
            if (current != null) {
                accumulate(current);
            }

            init(next);

            current = next;
            return next;
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public void remove() {
            i.remove();
            current = null;
        }

        /**
         * Does some preprocessing.
         * @param ctx the upsert context
         */
        private void init(RestoreRelationsRequestContext ctx) {

            for (Entry<String, List<RestoreFromRelationRequestContext>> entry : ctx.getRelationsFrom().entrySet()) {
                for (RestoreFromRelationRequestContext rCtx : entry.getValue()) {

                    if (Objects.isNull(rCtx.changeSet())) {

                        RelationRestoreBatchSet set = new RelationRestoreBatchSet(RelationRestoreBatchSetAccumulator.this);
                        set.setCollectedReferenceTimelines(collectedReferenceTimelines);

                        rCtx.changeSet(set);
                    }

                    rCtx.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, true);
                }
            }

            for (Entry<String, List<RestoreToRelationRequestContext>> entry : ctx.getRelationsTo().entrySet()) {
                for (RestoreToRelationRequestContext rCtx : entry.getValue()) {

                    if (Objects.isNull(rCtx.changeSet())) {

                        RelationRestoreBatchSet set = new RelationRestoreBatchSet(RelationRestoreBatchSetAccumulator.this);
                        set.setCollectedReferenceTimelines(collectedReferenceTimelines);

                        rCtx.changeSet(set);
                    }

                    rCtx.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, true);
                }
            }

            ctx.setFlag(DataContextFlags.FLAG_BATCH_OPERATION, true);
        }
    }
}
