package org.unidata.mdm.data.type.draft;

import org.unidata.mdm.core.type.calculables.ModificationBox;

/**
 * @author Mikhail Mikhailov on Sep 18, 2020
 */
public class DataDraftConstants {
    /**
     * Constructor.
     */
    private DataDraftConstants() {
        super();
    }
    /**
     * All exisiting periods are marked as this.
     * This allows {@link ModificationBox#toCalculationBase()} and {@link ModificationBox#toModifications()} to work as expected.
     */
    public static final int BASE_DRAFT_REVISION = 1;
    /**
     * New draft periods marked as this (see above).
     */
    public static final int USER_DRAFT_REVISION = 0;
    /////////////////////////////////////////////////////////////////////////////////////////
    // Context params
    /////////////////////////////////////////////////////////////////////////////////////////
    /**
     * The data operation.
     */
    public static final String DRAFT_OPERATION = "DRAFT_OPERATION";
    /////////////////////////////////////////////////////////////////////////////////////////
    // Common properties section
    /////////////////////////////////////////////////////////////////////////////////////////
    /**
     * New record mark.
     */
    public static final String IS_NEW_RECORD = "IS_NEW_RECORD";
    /**
     * Period operation mark (delete, restore, upsert).
     */
    public static final String IS_PERIOD_OPERATION = "IS_PERIOD_OPERATION";
    /**
     * New etalon ID.
     */
    public static final String ETALON_ID = "ETALON_ID";
    /**
     * The name of the entity, being upserted.
     */
    public static final String ENTITY_NAME = "ENTITY_NAME";
    /**
     * The source system.
     */
    public static final String SOURCE_SYSTEM = "SOURCE_SYSTEM";
    /**
     * External id.
     */
    public static final String EXTERNAL_ID = "EXTERNAL_ID";
    /**
     * The shard (needed for verification).
     */
    public static final String SHARD = "SHARD";
    /**
     * The lsn  (needed for verification).
     */
    public static final String LSN = "LSN";
    /**
     * The data operation at the beginning of the draft session.
     */
    public static final String INITIAL_OPERATION = "INITIAL_OPERATION";
    /////////////////////////////////////////////////////////////////////////////////////////
    // Relations sub section
    /////////////////////////////////////////////////////////////////////////////////////////
    /**
     * The name of the relation, being upserted.
     */
    public static final String RELATION_NAME = "RELATION_NAME";
    /**
     * The direction of the relation, being upserted.
     */
    public static final String RELATION_DIRECTION = "RELATION_DIRECTION";
    /**
     * The FROM shard (needed for verification).
     */
    public static final String FROM_SHARD = "FROM_SHARD";
    /**
     * The FROM lsn (needed for verification).
     */
    public static final String FROM_LSN = "FROM_LSN";
    /**
     * From etalon ID.
     */
    public static final String FROM_ETALON_ID = "FROM_ETALON_ID";
    /**
     * The from source system.
     */
    public static final String FROM_SOURCE_SYSTEM = "FROM_SOURCE_SYSTEM";
    /**
     * From external id.
     */
    public static final String FROM_EXTERNAL_ID = "FROM_EXTERNAL_ID";
    /**
     * The TO shard (needed for verification).
     */
    public static final String TO_SHARD = "TO_SHARD";
    /**
     * The TO lsn (needed for verification).
     */
    public static final String TO_LSN = "TO_LSN";
    /**
     * To etalon ID.
     */
    public static final String TO_ETALON_ID = "TO_ETALON_ID";
    /**
     * The to source system.
     */
    public static final String TO_SOURCE_SYSTEM = "TO_SOURCE_SYSTEM";
    /**
     * To external id.
     */
    public static final String TO_EXTERNAL_ID = "TO_EXTERNAL_ID";
    /////////////////////////////////////////////////////////////////////////////////////////
    // Record bundle fields
    /////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Data bundles (period records), added by user will have this set to 'true'.
     */
    public static final String IS_DRAFT_RECORD = "IS_DRAFT_RECORD";
    /**
     * Valid from.
     */
    public static final String VALID_FROM = "VALID_FROM";
    /**
     * Valid to.
     */
    public static final String VALID_TO = "VALID_TO";
    /**
     * Optype.
     */
    public static final String OPERATION_TYPE = "OPERATION_TYPE";
    /**
     * Upsert status.
     */
    public static final String PERIOD_STATUS = "PERIOD_STATUS";
    /**
     * Timestamp.
     */
    public static final String CREATE_TIMESTAMP = "CREATE_TIMESTAMP";
    /**
     * Created by.
     */
    public static final String CREATED_BY = "CREATED_BY";
}
