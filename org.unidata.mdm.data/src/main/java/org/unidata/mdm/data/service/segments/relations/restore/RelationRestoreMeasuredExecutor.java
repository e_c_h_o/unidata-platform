/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.restore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.data.context.RelationRestoreContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.segments.MeasurementMetaSettingSupport;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * Normalize measured attributes before upserting record
 */
@Component(RelationRestoreMeasuredExecutor.SEGMENT_ID)
public class RelationRestoreMeasuredExecutor extends Point<RelationRestoreContext>
        implements MeasurementMetaSettingSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_RESTORE_MEASURED]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.restore.measured.description";
    /**
     * Meta model service
     */
    @Autowired
    private MetaModelService metaModelService;

    public RelationRestoreMeasuredExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    private boolean execute(DataRecord record, String entityName) {
        MeasurementPoint.start();
        try {
            if (record != null) {
                normalize(record, metaModelService.instance(Descriptors.DATA).getElement(entityName));
            }
            return true;
        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService metaModelService() {
        return metaModelService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationRestoreContext ctx) {

        if (!ctx.hasData()) {
            return;
        }

        execute(ctx.getRecord(), ctx.relationName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationRestoreContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
