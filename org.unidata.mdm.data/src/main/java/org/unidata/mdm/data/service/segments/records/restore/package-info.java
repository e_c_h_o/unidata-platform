/**
 * @author Mikhail Mikhailov on Nov 21, 2020
 * Restore operation base points
 */
package org.unidata.mdm.data.service.segments.records.restore;