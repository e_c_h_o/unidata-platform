/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.data.service.segments.records;

import org.springframework.stereotype.Component;
import org.unidata.mdm.data.configuration.DataMessagingDomain;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.context.GetRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.messaging.DataTypes;
import org.unidata.mdm.system.type.annotation.DomainRef;
import org.unidata.mdm.system.type.messaging.DomainInstance;
import org.unidata.mdm.system.type.messaging.Message;
import org.unidata.mdm.system.type.pipeline.Fallback;
import org.unidata.mdm.system.type.pipeline.PipelineInput;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Alexander Malyshev
 */
@Component(RecordAuditNotificationFallback.SEGMENT_ID)
public class RecordAuditNotificationFallback extends Fallback<PipelineInput> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_AUDIT_FALLBACK]";

    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.audit.fallback";

    @DomainRef(DataMessagingDomain.NAME)
    private DomainInstance dataDomainInstance;

    public RecordAuditNotificationFallback() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }

    @Override
    public void accept(PipelineInput ctx, Throwable throwable) {

        if (ctx instanceof UpsertRequestContext) {
            dataDomainInstance.send(new Message(DataTypes.RECORD_UPSERT)
                    .withBody(ctx)
                    .withCause(throwable));
        } else if (ctx instanceof GetRequestContext) {
            dataDomainInstance.send(new Message(DataTypes.RECORD_GET)
                    .withBody(ctx)
                    .withCause(throwable));
        } else if (ctx instanceof DeleteRequestContext) {
            dataDomainInstance.send(new Message(DataTypes.RECORD_DELETE)
                    .withBody(ctx)
                    .withCause(throwable));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        final Class<?> inputTypeClass = start.getInputTypeClass();
        return UpsertRequestContext.class.isAssignableFrom(inputTypeClass)
            || GetRequestContext.class.isAssignableFrom(inputTypeClass)
            || DeleteRequestContext.class.isAssignableFrom(inputTypeClass);
    }
}
