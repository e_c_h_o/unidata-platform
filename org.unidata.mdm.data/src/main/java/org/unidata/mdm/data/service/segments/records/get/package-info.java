/**
 * @author Mikhail Mikhailov on Nov 22, 2020
 * GET records bits.
 */
package org.unidata.mdm.data.service.segments.records.get;