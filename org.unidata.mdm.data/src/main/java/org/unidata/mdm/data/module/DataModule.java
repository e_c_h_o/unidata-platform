/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.module;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.unidata.mdm.data.configuration.DataConfiguration;
import org.unidata.mdm.data.configuration.DataConfigurationConstants;
import org.unidata.mdm.data.configuration.DataConfigurationProperty;
import org.unidata.mdm.data.configuration.DataMessagingDomain;
import org.unidata.mdm.data.context.DistributedMigrationContext;
import org.unidata.mdm.data.convert.DataClusterConverter;
import org.unidata.mdm.data.dao.StorageDAO;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.migration.data.DataMigrations;
import org.unidata.mdm.data.migration.meta.MetaMigrations;
import org.unidata.mdm.data.po.storage.DataClusterPO;
import org.unidata.mdm.data.service.job.fraction.ReindexRelationsJobFraction;
import org.unidata.mdm.data.type.storage.DataCluster;
import org.unidata.mdm.data.util.DataDiffUtils;
import org.unidata.mdm.data.util.RecordFactoryUtils;
import org.unidata.mdm.data.util.StorageUtils;
import org.unidata.mdm.system.context.DatabaseMigrationContext;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.service.DatabaseMigrationService;
import org.unidata.mdm.system.service.PlatformConfiguration;
import org.unidata.mdm.system.type.annotation.ConfigurationRef;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.type.configuration.ConfigurationValue;
import org.unidata.mdm.system.type.job.ModularBatchJobFraction;
import org.unidata.mdm.system.type.messaging.DomainType;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.pipeline.Segment;

public class DataModule extends AbstractModule {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DataModule.class);
    /**
     * Deps.
     */
    private static final Set<Dependency> DEPENDENCIES = Set.of(
            new Dependency("org.unidata.mdm.meta", "6.0"),
            new Dependency("org.unidata.mdm.draft", "6.0"));
    /**
     * This module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.data";

    @ConfigurationRef(DataConfigurationConstants.PROPERTY_DATA_NODES)
    private ConfigurationValue<String> nodes;

    @ConfigurationRef(DataConfigurationConstants.PROPERTY_DATA_SHARDS)
    private ConfigurationValue<Long> shards;

    @Autowired
    private DataConfiguration configuration;

    @Autowired
    @Qualifier("storageDataSource")
    private DataSource storageDataSource;
    /**
     * Storage metadata DAO.
     */
    @Autowired
    private StorageDAO dataStorageDAO;

    @Autowired
    private DatabaseMigrationService migrationService;

    @Autowired
    private PlatformConfiguration platformConfiguration;

    @Autowired
    private ReindexRelationsJobFraction reindexRelationsJobFraction;

    private boolean install;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.0";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Data";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Unidata Data module";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{ "data_messages" };
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty<?>[] getConfigurationProperties() {
        return DataConfigurationProperty.values();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DomainType[] getMessagingDomains() {
        return new DomainType[] { DataMessagingDomain.DOMAIN };
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ModularBatchJobFraction> getBatchJobFractions() {
        return List.of(reindexRelationsJobFraction);
    }
    /**
     * Initialization logic is quite complicated:
     * - schema may not exist
     * - cluster metadata may not exist
     * {@inheritDoc}
     */
    @Override
    public void install() {

        LOGGER.info("Install");

        // Install/Migrate storage schemas.
        migrate();
        install = true;
    }

    @Override
    public void uninstall() {
        LOGGER.info("Uninstall");
        // TODO: UN-11830 Uninstall schema
    }

    @Override
    public void start() {

        LOGGER.info("Starting...");

        // 1. Static utils
        StorageUtils.init();
        RecordFactoryUtils.init();
        DataDiffUtils.init();

        if (platformConfiguration.isDeveloperMode() && !install) {
            migrate();
        }

        // 2. Add segments
        addSegments(configuration.getBeansOfType(Segment.class).values());

        LOGGER.info("Started.");
    }

    @Override
    public void stop() {

        LOGGER.info("Stopping...");
        dataStorageDAO.shutdown();
        LOGGER.info("Stopped.");
    }

    private void migrate() {

        // 1. Storage metadata
        migrationService.migrate(DatabaseMigrationContext.builder()
                .schemaName(DataConfigurationConstants.DATA_STORAGE_SCHEMA_NAME)
                .logName(DataConfigurationConstants.META_LOG_NAME)
                .dataSource(storageDataSource)
                .migrations(MetaMigrations.migrations())
                .build());

        // 2. Load-or-default cluster info. Stuff is temporary.
        DataClusterPO clusterPO = dataStorageDAO.load();
        if (clusterPO == null) {

            if (!shards.hasValue() || !nodes.hasValue()) {
                throw new PlatformFailureException(
                        "Data storage configuration not found in both DB and system properties.",
                        DataExceptionIds.EX_DATA_STORAGE_NOT_CONFIGURED);
            }

            clusterPO = DataClusterConverter.of(shards.getValue().intValue(), StringUtils.split(nodes.getValue(), ','));
            clusterPO.setVersion(1); // Avoid reload of spec on the very first run

            dataStorageDAO.save(clusterPO);
        }

        dataStorageDAO.configure(clusterPO);

        // 3. Install/Migrate data schema.
        DataCluster tempCluster = DataClusterConverter.of(clusterPO);
        List<DatabaseMigrationContext> migrations = Arrays.stream(tempCluster.getNodes())
                .map(node -> DistributedMigrationContext.distributed()
                        .cluster(tempCluster)
                        .node(node)
                        .schemaName(DataConfigurationConstants.DATA_STORAGE_SCHEMA_NAME)
                        .logName(DataConfigurationConstants.DATA_LOG_NAME)
                        .migrations(DataMigrations.migrations())
                        .dataSource(dataStorageDAO.nodeSelect(node.getNumber()).dataSource())
                        .build())
                .collect(Collectors.toList());

        migrationService.migrate(migrations);
    }
}
