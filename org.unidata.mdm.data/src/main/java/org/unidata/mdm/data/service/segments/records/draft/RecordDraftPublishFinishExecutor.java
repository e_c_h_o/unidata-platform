/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records.draft;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.timeline.MutableTimeInterval;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.type.timeline.impl.RevisionSlider;
import org.unidata.mdm.data.context.DeleteRequestContext;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.context.UpsertRequestContext.UpsertRecordHint;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.DataRecordsService;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.segments.RecordDraftTimelineSupport;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DataDraftOperation;
import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.exception.DraftProcessingException;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.support.IdentityHashSet;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftPublishFinishExecutor.SEGMENT_ID)
public class RecordDraftPublishFinishExecutor extends Finish<DraftPublishContext, DraftPublishResult> implements RecordDraftTimelineSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_PUBLISH_FINISH]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.publish.finish.description";
    /**
     * PC.
     */
    @Autowired
    private DataRecordsService dataRecordsService;
    /**
     * CRC.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;
    /**
     * Constructor.
     */
    public RecordDraftPublishFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftPublishResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPublishResult finish(DraftPublishContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();

        DraftPublishResult result = new DraftPublishResult(publish(draft, edition));
        result.setDraft(draft);

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftPublishContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * Does publish periods to persistent storage.
     * @param draft the context
     * @param edition current edition
     * @return true, if successful
     */
    private boolean publish(Draft draft, Edition edition) {

        DataDraftOperation operation = draft.getVariables().valueGet(DataDraftConstants.INITIAL_OPERATION, DataDraftOperation.class);
        Objects.requireNonNull(operation, "Initial operation must not be null!");

        Set<CalculableHolder<OriginRecord>> supply
            = new TreeSet<>(RevisionSlider.POP_TOP_COMPARATOR);

        Timeline<OriginRecord> timeline = timeline(draft, edition);
        RecordKeys keys = timeline.getKeys();

        supply.addAll(timeline.stream()
            .map(TimeInterval::unlock)
            .map(MutableTimeInterval::toModifications)
            .map(Map::values)
            .flatMap(Collection::stream)
            .flatMap(Collection::stream)
            .collect(Collectors.toCollection(IdentityHashSet::new)));

        switch (operation) {
        case DELETE:
            publishDelete(draft, keys, supply);
            break;
        case RESTORE:
            publishRestore(draft, keys, supply);
            break;
        case UPSERT:
            publishUpsert(draft, keys, supply);
            break;
        default:
            break;
        }

        return true;
    }

    private void publishUpsert(Draft draft, RecordKeys keys, Set<CalculableHolder<OriginRecord>> holders) {

        boolean isNewRecord = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_NEW_RECORD);

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRecord>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRecord> ch = it.next();
            UpsertRequestContext iCtx = UpsertRequestContext.builder()
                    .record(ch.getValue())
                    .etalonKey(isNewRecord ? null : keys.getEtalonKey().getId())
                    .sourceSystem(ch.getSourceSystem())
                    .externalId(ch.getExternalId())
                    .entityName(ch.getTypeName())
                    .validFrom(ch.getValidFrom())
                    .validTo(ch.getValidTo())
                    .applyDraft(true)
                    .hint(UpsertRecordHint.HINT_ETALON_ID, keys.getEtalonKey().getId())
                    .build();

            dataRecordsService.upsertRecord(iCtx);
        }
    }

    private void publishDelete(Draft draft, RecordKeys keys, Collection<CalculableHolder<OriginRecord>> holders) {

        boolean isPeriodOperation = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_PERIOD_OPERATION);

        if (isPeriodOperation) {
            ensureSingleOrEmptyPeriodUpdate(holders);
        }

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRecord>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRecord> ch = it.next();
            // Period delete
            if (isPeriodOperation) {

                DeleteRequestContext dCtx = DeleteRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .sourceSystem(ch.getSourceSystem())
                        .externalId(ch.getExternalId())
                        .entityName(ch.getTypeName())
                        .inactivatePeriod(true)
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .build();

                dataRecordsService.deleteRecord(dCtx);
            // Record delete + several editions
            } else {

                UpsertRequestContext iCtx = UpsertRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .sourceSystem(ch.getSourceSystem())
                        .externalId(ch.getExternalId())
                        .entityName(ch.getTypeName())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .applyDraft(true)
                        .build();

                dataRecordsService.upsertRecord(iCtx);
            }
        }

        // Final deactication for the record
        if (!isPeriodOperation) {

            DeleteRequestContext dCtx = DeleteRequestContext.builder()
                    .etalonKey(keys.getEtalonKey().getId())
                    .inactivateEtalon(true)
                    .build();

            dataRecordsService.deleteRecord(dCtx);
        }
    }

    private void publishRestore(Draft draft, RecordKeys keys, Set<CalculableHolder<OriginRecord>> holders) {

        boolean isPeriodOperation = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_PERIOD_OPERATION);

        if (isPeriodOperation) {
            ensureSingleOrEmptyPeriodUpdate(holders);
        }

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRecord>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRecord> ch = it.next();
            RestoreRecordRequestContext rCtx = RestoreRecordRequestContext.builder()
                    .record(ch.getValue())
                    .etalonKey(keys.getEtalonKey().getId())
                    .sourceSystem(ch.getSourceSystem())
                    .externalId(ch.getExternalId())
                    .entityName(ch.getTypeName())
                    .validFrom(ch.getValidFrom())
                    .validTo(ch.getValidTo())
                    .periodRestore(isPeriodOperation)
                    .applyDraft(!isPeriodOperation)
                    .build();

            dataRecordsService.restore(rCtx);
        }

        // Restore record with validation
        if (!isPeriodOperation) {

            RestoreRecordRequestContext rCtx = RestoreRecordRequestContext.builder()
                    .etalonKey(keys.getEtalonKey().getId())
                    .build();

            dataRecordsService.restore(rCtx);
        }
    }

    private void ensureSingleOrEmptyPeriodUpdate(Collection<CalculableHolder<OriginRecord>> update) {
        if (update.size() > 1) {
            throw new DraftProcessingException("Period draft operation produced more then one update record!",
                    DataExceptionIds.EX_DATA_RECORD_DRAFT_PERIOD_OPERATION_MORE_THEN_ONE_RECORD);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRecordsComponent getCommonRecordsComponent() {
        return commonRecordsComponent;
    }
}
