/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.records.draft;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.context.ValidityRangeContext;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.core.type.formless.DataBundle;
import org.unidata.mdm.core.type.timeline.MutableTimeInterval;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.data.context.ReadWriteTimelineContext;
import org.unidata.mdm.data.context.UpsertRequestContext;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.impl.CommonRecordsComponent;
import org.unidata.mdm.data.service.impl.RecordComposerComponent;
import org.unidata.mdm.data.service.segments.RecordDraftTimelineSupport;
import org.unidata.mdm.data.service.segments.ValidityRangeCheckSupport;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.EtalonRecordInfoSection;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.exception.DraftProcessingException;
import org.unidata.mdm.draft.service.DraftService;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.system.type.pipeline.Point;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.support.IdentityHashSet;
import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Alexey Tsarapkin
 */
@Component(RecordDraftUpsertProcessExecutor.SEGMENT_ID)
public class RecordDraftUpsertProcessExecutor extends Point<DraftUpsertContext>
    implements ValidityRangeCheckSupport<UpsertRequestContext>, RecordDraftTimelineSupport {
    /**
     * This segment id.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_DRAFT_UPSERT_PROCESS]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.draft.upsert.process.description";
    /**
     * The draft service.
     */
    @Autowired
    private DraftService draftService;
    /**
     * Common records component.
     */
    @Autowired
    private CommonRecordsComponent commonRecordsComponent;
    /**
     * The RCC.
     */
    @Autowired
    private RecordComposerComponent recordComposerComponent;
    /**
     * Constructor.
     */
    public RecordDraftUpsertProcessExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(DraftUpsertContext ctx) {

        // 1. The stuff runs only, if some data were supplied
        if (!ctx.hasPayload()) {
            return;
        }

        Draft draft = ctx.currentDraft();

        // 2. Load last edition
        Edition current = null;
        if (draft.isExisting()) {
            current = draftService.current(draft.getDraftId(), true);
        }

        // 3. Take data snapshot for new drafts, if no editions exist
        BundlesArray bundles = null;
        if (Objects.isNull(current)) {
            bundles = handleBase(draft, ctx);
        } else {
            bundles = current.getContent();
        }

        // 4. Create bundles array, if needed (no editions and the record is NEW)
        if (Objects.isNull(bundles)) {
            bundles = new BundlesArray();
        }

        // 5. Check input
        checkInput(draft, ctx);

        // 6. Process user input and add the payload to collection
        bundles.addAll(handleInput(ctx));

        Edition next = new Edition();
        next.setContent(bundles);
        next.setCreateDate(new Date(System.currentTimeMillis()));
        next.setCreatedBy(SecurityUtils.getCurrentUserName());

        // 6. Cleanse the bundle - remove invisible periods and save it to edition
        cleanseBundles(draft, next);

        ctx.currentEdition(next);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRecordsComponent getCommonRecordsComponent() {
        return commonRecordsComponent;
    }

    protected BundlesArray handleBase(Draft draft, DraftUpsertContext ctx) {

        boolean isNew = draft.getVariables().valueGet(DataDraftConstants.IS_NEW_RECORD);
        if (isNew) {
            return null;
        }

        ReadWriteTimelineContext<OriginRecord> original = ctx.getPayload();

        // The TL is expected to be calculated by the parent pipeline
        Timeline<OriginRecord> timeline = original.currentTimeline();
        if (timeline.isEmpty()) {
            return null;
        }

        BundlesArray result = new BundlesArray();
        for (TimeInterval<OriginRecord> i : timeline) {

            if (!i.isActive()) {
                continue;
            }

            EtalonRecord etalon = i.getCalculationResult();
            if (Objects.nonNull(etalon)) {

                EtalonRecordInfoSection is = etalon.getInfoSection();
                Instant from = is.getValidFrom() == null ? null : is.getValidFrom().toInstant();
                Instant to = is.getValidTo() == null ? null : is.getValidTo().toInstant();

                result.add(new DataBundle()
                        .withRecord(etalon)
                        .withVariables(new Variables()
                                .add(DataDraftConstants.VALID_FROM, from)
                                .add(DataDraftConstants.VALID_TO, to)
                                .add(DataDraftConstants.IS_DRAFT_RECORD, Boolean.FALSE)
                                .add(DataDraftConstants.OPERATION_TYPE, is.getOperationType())
                                .add(DataDraftConstants.PERIOD_STATUS, is.getStatus())
                                .add(DataDraftConstants.CREATE_TIMESTAMP, Instant.EPOCH)
                                .add(DataDraftConstants.CREATED_BY, is.getCreatedBy())));
            }
        }

        return result;
    }

    protected void checkInput(Draft draft, DraftUpsertContext ctx) {

        // No editions so far, nothing to verify
        if (!draft.hasEditions()) {
            return;
        }

        boolean isPeriodOperation = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_PERIOD_OPERATION);

        if (isPeriodOperation) {

            ValidityRangeContext vCtx = ctx.getPayload();

            Instant from = draft
                    .getVariables()
                    .valueGet(DataDraftConstants.VALID_FROM);

            Instant to = draft
                    .getVariables()
                    .valueGet(DataDraftConstants.VALID_TO);

            if (!vCtx.isSameRange(
                    from == null ? null : new Date(from.toEpochMilli()),
                    to == null ? null : new Date(to.toEpochMilli()))) {

                throw new DraftProcessingException("Period boundaries are different, compared to initial operation state.",
                        DataExceptionIds.EX_DATA_RECORD_DRAFT_PERIOD_OPERATION_DATE_RANGE);
            }
        }
    }

    protected List<DataBundle> handleInput(DraftUpsertContext ctx) {

        List<DataBundle> bundles = new ArrayList<>();
        ReadWriteTimelineContext<OriginRecord> source = ctx.getPayload();
        ValidityRangeContext vrc = ctx.getPayload();
        Timeline<OriginRecord> next = source.nextTimeline()
                .reduceBy(vrc.getValidFrom(), vrc.getValidTo());

        for (TimeInterval<OriginRecord> i : next) {

            MutableTimeInterval<OriginRecord> mti = i.unlock();

            // IM-788 Do not re-write pre-calculated etalon result,
            // already processed by EtalonAttributesProcessingAfterExecutor.
            if (Objects.isNull(mti.getCalculationResult())) {
                recordComposerComponent.toEtalon(next.getKeys(), mti);
            }

            EtalonRecord etalon = mti.getCalculationResult();
            if (Objects.nonNull(etalon)) {

                EtalonRecordInfoSection is = etalon.getInfoSection();
                Instant from = i.getValidFrom() == null ? null : i.getValidFrom().toInstant();
                Instant to = i.getValidTo() == null ? null : i.getValidTo().toInstant();

                bundles.add(new DataBundle()
                        .withRecord(etalon)
                        .withVariables(new Variables()
                                .add(DataDraftConstants.VALID_FROM, from)
                                .add(DataDraftConstants.VALID_TO, to)
                                .add(DataDraftConstants.IS_DRAFT_RECORD, Boolean.TRUE)
                                .add(DataDraftConstants.OPERATION_TYPE, is.getOperationType())
                                .add(DataDraftConstants.PERIOD_STATUS, is.getStatus())
                                .add(DataDraftConstants.CREATE_TIMESTAMP, Instant.now())
                                .add(DataDraftConstants.CREATED_BY, SecurityUtils.getCurrentUserName())));
            }
        }

        return bundles;
    }

    protected void cleanseBundles(Draft draft, Edition edition) {

        BundlesArray bundles = edition.getContent();
        if (Objects.isNull(bundles) || bundles.size() <= 1) {
            return;
        }

        Timeline<OriginRecord> timeline = timeline(draft, edition);
        Set<CalculableHolder<OriginRecord>> unique = timeline.stream()
            .map(TimeInterval::unlock)
            .map(MutableTimeInterval::toCalculables)
            .flatMap(Collection::stream)
            .filter(CalculableHolder::isModification)
            .collect(Collectors.toCollection(IdentityHashSet::new));

        ListIterator<DataBundle> li = bundles.listIterator();
        while (li.hasNext()) {

            DataBundle dataBundle = li.next();
            Instant ts = dataBundle.getVariables().valueGet(DataDraftConstants.CREATE_TIMESTAMP);
            Date timestamp = new Date(ts.toEpochMilli());

            boolean invisible = unique.stream().noneMatch(ch -> timestamp.getTime() == ch.getLastUpdate().getTime());
            if (invisible) {
                li.remove();
            }
        }
    }
}
