package org.unidata.mdm.data.dto;

import org.unidata.mdm.core.dto.ResourceSpecificRightDTO;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.draft.type.DraftPayloadResponse;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 */
public class UpsertDraftPeriodResponse implements DraftPayloadResponse {
    /**
     * The record.
     */
    private EtalonRecord record;
    /**
     * Rights.
     */
    private ResourceSpecificRightDTO rights;
    /**
     * Constructor.
     */
    public UpsertDraftPeriodResponse() {
        super();
    }
    /**
     * @return the record
     */
    public EtalonRecord getRecord() {
        return record;
    }
    /**
     * @param record the record to set
     */
    public void setRecord(EtalonRecord record) {
        this.record = record;
    }
    /**
     * @return the rights
     */
    public ResourceSpecificRightDTO getRights() {
        return rights;
    }
    /**
     * @param rights the rights to set
     */
    public void setRights(ResourceSpecificRightDTO rights) {
        this.rights = rights;
    }
}
