/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.records.restore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.RestoreRecordRequestContext;
import org.unidata.mdm.data.dto.RestoreRecordDTO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.data.OriginRecord;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Nov 8, 2019
 */
@Component(RecordRestoreFinishExecutor.SEGMENT_ID)
public class RecordRestoreFinishExecutor extends Finish<RestoreRecordRequestContext, RestoreRecordDTO> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RECORD_RESTORE_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".record.restore.finish.description";
    /**
     * Constructor.
     * @param id
     * @param description
     * @param outputTypeClass
     */
    public RecordRestoreFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, RestoreRecordDTO.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RestoreRecordDTO finish(RestoreRecordRequestContext ctx) {

        // Content from RSCI.
        MeasurementPoint.start();
        try {

            Timeline<OriginRecord> next = ctx.nextTimeline();
            EtalonRecord etalon = null;
            List<EtalonRecord> periods = new ArrayList<>(next.size());

            // Select point for traditional single "etalon" return value
            // null (now) or forDate will be taken for the whole record
            Date point = null;
            if (ctx.isPeriodRestore()) {
                point = ctx.getValidFrom() == null ? ctx.getValidTo() : ctx.getValidFrom();
            } else {
                point = ctx.getForDate() == null ? new Date() : ctx.getForDate();
            }

            for (TimeInterval<OriginRecord> interval : next) {

                if (Objects.isNull(etalon) && interval.isInRange(point)) {
                    etalon = interval.getCalculationResult();
                }

                periods.add(interval.getCalculationResult());
            }

            RestoreRecordDTO result = new RestoreRecordDTO();
            result.setRecordKeys(ctx.keys());
            result.setRights(ctx.accessRight());
            result.setEtalon(etalon);
            result.setPeriods(periods);

            return result;

        } finally {
            MeasurementPoint.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RestoreRecordRequestContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
