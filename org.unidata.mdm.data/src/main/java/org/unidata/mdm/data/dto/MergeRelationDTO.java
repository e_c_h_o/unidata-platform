/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.dto;


import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.data.type.keys.RecordKeys;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;

/**
 * @author Mikhail Mikhailov
 * Merge a single particular relation.
 */
public class MergeRelationDTO implements RelationDTO, PipelineOutput {
    /**
     * Operation successful, winner id is set.
     */
    private final RecordKeys winnerKeys;
    /**
     * The key of the merged relation.
     */
    private final RelationKeys relationKeys;
    /**
     * A possibly existing errors.
     */
    private List<ErrorInfoDTO> errors;
    /**
     * If there were conflicts during merge.
     */
    private boolean mergeWithConflicts;
    /**
     * Constructor.
     */
    public MergeRelationDTO(RecordKeys winnerKeys, RelationKeys mergedKeys) {
        super();
        this.winnerKeys = winnerKeys;
        this.relationKeys = mergedKeys;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public RelationKeys getRelationKeys() {
        return relationKeys;
    }
    /**
     * @return the winnerId
     */
    public RecordKeys getWinnerKeys() {
        return winnerKeys;
    }
    /**
     * Gets the list of errors.
     * @return list of errors.
     */
    public List<ErrorInfoDTO> getErrors() {
        return Objects.isNull(errors) ? Collections.emptyList() : errors;
    }
    /**
     * Sets list of errors.
     * @param errors the errors
     */
    public void setErrors(List<ErrorInfoDTO> errors) {
        this.errors = errors;
    }
    /**
     * Gets conflicts state.
     * @return true, if there were conflicts, false otherwise
     */
    public boolean isMergeWithConflicts() {
        return mergeWithConflicts;
    }
    /**
     * Sets conflicts state.
     * @param mergeWithConflicts true, if there were conflicts, false otherwise
     */
    public void setMergeWithConflicts(boolean mergeWithConflicts) {
        this.mergeWithConflicts = mergeWithConflicts;
    }
}
