/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.relations.draft;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.draft.dto.DraftUpsertResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Alexey Tsarapkin
 */
@Component(RelationDraftUpsertFinishExecutor.SEGMENT_ID)
public class RelationDraftUpsertFinishExecutor extends Finish<DraftUpsertContext, DraftUpsertResult> {

    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_DRAFT_UPSERT_FINISH]";

    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.draft.upsert.finish.description";

    public RelationDraftUpsertFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftUpsertResult finish(DraftUpsertContext ctx) {

        Draft draft = ctx.currentDraft();
        DraftUpsertResult result = new DraftUpsertResult(true);

        boolean firstPayload = !draft.hasEditions() && ctx.hasPayload();
        boolean setup = !draft.isExisting() || firstPayload;

        // First run
        if (setup) {

            result.setVariables(draft.getVariables());

            // Support update of subject id for new records,
            // created with this draft (association phase)
            String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
            if (StringUtils.isBlank(draft.getSubjectId()) && StringUtils.isNotBlank(etalonId)) {
                result.setSubjectId(etalonId);
            }
        }

        // Regular save runs
        result.setEdition(ctx.currentEdition());
        result.setDraft(draft);

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftUpsertContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
