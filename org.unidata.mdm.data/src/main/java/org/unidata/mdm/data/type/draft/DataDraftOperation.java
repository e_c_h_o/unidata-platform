package org.unidata.mdm.data.type.draft;

/**
 * @author Mikhail Mikhailov on Sep 24, 2020
 * Operations, that are supported by the data module via draft UPSERT call.
 */
public enum DataDraftOperation {
    /**
     * Upsert period data (new or existing record).
     */
    UPSERT,
    /**
     * Re-activate particular period or a whole record.
     */
    RESTORE,
    /**
     * Inactivate particular period or a whole record.
     */
    DELETE
}
