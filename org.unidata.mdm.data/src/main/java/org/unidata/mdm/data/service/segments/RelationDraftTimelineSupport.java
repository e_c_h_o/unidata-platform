package org.unidata.mdm.data.service.segments;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.data.DataShift;
import org.unidata.mdm.core.type.data.OperationType;
import org.unidata.mdm.core.type.data.RecordStatus;
import org.unidata.mdm.core.type.formless.BundlesArray;
import org.unidata.mdm.core.type.formless.DataBundle;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.data.context.GetRelationTimelineRequestContext;
import org.unidata.mdm.data.service.impl.CommonRelationsComponent;
import org.unidata.mdm.data.type.calculables.impl.RelationRecordHolder;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.data.OriginRelationInfoSection;
import org.unidata.mdm.data.type.data.RelationType;
import org.unidata.mdm.data.type.data.impl.OriginRelationImpl;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.keys.RecordEtalonKey;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.data.type.keys.RelationEtalonKey;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.data.type.keys.RelationOriginKey;
import org.unidata.mdm.data.type.timeline.RelationTimeline;
import org.unidata.mdm.data.util.StorageUtils;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.MetaModelService;

/**
 * @author Mikhail Mikhailov on Sep 26, 2020
 * Draft timeline and keys generation support.
 */
public interface RelationDraftTimelineSupport {
    /**
     * Gets common component, needed for key fetch.
     * @return component
     */
    CommonRelationsComponent getCommonRelationsComponent();
    /**
     * Gets meta model service.
     * @return the service
     */
    MetaModelService getMetaModelService();
    /**
     * Generates timeline from record and edition draft.
     * @param draft the draft
     * @param edition the edition
     * @return timeline
     */
    default Timeline<OriginRelation> timeline(Draft draft, Edition edition) {

        // Just for case
        if (Objects.isNull(edition)) {
            return null;
        }

        boolean isNew = draft.getVariables().valueGet(DataDraftConstants.IS_NEW_RECORD);
        RelationKeys keys = isNew ? generate(draft, edition) : load(draft);

        BundlesArray bundles = edition.getContent();
        List<CalculableHolder<OriginRelation>> calculables = bundles.stream()
            .map(b -> origin(b, edition, keys))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        return new RelationTimeline(keys, calculables);
    }
    /**
     * Generates a calculable holder from a bundle.
     * @param bundle the bundle
     * @param edition the edition
     * @param ok the keys
     * @return holder
     */
    default CalculableHolder<OriginRelation> origin(DataBundle bundle, Edition edition, RelationKeys keys) {

        if (Objects.isNull(bundle)) {
            return null;
        }

        boolean isDraft = bundle.getVariables().valueGet(DataDraftConstants.IS_DRAFT_RECORD);
        Instant from = bundle.getVariables().valueGet(DataDraftConstants.VALID_FROM);
        Instant to = bundle.getVariables().valueGet(DataDraftConstants.VALID_TO);
        Instant ts = bundle.getVariables().valueGet(DataDraftConstants.CREATE_TIMESTAMP);
        OperationType type = bundle.getVariables().valueGet(DataDraftConstants.OPERATION_TYPE, OperationType.class);
        RecordStatus status = bundle.getVariables().valueGet(DataDraftConstants.PERIOD_STATUS, RecordStatus.class);

        Date timestamp = new Date(ts.toEpochMilli());

        OriginRelationImpl origin = new OriginRelationImpl()
            .withDataRecord(bundle.getRecord())
            .withInfoSection(new OriginRelationInfoSection()
                    .withRelationName(keys.getRelationName())
                    .withRelationType(keys.getRelationType())
                    .withValidFrom(from == null ? null : new Date(from.toEpochMilli()))
                    .withValidTo(to == null ? null : new Date(to.toEpochMilli()))
                    .withFromEntityName(keys.getFromEntityName())
                    .withToEntityName(keys.getToEntityName())
                    .withStatus(status)
                    .withShift(DataShift.PRISTINE)
                    .withOperationType(type)
                    .withRelationOriginKey(keys.getOriginKey())
                    .withCreateDate(timestamp)
                    .withUpdateDate(timestamp)
                    .withCreatedBy(edition.getCreatedBy())
                    .withUpdatedBy(edition.getCreatedBy())
                    .withRevision(isDraft ? DataDraftConstants.USER_DRAFT_REVISION : DataDraftConstants.BASE_DRAFT_REVISION)
                    .withMajor(0)
                    .withMinor(0));

        return new RelationRecordHolder(origin);
    }
    /**
     * Loads exisitng record's keys using draft object.
     * @param draft the draft object
     * @return keys
     */
    default RelationKeys load(Draft draft) {
        String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
        return getCommonRelationsComponent().identify(GetRelationTimelineRequestContext.builder()
                .relationEtalonKey(etalonId)
                .build());
    }
    /**
     * Generates phantom keys for a new record using draft object.
     * @param draft the draft object
     * @param edition the edition
     * @return keys
     */
    default RelationKeys generate(Draft draft, Edition edition) {

        String etalonId = draft.getVariables().valueGet(DataDraftConstants.ETALON_ID);
        String relationName = draft.getVariables().valueGet(DataDraftConstants.RELATION_NAME);
        RelationElement re = getMetaModelService().instance(Descriptors.DATA).getRelation(relationName);

        RelationOriginKey rok = RelationOriginKey.builder()
                .id(StringUtils.EMPTY)
                .initialOwner(UUID.fromString(etalonId))
                .sourceSystem(draft.getVariables().valueGet(DataDraftConstants.SOURCE_SYSTEM))
                .enrichment(false)
                .revision(0)
                .status(RecordStatus.ACTIVE)
                .from(RecordOriginKey.builder()
                        .createDate(draft.getCreateDate())
                        .createdBy(draft.getCreatedBy())
                        .updateDate(edition.getCreateDate())
                        .updatedBy(edition.getCreatedBy())
                        .enrichment(false)
                        .entityName(re.getLeft().getName())
                        .externalId(draft.getVariables().valueGet(DataDraftConstants.FROM_EXTERNAL_ID))
                        .sourceSystem(draft.getVariables().valueGet(DataDraftConstants.FROM_SOURCE_SYSTEM))
                        .id(StringUtils.EMPTY)
                        .initialOwner(UUID.fromString(draft.getVariables().valueGet(DataDraftConstants.FROM_ETALON_ID)))
                        .revision(0)
                        .build())
                .to(RecordOriginKey.builder()
                        .createDate(draft.getCreateDate())
                        .createdBy(draft.getCreatedBy())
                        .updateDate(edition.getCreateDate())
                        .updatedBy(edition.getCreatedBy())
                        .enrichment(false)
                        .entityName(re.getRight().getName())
                        .externalId(draft.getVariables().valueGet(DataDraftConstants.TO_EXTERNAL_ID))
                        .sourceSystem(draft.getVariables().valueGet(DataDraftConstants.TO_SOURCE_SYSTEM))
                        .id(StringUtils.EMPTY)
                        .initialOwner(UUID.fromString(draft.getVariables().valueGet(DataDraftConstants.TO_ETALON_ID)))
                        .revision(0)
                        .build())
                .createDate(draft.getCreateDate())
                .createdBy(draft.getCreatedBy())
                .updateDate(edition.getCreateDate())
                .updatedBy(edition.getCreatedBy())
                .build();

        int shard = draft.getVariables().valueGet(DataDraftConstants.SHARD);
        long lsn = draft.getVariables().valueGet(DataDraftConstants.LSN);

        return RelationKeys.builder()
            .etalonKey(RelationEtalonKey.builder()
                    .id(etalonId)
                    .lsn(lsn)
                    .status(RecordStatus.ACTIVE)
                    .from(RecordEtalonKey.builder()
                            .id(draft.getVariables().valueGet(DataDraftConstants.FROM_ETALON_ID))
                            .lsn(draft.getVariables().valueGet(DataDraftConstants.FROM_LSN))
                            .status(RecordStatus.ACTIVE)
                            .build())
                    .to(RecordEtalonKey.builder()
                            .id(draft.getVariables().valueGet(DataDraftConstants.TO_ETALON_ID))
                            .lsn(draft.getVariables().valueGet(DataDraftConstants.TO_LSN))
                            .status(RecordStatus.ACTIVE)
                            .build())
                    .build())
            .originKey(rok)
            .supplementaryKeys(Collections.singletonList(rok))
            .published(false)
            .shard(shard)
            .node(StorageUtils.node(shard))
            .fromEntityName(re.getLeft().getName())
            .toEntityName(re.getRight().getName())
            .relationName(re.getName())
            .relationType(RelationType.fromModel(re))
            .createDate(draft.getCreateDate())
            .createdBy(draft.getCreatedBy())
            .updateDate(edition.getCreateDate())
            .updatedBy(edition.getCreatedBy())
            .build();
    }
}
