package org.unidata.mdm.data.context;

import java.util.Date;

import org.unidata.mdm.core.context.DataRecordContext;
import org.unidata.mdm.core.context.MutableValidityRangeContext;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.system.context.DraftAwareContext;
import org.unidata.mdm.system.context.SetupAwareContext;

/**
 * @author Mikhail Mikhailov on Jun 9, 2020
 */
public interface RelationRestoreContext
    extends
        RelationIdentityContext,
        ReadWriteTimelineContext<OriginRelation>,
        ReadWriteDataContext<OriginRelation>,
        ContainmentRelationContext<RestoreRecordRequestContext>,
        MutableValidityRangeContext,
        OperationTypeContext,
        SetupAwareContext,
        AccessRightContext,
        BatchAwareContext,
        DraftAwareContext,
        DataRecordContext {
    /**
     * @return define that is period restore request.
     */
    default boolean isPeriodRestore() {
        return getFlag(DataContextFlags.FLAG_IS_PERIOD_RESTORE);
    }
    /**
     * @return the forDate
     */
    Date getForDate();
    /**
     * @return the lastUpdate
     */
    Date getLastUpdate();
}
