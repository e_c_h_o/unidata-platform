/*
 *
 *  * Unidata Platform
 *  * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *  *
 *  * Commercial License
 *  * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *  *
 *  * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 *  * For clarification or additional options, please contact: info@unidata-platform.com
 *  * -------
 *  * Disclaimer:
 *  * -------
 *  * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 *  * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 *  * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 *  * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 *
 */

package org.unidata.mdm.data.service.segments.relations.draft;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.calculables.CalculableHolder;
import org.unidata.mdm.core.type.timeline.MutableTimeInterval;
import org.unidata.mdm.core.type.timeline.TimeInterval;
import org.unidata.mdm.core.type.timeline.Timeline;
import org.unidata.mdm.core.type.timeline.impl.RevisionSlider;
import org.unidata.mdm.data.context.DeleteRelationRequestContext;
import org.unidata.mdm.data.context.RestoreFromRelationRequestContext;
import org.unidata.mdm.data.context.RestoreToRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext;
import org.unidata.mdm.data.context.UpsertRelationRequestContext.UpsertRelationHint;
import org.unidata.mdm.data.context.UpsertRelationsRequestContext;
import org.unidata.mdm.data.exception.DataExceptionIds;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.DataRelationsService;
import org.unidata.mdm.data.service.impl.CommonRelationsComponent;
import org.unidata.mdm.data.service.segments.RelationDraftTimelineSupport;
import org.unidata.mdm.data.type.data.OriginRelation;
import org.unidata.mdm.data.type.draft.DataDraftConstants;
import org.unidata.mdm.data.type.draft.DataDraftOperation;
import org.unidata.mdm.data.type.keys.RelationKeys;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.exception.DraftProcessingException;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.RelativeDirection;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.support.IdentityHashSet;

/**
 * @author Alexey Tsarapkin
 */
@Component(RelationDraftPublishFinishExecutor.SEGMENT_ID)
public class RelationDraftPublishFinishExecutor extends Finish<DraftPublishContext, DraftPublishResult>
    implements RelationDraftTimelineSupport {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[RELATION_DRAFT_PUBLISH_FINISH]";
    /**
     * This segment description.
     */
    private static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".relation.draft.publish.finish.description";
    /**
     * PC.
     */
    @Autowired
    private DataRelationsService dataRelationsService;
    /**
     * CRC.
     */
    @Autowired
    private CommonRelationsComponent commonRelationsComponent;
    /**
     * MMS.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * Constructor.
     */
    public RelationDraftPublishFinishExecutor(){
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftPublishResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftPublishResult finish(DraftPublishContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition edition = ctx.currentEdition();

        DraftPublishResult result = new DraftPublishResult(publish(draft, edition));
        result.setDraft(draft);

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftPublishContext.class.isAssignableFrom(start.getInputTypeClass());
    }
    /**
     * Does publish periods to persistent storage.
     * @param draft the context
     * @param edition current edition
     * @return true, if successful
     */
    private boolean publish(Draft draft, Edition edition) {

        DataDraftOperation operation = draft.getVariables().valueGet(DataDraftConstants.INITIAL_OPERATION, DataDraftOperation.class);
        Objects.requireNonNull(operation, "Initial operation must not be null!");

        Set<CalculableHolder<OriginRelation>> supply
            = new TreeSet<>(RevisionSlider.POP_TOP_COMPARATOR);

        Timeline<OriginRelation> timeline = timeline(draft, edition);
        RelationKeys keys = timeline.getKeys();

        supply.addAll(timeline.stream()
            .map(TimeInterval::unlock)
            .map(MutableTimeInterval::toModifications)
            .map(Map::values)
            .flatMap(Collection::stream)
            .flatMap(Collection::stream)
            .collect(Collectors.toCollection(IdentityHashSet::new)));

        switch (operation) {
        case DELETE:
            publishDelete(draft, keys, supply);
            break;
        case RESTORE:
            publishRestore(draft, keys, supply);
            break;
        case UPSERT:
            publishUpsert(keys, supply);
            break;
        default:
            break;
        }

        return true;
    }

    private void publishUpsert(RelationKeys keys, Set<CalculableHolder<OriginRelation>> holders) {

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRelation>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRelation> ch = it.next();
            UpsertRelationsRequestContext uCtx = UpsertRelationsRequestContext.builder()
                .relationFrom(keys.getRelationName(), UpsertRelationRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getTo())
                        .originKey(keys.getOriginKey().getTo())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .hint(UpsertRelationHint.HINT_ETALON_ID, keys.getEtalonKey().getId())
                        .build())
                .originKey(keys.getOriginKey().getFrom())
                .etalonKey(keys.getEtalonKey().getFrom())
                .build();

            dataRelationsService.upsertRelations(uCtx);
        }
    }

    private void publishDelete(Draft draft, RelationKeys keys, Collection<CalculableHolder<OriginRelation>> holders) {

        boolean isPeriodOperation = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_PERIOD_OPERATION);

        if (isPeriodOperation) {
            ensureSingleOrEmptyPeriodUpdate(holders);
        }

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRelation>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRelation> ch = it.next();
            // Period delete
            if (isPeriodOperation) {

                DeleteRelationRequestContext dCtx = DeleteRelationRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .inactivatePeriod(true)
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .build();

                dataRelationsService.deleteRelation(dCtx);
            // Record delete + several editions
            } else {

                UpsertRelationRequestContext iCtx = UpsertRelationRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .build();

                dataRelationsService.upsertRelation(iCtx);
            }
        }

        // Final deactication for the record
        if (!isPeriodOperation) {

            DeleteRelationRequestContext dCtx = DeleteRelationRequestContext.builder()
                    .etalonKey(keys.getEtalonKey().getId())
                    .inactivateEtalon(true)
                    .build();

            dataRelationsService.deleteRelation(dCtx);
        }
    }

    private void publishRestore(Draft draft, RelationKeys keys, Set<CalculableHolder<OriginRelation>> holders) {

        boolean isPeriodOperation = draft
                .getVariables()
                .valueGet(DataDraftConstants.IS_PERIOD_OPERATION);

        RelativeDirection direction = draft
                .getVariables()
                .valueGet(DataDraftConstants.RELATION_DIRECTION, RelativeDirection.class);

        if (isPeriodOperation) {
            ensureSingleOrEmptyPeriodUpdate(holders);
        }

        // Default tree set ascending iterator
        Iterator<CalculableHolder<OriginRelation>> it = holders.iterator();
        while (it.hasNext()) {

            CalculableHolder<OriginRelation> ch = it.next();
            if (direction == RelativeDirection.FROM) {

                RestoreFromRelationRequestContext rCtx = RestoreFromRelationRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .periodRestore(isPeriodOperation)
                        .build();

                dataRelationsService.restore(rCtx);
            } else {

                RestoreToRelationRequestContext rCtx = RestoreToRelationRequestContext.builder()
                        .record(ch.getValue())
                        .etalonKey(keys.getEtalonKey().getId())
                        .validFrom(ch.getValidFrom())
                        .validTo(ch.getValidTo())
                        .periodRestore(isPeriodOperation)
                        .build();

                dataRelationsService.restore(rCtx);
            }
        }

        // Restore record with validation
        if (!isPeriodOperation) {

            if (direction == RelativeDirection.FROM) {

                RestoreFromRelationRequestContext rCtx = RestoreFromRelationRequestContext.builder()
                        .etalonKey(keys.getEtalonKey().getId())
                        .build();

                dataRelationsService.restore(rCtx);
            } else {

                RestoreToRelationRequestContext rCtx = RestoreToRelationRequestContext.builder()
                        .etalonKey(keys.getEtalonKey().getId())
                        .build();

                dataRelationsService.restore(rCtx);
            }
        }
    }

    private void ensureSingleOrEmptyPeriodUpdate(Collection<CalculableHolder<OriginRelation>> update) {
        if (update.size() > 1) {
            throw new DraftProcessingException("Period draft operation produced more then one update record!",
                    DataExceptionIds.EX_DATA_RELATION_DRAFT_PERIOD_OPERATION_MORE_THEN_ONE_RECORD);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public CommonRelationsComponent getCommonRelationsComponent() {
        return commonRelationsComponent;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public MetaModelService getMetaModelService() {
        return metaModelService;
    }
}
