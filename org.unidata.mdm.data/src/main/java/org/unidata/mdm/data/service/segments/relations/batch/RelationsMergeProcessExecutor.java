/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.data.service.segments.relations.batch;

import java.util.Collection;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.data.context.MergeRelationsRequestContext;
import org.unidata.mdm.data.dto.MergeRelationsDTO;
import org.unidata.mdm.data.module.DataModule;
import org.unidata.mdm.data.service.segments.relations.merge.RelationMergeConnectorExecutor;
import org.unidata.mdm.data.type.apply.batch.impl.RelationMergeBatchSetAccumulator;
import org.unidata.mdm.data.type.apply.batch.impl.RelationMergeBatchSetStatistics;
import org.unidata.mdm.system.type.batch.BatchIterator;
import org.unidata.mdm.system.type.pipeline.Start;
import org.unidata.mdm.system.type.pipeline.batch.BatchedPoint;
import org.unidata.mdm.system.type.runtime.MeasurementPoint;

/**
 * @author Mikhail Mikhailov on Dec 12, 2019
 */
@Component(RelationsMergeProcessExecutor.SEGMENT_ID)
public class RelationsMergeProcessExecutor extends BatchedPoint<RelationMergeBatchSetAccumulator> {
    /**
     * This logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RelationsMergeProcessExecutor.class);
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = DataModule.MODULE_ID + "[BATCH_RELATIONS_MERGE_PROCESS]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = DataModule.MODULE_ID + ".batch.relations.merge.process.description";
    /**
     * Connector.
     */
    @Autowired
    private RelationMergeConnectorExecutor relationsMergeConnectorExecutor;
    /**
     * Constructor.
     */
    public RelationsMergeProcessExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void point(RelationMergeBatchSetAccumulator accumulator) {

        MeasurementPoint.start();
        try {

            RelationMergeBatchSetStatistics statistics = accumulator.statistics();
            for (BatchIterator<MergeRelationsRequestContext> bi = accumulator.iterator(); bi.hasNext(); ) {

                MergeRelationsRequestContext ctx = bi.next();
                try {

                    MergeRelationsDTO result = relationsMergeConnectorExecutor.execute(ctx, accumulator.pipeline());

                    long processed = Stream.concat(ctx.getRelationsFrom().values().stream(), ctx.getRelationsTo().values().stream())
                        .flatMap(Collection::stream)
                        .count();

                    statistics.incrementMerged(processed);
                    if (statistics.collectResults()) {
                        statistics.addResult(result);
                    }

                } catch (Exception e) {

                    statistics.incrementFailed(
                        Stream.concat(ctx.getRelationsFrom().values().stream(), ctx.getRelationsTo().values().stream())
                            .flatMap(Collection::stream)
                            .count());

                    LOGGER.warn("Batch merge relations exception caught.", e);

                    if (accumulator.isAbortOnFailure()) {
                        throw e;
                    }

                    bi.remove();
                }
            }

        } finally {
            MeasurementPoint.stop();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return RelationMergeBatchSetAccumulator.class.isAssignableFrom(start.getInputTypeClass());
    }
}
