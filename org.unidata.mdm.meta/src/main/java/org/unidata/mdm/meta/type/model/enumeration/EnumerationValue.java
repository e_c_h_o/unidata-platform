package org.unidata.mdm.meta.type.model.enumeration;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class EnumerationValue implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -3334002619906238285L;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public EnumerationValue withName(String value) {
        setName(value);
        return this;
    }

    public EnumerationValue withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }
}
