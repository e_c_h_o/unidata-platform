package org.unidata.mdm.meta.type.model.attributes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

public class AttributeGroup implements Serializable {

    private final static long serialVersionUID = 987654321L;
    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected List<String> attributes;
    @JacksonXmlProperty(isAttribute = true)
    protected int row;
    @JacksonXmlProperty(isAttribute = true)
    protected int column;
    @JacksonXmlProperty(isAttribute = true)
    protected String title;

    public List<String> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<>();
        }
        return this.attributes;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int value) {
        this.row = value;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int value) {
        this.column = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public AttributeGroup withAttributes(String... values) {
        if (values!= null) {
            for (String value: values) {
                getAttributes().add(value);
            }
        }
        return this;
    }

    public AttributeGroup withAttributes(Collection<String> values) {
        if (values!= null) {
            getAttributes().addAll(values);
        }
        return this;
    }

    public AttributeGroup withRow(int value) {
        setRow(value);
        return this;
    }

    public AttributeGroup withColumn(int value) {
        setColumn(value);
        return this;
    }

    public AttributeGroup withTitle(String value) {
        setTitle(value);
        return this;
    }

}
