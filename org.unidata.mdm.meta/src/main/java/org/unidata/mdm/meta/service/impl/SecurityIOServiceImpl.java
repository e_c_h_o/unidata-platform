/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.unidata.mdm.security.LabelDef;
import com.unidata.mdm.security.RoleDef;
import com.unidata.mdm.security.RolePropertyDef;
import com.unidata.mdm.security.Security;
import com.unidata.mdm.security.UserDef;
import com.unidata.mdm.security.UserPropertyDef;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.unidata.mdm.core.dto.RoleDTO;
import org.unidata.mdm.core.dto.RolePropertyDTO;
import org.unidata.mdm.core.dto.SecuredResourceDTO;
import org.unidata.mdm.core.dto.UserDTO;
import org.unidata.mdm.core.dto.UserPropertyDTO;
import org.unidata.mdm.core.dto.UserWithPasswordDTO;
import org.unidata.mdm.core.exception.SystemRuntimeException;
import org.unidata.mdm.core.service.RoleService;
import org.unidata.mdm.core.service.UserService;
import org.unidata.mdm.core.type.security.Role;
import org.unidata.mdm.core.util.CoreJaxbUtils;
import org.unidata.mdm.core.util.SecurityJaxbUtils;
import org.unidata.mdm.meta.context.ExportContext;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.service.SecurityIOService;
import org.unidata.mdm.meta.service.converters.RoleObjectsToXmlDefinitionsConverter;
import org.unidata.mdm.meta.service.converters.RolesXmlDefinitionsToObjectsConverter;
import org.unidata.mdm.meta.service.converters.UserObjectsToXmlDefinitionsConverter;
import org.unidata.mdm.meta.service.converters.UsersXmlDefinitionsToObjectsConverter;

import static org.unidata.mdm.meta.type.InputOutputModelStructureConstants.SECURITY;

/**
 * todo: JavaDoc
 *
 * @author maria.chistyakova
 * @since 18.05.2020
 */
@Service
public class SecurityIOServiceImpl implements SecurityIOService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    public void importSecurityObjects(final Security security, boolean importRoles, boolean importUsers) {
        if (security == null) {
            return;
        }
        Map<String, Set<String>> reImportUserRoles = Collections.emptyMap();

        if (importRoles && CollectionUtils.isNotEmpty(security.getRoles())) {
            // Load db users with roles matched in imported roles.
            if (!importUsers) {
                reImportUserRoles = findUsersWithImportedRoles(security.getRoles());
            }

            loadLabels(security.getLabels());

            final Map<String, Long> rolePropertiesCache = mergeMissingRoleProperties(security.getRoleProperties());
            List<RoleDTO> roleDTOS = RolesXmlDefinitionsToObjectsConverter.toDTOs(security.getRoles(), rolePropertiesCache);
            final List<RoleDTO> roles = filterNotExistsResources(roleDTOS);

            List<String> rolesToReCreate = roles.stream().map(RoleDTO::getName).collect(Collectors.toList());


            roleService.cleanRolesDataByName(
                    rolesToReCreate
            );

            List<Role> allRoles = roleService.getAllRoles();

            List<String> currentRoles = allRoles.stream().map(Role::getName).collect(Collectors.toList());


            List<Role> newRoles = roles.stream()
                    .filter(r -> !currentRoles.contains(r.getName()))
                    .collect(Collectors.toList());
            List<Role> oldRoles = roles.stream()
                    .filter(r -> currentRoles.contains(r.getName()))
                    .collect(Collectors.toList());


            newRoles.forEach(roleService::create);


            oldRoles.forEach(r -> {
                RoleDTO roleDef1 = roleDTOS.stream()
                        .filter(roleDef ->
                                roleDef.getName().equals(r.getName()))
                        .findFirst()
                        .get();
                roleService.update(r.getName(), roleDef1);
            });

        }

        if (importUsers && CollectionUtils.isNotEmpty(security.getUsers())) {
            final Map<String, Long> userPropertiesCache = mergeMissingProperties(security.getUserProperties());
            final List<UserWithPasswordDTO> users =
                    UsersXmlDefinitionsToObjectsConverter.toDTOs(security.getUsers(), userPropertiesCache);
            userService.removeUsersByLogin(users.stream().map(UserDTO::getLogin).collect(Collectors.toList()));
            userService.saveUsers(users);
            userService.addUsersPasswords(
                    security.getUsers().stream()
                            .collect(Collectors.toMap(UserDef::getLogin, UserDef::getPasswords)));
        } else if (!importUsers && !reImportUserRoles.isEmpty()) {
            // Assign imported roles for existing users which were removed during roles re-import.
            userService.addUsersRoles(reImportUserRoles);
        }
    }

    @Override
    public void exportSecurityObjects(Path rootPath, ExportContext exportContext) throws IOException {
        final Path securityFolder = securityFolder(rootPath);

        final Security security = CoreJaxbUtils.getSecurityFactory().createSecurity();

        if (exportContext.isExportUsers()) {

            final List<UserDTO> users = userService.loadAllUsers();

            if (CollectionUtils.isNotEmpty(users)) {
                security
                        .withUsers(
                                UserObjectsToXmlDefinitionsConverter.convertUsers(users, userService.loadUserPasswords())
                        )
                        .withUserProperties(
                                UserObjectsToXmlDefinitionsConverter.convertUserProperties(userService.getAllProperties())
                        );
            }
        }

        if (exportContext.isExportRoles()) {
            final List<RoleDTO> allRoles = roleService.loadAllRoles();
            if (CollectionUtils.isNotEmpty(allRoles)) {
                security
                        .withRoles(RoleObjectsToXmlDefinitionsConverter.convertRoles(allRoles))
                        .withRoleProperties(RoleObjectsToXmlDefinitionsConverter.convertRoleProperties(roleService.loadAllProperties()))
                        .withLabels(RoleObjectsToXmlDefinitionsConverter.convertSecurityLabels(roleService.getAllSecurityLabels()));
            }
        }

        final String result = SecurityJaxbUtils.marshalSecurity(security);
        final Path file = Files.createFile(Paths.get(securityFolder.toString(), SECURITY + ".xml"));
        write(result, file);
    }


    private Map<String, Set<String>> findUsersWithImportedRoles(List<RoleDef> importedRoles) {
        Map<String, Set<String>> reImportUserRoles = new HashMap<>();

        Set<String> importRoleNames = importedRoles.stream()
                .map(RoleDef::getName).collect(Collectors.toSet());

        userService.loadAllUsers().forEach(user -> {
            Set<String> reimportRoleNames = user.getRoles().stream()
                    .map(Role::getName)
                    .filter(importRoleNames::contains)
                    .collect(Collectors.toSet());

            if (!reimportRoleNames.isEmpty()) {
                reImportUserRoles.put(user.getLogin(), reimportRoleNames);
            }
        });

        return reImportUserRoles;
    }


    private void loadLabels(final List<LabelDef> labels) {
        final Set<String> existsResources = roleService.getSecuredResourcesFlatList().stream()
                .map(SecuredResourceDTO::getName)
                .collect(Collectors.toSet());
        final List<LabelDef> filteredLabels = labels.stream()
                .filter(l -> l.getAttributes().stream().allMatch(la -> existsResources.contains(la.getPath())))
                .collect(Collectors.toList());
        filteredLabels.stream()
                .map(LabelDef::getName)
                .forEach(roleService::deleteLabel);
        filteredLabels.stream()
                .map(RolesXmlDefinitionsToObjectsConverter::toDTO)
                .forEach(roleService::createLabel);
    }


    private Map<String, Long> mergeMissingRoleProperties(final List<RolePropertyDef> roleProperties) {
        return merge(
                roleProperties,
                () -> roleService.loadAllProperties(),
                RolePropertyDTO::getName,
                RolePropertyDTO::getId,
                RolePropertyDef::getName,
                RolesXmlDefinitionsToObjectsConverter::toDTO,
                (rolePropertyDTO, rolePropertyDef) ->
                        !rolePropertyDTO.getDisplayName().equals(rolePropertyDef.getDisplayName()),
                properties -> properties.forEach(roleService::saveProperty)
        );
    }

    private <K, V, LOC_TYPE, IN_TYPE> Map<K, V> merge(
            final List<IN_TYPE> inElements,
            final Supplier<List<LOC_TYPE>> localElementsProducer,
            final Function<LOC_TYPE, K> cacheKeyGenerator,
            final Function<LOC_TYPE, V> cacheValueGenerator,
            final Function<IN_TYPE, K> inValueKeyGenerator,
            final Function<IN_TYPE, LOC_TYPE> inValueToLocalValueGenerator,
            final BiPredicate<LOC_TYPE, IN_TYPE> applyAllPredicate,
            final Consumer<List<LOC_TYPE>> newElementsConsumer
    ) {
        final List<LOC_TYPE> allLocalElements = localElementsProducer.get();
        final Map<K, V> cache = new HashMap<>(
                allLocalElements.stream()
                        .collect(Collectors.toMap(cacheKeyGenerator, cacheValueGenerator))
        );
        if (CollectionUtils.isNotEmpty(inElements)) {
            final List<LOC_TYPE> newValues = inElements.stream()
                    .filter(el -> !cache.keySet().contains(inValueKeyGenerator.apply(el)))
                    .filter(el -> applyAllPredicate == null || CollectionUtils.isEmpty(allLocalElements) ||
                            allLocalElements.stream().allMatch(localEl -> applyAllPredicate.test(localEl, el)))
                    .map(inValueToLocalValueGenerator)
                    .collect(Collectors.toList());
            newElementsConsumer.accept(newValues);
            cache.putAll(
                    newValues.stream().filter(Objects::nonNull).collect(Collectors.toMap(cacheKeyGenerator, cacheValueGenerator))
            );
        }
        return cache;
    }

    private List<RoleDTO> filterNotExistsResources(List<RoleDTO> roles) {
        final Set<String> existsResources = roleService.getAllSecuredResources().stream()
                .flatMap(resourceDTO -> {
                    if (!CollectionUtils.isEmpty(resourceDTO.getChildren())) {
                        return Stream.concat(Stream.of(resourceDTO), resourceDTO.getChildren().stream());
                    }

                    return Stream.of(resourceDTO);
                })
                .map(SecuredResourceDTO::getName)
                .collect(Collectors.toSet());
        return roles.stream()
                .peek(r -> r.setRights(
                        r.getRights().stream()
                                .filter(right -> existsResources.contains(right.getSecuredResource().getName()))
                                .collect(Collectors.toList())
                )).collect(Collectors.toList());
    }

    private Map<String, Long> mergeMissingProperties(final List<UserPropertyDef> userProperties) {
        return merge(
                userProperties,
                () -> userService.getAllProperties(),
                UserPropertyDTO::getName,
                UserPropertyDTO::getId,
                UserPropertyDef::getName,
                UsersXmlDefinitionsToObjectsConverter::toDTO,
                (userPropertyDTO, userPropertyDef) ->
                        !userPropertyDTO.getDisplayName().equals(userPropertyDef.getDisplayName()),
                properties -> properties.forEach(userService::saveProperty)
        );
    }

    /**
     * Write.
     *
     * @param string the string
     * @param file the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void write(String string, Path file) throws IOException {
        try (final BufferedWriter bw =
                     new BufferedWriter(
                             new OutputStreamWriter(new FileOutputStream(file.toString()), StandardCharsets.UTF_8.name())
                     )
        ) {
            bw.write(string);
        } catch (Exception e) {
            throw new SystemRuntimeException("Security resource write exception occurred.",
                    MetaExceptionIds.EX_META_SECURITY_RESOURCE_WRITE, e);
        }
    }


    private Path securityFolder(final Path rootPath) throws IOException {
        return Files.createDirectories(Paths.get(rootPath.toString(), SECURITY));
    }


}
