/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.context;

import org.unidata.mdm.meta.service.segments.io.ApplyUploadedModelStartExecutor;
import org.unidata.mdm.meta.type.input.meta.MetaGraph;
import org.unidata.mdm.system.context.AbstractCompositeRequestContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

/**
 * @author maria.chistyakova
 */
public class ApplyUploadedModelRequestContext
        extends AbstractCompositeRequestContext
        implements PipelineInput {

    private MetaGraph metaGraph;

    /**
     * Constructor.
     *
     * @param b the builder.
     */
    private ApplyUploadedModelRequestContext(Builder b) {
        super(b);
        this.metaGraph = b.metaGraph;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ApplyUploadedModelStartExecutor.SEGMENT_ID;
    }

    public static Builder builder() {
        return new Builder();
    }

    public MetaGraph getMetaGraph() {
        return metaGraph;
    }

    public static class Builder extends AbstractCompositeRequestContextBuilder<Builder> {

        private MetaGraph metaGraph;


        /**
         * Constructor.
         */
        protected Builder() {
            super();
        }

        /**
         * optional
         *
         * @param metaGraph
         * @return this
         */
        public Builder setMetaGraph(MetaGraph metaGraph) {
            this.metaGraph = metaGraph;
            return this;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public ApplyUploadedModelRequestContext build() {
            return new ApplyUploadedModelRequestContext(this);
        }


    }
}
