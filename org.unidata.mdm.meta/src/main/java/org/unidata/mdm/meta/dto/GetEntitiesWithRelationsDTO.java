/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.type.model.entities.Entity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.type.model.entities.Relation;

/**
 * @author Mikhail Mikhailov
 * Entities and relations view, filtered by side ('to' or 'from').
 */
public class GetEntitiesWithRelationsDTO {
    /**
     * Filtered entities view.
     */
    final Map<Entity, Pair<List<NestedEntity>, List<Relation>>> entities;
    /**
     * Constructor.
     */
    public GetEntitiesWithRelationsDTO(Map<Entity, Pair<List<NestedEntity>, List<Relation>>> entities) {
        this.entities = entities;
    }
    /**
     * @return the entities
     */
    public Map<Entity, Pair<List<NestedEntity>, List<Relation>>> getEntities() {
        return Objects.isNull(entities) ? Collections.emptyMap() : entities;
    }
}
