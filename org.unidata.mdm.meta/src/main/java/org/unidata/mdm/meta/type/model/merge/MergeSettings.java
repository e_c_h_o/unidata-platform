package org.unidata.mdm.meta.type.model.merge;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

@JsonPropertyOrder({"bvtSettings", "bvrSettings"})
public class MergeSettings implements Serializable {

    private final static long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected BVTMergeType bvtSettings;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    protected BVRMergeType bvrSettings;

    public BVTMergeType getBvtSettings() {
        return bvtSettings;
    }

    public void setBvtSettings(BVTMergeType value) {
        this.bvtSettings = value;
    }

    public BVRMergeType getBvrSettings() {
        return bvrSettings;
    }

    public void setBvrSettings(BVRMergeType value) {
        this.bvrSettings = value;
    }

    public MergeSettings withBvtSettings(BVTMergeType value) {
        setBvtSettings(value);
        return this;
    }

    public MergeSettings withBvrSettings(BVRMergeType value) {
        setBvrSettings(value);
        return this;
    }

}
