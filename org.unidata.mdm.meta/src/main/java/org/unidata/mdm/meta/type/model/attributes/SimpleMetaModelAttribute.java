package org.unidata.mdm.meta.type.model.attributes;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.meta.type.model.OrderedElement;
import org.unidata.mdm.meta.type.model.SimpleDataType;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class SimpleMetaModelAttribute
    extends AbstractSimpleMetaModelAttribute<SimpleMetaModelAttribute>
    implements OrderedElement, Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 1529579802544024535L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private AttributeMeasurementSettings measureSettings;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> lookupEntityDisplayAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> lookupEntitySearchAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> dictionaryDataType;

    @JacksonXmlProperty(isAttribute = true)
    private boolean useAttributeNameForDisplay;

    @JacksonXmlProperty(isAttribute = true)
    private boolean searchMorphologically;

    @JacksonXmlProperty(isAttribute = true)
    private boolean searchCaseInsensitive;

    @JacksonXmlProperty(isAttribute = true)
    private String enumDataType;

    @JacksonXmlProperty(isAttribute = true)
    private String linkDataType;

    @JacksonXmlProperty(isAttribute = true)
    private String lookupEntityType;

    @JacksonXmlProperty(isAttribute = true)
    private SimpleDataType lookupEntityCodeAttributeType;

    @JacksonXmlProperty(isAttribute = true)
    private int order;

    // with

    public AttributeMeasurementSettings getMeasureSettings() {
        return measureSettings;
    }

    public void setMeasureSettings(AttributeMeasurementSettings value) {
        this.measureSettings = value;
    }

    public List<String> getLookupEntityDisplayAttributes() {
        if (lookupEntityDisplayAttributes == null) {
            lookupEntityDisplayAttributes = new ArrayList<>();
        }
        return this.lookupEntityDisplayAttributes;
    }

    public List<String> getLookupEntitySearchAttributes() {
        if (lookupEntitySearchAttributes == null) {
            lookupEntitySearchAttributes = new ArrayList<>();
        }
        return this.lookupEntitySearchAttributes;
    }

    public List<String> getDictionaryDataType() {
        if (dictionaryDataType == null) {
            dictionaryDataType = new ArrayList<>();
        }
        return this.dictionaryDataType;
    }

    public void setLookupEntityDisplayAttributes(List<String> lookupEntityDisplayAttributes) {
        this.lookupEntityDisplayAttributes = lookupEntityDisplayAttributes;
    }

    public void setLookupEntitySearchAttributes(List<String> lookupEntitySearchAttributes) {
        this.lookupEntitySearchAttributes = lookupEntitySearchAttributes;
    }

    public void setDictionaryDataType(List<String> dictionaryDataType) {
        this.dictionaryDataType = dictionaryDataType;
    }

    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    public void setUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
    }

    public boolean isSearchMorphologically() {
        return searchMorphologically;
    }

    public void setSearchMorphologically(boolean searchMorphologically) {
        this.searchMorphologically = searchMorphologically;
    }

    public boolean isSearchCaseInsensitive() {
        return searchCaseInsensitive;
    }

    public void setSearchCaseInsensitive(boolean searchCaseInsensitive) {
        this.searchCaseInsensitive = searchCaseInsensitive;
    }

    public String getEnumDataType() {
        return enumDataType;
    }

    public void setEnumDataType(String enumDataType) {
        this.enumDataType = enumDataType;
    }

    public String getLinkDataType() {
        return linkDataType;
    }

    public void setLinkDataType(String linkDataType) {
        this.linkDataType = linkDataType;
    }

    public String getLookupEntityType() {
        return lookupEntityType;
    }

    public void setLookupEntityType(String lookupEntityType) {
        this.lookupEntityType = lookupEntityType;
    }

    public SimpleDataType getLookupEntityCodeAttributeType() {
        return lookupEntityCodeAttributeType;
    }

    public void setLookupEntityCodeAttributeType(SimpleDataType lookupEntityCodeAttributeType) {
        this.lookupEntityCodeAttributeType = lookupEntityCodeAttributeType;
    }

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    public void setOrder(int order) {
        this.order = order;
    }

    // With
    public SimpleMetaModelAttribute withMeasureSettings(AttributeMeasurementSettings value) {
        this.measureSettings = value;
        return this;
    }

    public SimpleMetaModelAttribute withLookupEntityDisplayAttributes(List<String> lookupEntityDisplayAttributes) {
        this.lookupEntityDisplayAttributes = lookupEntityDisplayAttributes;
        return this;
    }

    public SimpleMetaModelAttribute withLookupEntitySearchAttributes(List<String> lookupEntitySearchAttributes) {
        this.lookupEntitySearchAttributes = lookupEntitySearchAttributes;
        return this;
    }

    public SimpleMetaModelAttribute withDictionaryDataType(List<String> dictionaryDataType) {
        this.dictionaryDataType = dictionaryDataType;
        return this;
    }

    //
    public SimpleMetaModelAttribute withLookupEntityDisplayAttributes(String... lookupEntityDisplayAttributes) {
        if (lookupEntityDisplayAttributes != null) {
            Collections.addAll(getLookupEntityDisplayAttributes(), lookupEntityDisplayAttributes);
        }
        return this;
    }

    public SimpleMetaModelAttribute withLookupEntitySearchAttributes(String... lookupEntitySearchAttributes) {
        if (lookupEntitySearchAttributes != null) {
            Collections.addAll(getLookupEntitySearchAttributes(), lookupEntitySearchAttributes);
        }
        return this;
    }

    public SimpleMetaModelAttribute withDictionaryDataType(String... dictionaryDataType) {
        if (dictionaryDataType != null) {
            Collections.addAll(getDictionaryDataType(), dictionaryDataType);
        }
        return this;
    }

    public SimpleMetaModelAttribute withUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
        return this;
    }

    public SimpleMetaModelAttribute withSearchMorphologically(boolean searchMorphologically) {
        this.searchMorphologically = searchMorphologically;
        return this;
    }

    public SimpleMetaModelAttribute withSearchCaseInsensitive(boolean searchCaseInsensitive) {
        this.searchCaseInsensitive = searchCaseInsensitive;
        return this;
    }

    public SimpleMetaModelAttribute withEnumDataType(String enumDataType) {
        this.enumDataType = enumDataType;
        return this;
    }

    public SimpleMetaModelAttribute withLinkDataType(String linkDataType) {
        this.linkDataType = linkDataType;
        return this;
    }

    public SimpleMetaModelAttribute withLookupEntityType(String lookupEntityType) {
        this.lookupEntityType = lookupEntityType;
        return this;
    }

    public SimpleMetaModelAttribute withLookupEntityCodeAttributeType(SimpleDataType lookupEntityCodeAttributeType) {
        this.lookupEntityCodeAttributeType = lookupEntityCodeAttributeType;
        return this;
    }

    public SimpleMetaModelAttribute withOrder(int order) {
        this.order = order;
        return this;
    }
}
