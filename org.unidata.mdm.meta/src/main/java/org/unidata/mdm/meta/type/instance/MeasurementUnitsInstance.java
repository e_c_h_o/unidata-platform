package org.unidata.mdm.meta.type.instance;

import java.util.Collection;

import javax.annotation.Nullable;

import org.unidata.mdm.core.type.model.MeasurementCategoryElement;
import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.meta.type.model.measurement.MeasurementUnitsModel;

/**
 * @author Mikhail Mikhailov on Nov 5, 2020
 */
public interface MeasurementUnitsInstance extends ModelInstance<MeasurementUnitsModel> {
    /**
     * Gets all categories.
     * @return categories collection
     */
    Collection<MeasurementCategoryElement> getCategories();
    /**
     * Gets specific category by name.
     * @param name the name
     * @return element or null
     */
    @Nullable
    MeasurementCategoryElement getCategory(String name);
    /**
     * Returns true, if the MUC with the given name exists.
     * @param name the name to look for
     * @return true, if the MUC with the given name exists
     */
    boolean exists(String name);
}
