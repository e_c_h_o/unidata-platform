package org.unidata.mdm.meta.type.model.attributes;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Base implementation from searchable attributes
 *
 * @author Alexandr Serov
 * @since 26.08.2020
 * @see SearchableMetaModelAttribute
 **/
public abstract class AbstractSearchableMetaModelAttribute<X extends AbstractSearchableMetaModelAttribute<X>>
    extends AbstractMetaModelAttribute<X>
    implements SearchableMetaModelAttribute {

    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 4625672658647381615L;

    @JacksonXmlProperty(isAttribute = true)
    private boolean searchable;

    @JacksonXmlProperty(isAttribute = true)
    private boolean displayable;

    @JacksonXmlProperty(isAttribute = true)
    private boolean mainDisplayable;

    @JacksonXmlProperty(isAttribute = true)
    private String mask;

    @Override
    public boolean isSearchable() {
        return searchable;
    }

    @Override
    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    @Override
    public boolean isDisplayable() {
        return displayable;
    }

    @Override
    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }

    @Override
    public boolean isMainDisplayable() {
        return mainDisplayable;
    }

    @Override
    public void setMainDisplayable(boolean mainDisplayable) {
        this.mainDisplayable = mainDisplayable;
    }

    @Override
    public String getMask() {
        return StringUtils.firstNonBlank(mask, StringUtils.EMPTY);
    }

    @Override
    public void setMask(String mask) {
        this.mask = mask;
    }

    public X withSearchable(boolean searchable) {
        setSearchable(searchable);
        return self();
    }

    public X withDisplayable(boolean displayable) {
        setDisplayable(displayable);
        return self();
    }

    public X withMainDisplayable(boolean mainDisplayable) {
        setMainDisplayable(mainDisplayable);
        return self();
    }

    public X withMask(String mask) {
        setMask(mask);
        return self();
    }
}
