package org.unidata.mdm.meta.type.model.enumeration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.util.ModelUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * @author Mikhail Mikhailov on Oct 5, 2020
 * Enumerations root element.
 */
@JacksonXmlRootElement(localName = "enumerations", namespace = EnumerationsModel.NAMESPACE)
public class EnumerationsModel extends Model<EnumerationsModel> implements ModelSource {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -8127110358924846838L;
    /**
     * The NS.
     */
    public static final String NAMESPACE = "http://enumerations.mdm.unidata.org/";
    /**
     * Enumeration list.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "enumeration")
    private List<EnumerationType> values;
    /**
     * Constructor.
     */
    public EnumerationsModel() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "instanceId")
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "typeId")
    @Override
    public String getTypeId() {
        return TypeIds.ENUMERATIONS_MODEL;
    }
    /**
     * @return the values
     */
    public List<EnumerationType> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }

    public EnumerationsModel withValues(EnumerationType... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            Collections.addAll(getValues(), values);
        }
        return this;
    }

    public EnumerationsModel withValues(Collection<EnumerationType> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getValues().addAll(values);
        }
        return this;
    }
}
