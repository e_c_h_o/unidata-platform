/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import org.unidata.mdm.core.context.AbstractModelSourceContext;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.meta.type.model.measurement.MeasurementUnitsModel;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class SourceMeasurementUnitsContext extends AbstractModelSourceContext<MeasurementUnitsModel> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -8974711488212823593L;
    /**
     * Constructor.
     * @param b the builder.
     */
    private SourceMeasurementUnitsContext(SourceMeasurementUnitsContextBuilder b) {
        super(b);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.MEASUREMENT_UNITS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // Enumerations models are singletons per storage
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static SourceMeasurementUnitsContextBuilder builder() {
        return new SourceMeasurementUnitsContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class SourceMeasurementUnitsContextBuilder
        extends AbstractModelSourceContextBuilder<MeasurementUnitsModel, SourceMeasurementUnitsContextBuilder> {
        /**
         * Constructor.
         */
        protected SourceMeasurementUnitsContextBuilder() {
            super();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public SourceMeasurementUnitsContext build() {
            return new SourceMeasurementUnitsContext(this);
        }
    }
}
