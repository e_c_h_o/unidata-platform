/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl.data.instance;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.AttributedElement;
import org.unidata.mdm.core.type.model.ModelSearchEntry;
import org.unidata.mdm.meta.service.impl.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.meta.type.model.entities.AbstractEntity;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov
 * Base entity wrapper type.
 */
public abstract class AbstractAttributesImpl<X extends AbstractEntity<?>> extends AbstractNamedDisplayableCustomPropertiesImpl
    implements AttributedElement, EntitySource<X> {
    /**
     * Entry type 'ATTRIBUTE'.
     */
    protected static final String ENTRY_TYPE_ATTRIBUTE = "ATTRIBUTE";
    /**
     * Attributes map.
     */
    private final Map<String, AttributeElement> attrs;
    /**
     * The source.
     */
    private final X source;
    /**
     * Constructor.
     */
    protected AbstractAttributesImpl(X entity, List<NestedEntity> nested) {
        super(entity.getName(), entity.getDisplayName(), entity.getDescription(), entity.getCustomProperties());
        this.source = entity;
        this.attrs = ModelUtils.createAttributesMap(entity, nested);
    }
    /**
     * Generates model search entries for attributes hold.
     * @param subjectType the holding subject type
     * @param subjectName the holding subject name
     * @return collection
     */
    protected Collection<ModelSearchEntry> getAttributeEntries(String subjectType, String subjectName) {
        return getAttributes().values().stream()
            .map(attr ->
                new ModelSearchEntry(subjectType, subjectName, ENTRY_TYPE_ATTRIBUTE, attr.getName())
                        .withEntryDisplayName(attr.getDisplayName())
                        .withEntryDescription(attr.getDescription())
            )
            .collect(Collectors.toList());

    }
    /**
     * {@inheritDoc}
     */
    @Override
    public X getSource() {
        return source;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, AttributeElement> getAttributes() {
        return attrs;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, AttributeElement> getMainDisplayableAttributes() {
        return attrs.entrySet().stream()
                .filter(e -> e.getValue().isMainDisplayable())
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }
}
