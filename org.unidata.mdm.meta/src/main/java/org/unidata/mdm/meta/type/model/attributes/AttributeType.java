package org.unidata.mdm.meta.type.model.attributes;

import com.fasterxml.jackson.annotation.JsonProperty;


public enum AttributeType {

    @JsonProperty("Simple")
    SIMPLE("Simple"),
    @JsonProperty("Code")
    CODE("Code"),
    @JsonProperty("Array")
    ARRAY("Array"),
    @JsonProperty("Complex")
    COMPLEX("Complex");

    private final String value;

    AttributeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AttributeType fromValue(String v) {
        for (AttributeType c: AttributeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
