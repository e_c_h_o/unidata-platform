/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.impl.instance;

import java.time.OffsetDateTime;

import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.core.type.model.VersionedElement;
import org.unidata.mdm.meta.type.model.Model;

/**
 * @author Mikhail Mikhailov on Oct 7, 2020
 * Version info.
 */
public abstract class AbstractModelInstanceImpl<X extends ModelSource>
    extends AbstractNamedDisplayableCustomPropertiesImpl
    implements ModelInstance<X>, VersionedElement {
    /**
     * Current version.
     */
    private final int version;
    /**
     * Create date.
     */
    private final OffsetDateTime createDate;
    /**
     * Created by.
     */
    private final String createdBy;
    /**
     * The storage id.
     */
    private final String storageId;
    /**
     * Constructor.
     * @param m the object
     */
    protected AbstractModelInstanceImpl(Model<?> m) {
        super(m.getName(), m.getDisplayName(), m.getDescription(), m.getCustomProperties());
        this.version = m.getVersion();
        this.createDate = m.getCreateDate();
        this.createdBy = m.getCreatedBy();
        this.storageId = m.getStorageId();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getVersion() {
        return version;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime getCreateDate() {
        return createDate;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStorageId() {
        return storageId;
    }
}
