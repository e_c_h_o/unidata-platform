/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.instance;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.unidata.mdm.core.type.model.EntitiesGroupElement;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.model.RegisterElement;
import org.unidata.mdm.core.type.model.RelationElement;
import org.unidata.mdm.meta.type.model.DataModel;

/**
 * @author Mikhail Mikhailov on Oct 8, 2020
 * The data model interface.
 */
public interface DataModelInstance extends ModelInstance<DataModel> {
    /**
     * Gets all register entities.
     * @return register entities
     */
    @Nonnull
    Collection<RegisterElement> getRegisters();
    /**
     * Gets all lookup entities.
     * @return lookup entities
     */
    @Nonnull
    Collection<LookupElement> getLookups();
    /**
     * Gets all relation entities.
     * @return relation entities
     */
    @Nonnull
    Collection<RelationElement> getRelations();
    /**
     * Gets all nested entities.
     * @return nested entities
     */
    @Nonnull
    Collection<NestedElement> getNested();
    /**
     * Gets all groups.
     * @return groups
     */
    @Nonnull
    Collection<EntitiesGroupElement> getGroups();
    /**
     * Tells, whether this id belongs to a top - level element (either register or lookup or relation or nested).
     * @param id the id
     * @return true, if this id belongs to a top - level element (either register or lookup or relation or nested)
     */
    boolean isElement(String id);
    /**
     * Tells whether this id belongs to a top level register.
     * @param id the id
     * @return true, if this id belongs to a top level register.
     */
    boolean isRegister(String id);
    /**
     * Tells whether this id belongs to a top level lookup.
     * @param id the id
     * @return true, if this id belongs to a top level lookup.
     */
    boolean isLookup(String id);
    /**
     * Tells whether this id belongs to a relation.
     * @param id the id
     * @return true, if this id belongs to a relation.
     */
    boolean isRelation(String id);
    /**
     * Tells whether this id belongs to a nested entity.
     * @param id the id
     * @return true, if this id belongs to a nested entity.
     */
    boolean isNested(String id);
    /**
     * Gets a top - level element (either register or lookup or relation or nested).
     * @param id the id
     * @return element or null
     */
    @Nullable
    EntityElement getElement(String id);
    /**
     * Gets register by id.
     * @param id the id
     * @return register or null
     */
    @Nullable
    RegisterElement getRegister(String id);
    /**
     * Gets lookup by id.
     * @param id the id
     * @return lookup or null
     */
    @Nullable
    LookupElement getLookup(String id);
    /**
     * Gets relation by id.
     * @param id the id
     * @return relation or null
     */
    @Nullable
    RelationElement getRelation(String id);
    /**
     * Gets nested by id.
     * @param id the id
     * @return nested or null
     */
    @Nullable
    NestedElement getNested(String id);
    /**
     * Gets group by 'path'.
     * @param path the path
     * @return group or null
     */
    @Nullable
    EntitiesGroupElement getGroup(String path);
    /**
     * Gets the root entities group.
     * @return root group
     */
    @Nonnull
    EntitiesGroupElement getRootGroup();
}
