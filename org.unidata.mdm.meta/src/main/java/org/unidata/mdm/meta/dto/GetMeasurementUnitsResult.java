package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
public class GetMeasurementUnitsResult extends AbstractModelResult {
    /**
     * MMVs.
     */
    private List<GetMeasurementCategoryResult> measurementCategories;
    /**
     * Constructor.
     */
    public GetMeasurementUnitsResult() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.MEASUREMENT_UNITS_MODEL;
    }
    /**
     * @return the measurementValues
     */
    public List<GetMeasurementCategoryResult> getMeasurementCategories() {
        return Objects.isNull(measurementCategories) ? Collections.emptyList() : measurementCategories;
    }
    /**
     * @param measurementValues the measurementValues to set
     */
    public void setMeasurementCategories(List<GetMeasurementCategoryResult> measurementValues) {
        this.measurementCategories = measurementValues;
    }
}
