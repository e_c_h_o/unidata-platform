/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * @author Mikhail Mikhailov
 * Model header fields (all types of models).
 * Consists of:
 * SUBJECT_NAME (a top level subject name - i. e. entity name, enumeration, measurement units category etc.)
 * SUBJECT_TYPE (type string for above's value RELATION, REGISTER, LOOKUP, CLASSIFIER, etc.)
 * ENTRY_TYPE (a subtype, i. e. SIMPLE_ATTRIBUTE, COMPLEX_ATTRIBUTE, NODE etc. Same as SUBJECT_TYPE for top level elements).
 * ENTRY_NAME, (content of the 'name' field in NamedDisplayable. Same as SUBJECT_TYPE for top level elements)
 * ENTRY_DISPLAY_NAME, (content of the 'displayName' field in NamedDisplayable)
 * ENTRY_DESCRIPTION  (content of the 'description' field in NamedDisplayable)
 */
public enum ModelHeaderField implements IndexField {
    /**
     * SUBJECT_TYPE (type string for above's value RELATION, REGISTER, LOOKUP, CLASSIFIER, etc.).
     */
    SUBJECT_TYPE("subject_type", FieldType.STRING),
    /**
     * SUBJECT_NAME (a top level subject name - i. e. entity name, enumeration, measurement units category etc.).
     */
    SUBJECT_NAME("subject_name", FieldType.STRING),
    /**
     * ENTRY_TYPE (a subtype, i. e. SIMPLE_ATTRIBUTE, COMPLEX_ATTRIBUTE, NODE etc.).
     */
    ENTRY_TYPE("entry_type", FieldType.STRING),
    /**
     * ENTRY_NAME, (content of the 'name' field in NamedDisplayable).
     */
    ENTRY_NAME("entry_name", FieldType.STRING),
    /**
     * ENTRY_DISPLAY_NAME, (content of the 'displayName' field in NamedDisplayable)
     */
    ENTRY_DISPLAY_NAME("entry_display_name", FieldType.STRING),
    /**
     * ENTRY_DESCRIPTION  (content of the 'description' field in NamedDisplayable).
     */
    ENTRY_DESCRIPTION("entry_description", FieldType.STRING),
    /**
     * Other params as nested objects.
     */
    ENTRY_DETAILS("entry_details", FieldType.COMPOSITE),
    /**
     * Detail key name.
     */
    DETAIL_KEY("key", FieldType.STRING),
    /**
     * Detail value.
     */
    DETAIL_VALUE("value", FieldType.STRING);
    /**
     * Field.
     */
    private final String field;
    /**
     * The type.
     */
    private final FieldType type;
    /**
     * Constructor.
     * @param field the field name
     */
    ModelHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }
    /**
     * @return the field
     */
    @Override
    public String getName() {
        return field;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath() {
        return this == DETAIL_KEY || this == DETAIL_VALUE ? ENTRY_DETAILS.getPath() + "." + field : getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
}
