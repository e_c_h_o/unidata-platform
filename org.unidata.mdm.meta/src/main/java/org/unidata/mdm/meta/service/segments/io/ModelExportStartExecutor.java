/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.segments.io;

import java.io.IOException;
import java.nio.file.Files;

import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.context.ExportModelRequestContext;
import org.unidata.mdm.meta.exception.MetaExceptionIds;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.system.exception.PlatformFailureException;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author maria.chistyakova
 * @since 06.12.2019
 */
@Component(ModelExportStartExecutor.SEGMENT_ID)
public class ModelExportStartExecutor extends Start<ExportModelRequestContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MetaModule.MODULE_ID + "[MODEL_EXPORT_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MetaModule.MODULE_ID + ".model.export.start.description";

    /**
     * Constructor.
     */
    public ModelExportStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, ExportModelRequestContext.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(ExportModelRequestContext ctx) {
        try {
            Files.createDirectories(ctx.getPath());
        } catch (IOException e) {
            throw new PlatformFailureException(
                    "Unable to create zip file for metamodel. Exception occured.",
                    MetaExceptionIds.EX_META_CANNOT_ASSEMBLE_MODEL
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(ExportModelRequestContext ctx) {
        // No subject for this type of pipelines
        // This may be storage id in the future
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return start == this;
    }
}
