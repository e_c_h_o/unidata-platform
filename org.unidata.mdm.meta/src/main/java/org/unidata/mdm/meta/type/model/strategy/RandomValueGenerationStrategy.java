package org.unidata.mdm.meta.type.model.strategy;

import java.io.Serializable;

import org.unidata.mdm.meta.type.model.ValueGenerationStrategyType;

public class RandomValueGenerationStrategy extends AbstractValueGenerationStrategy implements Serializable {

    private final static long serialVersionUID = 987654321L;

    public RandomValueGenerationStrategy() {
        super(ValueGenerationStrategyType.RANDOM);
    }

}
