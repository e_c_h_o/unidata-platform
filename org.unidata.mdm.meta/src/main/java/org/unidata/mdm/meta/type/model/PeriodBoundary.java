package org.unidata.mdm.meta.type.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@JsonPropertyOrder({"start", "end"})
public class PeriodBoundary implements Serializable {

    private static final long serialVersionUID = 987654321L;

    @JacksonXmlProperty(namespace = ModelNamespace.META_MODEL_NAMESPACE)
    protected LocalDateTime start;

    @JacksonXmlProperty(namespace = ModelNamespace.META_MODEL_NAMESPACE)
    protected LocalDateTime end;

    @JacksonXmlProperty(isAttribute = true)
    protected DateGranularityMode mode;

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime value) {
        this.start = value;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime value) {
        this.end = value;
    }

    public DateGranularityMode getMode() {
        return mode;
    }

    public void setMode(DateGranularityMode value) {
        this.mode = value;
    }

    public PeriodBoundary withStart(LocalDateTime value) {
        setStart(value);
        return this;
    }

    public PeriodBoundary withEnd(LocalDateTime value) {
        setEnd(value);
        return this;
    }

    public PeriodBoundary withMode(DateGranularityMode value) {
        setMode(value);
        return this;
    }
}
