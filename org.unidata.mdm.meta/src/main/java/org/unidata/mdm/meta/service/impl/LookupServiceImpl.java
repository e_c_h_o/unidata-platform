/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.core.type.model.AttributeElement;
import org.unidata.mdm.core.type.model.LookupElement;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.LookupService;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.type.search.EntityIndexType;
import org.unidata.mdm.meta.type.search.RecordHeaderField;
import org.unidata.mdm.search.context.SearchRequestContext;
import org.unidata.mdm.search.dto.SearchResultDTO;
import org.unidata.mdm.search.dto.SearchResultHitDTO;
import org.unidata.mdm.search.dto.SearchResultHitFieldDTO;
import org.unidata.mdm.search.service.SearchService;
import org.unidata.mdm.search.type.form.FieldsGroup;
import org.unidata.mdm.search.type.form.FormField;
import org.unidata.mdm.search.type.query.SearchQuery;
import org.unidata.mdm.search.util.SearchUtils;
import org.unidata.mdm.system.service.TextService;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * @author Dmitry Kopin on 31.05.2019.
 */
@Component
public class LookupServiceImpl implements LookupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LookupServiceImpl.class);

    @Autowired
    private SearchService searchService;

    @Autowired
    private MetaModelService metaModelService;

    @Autowired
    private TextService textService;

    private static final String DISPLAY_VALUE_NOT_FOUND = "app.error.no.display.value";

    public LookupServiceImpl() {
        super();
    }

    private LoadingCache<LookupKey, SearchResultDTO> lookupSearchCache = CacheBuilder.newBuilder()
            .expireAfterWrite(3, TimeUnit.SECONDS)
            .build(new LookupSearchCacheLoader());

    @Override
    public Pair<String, String> getLookupDisplayNameById(String lookupName, Object codeAttrValue, Date validFrom, Date validTo, Collection<String> toBuildAttrs, boolean useAttributeNameForDisplay) {

        LookupElement lookup = metaModelService.instance(Descriptors.DATA)
            .getLookup(lookupName);

        Objects.requireNonNull(lookup, "Lookup element with name " + lookupName + " is null.");

        Collection<String> displayAttrs = CollectionUtils.isNotEmpty(toBuildAttrs)
                ? toBuildAttrs
                : lookup.getMainDisplayableAttributes().values().stream()
                        .map(AttributeElement::getName)
                        .collect(Collectors.toList());

        Map<String, AttributeElement> attrsMap = lookup.getAttributes();
        AttributeElement codeAttr = attrsMap.values().stream()
                .filter(AttributeElement::isCode)
                .findFirst()
                .orElse(null);

        if (Objects.isNull(codeAttr)) {
            return null;
        }

        try {

            SearchResultDTO searchResult = lookupSearchCache.get(new LookupKey(lookupName, codeAttr, codeAttrValue, displayAttrs));

            List<String> arrtValues = new ArrayList<>();
            SearchResultHitDTO hit = searchResult.getHits().stream()
                    .filter(r -> {
                        Date lookupValidFrom = SearchUtils.parse(r.getFieldFirstValue(RecordHeaderField.FIELD_FROM.getName()));
                        Date lookupValidTo = SearchUtils.parse(r.getFieldFirstValue(RecordHeaderField.FIELD_TO.getName()));
                        return ((validTo == null || lookupValidFrom == null || !lookupValidFrom.after(validTo))
                                && ((validFrom == null) || lookupValidTo == null || !lookupValidTo.before(validFrom)));
                    })
                    .findFirst()
                    .orElse(null);

            if (hit == null) {
                return Pair.of(null, textService.getText(DISPLAY_VALUE_NOT_FOUND));
            }

            for (String attr : displayAttrs) {
                SearchResultHitFieldDTO hf = hit.getFieldValue(attr);

                // UN-7814
                if (hf != null && !hf.isEmpty()) {

                    AttributeElement attrHolder = attrsMap.get(hf.getField());
                    String converted;
                    if (attrHolder != null && attrHolder.isArray()) {
                        converted = "[" + hf.getValues().subList(0, hf.getValues().size()).stream()
                                .filter(Objects::nonNull)
                                .map(Object::toString)
                                .collect(Collectors.joining(", ")) + "]";
                    } else {
                        converted = String.valueOf(hf.isCollection()
                                ? hf.getFirstValue() + " (" + String.join(", ", hf.getValues().subList(1, hf.getValues().size()).stream()
                                .filter(Objects::nonNull)
                                .map(Object::toString)
                                .collect(Collectors.toList())) + ")"
                                : hf.getFirstValue());
                        if (attrHolder.isMeasured()) {
                            converted += " " + metaModelService.instance(Descriptors.MEASUREMENT_UNITS)
                                    .getCategory(attrHolder.getMeasured().getCategoryId())
                                    .getUnit(attrHolder.getMeasured().getDefaultUnitId())
                                    .getDisplayName();
                        }

                    }

                    if (useAttributeNameForDisplay) {
                        converted = attrHolder != null
                                ? attrHolder.getDisplayName() + ": " + converted
                                : converted;
                    }

                    arrtValues.add(converted);
                }
            }

            return Pair.of(hit.getFieldFirstValue(RecordHeaderField.FIELD_ETALON_ID.getName()), String.join(StringUtils.SPACE, arrtValues));
        } catch (Exception e) {
            LOGGER.error("Can't get lookup display name", e);
            return Pair.of(null, textService.getText(DISPLAY_VALUE_NOT_FOUND));
        }
    }

    private class LookupSearchCacheLoader extends CacheLoader<LookupKey, SearchResultDTO> {

        /**
         * Load.
         *
         * @param ctx
         * @return search result
         * @throws Exception the exception
         */
        /*
         * (non-Javadoc)
         *
         * @see com.google.common.cache.CacheLoader#load(java.lang.Object)
         */
        @Override
        public SearchResultDTO load(LookupKey lookupKey) {

            FieldsGroup restrictions = FieldsGroup.and(
                    FormField.exact(lookupKey.codeAttr.getIndexed(), lookupKey.codeValue),
                    FormField.exact(RecordHeaderField.FIELD_PUBLISHED, true));

            SearchRequestContext ctx = SearchRequestContext.builder(EntityIndexType.RECORD, lookupKey.entityName)
                    .query(SearchQuery.formQuery(restrictions))
                    .returnFields(ListUtils.union(lookupKey.returnFields, Arrays.asList(
                            RecordHeaderField.FIELD_FROM.getName(),
                            RecordHeaderField.FIELD_TO.getName(),
                            RecordHeaderField.FIELD_ETALON_ID.getName(),
                            lookupKey.codeAttr.getPath())))
                    .count(100)
                    .build();

            return searchService.search(ctx);
        }
    }

    private class LookupKey {
        private String entityName;
        private AttributeElement codeAttr;
        private Object codeValue;
        private List<String> returnFields;

        public LookupKey(String entityName, AttributeElement codeAttr, Object codeValue, Collection<String> returnFields) {
            this.entityName = entityName;
            this.codeAttr = codeAttr;
            this.codeValue = codeValue;
            this.returnFields = new ArrayList<>(returnFields);
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
            result = prime * result + ((codeAttr == null) ? 0 : codeAttr.hashCode());
            result = prime * result + ((codeValue == null) ? 0 : codeValue.hashCode());
            result = prime * result + ((returnFields == null) ? 0 : returnFields.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            LookupKey other = (LookupKey) obj;
            if (entityName == null) {
                if (other.entityName != null) {
                    return false;
                }
            } else if (!entityName.equals(other.entityName)) {
                return false;
            }

            if (codeAttr == null) {
                if (other.codeAttr != null) {
                    return false;
                }
            } else if (!codeAttr.equals(other.codeAttr)) {
                return false;
            }

            if (codeValue == null) {
                if (other.codeValue != null) {
                    return false;
                }
            } else if (!codeValue.equals(other.codeValue)) {
                return false;
            }
            if (returnFields == null) {
                if (other.returnFields != null) {
                    return false;
                }
            } else if (!returnFields.equals(other.returnFields)) {
                return false;
            }

            return true;
        }
    }
}
