package org.unidata.mdm.meta.type.model.measurement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.meta.type.model.AbstractCustomPropertiesHolder;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@JsonPropertyOrder({
    "name",
    "displayName",
    "description"
})
public class MeasurementCategory extends AbstractCustomPropertiesHolder<MeasurementCategory> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -2803522282209941818L;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = MeasurementUnitsModel.NAMESPACE, localName = "unit")
    private List<MeasurementUnit> units;

    @JacksonXmlProperty(isAttribute = true, localName = "name")
    private String name;

    @JacksonXmlProperty(isAttribute = true, localName = "displayName")
    private String displayName;

    @JacksonXmlProperty(isAttribute = true, localName = "description")
    private String description;

    public List<MeasurementUnit> getUnits() {
        if (units == null) {
            units = new ArrayList<>();
        }
        return this.units;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public MeasurementCategory withUnits(MeasurementUnit... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            withUnits(Arrays.asList(values));
        }
        return this;
    }

    public MeasurementCategory withUnits(Collection<MeasurementUnit> values) {
        if (values!= null) {
            getUnits().addAll(values);
        }
        return this;
    }

    public MeasurementCategory withName(String value) {
        setName(value);
        return this;
    }

    public MeasurementCategory withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public MeasurementCategory withDescription(String value) {
        setDescription(value);
        return this;
    }
}
