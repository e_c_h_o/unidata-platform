/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class GetSourceSystemsContext extends AbstractModelGetContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -6245429588435518790L;
    /**
     * Requested source system ids.
     */
    private final List<String> sourceSystemIds;
    /**
     * Constructor.
     * @param b the builder.
     */
    private GetSourceSystemsContext(GetSourceSystemsContextBuilder b) {

        super(b);
        this.sourceSystemIds = Objects.isNull(b.sourceSystemIds) ? Collections.emptyList() : b.sourceSystemIds;

        setFlag(MetaContextFlags.FLAG_ADMIN_SOURCE_SYSTEM, b.adminSourceSystem);
        setFlag(MetaContextFlags.FLAG_ALL_SOURCE_SYSTEMS, b.allSourceSystems);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.SOURCE_SYSTEMS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // Source systems models are singletons per storage
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * Is admin SS requested?
     * @return request state
     */
    public boolean isAdminSourceSystem() {
        return getFlag(MetaContextFlags.FLAG_ADMIN_SOURCE_SYSTEM);
    }
    /**
     * All source systems requested?
     * @return request state
     */
    public boolean isAllSourceSystems() {
        return getFlag(MetaContextFlags.FLAG_ALL_SOURCE_SYSTEMS);
    }
    /**
     * @return the sourceSystemIds
     */
    public List<String> getSourceSystemIds() {
        return sourceSystemIds;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static GetSourceSystemsContextBuilder builder() {
        return new GetSourceSystemsContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class GetSourceSystemsContextBuilder extends AbstractModelGetContextBuilder<GetSourceSystemsContextBuilder> {
        /**
         * Requested source system ids.
         */
        private List<String> sourceSystemIds;
        /**
         * All source systems requested.
         */
        private boolean allSourceSystems;
        /**
         * Admin SS requested.
         */
        private boolean adminSourceSystem;
        /**
         * Constructor.
         */
        protected GetSourceSystemsContextBuilder() {
            super();
        }
        /**
         * @param sourceSystemIds the sourceSystemIds to set
         */
        public GetSourceSystemsContextBuilder sourceSystemIds(List<String> sourceSystemIds) {
            this.sourceSystemIds = sourceSystemIds;
            return self();
        }
        /**
         * @param allSourceSystems the allSourceSystems to set
         */
        public GetSourceSystemsContextBuilder allSourceSystems(boolean allSourceSystems) {
            this.allSourceSystems = allSourceSystems;
            return self();
        }
        /**
         * @param adminSourceSystem the adminSourceSystem to set
         */
        public GetSourceSystemsContextBuilder adminSourceSystem(boolean adminSourceSystem) {
            this.adminSourceSystem = adminSourceSystem;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public GetSourceSystemsContext build() {
            return new GetSourceSystemsContext(this);
        }


    }
}
