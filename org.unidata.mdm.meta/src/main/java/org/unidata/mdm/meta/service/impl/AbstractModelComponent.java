package org.unidata.mdm.meta.service.impl;

import org.unidata.mdm.core.context.AbstractModelChangeContext;
import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.core.type.model.ModelInstance;
import org.unidata.mdm.meta.dto.AbstractModelResult;
import org.unidata.mdm.meta.type.model.Model;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
public class AbstractModelComponent {
    /**
     * Constructor.
     */
    public AbstractModelComponent() {
        super();
    }
    /**
     * Sets info fields, common to all models.
     * @param target the target model
     * @param change the change source
     */
    protected void processInfoFields(Model<?> target, AbstractModelChangeContext change) {

        if (change.hasNameSet()) {
            target.withName(change.getName());
        }

        if (change.hasDescriptionSet()) {
            target.withDescription(change.getDescription());
        }

        if (change.hasDisplayNameSet()) {
            target.withDisplayName(change.getDisplayName());
        }
    }

    protected void processInfoFields(ModelInstance<?> i, AbstractModelGetContext get, AbstractModelResult result) {

        if (get.isModelInfo()) {
            result.setDescription(i.getDescription());
            result.setDisplayName(i.getDisplayName());
            result.setName(i.getName());
            result.setVersion(i.getVersion());
        }
    }
}
