/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.impl.data.refresh;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.context.ModelRefreshContext;
import org.unidata.mdm.core.context.PreviousModelStateContext;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.dao.DataDAO;
import org.unidata.mdm.meta.po.DataPO;
import org.unidata.mdm.meta.serialization.MetaSerializer;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.service.impl.ModelRefreshListener;
import org.unidata.mdm.meta.service.impl.data.instance.DataModelInstanceImpl;
import org.unidata.mdm.meta.type.instance.DataModelInstance;

/**
 * @author Mikhail Mikhailov on Nov 2, 2020
 */
public abstract class AbstractDataModelRefreshListener implements ModelRefreshListener {
    /**
     * Models DAO.
     */
    @Autowired
    protected DataDAO modelsDAO;
    /**
     * The MMS.
     */
    @Autowired
    protected MetaModelService metaModelService;
    /**
     * Constructor.
     */
    protected AbstractDataModelRefreshListener() {
        super();
    }

    protected DataModelInstance getPreviousState(ModelRefreshContext ctx) {

        DataModelInstance p = null;
        if (ctx instanceof PreviousModelStateContext) {
            p = ((PreviousModelStateContext) ctx).prevousState();
        }

        if (Objects.isNull(p)) {

            String storageId = SecurityUtils.getStorageId(ctx);
            DataModelInstance fresh = metaModelService.instance(Descriptors.DATA, storageId, null);

            if (fresh.getVersion() == 1) {
                return null;
            }

            DataPO prev = modelsDAO.previous(storageId, fresh.getVersion() - 1);
            p = new DataModelInstanceImpl(
                    MetaSerializer.modelFromCompressedXml(prev.getContent()),
                    metaModelService.instance(Descriptors.SOURCE_SYSTEMS));

            if (ctx instanceof PreviousModelStateContext) {
                ((PreviousModelStateContext) ctx).prevousState(p);
            }
        }

        return p;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.DATA_MODEL;
    }
}
