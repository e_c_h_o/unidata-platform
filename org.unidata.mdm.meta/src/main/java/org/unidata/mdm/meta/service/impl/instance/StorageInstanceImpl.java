package org.unidata.mdm.meta.service.impl.instance;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.unidata.mdm.core.type.model.StorageInstance;
import org.unidata.mdm.core.type.model.StorageElement;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.type.model.Storage;

/**
 * @author Mikhail Mikhailov on Oct 5, 2020
 */
public class StorageInstanceImpl implements StorageInstance {
    /**
     * The storages.
     */
    private final Map<String, StorageElement> storages;
    /**
     * Constructor.
     */
    public StorageInstanceImpl(Collection<Storage> storages) {
        super();
        this.storages = storages.stream().collect(Collectors.toMap(Storage::getStorageId, Function.identity()));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<StorageElement> getActive() {
        return storages.values();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public StorageElement getStorageForCurrentUser() {
        return storages.get(SecurityUtils.getCurrentUserStorageId());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public StorageElement getStorageById(String storageId) {
        return storages.get(storageId);
    }
}
