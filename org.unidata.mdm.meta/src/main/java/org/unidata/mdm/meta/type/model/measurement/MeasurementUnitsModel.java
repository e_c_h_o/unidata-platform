package org.unidata.mdm.meta.type.model.measurement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.util.ModelUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "measurement-units", namespace = MeasurementUnitsModel.NAMESPACE)
public class MeasurementUnitsModel extends Model<MeasurementUnitsModel> implements ModelSource {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 7238376552602108997L;
    /**
     * The NS.
     */
    public static final String NAMESPACE = "http://measurement-units.mdm.unidata.org/";

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(namespace = NAMESPACE, localName = "category")
    private List<MeasurementCategory> values;

    public List<MeasurementCategory> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return this.values;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.MEASUREMENT_UNITS_MODEL;
    }

    public MeasurementUnitsModel withValues(MeasurementCategory... values) {
        if (values!= null) {
            Collections.addAll(getValues(), values);
        }
        return this;
    }

    public MeasurementUnitsModel withValues(Collection<MeasurementCategory> values) {
        if (values!= null) {
            getValues().addAll(values);
        }
        return this;
    }
}
