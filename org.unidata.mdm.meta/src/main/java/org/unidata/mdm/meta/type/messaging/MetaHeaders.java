package org.unidata.mdm.meta.type.messaging;

import org.unidata.mdm.system.type.messaging.Header;
import org.unidata.mdm.system.type.messaging.Header.HeaderType;

/**
 * @author Mikhail Mikhailov on Jul 15, 2020
 */
public class MetaHeaders {
    /**
     * Constructor.
     */
    private MetaHeaders() {
        super();
    }
    /**
     * Role name.
     */
    public static final Header CONTEXT = new Header("context", HeaderType.OBJECT);
}
