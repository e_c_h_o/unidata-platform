/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.core.context.AbstractModelGetContext;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.service.segments.ModelGetStartExecutor;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
public class GetMeasurementUnitsContext extends AbstractModelGetContext {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 8179228975022535916L;
    /**
     * Requested category ids.
     */
    private final List<String> categoryIds;
    /**
     * Constructor.
     * @param b the builder.
     */
    private GetMeasurementUnitsContext(GetMeasurementUnitsContextBuilder b) {
        super(b);
        this.categoryIds = Objects.isNull(b.categoryIds) ? Collections.emptyList() : b.categoryIds;

        setFlag(MetaContextFlags.FLAG_ALL_MEASUREMENT_UNIT_CATEGORIES, b.allCategories);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.MEASUREMENT_UNITS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        // Enumerations models are singletons per storage
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelGetStartExecutor.SEGMENT_ID;
    }
    /**
     * All categories requested?
     * @return request state
     */
    public boolean isAllCategories() {
        return getFlag(MetaContextFlags.FLAG_ALL_MEASUREMENT_UNIT_CATEGORIES);
    }
    /**
     * @return the categoryIds
     */
    public List<String> getCategoryIds() {
        return categoryIds;
    }
    /**
     * Gets a builder instance.
     * @return builder instance
     */
    public static GetMeasurementUnitsContextBuilder builder() {
        return new GetMeasurementUnitsContextBuilder();
    }
    /**
     * The builder for this context.
     * @author Mikhail Mikhailov on Nov 28, 2019
     */
    public static class GetMeasurementUnitsContextBuilder extends AbstractModelGetContextBuilder<GetMeasurementUnitsContextBuilder> {
        /**
         * Requested enumeration ids.
         */
        private List<String> categoryIds;
        /**
         * All enumerations requested.
         */
        private boolean allCategories;
        /**
         * Constructor.
         */
        protected GetMeasurementUnitsContextBuilder() {
            super();
        }
        /**
         * @param categoryIds the categoryIds to set
         */
        public GetMeasurementUnitsContextBuilder categoryIds(List<String> categoryIds) {
            this.categoryIds = categoryIds;
            return self();
        }
        /**
         * @param allCategories the allCategories to set
         */
        public GetMeasurementUnitsContextBuilder allCategories(boolean allCategories) {
            this.allCategories = allCategories;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public GetMeasurementUnitsContext build() {
            return new GetMeasurementUnitsContext(this);
        }
    }
}
