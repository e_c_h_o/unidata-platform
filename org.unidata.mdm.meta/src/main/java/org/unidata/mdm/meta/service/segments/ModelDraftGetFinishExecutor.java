/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.segments;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.context.DraftGetContext;
import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.Edition;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.meta.service.impl.data.DataModelComponent;
import org.unidata.mdm.meta.service.impl.data.instance.DataModelInstanceImpl;
import org.unidata.mdm.meta.type.instance.DataModelInstance;
import org.unidata.mdm.meta.type.model.DataModel;
import org.unidata.mdm.system.type.pipeline.Finish;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author Mikhail Mikhailov on Nov 28, 2019
 */
@Component(ModelDraftGetFinishExecutor.SEGMENT_ID)
public class ModelDraftGetFinishExecutor extends Finish<DraftGetContext, DraftGetResult> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MetaModule.MODULE_ID + "[MODEL_DRAFT_GET_FINISH]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MetaModule.MODULE_ID + ".model.get.finish.description";
    /**
     * MMS. Cheap and dirty.
     */
    @Autowired
    private MetaModelService metaModelService;
    /**
     * The data model component.
     */
    @Autowired
    private DataModelComponent dataModelComponent;
    /**
     * Constructor.
     */
    public ModelDraftGetFinishExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftGetResult.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public DraftGetResult finish(DraftGetContext ctx) {

        Draft draft = ctx.currentDraft();
        Edition current = ctx.currentEdition();

        DataModel m =  Objects.isNull(current)
                ? new DataModel().withVersion(0)
                : current.getContent();

        DataModelInstance i = new DataModelInstanceImpl(m, metaModelService.instance(Descriptors.SOURCE_SYSTEMS));

        DraftGetResult result = new DraftGetResult();
        result.setDraft(draft);
        result.setPayload(dataModelComponent.disassemble(i, ctx.getPayload()));

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(Start<?> start) {
        return DraftGetContext.class.isAssignableFrom(start.getInputTypeClass());
    }
}
