package org.unidata.mdm.meta.type.model.sourcesystem;

import java.io.Serializable;

import org.unidata.mdm.meta.type.model.AbstractCustomPropertiesHolder;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class SourceSystem extends AbstractCustomPropertiesHolder<SourceSystem>  implements Serializable {

    private static final long serialVersionUID = 1L;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlProperty(isAttribute = true)
    protected Integer weight;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    @JacksonXmlProperty(isAttribute = true)
    protected boolean admin;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer value) {
        this.weight = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean value) {
        this.admin = value;
    }

    public SourceSystem withName(String value) {
        setName(value);
        return this;
    }

    public SourceSystem withWeight(Integer value) {
        setWeight(value);
        return this;
    }

    public SourceSystem withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public SourceSystem withAdmin(boolean value) {
        setAdmin(value);
        return this;
    }
}
