package org.unidata.mdm.meta.type.model.sourcesystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.core.type.model.ModelSource;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.type.model.Model;
import org.unidata.mdm.meta.util.ModelUtils;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * @author Mikhail Mikhailov on Oct 5, 2020
 */
@JacksonXmlRootElement(localName = "source-systems", namespace = SourceSystemsModel.NAMESPACE)
public class SourceSystemsModel extends Model<SourceSystemsModel> implements ModelSource {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 6148415242376791903L;
    /**
     * The NS.
     */
    public static final String NAMESPACE = "http://source-systems.mdm.unidata.org/";
    /**
     * Values.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "source-system")
    private List<SourceSystem> values;
    /**
     * Constructor.
     */
    public SourceSystemsModel() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "instanceId")
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @JacksonXmlProperty(isAttribute = true, localName = "typeId")
    @Override
    public String getTypeId() {
        return TypeIds.SOURCE_SYSTEMS_MODEL;
    }
    /**
     * @return the values
     */
    public List<SourceSystem> getValues() {
        if (values == null) {
            values = new ArrayList<>();
        }
        return values;
    }

    public SourceSystemsModel withValues(SourceSystem... values) {
        if (ArrayUtils.isNotEmpty(values)) {
            Collections.addAll(getValues(), values);
        }
        return this;
    }

    public SourceSystemsModel withValues(Collection<SourceSystem> values) {
        if (CollectionUtils.isNotEmpty(values)) {
            getValues().addAll(values);
        }
        return this;
    }
}
