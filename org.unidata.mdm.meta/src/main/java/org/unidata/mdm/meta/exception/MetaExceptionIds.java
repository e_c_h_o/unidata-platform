/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Mikhail Mikhailov on Oct 3, 2019
 */
public final class MetaExceptionIds {
    /**
     * JAXB context init failure.
     */
    public static final ExceptionId EX_META_JAXB_CONTEXT_INIT_FAILURE
        = new ExceptionId("EX_META_JAXB_CONTEXT_INIT_FAILURE", "app.meta.jaxb.context.init.failure");
    /**
     * Metadata insert of an object failed.
     */
    public static final ExceptionId EX_META_INSERT_FAILED
        = new ExceptionId("EX_META_INSERT_FAILED", "app.meta.metadataInsertFailed");
    /**
     * Metadata update of an object failed.
     */
    public static final ExceptionId EX_META_UPDATE_FAILED
        = new ExceptionId("EX_META_UPDATE_FAILED", "app.meta.metadataUpdateFailed");
    /**
     * The 'to' side containment entity '{}' of the relation '{}' not found in model.
     */
    public static final ExceptionId EX_META_TO_CONTAINMENT_ENTITY_NOT_FOUND_INIT
        = new ExceptionId("EX_META_TO_CONTAINMENT_ENTITY_NOT_FOUND_INIT", "app.meta.to.containment.entity.not.found.init");

    public static final ExceptionId EX_META_NESTED_ENTITIES_DUPLICATE2
        = new ExceptionId("EX_META_NESTED_ENTITIES_DUPLICATE2", "app.meta.nestedEntitiesIsDuplicated2");

    public static final ExceptionId EX_META_IS_INCORRECT
        = new ExceptionId("EX_META_IS_INCORRECT", "app.meta.incorrect");

    public static final ExceptionId EX_META_IMPORT_MODEL_EL_DUPLICATE
        = new ExceptionId("EX_META_IMPORT_MODEL_EL_DUPLICATE", "app.meta.import.elDuplicate");

    public static final ExceptionId EX_MEASUREMENT_UNITS_IDS_DUPLICATED
        = new ExceptionId("EX_MEASUREMENT_UNITS_IDS_DUPLICATED", "app.measurement.unit.ids.duplicated");

    public static final ExceptionId EX_MEASUREMENT_MERGE_IMPOSSIBLE_DIFFERENT_UNITS
        = new ExceptionId("EX_MEASUREMENT_MERGE_IMPOSSIBLE_DIFFERENT_UNITS", "app.measurement.merge.impossible.unit.is.different");

    public static final ExceptionId EX_MEASUREMENT_MERGE_IMPOSSIBLE_UNIT_WAS_REMOVED
        = new ExceptionId("EX_MEASUREMENT_MERGE_IMPOSSIBLE_UNIT_WAS_REMOVED", "app.measurement.merge.impossible.unit.was.removed");

    public static final ExceptionId EX_MEASUREMENT_MERGE_IMPOSSIBLE_UNIT_WAS_CHANGED
        = new ExceptionId("EX_MEASUREMENT_MERGE_IMPOSSIBLE_UNIT_WAS_CHANGED", "app.measurement.merge.impossible.unit.was.changed");

    public static final ExceptionId EX_MEASUREMENT_CONVERSION_FUNCTION_INCORRECT
        = new ExceptionId("EX_MEASUREMENT_CONVERSION_FUNCTION_INCORRECT", "app.measurement.conversion.function.incorrect");

    public static final ExceptionId EX_MEASUREMENT_REMOVING_FORBIDDEN
        = new ExceptionId("EX_MEASUREMENT_REMOVING_FORBIDDEN", "app.measurement.removing.forbidden");

    public static final ExceptionId EX_MEASUREMENT_BASE_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_BASE_IS_NOT_DEFINE", "app.measurement.base.not.define");

    public static final ExceptionId EX_MEASUREMENT_SOMEONE_ALREADY_REMOVE_VALUE
        = new ExceptionId("EX_MEASUREMENT_SOMEONE_ALREADY_REMOVE_VALUE", "app.measurement.value.removed");

    public static final ExceptionId EX_MEASUREMENT_DUPL_ID
        = new ExceptionId("EX_MEASUREMENT_DUPL_ID", "app.measurement.dupl.id");

    public static final ExceptionId EX_MEASUREMENT_VALUE_ID_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_VALUE_ID_IS_NOT_DEFINE", "app.measurement.value.id.not.define");

    public static final ExceptionId EX_MEASUREMENT_ID_INCORRECT_FOR_PATTERN
        = new ExceptionId("EX_MEASUREMENT_ID_INCORRECT_FOR_PATTERN", "app.measurement.id.incorrect.pattern");

    public static final ExceptionId EX_MEASUREMENT_VALUE_NAME_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_VALUE_NAME_IS_NOT_DEFINE", "app.measurement.value.name.not.define");

    public static final ExceptionId EX_MEASUREMENT_VALUE_SHORT_NAME_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_VALUE_SHORT_NAME_IS_NOT_DEFINE", "app.measurement.value.shortName.not.define");

    public static final ExceptionId EX_MEASUREMENT_BASE_UNIT_DUPL
        = new ExceptionId("EX_MEASUREMENT_BASE_UNIT_DUPL", "app.measurement.base.unit.dupl");

    public static final ExceptionId EX_MEASUREMENT_FUNCTION_SHOULD_BE_STANDARD
        = new ExceptionId("EX_MEASUREMENT_FUNCTION_SHOULD_BE_STANDARD", "app.measurement.base.conversion.function.incorrect");

    public static final ExceptionId EX_MEASUREMENT_UNIT_ID_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_UNIT_ID_IS_NOT_DEFINE", "app.measurement.unit.id.not.define");

    public static final ExceptionId EX_MEASUREMENT_UNIT_NAME_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_UNIT_NAME_IS_NOT_DEFINE", "app.measurement.unit.name.not.define");

    public static final ExceptionId EX_MEASUREMENT_UNIT_SHORT_NAME_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_UNIT_SHORT_NAME_IS_NOT_DEFINE", "app.measurement.unit.shortName.not.define");

    public static final ExceptionId EX_MEASUREMENT_UNIT_FUNCTION_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_UNIT_FUNCTION_IS_NOT_DEFINE", "app.measurement.unit.function.not.define");

    public static final ExceptionId EX_MEASUREMENT_UNIT_VALUE_ID_IS_NOT_DEFINE
        = new ExceptionId("EX_MEASUREMENT_UNIT_VALUE_ID_IS_NOT_DEFINE", "app.measurement.unit.valueId.not.define");

    public static final ExceptionId EX_MEASUREMENT_CONVERSION_FAILED
        = new ExceptionId("EX_MEASUREMENT_CONVERSION_FAILED", "app.measurement.conversion.failed");

    public static final ExceptionId EX_DRAFT_IS_INCORRECT
        = new ExceptionId("EX_DRAFT_IS_INCORRECT", "app.draft.incorrect");

    public static final ExceptionId EX_DRAFT_UNABLE_TO_CHANGE
        = new ExceptionId("EX_DRAFT_UNABLE_TO_CHANGE", "app.draft.unable.to.change");

    public static final ExceptionId EX_META_INIT_METADATA_FAILED
        = new ExceptionId("EX_META_INIT_METADATA_FAILED", "app.meta.metadataServiceFailedToInitilalize");

    public static final ExceptionId EX_META_ENTITY_NOT_FOUND
        = new ExceptionId("EX_META_ENTITY_NOT_FOUND", "app.meta.entityNotFound");
    /**
     * Interrupted caught.
     */
    public static final ExceptionId EX_MODEL_CREATE_INDEX_INTERRUPTED
        = new ExceptionId("EX_MODEL_CREATE_INDEX_INTERRUPTED", "app.error.while.creating.indexes.interrupted");
    /**
     * Nested entity not found in model.
     */
    public static final ExceptionId EX_META_MAPPING_NESTED_ENTITY_NOT_FOUND
        = new ExceptionId("EX_META_MAPPING_NESTED_ENTITY_NOT_FOUND", "app.search.mappingUnknownNestedEntity");
    /**
     * More than one root group found.
     */
    public static final ExceptionId EX_META_MORE_THEN_ONE_ROOT_GROUP
        = new ExceptionId("EX_META_MORE_THEN_ONE_ROOT_GROUP", "app.meta.more.then.one.root.group");
    /**
     * Not a sys init and no root group.
     */
    public static final ExceptionId EX_META_NOT_SYS_NO_ROOT_GROUP
        = new ExceptionId("EX_META_NOT_SYS_NO_ROOT_GROUP", "app.meta.not.sys.no.root.group");

    public static final ExceptionId EX_META_PIPELINE_LOADING_ERROR =
            new ExceptionId("EX_META_PIPELINE_LOADING_ERROR", "app.meta.pipeline.loading.error");

    public static final ExceptionId EX_META_IMPORT_MODEL_UNABLE_TO_PARSE =
            new ExceptionId("EX_META_IMPORT_MODEL_UNABLE_TO_PARSE", "app.meta.import.unableToParse");

    public static final ExceptionId EX_META_IMPORT_MODEL_EL_NOT_FOUND =
            new ExceptionId("EX_META_IMPORT_MODEL_EL_NOT_FOUND", "app.meta.import.elNotFound");

    public static final ExceptionId EX_META_IMPORT_MODEL_FILE_UNKNOWN_ONLY_DIRS =
            new ExceptionId("EX_META_IMPORT_MODEL_FILE_UNKNOWN_ONLY_DIRS", "app.meta.import.fileUnknownOnlyDirs");

    public static final ExceptionId EX_META_IMPORT_MODEL_FILE_STRUCTURE_INVALID =
            new ExceptionId("EX_META_IMPORT_MODEL_FILE_STRUCTURE_INVALID", "app.meta.import.fileStructureInvalid");

    public static final ExceptionId EX_META_CANNOT_ASSEMBLE_MODEL =
            new ExceptionId("EX_META_CANNOT_ASSEMBLE_MODEL", "app.meta.cannotAssembleModel");

    public static final ExceptionId EX_META_IMPORT_MODEL_FILE_DUPL_NOT_ALLOWED =
            new ExceptionId("EX_META_IMPORT_MODEL_FILE_DUPL_NOT_ALLOWED", "app.meta.import.fileDuplNotAllowed");

    public static final ExceptionId EX_META_IMPORT_MODEL_FILE_UNKNOWN =
            new ExceptionId("EX_META_IMPORT_MODEL_FILE_UNKNOWN", "app.meta.import.fileUnknown");

    public static final ExceptionId EX_META_CANNOT_APPLY_ATTRIBUTE_CONCAT_STRATEGY =
            new ExceptionId("EX_META_CANNOT_APPLY_ATTRIBUTE_CONCAT_STRATEGY", "app.meta.cannot.apply.attribute.concat.strategy");

    public static final ExceptionId EX_META_CANNOT_APPLY_ENTITY_CONCAT_STRATEGY =
            new ExceptionId("EX_META_CANNOT_APPLY_ENTITY_CONCAT_STRATEGY", "app.meta.cannot.apply.entity.concat.strategy");

    public static final ExceptionId EX_META_CANNOT_APPLY_ATTRIBUTE_CUSTOM_STRATEGY =
            new ExceptionId("EX_META_CANNOT_APPLY_ATTRIBUTE_CUSTOM_STRATEGY", "app.meta.cannot.apply.attribute.custom.strategy");

    public static final ExceptionId EX_META_CANNOT_APPLY_ENTITY_CUSTOM_STRATEGY =
            new ExceptionId("EX_META_CANNOT_APPLY_ENTITY_CUSTOM_STRATEGY", "app.meta.cannot.apply.entity.custom.strategy");

    public static final ExceptionId EX_META_SECURITY_RESOURCE_WRITE =
            new ExceptionId("EX_META_SECURITY_RESOURCE_WRITE", "todo");

    public static final ExceptionId EX_META_UPSERT_SOURCE_SYSTEMS_REVISION_EXISTS =
            new ExceptionId("EX_META_UPSERT_SOURCE_SYSTEMS_REVISION_EXISTS", "app.meta.upsert.source.systems.revision.exists");

    public static final ExceptionId EX_META_UPSERT_ENUMERATIONS_REVISION_EXISTS =
            new ExceptionId("EX_META_UPSERT_ENUMERATIONS_REVISION_EXISTS", "app.meta.upsert.enumerations.revision.exists");

    public static final ExceptionId EX_META_UPSERT_DATA_MODEL_REVISION_EXISTS =
            new ExceptionId("EX_META_UPSERT_DATA_MODEL_REVISION_EXISTS", "app.meta.upsert.data.model.revision.exists");

    public static final ExceptionId EX_META_UPSERT_MEASUREMENT_UNITS_REVISION_EXISTS =
            new ExceptionId("EX_META_UPSERT_MEASUREMENT_UNITS_REVISION_EXISTS", "app.meta.upsert.measurement.units.revision.exists");

    public static final ExceptionId EX_META_UPSERT_SOURCE_SYSTEMS_VALIDATION =
            new ExceptionId("EX_META_UPSERT_SOURCE_SYSTEMS_VALIDATION", "app.meta.upsert.source.systems.validation");

    public static final ExceptionId EX_META_UPSERT_ENUMERATIONS_VALIDATION =
            new ExceptionId("EX_META_UPSERT_ENUMERATIONS_VALIDATION", "app.meta.upsert.enumerations.validation");

    public static final ExceptionId EX_META_UPSERT_MEASUREMENT_UNITS_VALIDATION =
            new ExceptionId("EX_META_UPSERT_MEASUREMENT_UNITS_VALIDATION", "app.meta.upsert.measurement.units.validation");

    public static final ExceptionId EX_META_UPSERT_DATA_MODEL_VALIDATION =
            new ExceptionId("EX_META_UPSERT_DATA_MODEL_VALIDATION", "app.meta.upsert.data.model.validation");

    public static final ExceptionId EX_META_UPSERT_DATA_MODEL_CONSISTENCY =
            new ExceptionId("EX_META_UPSERT_DATA_MODEL_CONSISTENCY", "app.meta.upsert.data.model.consistency");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_CHANGE_WRONG_TYPE =
            new ExceptionId("EX_META_SOURCE_SYSTEMS_CHANGE_WRONG_TYPE", "app.meta.source.systems.change.wrong.type");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_GET_WRONG_TYPE =
            new ExceptionId("EX_META_SOURCE_SYSTEMS_GET_WRONG_TYPE", "app.meta.source.systems.get.wrong.type");

    public static final ExceptionId EX_META_ENUMERATIONS_CHANGE_WRONG_TYPE =
            new ExceptionId("EX_META_ENUMERATIONS_CHANGE_WRONG_TYPE", "app.meta.enumerations.change.wrong.type");

    public static final ExceptionId EX_META_ENUMERATIONS_GET_WRONG_TYPE =
            new ExceptionId("EX_META_ENUMERATIONS_GET_WRONG_TYPE", "app.meta.enumerations.get.wrong.type");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_SOURCE_WRONG_TYPE =
            new ExceptionId("EX_META_SOURCE_SYSTEMS_SOURCE_WRONG_TYPE", "app.meta.upsert.source.systems.source.wrong.type");

    public static final ExceptionId EX_META_ENUMERATIONS_SOURCE_WRONG_TYPE =
            new ExceptionId("EX_META_ENUMERATIONS_SOURCE_WRONG_TYPE", "app.meta.upsert.enumerations.source.wrong.type");

    public static final ExceptionId EX_META_DATA_MODEL_CHANGE_WRONG_TYPE =
            new ExceptionId("EX_META_DATA_MODEL_CHANGE_WRONG_TYPE", "app.meta.data.model.change.wrong.type");

    public static final ExceptionId EX_META_DATA_MODEL_PUBLISH_WRONG_TYPE =
            new ExceptionId("EX_META_DATA_MODEL_PUBLISH_WRONG_TYPE", "app.meta.data.model.publish.wrong.type");

    public static final ExceptionId EX_META_DATA_MODEL_GET_WRONG_TYPE =
            new ExceptionId("EX_META_DATA_MODEL_GET_WRONG_TYPE", "app.meta.data.model.get.wrong.type");

    public static final ExceptionId EX_META_DATA_MODEL_SOURCE_WRONG_TYPE =
            new ExceptionId("EX_META_DATA_MODEL_SOURCE_WRONG_TYPE", "app.meta.upsert.data.model.source.wrong.type");

    /**
     * Cannot compress enumerations.
     */
    public static final ExceptionId EX_META_CANNOT_COMPRESS_SOURCE_SYSTEMS =
            new ExceptionId("EX_META_CANNOT_COMPRESS_SOURCE_SYSTEMS", "app.meta.cannot.compress.source.systems");
    /**
     * Cannot uncompress source systems.
     */
    public static final ExceptionId EX_META_CANNOT_UNCOMPRESS_SOURCE_SYSTEMS
        = new ExceptionId("EX_META_CANNOT_UNCOMPRESS_SOURCE_SYSTEMS", "app.meta.cannot.uncompress.source.systems");
    /**
     * Cannot compress enumerations.
     */
    public static final ExceptionId EX_META_CANNOT_COMPRESS_ENUMERATIONS =
            new ExceptionId("EX_META_CANNOT_COMPRESS_ENUMERATIONS", "app.meta.cannot.compress.enumerations");
    /**
     * Cannot uncompress enumerations.
     */
    public static final ExceptionId EX_META_CANNOT_UNCOMPRESS_ENUMERATIONS
        = new ExceptionId("EX_META_CANNOT_UNCOMPRESS_ENUMERATIONS", "app.meta.cannot.uncompress.enumerations");
    /**
     * Cannot compress measurement units.
     */
    public static final ExceptionId EX_META_CANNOT_COMPRESS_MEASUREMENT_UNITS =
            new ExceptionId("EX_META_CANNOT_COMPRESS_MEASUREMENT_UNITS", "app.meta.cannot.compress.measurement.units");
    /**
     * Cannot uncompress measurement units.
     */
    public static final ExceptionId EX_META_CANNOT_UNCOMPRESS_MEASUREMENT_UNITS
        = new ExceptionId("EX_META_CANNOT_UNCOMPRESS_MEASUREMENT_UNITS", "app.meta.cannot.uncompress.measurement.units");
    /**
     * Cannot compress data.
     */
    public static final ExceptionId EX_META_CANNOT_COMPRESS_DATA =
            new ExceptionId("EX_META_CANNOT_COMPRESS_DATA", "app.meta.cannot.compress.data");
    /**
     * Cannot uncompress data.
     */
    public static final ExceptionId EX_META_CANNOT_UNCOMPRESS_DATA
        = new ExceptionId("EX_META_CANNOT_UNCOMPRESS_DATA", "app.meta.cannot.uncompress.data");
    /**
     * Constructor.
     */
    private MetaExceptionIds() {
        super();
    }

}
