/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.service.impl.enumerations.instance;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.unidata.mdm.core.type.model.EnumerationElement;
import org.unidata.mdm.core.type.model.NamedDisplayableElement;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.service.CustomPropertiesSupport;
import org.unidata.mdm.meta.service.impl.instance.AbstractModelInstanceImpl;
import org.unidata.mdm.meta.service.impl.instance.AbstractNamedDisplayableCustomPropertiesImpl;
import org.unidata.mdm.meta.service.impl.instance.AbstractNamedDisplayableImpl;
import org.unidata.mdm.meta.type.instance.EnumerationsInstance;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationType;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationValue;
import org.unidata.mdm.meta.type.model.enumeration.EnumerationsModel;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Oct 6, 2020
 * Enumerations holder.
 */
public class EnumerationsInstanceImpl extends AbstractModelInstanceImpl<EnumerationsModel> implements EnumerationsInstance, CustomPropertiesSupport {
    /**
     * Values.
     */
    private final Map<String, EnumerationElement> enumerations;
    /**
     * Constructor.
     */
    public EnumerationsInstanceImpl(EnumerationsModel ess) {
        super(ess);
        this.enumerations = ess.getValues().stream()
                .map(EnumerationImpl::new)
                .collect(Collectors.toMap(EnumerationImpl::getName, Function.identity()));
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.ENUMERATIONS_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return enumerations.isEmpty();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<EnumerationElement> getEnumerations() {
        return enumerations.values();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public EnumerationElement getEnumeration(String name) {
        return enumerations.get(name);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(String name) {
        return enumerations.containsKey(name);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public EnumerationsModel toSource() {
        return new EnumerationsModel()
            .withCreateDate(getCreateDate())
            .withCreatedBy(getCreatedBy())
            .withVersion(getVersion())
            .withValues(getEnumerations().stream()
                .map(ssi -> new EnumerationType()
                    .withName(ssi.getName())
                    .withDisplayName(ssi.getDisplayName())
                    .withCustomProperties(assembleCustomProperties(ssi.getCustomProperties()))
                    .withValues(ssi.getEnumerationValues().stream()
                        .map(v -> new EnumerationValue()
                                .withName(v.getName())
                                .withDisplayName(v.getDisplayName()))
                        .collect(Collectors.toList())))
                .collect(Collectors.toList()));
    }

    /**
     * @author Mikhail Mikhailov on Oct 6, 2020
     * Single enumeration.
     */
    public static class EnumerationImpl extends AbstractNamedDisplayableCustomPropertiesImpl implements EnumerationElement {
        /**
         * Entries.
         */
        private final Map<String, NamedDisplayableElement> entries;
        /**
         * The source.
         */
        private final EnumerationType source;
        /**
         * Constructor.
         * @param en the type
         */
        private EnumerationImpl(EnumerationType en) {
            super(en.getName(), en.getDisplayName(), en.getCustomProperties());
            this.source = en;
            this.entries = en.getValues().stream()
                    .map(v -> new EnumerationValueImpl(v.getName(), v.getDisplayName()))
                    .collect(Collectors.toMap(EnumerationValueImpl::getName, Function.identity()));
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public String getId() {
            return getName();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public Collection<NamedDisplayableElement> getEnumerationValues() {
            return entries.values();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean valueExists(String name) {
            return entries.containsKey(name);
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public NamedDisplayableElement getEnumerationValue(String name) {
            return entries.get(name);
        }
        /**
         * @return the source
         */
        public EnumerationType getSource() {
            return source;
        }
    }
    /**
     * @author Mikhail Mikhailov on Oct 7, 2020
     * Enumeration value
     */
    public static class EnumerationValueImpl extends AbstractNamedDisplayableImpl {
        /**
         * Constructor.
         * @param name
         * @param displayName
         */
        private EnumerationValueImpl(String name, String displayName) {
            super(name, displayName);
        }
    }
}
