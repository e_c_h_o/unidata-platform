/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 */
package org.unidata.mdm.meta.service.impl.data.instance;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.unidata.mdm.core.type.model.NestedElement;
import org.unidata.mdm.core.type.model.RegisterElement;
import org.unidata.mdm.meta.type.model.entities.NestedEntity;
import org.unidata.mdm.system.type.support.IdentityHashSet;

/**
 * @author Mikhail Mikhailov
 * Nested entities wrapper.
 */
public class NestedImpl extends AbstractAttributesImpl<NestedEntity>
    implements NestedElement {
    /**
     * Referencing registers.
     */
    private final Set<RegisterElement> referencingRegisters = new IdentityHashSet<>();
    /**
     * Constructor.
     * @param entity nested entity
     * @param refs nested references, needed for complex attributes
     */
    public NestedImpl(NestedEntity entity, List<NestedEntity> refs) {
        super(entity, refs);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return getName();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RegisterElement> getReferencingRegisters() {
        return referencingRegisters;
    }

    public void addReferencingRegister(RegisterElement e) {
        referencingRegisters.add(e);
    }
}
