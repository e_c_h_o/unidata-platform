package org.unidata.mdm.meta.service.segments;

import org.springframework.stereotype.Component;
import org.unidata.mdm.draft.context.DraftUpsertContext;
import org.unidata.mdm.meta.module.MetaModule;
import org.unidata.mdm.system.type.pipeline.Start;

/**
 * @author maria.chistyakova
 * @since  13.01.2020
 */
@Component(ModelDraftUpsertStartExecutor.SEGMENT_ID)
public class ModelDraftUpsertStartExecutor extends Start<DraftUpsertContext> {
    /**
     * This segment ID.
     */
    public static final String SEGMENT_ID = MetaModule.MODULE_ID + "[MODEL_DRAFT_UPSERT_START]";
    /**
     * Localized message code.
     */
    public static final String SEGMENT_DESCRIPTION = MetaModule.MODULE_ID + ".model.draft.upsert.start.description";
    /**
     * Constructor.
     */
    public ModelDraftUpsertStartExecutor() {
        super(SEGMENT_ID, SEGMENT_DESCRIPTION, DraftUpsertContext.class);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void start(DraftUpsertContext ctx) {
        // NOOP. Start does nothing here.
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String subject(DraftUpsertContext ctx) {
        // No subject for this type of pipelines
        // This may be storage id in the future
        return null;
    }
}
