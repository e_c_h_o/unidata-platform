/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.meta.type.input.meta;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.experimental.dag.DirectedAcyclicGraph;

/**
 * @author maria.chistyakova
 * @since 22.04.2020
 */
public class GroupGraph extends DirectedAcyclicGraph<GroupVertex, GroupEdge> {

    /**
     * The vertex map.
     */
    private Map<String, GroupVertex> vertexMap = new HashMap<>();

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new group graph.
     */
    public GroupGraph() {
        super(
                (sourceVertex, targetVertex) -> {
                    GroupEdge edge = new GroupEdge();
                    edge.setFrom(sourceVertex);
                    edge.setTo(targetVertex);
                    return edge;
                });
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jgrapht.experimental.dag.DirectedAcyclicGraph#addVertex(java.lang
     * .Object)
     */
    @Override
    public boolean addVertex(GroupVertex v) {
        vertexMap.put(v.getId(), v);
        return super.addVertex(v);
    }

    /**
     * Gets the vertex by id.
     *
     * @param id the id
     * @return the vertex by id
     */
    public GroupVertex getVertexById(String id) {
        return vertexMap.get(id);
    }
}
