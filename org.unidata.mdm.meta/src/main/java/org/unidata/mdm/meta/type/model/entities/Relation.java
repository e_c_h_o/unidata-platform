package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.meta.type.model.ValueGenerationStrategy;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Relation extends ComplexAttributesHolderEntity<Relation> implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -1300008093859687660L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private ValueGenerationStrategy externalIdGenerationStrategy;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> toEntityDefaultDisplayAttributes;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<String> toEntitySearchAttributes;

    @JacksonXmlProperty(isAttribute = true)
    private boolean useAttributeNameForDisplay;

    @JacksonXmlProperty(isAttribute = true)
    private String fromEntity;

    @JacksonXmlProperty(isAttribute = true)
    private String toEntity;

    @JacksonXmlProperty(isAttribute = true)
    private RelType relType;

    @JacksonXmlProperty(isAttribute = true)
    private boolean required;

    public ValueGenerationStrategy getExternalIdGenerationStrategy() {
        return externalIdGenerationStrategy;
    }

    public void setExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        this.externalIdGenerationStrategy = value;
    }

    public List<String> getToEntityDefaultDisplayAttributes() {
        if (toEntityDefaultDisplayAttributes == null) {
            toEntityDefaultDisplayAttributes = new ArrayList<>();
        }
        return this.toEntityDefaultDisplayAttributes;
    }

    public List<String> getToEntitySearchAttributes() {
        if (toEntitySearchAttributes == null) {
            toEntitySearchAttributes = new ArrayList<>();
        }
        return this.toEntitySearchAttributes;
    }

    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }

    public void setUseAttributeNameForDisplay(Boolean value) {
        this.useAttributeNameForDisplay = value;
    }

    public String getFromEntity() {
        return fromEntity;
    }

    public void setFromEntity(String value) {
        this.fromEntity = value;
    }

    public String getToEntity() {
        return toEntity;
    }

    public void setToEntity(String value) {
        this.toEntity = value;
    }

    public RelType getRelType() {
        return relType;
    }

    public void setRelType(RelType value) {
        this.relType = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(Boolean value) {
        this.required = value;
    }

    public Relation withExternalIdGenerationStrategy(ValueGenerationStrategy value) {
        setExternalIdGenerationStrategy(value);
        return this;
    }

    public Relation withToEntityDefaultDisplayAttributes(String... values) {
        if (values != null) {
            Collections.addAll(getToEntityDefaultDisplayAttributes(), values);
        }
        return this;
    }

    public Relation withToEntityDefaultDisplayAttributes(Collection<String> values) {
        if (values != null) {
            getToEntityDefaultDisplayAttributes().addAll(values);
        }
        return this;
    }

    public Relation withToEntitySearchAttributes(String... values) {
        if (values != null) {
            Collections.addAll(getToEntitySearchAttributes(), values);
        }
        return this;
    }

    public Relation withToEntitySearchAttributes(Collection<String> values) {
        if (values != null) {
            getToEntitySearchAttributes().addAll(values);
        }
        return this;
    }

    public Relation withUseAttributeNameForDisplay(Boolean value) {
        setUseAttributeNameForDisplay(value);
        return this;
    }

    public Relation withFromEntity(String value) {
        setFromEntity(value);
        return this;
    }

    public Relation withToEntity(String value) {
        setToEntity(value);
        return this;
    }

    public Relation withRelType(RelType value) {
        setRelType(value);
        return this;
    }

    public Relation withRequired(Boolean value) {
        setRequired(value);
        return this;
    }
}
