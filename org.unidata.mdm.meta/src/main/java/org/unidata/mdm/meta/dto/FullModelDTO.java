/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.dto;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Map;

import com.unidata.mdm.security.Security;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.unidata.mdm.meta.type.model.measurement.MeasurementUnitsModel;
import org.unidata.mdm.meta.type.model.DataModel;

/**
 * The Class FullModelDTO.
 * @author ilya.bykov
 */
public class FullModelDTO {

    /** The override. */
    private boolean override;
    /** The model. */
    private DataModel model;
    private String storageId;

    /** The measurement values. */
    private MeasurementUnitsModel measurementValues;

    private Map<Pair<String, String>, ByteBuffer> customCfs;

    private Security security;

    /**
     * Gets the model.
     *
     * @return the model
     */
    public DataModel getModel() {
        return model;
    }

    /**
     * With model.
     *
     * @param model
     *            the model
     * @return the full model DTO
     */
    public FullModelDTO withModel(DataModel model) {
        this.model = model;
        return this;
    }


    /**
     * Gets the measurement values.
     *
     * @return the measurement values
     */
    public MeasurementUnitsModel getMeasurementValues() {
        return measurementValues;
    }

    /**
     * With measurement values.
     *
     * @param measurementValues
     *            the measurement values
     * @return the full model DTO
     */
    public FullModelDTO withMeasurementValues(MeasurementUnitsModel measurementValues) {
        this.measurementValues = measurementValues;
        return this;
    }

    public Map<Pair<String, String>, ByteBuffer> getCustomCfs() {
        if (MapUtils.isEmpty(customCfs)) {
            return Collections.emptyMap();
        }
        return customCfs;
    }

    public FullModelDTO withCustomCfs(Map<Pair<String, String>, ByteBuffer> customCfs) {
        this.customCfs = customCfs;
        return this;
    }

    /**
     * Checks if is override.
     *
     * @return true, if is override
     */
    public boolean isOverride() {
        return override;
    }

    /**
     * Sets the override.
     *
     * @param override the new override
     */
    public void setOverride(boolean override) {
        this.override = override;
    }

    public String getStorageId() {
        return storageId;
    }

    public FullModelDTO withStorageId(String storageId) {
        this.storageId = storageId;
        return this;
    }

    public Security getSecurity() {
        return security;
    }

    public FullModelDTO withSecurity(final Security security) {
        this.security = security;
        return this;
    }
}
