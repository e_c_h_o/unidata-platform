package org.unidata.mdm.meta.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.util.ModelUtils;

/**
 * @author Mikhail Mikhailov on Dec 18, 2020
 */
public class GetSourceSystemsResult extends AbstractModelResult {
    /**
     * Source systems.
     */
    private List<GetSourceSystemResult> sourceSystems;
    /**
     * Constructor.
     */
    public GetSourceSystemsResult() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.SOURCE_SYSTEMS_MODEL;
    }
    /**
     * @return the sourceSystems
     */
    public List<GetSourceSystemResult> getSourceSystems() {
        return Objects.isNull(sourceSystems) ? Collections.emptyList() : sourceSystems;
    }
    /**
     * @param sourceSystems the sourceSystems to set
     */
    public void setSourceSystems(List<GetSourceSystemResult> sourceSystems) {
        this.sourceSystems = sourceSystems;
    }
}
