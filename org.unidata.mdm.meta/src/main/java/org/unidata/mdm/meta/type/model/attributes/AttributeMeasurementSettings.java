package org.unidata.mdm.meta.type.model.attributes;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AttributeMeasurementSettings implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = 6059227756784837727L;

    @JacksonXmlProperty(isAttribute = true)
    protected String categoryId;

    @JacksonXmlProperty(isAttribute = true)
    protected String defaultUnitId;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String value) {
        this.categoryId = value;
    }

    public String getDefaultUnitId() {
        return defaultUnitId;
    }

    public void setDefaultUnitId(String value) {
        this.defaultUnitId = value;
    }

    public AttributeMeasurementSettings withCategoryId(String value) {
        setCategoryId(value);
        return this;
    }

    public AttributeMeasurementSettings withDefaultUnitId(String value) {
        setDefaultUnitId(value);
        return this;
    }
}
