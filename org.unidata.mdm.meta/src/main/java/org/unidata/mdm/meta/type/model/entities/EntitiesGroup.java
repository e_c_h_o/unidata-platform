package org.unidata.mdm.meta.type.model.entities;

import static org.unidata.mdm.meta.type.model.ModelNamespace.META_MODEL_NAMESPACE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.unidata.mdm.meta.type.model.VersionedObject;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class EntitiesGroup extends VersionedObject<EntitiesGroup> {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -3721455282636798494L;

    @JacksonXmlProperty(namespace = META_MODEL_NAMESPACE)
    private List<EntitiesGroup> innerGroups;

    @JacksonXmlProperty(isAttribute = true)
    protected String name;

    @JacksonXmlProperty(isAttribute = true)
    protected String displayName;

    private transient List<String> mappedRegisters;

    private transient List<String> mappedLookups;

    public List<EntitiesGroup> getInnerGroups() {
        if (innerGroups == null) {
            innerGroups = new ArrayList<>();
        }
        return this.innerGroups;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the mappedRegisters
     */
    public List<String> getMappedRegisters() {
        if (mappedRegisters == null) {
            mappedRegisters = new ArrayList<>();
        }
        return mappedRegisters;
    }

    /**
     * @return the mappedLookups
     */
    public List<String> getMappedLookups() {
        if (mappedLookups == null) {
            mappedLookups = new ArrayList<>();
        }
        return mappedLookups;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    public EntitiesGroup withInnerGroups(EntitiesGroup... values) {
        if (values!= null) {
            Collections.addAll(getInnerGroups(), values);
        }
        return this;
    }

    public EntitiesGroup withInnerGroups(Collection<EntitiesGroup> values) {
        if (values!= null && !values.isEmpty()) {
            getInnerGroups().addAll(values);
        }
        return this;
    }

    public EntitiesGroup withMappedRegisters(String... values) {
        if (values!= null) {
            Collections.addAll(getMappedRegisters(), values);
        }
        return this;
    }

    public EntitiesGroup withMappedRegisters(Collection<String> values) {
        if (values!= null && !values.isEmpty()) {
            getMappedRegisters().addAll(values);
        }
        return this;
    }

    public EntitiesGroup withMappedLookups(String... values) {
        if (values!= null) {
            Collections.addAll(getMappedLookups(), values);
        }
        return this;
    }

    public EntitiesGroup withMappedLookups(Collection<String> values) {
        if (values!= null && !values.isEmpty()) {
            getMappedLookups().addAll(values);
        }
        return this;
    }

    public EntitiesGroup withName(String value) {
        setName(value);
        return this;
    }

    public EntitiesGroup withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }
}
