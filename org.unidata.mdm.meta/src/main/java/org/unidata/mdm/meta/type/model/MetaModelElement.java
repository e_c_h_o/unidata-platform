package org.unidata.mdm.meta.type.model;

import java.io.Serializable;
import java.util.List;

/**
 * Metamodel element
 *
 * @author Alexandr Serov
 * @since 27.08.2020
 **/
public interface MetaModelElement extends Serializable {

    /**
     * @return element name
     */
    String getName();

    /**
     * Set element name
     *
     * @param name element name
     */
    void setName(String name);

    /**
     * @return custom element properties
     */
    List<CustomProperty> getCustomProperties();

    /**
     * @param customProperties custom element properties
     */
    void setCustomProperties(List<CustomProperty> customProperties);


}
