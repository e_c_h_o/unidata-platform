/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.service.impl.data.validation;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.unidata.mdm.meta.service.CustomPropertiesSupport;
import org.unidata.mdm.meta.type.model.entities.Relation;
import org.unidata.mdm.system.exception.ValidationResult;

@Component
public class RelationValidaor extends AbstractDataModelElementValidator<Relation> implements CustomPropertiesSupport {

    private static final String RELATION_TO_SIDE_IS_ABSENT = "app.meta.relation.to.side.absent";

    private static final String RELATION_FROM_SIDE_IS_ABSENT = "app.meta.relation.from.side.absent";

    @Nonnull
    @Override
    public DataModelElementType getSupportedElementType() {
        return DataModelElementType.RELATION;
    }

    @Nullable
    @Override
    public String getModelElementId(@Nonnull Relation el) {
        return el.getName();
    }

    @Override
    public Collection<ValidationResult> checkElement(Relation el) {

        Collection<ValidationResult> errors = new ArrayList<>();

        errors.addAll(super.checkElement(el));
        errors.addAll(validateCustomProperties(el.getDisplayName(), el.getCustomProperties()));

        if (StringUtils.isBlank(el.getFromEntity())) {
            final String message = "Relation [{}] does not define the FROM side register name.";
            errors.add(new ValidationResult(message, RELATION_FROM_SIDE_IS_ABSENT, el.getDisplayName()));
        }

        if (StringUtils.isBlank(el.getToEntity())) {
            final String message = "Relation [{}] does not define the TO side register name.";
            errors.add(new ValidationResult(message, RELATION_TO_SIDE_IS_ABSENT, el.getDisplayName()));
        }

        el.getSimpleAttribute().forEach(attr -> errors.addAll(checkSimpleAttribute(attr, el.getDisplayName())));

        return errors;
    }
}
