package org.unidata.mdm.meta.configuration;

import java.util.Date;

import org.unidata.mdm.core.module.CoreModule;
import org.unidata.mdm.core.type.model.ValidityPeriodElement.Granularity;
import org.unidata.mdm.meta.util.ValidityPeriodUtils;
import org.unidata.mdm.system.type.configuration.ConfigurationProperty;
import org.unidata.mdm.system.util.ConvertUtils;

/**
 * Runtime properties for meta module.
 * @author Mikhail Mikhailov on Apr 20, 2020
 */
public final class MetaConfigurationProperty {
    /**
     * No instance constructor.
     */
    private MetaConfigurationProperty() {
        super();
    }
    /**
     * Global validity period start.
     */
    public static final ConfigurationProperty<Date> META_VALIDITY_PERIOD_START = ConfigurationProperty.custom(Date.class)
            .key(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_START)
            .groupKey(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .serializer(ConvertUtils::date2String)
            .deserializer(ConvertUtils::string2Date)
            .setter(ValidityPeriodUtils::setGlobalValidityPeriodStart)
            .required(false)
            .readOnly(false)
            .build();
    /**
     * Global validity period end.
     */
    public static final ConfigurationProperty<Date> META_VALIDITY_PERIOD_END = ConfigurationProperty.custom(Date.class)
            .key(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_END)
            .groupKey(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .serializer(ConvertUtils::date2String)
            .deserializer(ConvertUtils::string2Date)
            .setter(ValidityPeriodUtils::setGlobalValidityPeriodEnd)
            .required(false)
            .readOnly(false)
            .build();
    /**
     * Global validity period mode.
     */
    public static final ConfigurationProperty<Granularity> META_VALIDITY_PERIOD_MODE = ConfigurationProperty.custom(Granularity.class)
            .key(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_MODE)
            .groupKey(MetaConfigurationConstants.PROPERTY_VALIDITY_PERIOD_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .serializer(Granularity::name)
            .deserializer(Granularity::valueOf)
            .setter(ValidityPeriodUtils::setGlobalDateGranularityMode)
            .defaultValue(Granularity.DATE)
            .required(false)
            .readOnly(false)
            .build();
    /**
     * FIXME: Use
     * Entity shards.
     */
    public static final ConfigurationProperty<Long> META_INDEXING_ENTITY_SHARDS = ConfigurationProperty.integer()
            .key(MetaConfigurationConstants.PROPERTY_INDEXING_ENTITY_SHARDS)
            .groupKey(MetaConfigurationConstants.PROPERTY_INDEXING_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .defaultValue(Long.valueOf(1L))
            .required(false)
            .readOnly(false)
            .build();
    /**
     * FIXME: Use
     * Entity replicas.
     */
    public static final ConfigurationProperty<Long> META_INDEXING_ENTITY_REPLICAS = ConfigurationProperty.integer()
            .key(MetaConfigurationConstants.PROPERTY_INDEXING_ENTITY_REPLICAS)
            .groupKey(MetaConfigurationConstants.PROPERTY_INDEXING_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .defaultValue(Long.valueOf(0L))
            .required(false)
            .readOnly(false)
            .build();
    /**
     * FIXME: Use
     * Lookups shards.
     */
    public static final ConfigurationProperty<Long> META_INDEXING_LOOKUP_SHARDS = ConfigurationProperty.integer()
            .key(MetaConfigurationConstants.PROPERTY_INDEXING_LOOKUP_SHARDS)
            .groupKey(MetaConfigurationConstants.PROPERTY_INDEXING_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .defaultValue(Long.valueOf(1L))
            .required(false)
            .readOnly(false)
            .build();
    /**
     * FIXME: Use
     * Lookups replicas.
     */
    public static final ConfigurationProperty<Long> META_INDEXING_LOOKUP_REPLICAS = ConfigurationProperty.integer()
            .key(MetaConfigurationConstants.PROPERTY_INDEXING_LOOKUP_REPLICAS)
            .groupKey(MetaConfigurationConstants.PROPERTY_INDEXING_GROUP)
            .moduleId(CoreModule.MODULE_ID)
            .defaultValue(Long.valueOf(0L))
            .required(false)
            .readOnly(false)
            .build();
    /**
     * Values as array.
     */
    private static final ConfigurationProperty<?>[] VALUES = {
            META_VALIDITY_PERIOD_END,
            META_VALIDITY_PERIOD_MODE,
            META_VALIDITY_PERIOD_START,
            META_INDEXING_ENTITY_REPLICAS,
            META_INDEXING_ENTITY_SHARDS,
            META_INDEXING_LOOKUP_REPLICAS,
            META_INDEXING_LOOKUP_SHARDS
    };
    /**
     * Enum like array accessor.
     * @return array of values.
     */
    public static ConfigurationProperty<?>[] values() {
        return VALUES;
    }
}
