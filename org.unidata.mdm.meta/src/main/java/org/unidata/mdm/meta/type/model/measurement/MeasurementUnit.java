package org.unidata.mdm.meta.type.model.measurement;

import java.io.Serializable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class MeasurementUnit implements Serializable {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -3985634234968570363L;

    @JacksonXmlProperty(isAttribute = true)
    private String name;

    @JacksonXmlProperty(isAttribute = true)
    private String displayName;

    @JacksonXmlProperty(isAttribute = true)
    private String description;

    @JacksonXmlProperty(isAttribute = true)
    private boolean base;

    @JacksonXmlProperty(isAttribute = true)
    private String conversionFunction;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isBase() {
        return base;
    }

    public void setBase(boolean value) {
        this.base = value;
    }

    public String getConversionFunction() {
        return conversionFunction;
    }

    public void setConversionFunction(String value) {
        this.conversionFunction = value;
    }

    public MeasurementUnit withName(String value) {
        setName(value);
        return this;
    }

    public MeasurementUnit withDisplayName(String value) {
        setDisplayName(value);
        return this;
    }

    public MeasurementUnit withDescription(String value) {
        setDescription(value);
        return this;
    }

    public MeasurementUnit withBase(boolean value) {
        setBase(value);
        return this;
    }

    public MeasurementUnit withConversionFunction(String value) {
        setConversionFunction(value);
        return this;
    }
}
