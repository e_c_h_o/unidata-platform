/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.context;

import org.unidata.mdm.core.context.AbstractModelPublishContext;
import org.unidata.mdm.draft.context.DraftPublishContext;
import org.unidata.mdm.meta.configuration.TypeIds;
import org.unidata.mdm.meta.service.segments.ModelPublishStartExecutor;
import org.unidata.mdm.meta.util.ModelUtils;
import org.unidata.mdm.system.context.DraftAwareContext;
import org.unidata.mdm.system.type.pipeline.PipelineInput;

/**
 * @author maria.chistyakova
 * @since  18.12.2019
 * Publish model context - a wrapper around {@link DraftPublishContext}.
 */
public class PublishDataModelContext extends AbstractModelPublishContext implements DraftAwareContext, PipelineInput {
    /**
     * GSVUID.
     */
    private static final long serialVersionUID = -1603232354459892740L;
    /**
     * A possibly set draft id.
     */
    private final Long draftId;
    /**
     * A possibly set parent draft id.
     */
    private final Long parentDraftId;
    /**
     * Constructor.
     *
     * @param b
     */
    public PublishDataModelContext(PublishDataModelContextBuilder b) {
        super(b);
        this.draftId = b.draftId;
        this.parentDraftId = b.parentDraftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getDraftId() {
        return draftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Long getParentDraftId() {
        return parentDraftId;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getStartTypeId() {
        return ModelPublishStartExecutor.SEGMENT_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getTypeId() {
        return TypeIds.DATA_MODEL;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getInstanceId() {
        return ModelUtils.DEFAULT_MODEL_INSTANCE_ID;
    }
    /**
     * @return builder
     */
    public static PublishDataModelContextBuilder builder() {
        return new PublishDataModelContextBuilder();
    }
    /**
     * @author Mikhail Mikhailov on Oct 26, 2020
     * Publish model context - a wrapper around {@link DraftPublishContext}.
     */
    public static class PublishDataModelContextBuilder extends AbstractModelPublishContextBuilder<PublishDataModelContextBuilder> {
        /**
         * The draft id.
         */
        private Long draftId;
        /**
         * The parent draft id.
         */
        private Long parentDraftId;

        /**
         * Sets draft id
         * @param draftId the draft id
         * @return self
         */
        public PublishDataModelContextBuilder draftId(Long draftId) {
            this.draftId = draftId;
            return self();
        }
        /**
         * Sets parent draft id
         * @param parentDraftId the parent draft id
         * @return self
         */
        public PublishDataModelContextBuilder parentDraftId(Long parentDraftId) {
            this.parentDraftId = parentDraftId;
            return self();
        }
        /**
         * {@inheritDoc}
         */
        @Override
        public PublishDataModelContext build() {
            return new PublishDataModelContext(this);
        }
    }
}
