/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.unidata.mdm.meta.type.search;

import org.unidata.mdm.search.type.FieldType;
import org.unidata.mdm.search.type.IndexField;

/**
 * Header mark fields for indexed relation.
 */
public enum RelationHeaderField implements IndexField {
    /**
     * Type of relation {@link com.unidata.mdm.meta.RelType}
     */
    FIELD_RELATION_TYPE("$relation_type", FieldType.STRING),
    /**
     * User defined relation name
     */
    FIELD_RELATION_NAME("$relation_name", FieldType.STRING),
    /**
     * Left end of relation
     */
    FIELD_ETALON_ID("$etalon_id", FieldType.STRING),
    /**
     * Left end of relation
     */
    FIELD_FROM_ETALON_ID("$etalon_id_from", FieldType.STRING),
    /**
     * Right end of relation
     */
    FIELD_TO_ETALON_ID("$etalon_id_to", FieldType.STRING),
    /**
     * Period id.
     */
    FIELD_PERIOD_ID("$period_id", FieldType.STRING),
    /**
     * 'from' validity range mark.
     */
    FIELD_FROM("$from", FieldType.TIMESTAMP),
    /**
     * 'to' validity range mark
     */
    FIELD_TO("$to", FieldType.TIMESTAMP),
    /**
     * 'created_at' creation date
     */
    FIELD_CREATED_AT("$created_at", FieldType.TIMESTAMP),
    /**
     * 'updated_at' date of the last update
     */
    FIELD_UPDATED_AT("$updated_at", FieldType.TIMESTAMP),
    /**
     * 'created_by' creation date
     */
    FIELD_CREATED_BY("$created_by", FieldType.STRING),
    /**
     * 'updated_by' update date
     */
    FIELD_UPDATED_BY("$updated_by", FieldType.STRING),
    /**
     * Is pending mark.
     */
    FIELD_PENDING("$pending", FieldType.BOOLEAN),
    /**
     * Is published mark.
     */
    FIELD_PUBLISHED("$published", FieldType.BOOLEAN),
    /**
     * Is deleted mark.
     * The whole record is deleted.
     */
    FIELD_DELETED("$deleted", FieldType.BOOLEAN),
    /**
     * Is inactivity mark.
     * This period is inactive.
     */
    FIELD_INACTIVE("$inactive", FieldType.BOOLEAN),
    /**
     * Is direct index record or not.
     */
    FIELD_DIRECTION_FROM("$direct", FieldType.BOOLEAN),
    /**
     * operation type of vistory
     */
    FIELD_OPERATION_TYPE("$operation_type", FieldType.STRING),
    /**
     * Originator field.
     */
    FIELD_ORIGINATOR("$originator", FieldType.STRING);
    /**
     * The name.
     */
    private final String field;
    /**
     * The type.
     */
    private final FieldType type;
    /**
     * Constructor.
     * @param field
     */
    RelationHeaderField(String field, FieldType type) {
        this.field = field;
        this.type = type;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public FieldType getFieldType() {
        return type;
    }
    /**
     * @return the field
     */
    @Override
    public String getName() {
        return field;
    }
}
