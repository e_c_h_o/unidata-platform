/**
 * The sole search rest service.
 * @author Mikhail Mikhailov on Mar 5, 2020
 */
package org.unidata.mdm.rest.search.service;