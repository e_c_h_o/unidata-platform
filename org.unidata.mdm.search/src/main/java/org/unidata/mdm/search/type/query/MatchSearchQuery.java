package org.unidata.mdm.search.type.query;

import java.util.List;

import org.unidata.mdm.search.type.IndexField;

/**
 * The (multi)match FTS query.
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
public class MatchSearchQuery implements SearchQuery {
    /**
     * The text.
     */
    private final String text;
    /**
     * Search with regard to NL morphology.
     */
    private final boolean morhological;
    /**
     * Fields involved.
     */
    private final List<IndexField> fields;
    /**
     * Constructor.
     * @param text
     * @param fields
     */
    protected MatchSearchQuery(String text, List<IndexField> fields) {
        this(text, false, fields);
    }
    /**
     * Constructor.
     * @param text the query
     * @param morphological the morphology feature
     * @param fields search fields
     */
    protected MatchSearchQuery(String text, boolean morphological, List<IndexField> fields) {
        super();
        this.text = text;
        this.morhological = morphological;
        this.fields = fields;
    }
    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public SearchQueryType getType() {
        return SearchQueryType.MATCH;
    }
    /**
     * @return the fields
     */
    public List<IndexField> getFields() {
        return fields;
    }
    /**
     * @return the morhological
     */
    public boolean isMorhological() {
        return morhological;
    }
}
