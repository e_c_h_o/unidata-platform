package org.unidata.mdm.search.type.query;

import java.util.Arrays;
import java.util.List;

import org.unidata.mdm.search.type.IndexField;
import org.unidata.mdm.search.type.form.FieldsGroup;

/**
 * @author Mikhail Mikhailov on Feb 20, 2020
 */
public interface SearchQuery {
    /**
     * The query type.
     * @author Mikhail Mikhailov on Feb 20, 2020
     */
    public enum SearchQueryType {
        /**
         * Form. TERM/MATCH/CONST SCORE in ES.
         */
        FORM,
        /**
         * String. QSTRING in ES.
         */
        STRING,
        /**
         * Match. (Multi) match query in ES.
         */
        MATCH
    }
    /**
     * Creates {@link SearchQueryType#STRING} query.
     * @param query the query string
     * @return query
     */
    static StringSearchQuery stringQuery(String query) {
        return new StringSearchQuery(query);
    }
    /**
     * Creates {@link SearchQueryType#MATCH} query.
     * @param text the query string
     * @param fields the search fields
     * @return query
     */
    static MatchSearchQuery matchQuery(String text, IndexField...fields) {
        return matchQuery(text, false, Arrays.asList(fields));
    }
    /**
     * Creates {@link SearchQueryType#MATCH} query.
     * @param text the query string
     * @param morphological search with regard to language morphology
     * @param fields the search fields
     * @return query
     */
    static MatchSearchQuery matchQuery(String text, boolean morphological, IndexField...fields) {
        return matchQuery(text, morphological, Arrays.asList(fields));
    }
    /**
     * Creates {@link SearchQueryType#MATCH} query.
     * @param text the query string
     * @param morphological search with regard to language morphology
     * @param fields the search fields
     * @return query
     */
    static MatchSearchQuery matchQuery(String text, boolean morphological, List<IndexField> fields) {
        return new MatchSearchQuery(text, morphological, fields);
    }
    /**
     * Creates {@link SearchQueryType#FORM} query.
     * @param groups the field groups
     * @return query
     */
    static FormSearchQuery formQuery(FieldsGroup...groups) {
        return new FormSearchQuery(Arrays.asList(groups));
    }
    /**
     * Creates {@link SearchQueryType#FORM} query.
     * @param groups the field groups
     * @return query
     */
    static FormSearchQuery formQuery(List<FieldsGroup> groups) {
        return new FormSearchQuery(groups);
    }
    /**
     * Gets the type of this query.
     * @return query type
     */
    SearchQueryType getType();
}
