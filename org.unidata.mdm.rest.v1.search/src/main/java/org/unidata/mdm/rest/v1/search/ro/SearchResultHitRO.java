/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.v1.search.ro;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;

/**
 * @author Mikhail Mikhailov
 * DTO, implementing a search hit.
 */
public class SearchResultHitRO {

    /**
     * Object id. The object identifier, supplied by the DB during create.
     */
    private final String id;
    /**
     * Record status.
     */
    @JsonProperty(value = "status", required = true, defaultValue = "ACTIVE")
    @JsonInclude(content = Include.ALWAYS)
    private String status;
    /**
     * Preview map, containing search keys alone with their values as strings.
     */
    @JsonProperty(value = "preview", required = true, defaultValue = "[]")
    @JsonInclude(content = Include.ALWAYS)
    private final List<SearchResultHitFieldRO> preview = new ArrayList<SearchResultHitFieldRO>();
    
    /**
     * JSON object at whole as raw value.
     */
    @JsonProperty(value = "source", required = true, defaultValue = "{}")
    @JsonInclude(content = Include.ALWAYS)
    private Object source;

    /**
     * score for hit
     */
    @JsonProperty(required = false)
    private Float score;
    /**
     * Constructor.
     */
    public SearchResultHitRO(final String id) {
        super();
        this.id = id;
    }

    /**
     * Gets the object, found by search.
     * @return the source
     */
    @JsonRawValue
    public Object getSource() {
        return source;
    }

    /**
     * Sets the object, found by search.
     * @param source the source to set
     */
    public void setSource(Object source) {
        this.source = source;
    }

    /**
     * Gets the object's id.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the preview as key = value pairs.
     * @return the preview
     */
    public List<SearchResultHitFieldRO> getPreview() {
        return preview;
    }

    public String getStatus() {
        return status;
    }
    

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }
}
