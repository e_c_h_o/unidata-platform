package org.unidata.mdm.rest.v1.search.ro;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.Parameter;

/**
 * Search results
 *
 * @author Alexandr Serov
 * @since 07.12.2020
 **/
public class SearchResultRO {

    /**
     * Fields, participating in a query, if any.
     * Null means '_all'. If set, the same fields will be filled in the 'this.preview' field.
     */
    @JsonProperty
    @Parameter(description = "Fields, participating in a query, if any.")
    private List<String> fields;

    /**
     * Search hits.
     */
    @JsonProperty
    private final List<SearchResultHitRO> hits = new ArrayList<>();

    /**
     * Optional number of all potential hits.
     */
    @Parameter(description = "Optional number of all potential hits.")
    private long totalCount;

    /**
     * Optional total count limit (max window size).
     */
    @Parameter(description = "Optional total count limit (max window size).")
    private long totalCountLimit;

    @Parameter(description = "")
    private boolean hasRecords;

    /**
     * Max score for search
     */
    @Parameter(description = "Max score for search")
    private float maxScore;

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<SearchResultHitRO> getHits() {
        return hits;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalCountLimit() {
        return totalCountLimit;
    }

    public void setTotalCountLimit(long totalCountLimit) {
        this.totalCountLimit = totalCountLimit;
    }

    public boolean isHasRecords() {
        return hasRecords;
    }

    public void setHasRecords(boolean hasRecords) {
        this.hasRecords = hasRecords;
    }

    public float getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(float maxScore) {
        this.maxScore = maxScore;
    }
}
