package org.unidata.mdm.system.serialization.json;

/**
 * @author Mikhail Mikhailov on May 25, 2020
 */
public class ConnectorSegmentJS extends SegmentJS {
    /**
     * Connected start id.
     */
    private String connectedId;
    /**
     * Constructor.
     */
    public ConnectorSegmentJS() {
        super();
    }
    /**
     * Constructor.
     */
    public ConnectorSegmentJS(String connectedId) {
        super();
        this.connectedId = connectedId;
    }
    /**
     * @return the start
     */
    public String getConnectedId() {
        return connectedId;
    }
    /**
     * @param start the start to set
     */
    public void setConnectedId(String connectedId) {
        this.connectedId = connectedId;
    }
}
