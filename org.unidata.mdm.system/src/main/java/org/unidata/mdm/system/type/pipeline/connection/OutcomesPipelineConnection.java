package org.unidata.mdm.system.type.pipeline.connection;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.unidata.mdm.system.exception.PipelineException;
import org.unidata.mdm.system.exception.SystemExceptionIds;
import org.unidata.mdm.system.type.pipeline.Outcome;
import org.unidata.mdm.system.type.pipeline.Pipeline;
import org.unidata.mdm.system.type.pipeline.Segment;
import org.unidata.mdm.system.type.pipeline.Splitter;
import org.unidata.mdm.system.util.PipelineUtils;

/**
 * @author Mikhail Mikhailov on May 26, 2020
 */
public class OutcomesPipelineConnection extends PipelineConnection {
    /**
     * Collected outcomes.
     */
    protected Map<Outcome, Pipeline> outcomes;
    /**
     * Links to resolve.
     */
    protected Map<String, String> links;
    /**
     * Constructor.
     */
    protected OutcomesPipelineConnection() {
        super();
    }
    /**
     * @return the outcomes
     */
    public Map<Outcome, Pipeline> getOutcomes() {
        return Objects.isNull(outcomes) ? Collections.emptyMap() : outcomes;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public ConnectionType getConnectionType() {
        return ConnectionType.OUTCOME;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void connect(Segment segment) {

        if (isConnected()) {
            return;
        }

        if (MapUtils.isNotEmpty(links)) {

            Splitter<?, ?> s = (Splitter<?, ?>) segment;
            Outcome[] out = s.getOutcomes();
            Map<Outcome, Pipeline> collected = new HashMap<>(links.size());

            links.forEach((o, p) -> {

                String[] parts = PipelineUtils.fromSerializedPipelineId(p);
                if (ArrayUtils.isNotEmpty(parts)) {

                    String start = null, subject = null;
                    if (parts.length == 1) {
                        start = parts[0];
                    } else if (parts.length == 2) {
                        subject = parts[0];
                        start = parts[1];
                    }

                    Pipeline hit = PipelineUtils.findPipeline(start, subject);
                    throwPipelineNotFoundIfNull(hit, start, subject);

                    Outcome found = Arrays.stream(out)
                        .filter(e -> e.getName().equals(o))
                        .findFirst()
                        .orElseThrow(() -> new PipelineException("Outcome not found by name [{}].",
                                SystemExceptionIds.EX_PIPELINE_SPLITTER_OUTCOME_NOT_FOUND_BY_NAME, o));

                    collected.put(found, hit);
                }
            });

            throwIfSplitterAndPipelinesUnrelated(s, collected);

            setConnected(MapUtils.isNotEmpty(collected));
            if (isConnected()) {
                outcomes = collected;
                links.clear();
                links = null;
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> serialize() {

        if (!isConnected()) {
            return null;
        }

        return outcomes.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey().getName(),
                        entry -> PipelineUtils.toSerializablePipelineId(entry.getValue())));
    }
    /**
     * Throws if the splitter and outcomes are unbrelated (i. e. have different return types, mapped to unknown outcome labels etc.)
     * @param s the splitter instance
     * @param o the outcomes map
     */
    private void throwIfSplitterAndPipelinesUnrelated(Splitter<?, ?> s, Map<Outcome, Pipeline> o) {

        Set<Outcome> known = SetUtils.hashSet(s.getOutcomes());
        for (Entry<Outcome, Pipeline> entry : o.entrySet()) {

            if (!known.contains(entry.getKey())) {
                throw new PipelineException("Attempt to add an unrelated element [{}] to outcomes map.",
                        SystemExceptionIds.EX_PIPELINE_SPLITTER_OUTCOME_UNKNOWN, entry.getKey().getName());
            }

            Class<?> target = s.getOutputTypeClass();
            Class<?> wants = entry.getValue().getOutputTypeClass();

            if (!target.isAssignableFrom(wants)) {
                throw new PipelineException("Outcome pipeline and the splitter element do not match by return type [{} and {} resp.].",
                        SystemExceptionIds.EX_PIPELINE_SPLITTER_AND_PIPELINE_DONT_MATCH, wants.getName(), target.getName());
            }
        }
    }
}
