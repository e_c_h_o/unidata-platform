package org.unidata.mdm.system.type.pipeline;

/**
 * The splitter segment type.
 * @author Mikhail Mikhailov on May 24, 2020
 */
public abstract class Splitter<C extends PipelineInput, O extends PipelineOutput> extends Segment {
    /**
     * The exact output type class.
     */
    private final Class<O> outputTypeClass;
    /**
     * The outcomes, known to this splitter.
     */
    private final Outcome[] outcomes;
    /**
     * Constructor.
     * @param id
     * @param description
     */
    public Splitter(String id, String description, Class<O> outputTypeClass, Outcome... outcomes) {
        super(id, description);
        this.outputTypeClass = outputTypeClass;
        this.outcomes = outcomes;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public SegmentType getType() {
        return SegmentType.SPLITTER;
    }
    /**
     * @return the outputTypeClass
     */
    public Class<O> getOutputTypeClass() {
        return outputTypeClass;
    }
    /**
     * @return the outcomes
     */
    public Outcome[] getOutcomes() {
        return outcomes;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isBatched() {
        return false;
    }
    /**
     * Evaluates current context and defines the outcome direction.
     * @param ctx the context
     * @return the split direction
     */
    public abstract Outcome evaluate(C ctx);
}
