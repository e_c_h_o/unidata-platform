/*
 * Unidata Platform Community Edition
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 * This file is part of the Unidata Platform Community Edition software.
 *
 * Unidata Platform Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Unidata Platform Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.unidata.mdm.system.context;

import java.util.Objects;

/**
 * @author Mikhail Mikhailov on Oct 14, 2020
 * Contexts, that potentially participate in draft operations, can implement this interface and components,
 * serving those contexts can decide whether to start a draft operation or do it in some other way.
 */
public interface DraftAwareContext {
    /**
     * Gets draft id.
     * @return draft id
     */
    Long getDraftId();
    /**
     * Gets draft parent id.
     * @return the parent draft id
     */
    Long getParentDraftId();
    /**
     * Returns true, if this context defines a valid draftId field,
     * @return true, if this context defines a valid draftId field,
     */
    default boolean hasDraftId() {
        return Objects.nonNull(getDraftId()) && getDraftId() > 0;
    }
    /**
     * Returns true, if this context defines a valid parent draftId field,
     * @return true, if this context defines a valid parent draftId field,
     */
    default boolean hasParentDraftId() {
        return Objects.nonNull(getParentDraftId()) && getParentDraftId() > 0;
    }
    /**
     * Returns true, if one of the draft ids is set,
     * marking this context as capable for a draft operation.
     * @return true, if one of the draft ids is set
     */
    default boolean isDraftOperation() {
        return hasDraftId() || hasParentDraftId();
    }
}
