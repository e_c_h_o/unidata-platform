package org.unidata.mdm.system.dto;

/**
 * Execution result marker.
 * @author Mikhail Mikhailov on Jan 17, 2020
 */
public interface ExecutionResult {}
