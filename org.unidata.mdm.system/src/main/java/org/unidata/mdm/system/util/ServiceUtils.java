package org.unidata.mdm.system.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.unidata.mdm.system.configuration.SystemConfiguration;

/**
 * Get any service interface implementation singleton from context without having
 * to participate in Spring container.
 * @author Mikhail Mikhailov on Apr 18, 2020
 */
public final class ServiceUtils {
    /**
     * The logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtils.class);
    /**
     * The app context, that actually resoves requests.
     */
    private static ApplicationContext applicationContext;

    /**
     * No instances constructor.
     */
    private ServiceUtils() {
        super();
    }
    /**
     * Static init method.
     */
    public static void init() {
        try {
            applicationContext = SystemConfiguration.getApplicationContext();
        } catch (Exception exc) {
            LOGGER.warn("Filed AC GET. Exception caught.", exc);
        }
    }
    /**
     * Gets any registered implementation from app context.
     * @param <T> requested service type
     * @param klass the class
     * @return type singleton
     */
    // FIXME Kill this util and method. Doesn't work!
    public static <T> T service(Class<T> klass) {
        return applicationContext.getBean(klass);
    }
}
