create table if not exists drafts (
    id bigserial,
    parent_id bigint,
    provider varchar(128) not null,
    subject varchar(256) not null,
    owner varchar(256) not null,
    description text,
    tags text[],
    variables bytea,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_drafts_id primary key(id),
    constraint fk_drafts_parent_id foreign key (parent_id) references drafts (id) on delete cascade
);

create index if not exists ix_drafts_provider on drafts using hash (provider);
create index if not exists ix_drafts_subject on drafts using hash (subject);
create index if not exists ix_drafts_owner on drafts using hash (owner);
create index if not exists ix_drafts_parent_id on drafts using hash (parent_id);
create index if not exists ix_drafts_tags on drafts using gin (tags);

create table if not exists editions (
    draft_id bigint not null,
    revision integer not null,
    content bytea not null,
    created_by varchar(256) not null,
    create_date timestamptz not null default current_timestamp,
    constraint pk_editions_draft_id_revision primary key(draft_id, revision),
    constraint fk_drafts_editions_draft_id foreign key (draft_id) references drafts (id) on delete cascade
);
