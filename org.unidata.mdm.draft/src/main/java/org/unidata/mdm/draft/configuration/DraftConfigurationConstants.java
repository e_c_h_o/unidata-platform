package org.unidata.mdm.draft.configuration;

import org.unidata.mdm.draft.module.DraftModule;

/**
 * @author Mikhail Mikhailov on Sep 18, 2020
 * This module constants.
 */
public final class DraftConfigurationConstants {
    /**
     * Core data source properties prefix. A non - property, not managed.
     */
    public static final String PROPERTY_DRAFT_DATASOURCE_PREFIX = DraftModule.MODULE_ID + ".datasource.";
    /**
     * This module schema name.
     */
    public static final String DRAFT_SCHEMA_NAME = "org_unidata_mdm_draft";
    /**
     * This module log name.
     */
    public static final String DRAFT_LOG_NAME = "change_log";
    /**
     * Constructor.
     */
    private DraftConfigurationConstants() {
        super();
    }
}
