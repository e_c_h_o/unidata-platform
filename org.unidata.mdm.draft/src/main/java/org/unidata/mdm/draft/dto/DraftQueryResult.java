package org.unidata.mdm.draft.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.draft.type.Draft;

/**
 * @author Mikhail Mikhailov on Sep 16, 2020
 * Response to draft QUERY operations.
 */
public class DraftQueryResult {
    /**
     * Draft objects, fetched by given criteria.
     */
    private List<Draft> drafts;
    /**
     * Constructor.
     */
    public DraftQueryResult() {
        super();
    }
    /**
     * Constructor.
     */
    public DraftQueryResult(List<Draft> drafts) {
        super();
        setDrafts(drafts);
    }
    /**
     * @return the drafts
     */
    @Nonnull
    public List<Draft> getDrafts() {
        return Objects.isNull(drafts) ? Collections.emptyList() : drafts;
    }
    /**
     * @param drafts the drafts to set
     */
    public void setDrafts(List<Draft> drafts) {
        this.drafts = drafts;
    }
    /**
     * Has any drafts.
     * @return true if not empty, false otherwise
     */
    public boolean hasDrafts() {
        return CollectionUtils.isNotEmpty(getDrafts());
    }
}
