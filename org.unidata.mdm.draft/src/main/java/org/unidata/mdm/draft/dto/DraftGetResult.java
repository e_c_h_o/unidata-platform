package org.unidata.mdm.draft.dto;

import java.util.Objects;

import org.unidata.mdm.draft.type.Draft;
import org.unidata.mdm.draft.type.DraftPayloadResponse;
import org.unidata.mdm.system.dto.AbstractCompositeResult;
import org.unidata.mdm.system.type.pipeline.PipelineOutput;

/**
 * @author Mikhail Mikhailov on Sep 16, 2020
 * Response to drat GET requests.
 */
public class DraftGetResult extends AbstractCompositeResult implements PipelineOutput {
    /**
     * The draft.
     */
    private Draft draft;
    /**
     * Any payload, that this draft provider produces.
     */
    private DraftPayloadResponse payload;
    /**
     * Constructor.
     */
    public DraftGetResult() {
        super();
    }
    /**
     * Gets the draft object.
     * @return the draft
     */
    public Draft getDraft() {
        return draft;
    }
    /**
     * Sets the draft object.
     * @param draft the draft to set
     */
    public void setDraft(Draft draft) {
        this.draft = draft;
    }
    /**
     * Has draft set.
     * @return true if set, false otherwise
     */
    public boolean hasDraft() {
        return Objects.nonNull(draft);
    }
    /**
     * Gets the payload.
     * @return the payload
     */
    public DraftPayloadResponse getPayload() {
        return payload;
    }
    /**
     * @param payload the payload to set
     */
    public void setPayload(DraftPayloadResponse data) {
        this.payload = data;
    }
    /**
     * Narrows payload to a particular type.
     * @param <T> the type to narrow to
     * @return payload
     * @throws ClassCastException if you try to narrow the payload to a wrong type
     */
    @SuppressWarnings("unchecked")
    public <T extends DraftPayloadResponse> T narrow() {
        return (T) getPayload();
    }
    /**
     * Has payload set.
     * @return true if set, false otherwise
     */
    public boolean hasPayload() {
        return Objects.nonNull(payload);
    }
    /**
     * Returns true for empty response
     * @return true for empty response
     */
    public boolean isEmpty() {
        return !hasDraft() && !hasPayload();
    }
}
