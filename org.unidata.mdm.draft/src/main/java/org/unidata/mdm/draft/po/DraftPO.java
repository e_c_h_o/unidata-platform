package org.unidata.mdm.draft.po;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.unidata.mdm.system.type.variables.Variables;

/**
 * @author Mikhail Mikhailov on Sep 9, 2020
 */
public class DraftPO {
    /**
     * Table name.
     */
    public static final String TABLE_NAME = "drafts";
    /**
     * ID.
     */
    public static final String FIELD_ID = "id";
    /**
     * Parent ID.
     */
    public static final String FIELD_PARENT_ID = "parent_id";
    /**
     * Provider ID.
     */
    public static final String FIELD_PROVIDER = "provider";
    /**
     * The subject ID.
     */
    public static final String FIELD_SUBJECT = "subject";
    /**
     * The owner.
     */
    public static final String FIELD_OWNER = "owner";
    /**
     * General description.
     */
    public static final String FIELD_DESCRIPTION = "description";
    /**
     * Tags array.
     */
    public static final String FIELD_TAGS = "tags";
    /**
     * Variables.
     */
    public static final String FIELD_VARIABLES = "variables";
    /**
     * Create timestamp.
     */
    public static final String FIELD_CREATE_DATE = "create_date";
    /**
     * Created by.
     */
    public static final String FIELD_CREATED_BY = "created_by";
    /**
     * Update timestamp.
     */
    public static final String FIELD_UPDATE_DATE = "update_date";
    /**
     * Updated by.
     */
    public static final String FIELD_UPDATED_BY = "updated_by";
    /**
     * Number of revisions.
     */
    public static final String FIELD_EDITIONS_COUNT = "editions_count";
    /**
     * ID.
     */
    private Long id;
    /**
     * Parent ID.
     */
    private Long parentId;
    /**
     * Provider ID.
     */
    private String provider;
    /**
     * The subject ID.
     */
    private String subject;
    /**
     * The owner.
     */
    private String owner;
    /**
     * General description.
     */
    private String description;
    /**
     * Tags value.
     */
    private String[] tags;
    /**
     * User supplied named properties.
     */
    private Variables variables;
    /**
     * Create timestamp.
     */
    private Date createDate;
    /**
     * Owner.
     */
    private String createdBy;
    /**
     * Update timestamp.
     */
    private Date updateDate;
    /**
     * Updated by.
     */
    private String updatedBy;
    /**
     * The editions.
     */
    private List<EditionPO> editions;
    /**
     * Read only revcount.
     */
    private int editionsCount;
    /**
     * Constructor.
     */
    public DraftPO() {
        super();
    }
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * @return the parentId
     */
    public Long getParentId() {
        return parentId;
    }
    /**
     * @param parentId the parentId to set
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }
    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }
    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }
    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }
    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return tags
     */
    public String[] getTags() {
        return tags;
    }
    /**
     * Sets tags. Nulls allowed.
     * @param tags
     */
    public void setTags(String[] tags) {
        this.tags = tags;
    }
    /**
     * @return the properties
     */
    public Variables getVariables() {
        return variables;
    }
    /**
     * @param properties the properties to set
     */
    public void setVariables(Variables properties) {
        this.variables = properties;
    }
    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }
    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }
    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String owner) {
        this.createdBy = owner;
    }
    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }
    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    /**
     * @return the updatedBy
     */
    public String getUpdatedBy() {
        return updatedBy;
    }
    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    /**
     * @return the subjects
     */
    public List<EditionPO> getEditions() {
        return Objects.isNull(editions) ? Collections.emptyList() : editions;
    }
    /**
     * @param subjects the subjects to set
     */
    public void setEditions(List<EditionPO> subjects) {
        this.editions = subjects;
    }
    /**
     * @return the revisionsCount
     */
    public int getEditionsCount() {
        return editionsCount;
    }
    /**
     * @param editionsCount the editionsCount to set
     */
    public void setEditionsCount(int editionsCount) {
        this.editionsCount = editionsCount;
    }

}
