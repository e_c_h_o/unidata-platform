package org.unidata.mdm.draft.migration;

import org.unidata.mdm.system.util.ResourceUtils;

import nl.myndocs.database.migrator.MigrationScript;
import nl.myndocs.database.migrator.definition.Migration;

/**
 * Init schema.
 *
 * @author Mikhail Mikhailov
 * @since 17.07.2020
 **/
public class UN14594InitialDraftSchema implements MigrationScript {

    @Override
    public void migrate(Migration migration) {
        migration.raw()
            .sql(ResourceUtils.asStrings("classpath:/migration/UN-14594-initial-draft-schema.sql"))
            .save();
    }

    @Override
    public String migrationId() {
        return "UN-14594-initial-draft-schema";
    }

    @Override
    public String author() {
        return "mikhail.mikhailov";
    }

}
