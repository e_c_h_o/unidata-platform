package org.unidata.mdm.draft.type;

import org.unidata.mdm.draft.dto.DraftGetResult;
import org.unidata.mdm.draft.dto.DraftPublishResult;
import org.unidata.mdm.draft.dto.DraftUpsertResult;

/**
 * @author Mikhail Mikhailov on Sep 15, 2020 Operations, that are supported by
 *         draft module.
 */
public enum DraftOperation {
    /**
     * Upserts draft data (creates a new edition) and also creates a new draft
     * object at the very first call on subjectId.
     * The pipeline returns {@link DraftUpsertResult}.
     */
    UPSERT_DATA,
    /**
     * Gets draft data.
     * The pipeline returns {@link DraftGetResult}.
     */
    GET_DATA,
    /**
     * Does objects publication.
     * The pipeline returns {@link DraftPublishResult}.
     */
    PUBLISH_DATA
}
