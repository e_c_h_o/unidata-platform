/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.rest.v1.meta.ro.CustomPropertyRO;
import org.unidata.mdm.rest.v1.meta.ro.attributes.ArrayAttributeDefinitionRO;
import org.unidata.mdm.rest.v1.meta.ro.attributes.SimpleAttributeRO;
import org.unidata.mdm.rest.v1.meta.ro.references.ReferenceInfoRO;

/**
 *
 * @author Michael Yashin. Created on 29.05.2015.
 */
public class AbstractEntityRO {
    /**
     * List of simple attributes.
     */
    protected List<SimpleAttributeRO> simpleAttributes;
    /**
     * List of array attributes.
     */
    protected List<ArrayAttributeDefinitionRO> arrayAttributes;

    /** The entity dependency. */
    protected List<ReferenceInfoRO> entityDependency;

    /** The custom properties. */
    protected final List<CustomPropertyRO> customProperties = new ArrayList<>();
    /**
     * The name - [_a-zA-Z0-9].
     */
    protected String name;
    /**
     * Display name.
     */
    protected String displayName;
    /**
     * Description.
     */
    protected String description;
    /**
     * Display order.
     */
    protected int order;

    /**
     * Gets the simple attributes.
     *
     * @return the simple attributes
     */
    public List<SimpleAttributeRO> getSimpleAttributes() {
        if (simpleAttributes == null) {
            simpleAttributes = new ArrayList<>();
        }

        return simpleAttributes;
    }

    /**
     * Sets the simple attributes.
     *
     * @param simpleAttribute the new simple attributes
     */
    public void setSimpleAttributes(List<SimpleAttributeRO> simpleAttribute) {
        this.simpleAttributes = simpleAttribute;
    }

    /**
     * Gets the custom properties.
     *
     * @return the custom properties
     */
    public List<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * Sets the custom properties.
     *
     * @param customProperties the new custom properties
     */
    public void setCustomProperties(Collection<CustomPropertyRO> customProperties) {
        if (CollectionUtils.isEmpty(customProperties)) {
            return;
        }
        this.customProperties.clear();
        this.customProperties.addAll(customProperties);
    }

    /**
     * Adds the custom properties.
     *
     * @param customProperties the custom properties
     */
    public void addCustomProperties(Collection<CustomPropertyRO> customProperties) {
        if (CollectionUtils.isEmpty(customProperties)) {
            return;
        }
        this.customProperties.addAll(customProperties);
    }

    /**
     * Gets the array attributes.
     *
     * @return the array attributes
     */
    public List<ArrayAttributeDefinitionRO> getArrayAttributes() {
        if (arrayAttributes == null) {
            arrayAttributes = new ArrayList<>();
        }

        return arrayAttributes;
    }

    /**
     * Sets the array attributes.
     *
     * @param arrayAttributes the new array attributes
     */
    public void setArrayAttributes(List<ArrayAttributeDefinitionRO> arrayAttributes) {
        this.arrayAttributes = arrayAttributes;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the display name.
     *
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     *
     * @param displayName the new display name
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }


    /**
     * Gets the entity dependency.
     *
     * @return the entity dependency
     */
    public List<ReferenceInfoRO> getEntityDependency() {
        return entityDependency;
    }

    /**
     * Sets the entity dependency.
     *
     * @param entityDependency the new entity dependency
     */
    public void setEntityDependency(List<ReferenceInfoRO> entityDependency) {
        this.entityDependency = entityDependency;
    }

}
