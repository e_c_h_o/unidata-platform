/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.references;

import javax.annotation.Nonnull;

/**
 * Unique key of entity help identifying entity
 */
public class EntityReferenceRO implements UniqueReferenceRO {
    /**
     * The type.
     */
    public static final ReferenceType TYPE = new ReferenceType("ENTITY", "реестр");
    /**
     * entity name
     */
    @Nonnull
    private final String entityName;

    /**
     * constructor
     *
     * @param entityName - entity name
     */
    public EntityReferenceRO(@Nonnull String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return entity name
     */
    @Nonnull
    public String getEntityName() {
        return entityName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EntityReferenceRO)) return false;

        EntityReferenceRO that = (EntityReferenceRO) o;

        return entityName.equals(that.entityName);

    }

    @Override
    public int hashCode() {
        return entityName.hashCode();
    }

    @Override
    public ReferenceType keyType() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "{" +
                "entityName='" + entityName + '\'' +
                '}';
    }
}
