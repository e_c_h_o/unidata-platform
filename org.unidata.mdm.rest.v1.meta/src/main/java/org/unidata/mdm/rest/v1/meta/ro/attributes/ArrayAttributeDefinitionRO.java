/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.attributes;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.unidata.mdm.rest.v1.meta.ro.ArrayDataType;

/**
 * @author Mikhail Mikhailov
 * Array type definition.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArrayAttributeDefinitionRO extends AbstractAttributeRO {
    /**
     * Attribute is generally searchable.
     */
    private boolean searchable = false;
    /**
     * Attribute is generally displayable.
     */
    private boolean displayable = false;
    /**
     * The attribute is the main displayable attribute.
     */
    private boolean mainDisplayable = false;
    /**
     * Can be null or not.
     */
    private boolean nullable = true;
    /**
     * Mask.
     */
    private String mask;
    /**
     * Attribute order.
     */
    private int order;
    /**
     * Type of array values.
     */
    private ArrayDataType arrayDataType;
    /**
     * Possible lookup entity type.
     */
    private String lookupEntityType;
    /**
     * Type of code attribute values.
     */
    private ArrayDataType lookupEntityCodeAttributeType;
    /**
     * Alternative display attributes
     */
    private List<String> lookupEntityDisplayAttributes;
    /**
     * Alternative display attributes
     */
    private List<String> lookupEntitySearchAttributes;
    /**
     * Dictionary attribute values separated by delimiter
     */
    private String dictionaryDataType;
    /**
     * Show attr name or not.
     */
    private boolean useAttributeNameForDisplay;
    /**
     * Exchange separator.
     */
    private String exchangeSeparator;
    /**
     * This _STRING_ attribute supports morphological search.
     */
    protected boolean searchMorphologically;
    /**
     * This _STRING_ attribute supports case insensitive search.
     */
    protected boolean searchCaseInsensitive;
    /**
     * Constructor.
     */
    public ArrayAttributeDefinitionRO() {
        super();
    }
    /**
     * @return the searchMorphologically
     */
    public boolean isSearchMorphologically() {
        return searchMorphologically;
    }
    /**
     * @param searchMorphologically the searchMorphologically to set
     */
    public void setSearchMorphologically(boolean searchMorphologically) {
        this.searchMorphologically = searchMorphologically;
    }
    /**
     * @return the searchable
     */
    public boolean isSearchable() {
        return searchable;
    }
    /**
     * @param searchable the searchable to set
     */
    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }
    /**
     * @return the nullable
     */
    public boolean isNullable() {
        return nullable;
    }
    /**
     * @param nullable the nullable to set
     */
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }
    /**
     * @return the mask
     */
    public String getMask() {
        return mask;
    }
    /**
     * @param mask the mask to set
     */
    public void setMask(String mask) {
        this.mask = mask;
    }
    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }
    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }
    /**
     * @return the arrayDataType
     */
    public ArrayDataType getArrayDataType() {
        return arrayDataType;
    }
    /**
     * @param arrayDataType the arrayDataType to set
     */
    public void setArrayDataType(ArrayDataType arrayDataType) {
        this.arrayDataType = arrayDataType;
    }
    public String getLookupEntityType() {
        return lookupEntityType;
    }

    public void setLookupEntityType(String lookupEntityType) {
        this.lookupEntityType = lookupEntityType;
    }
    /**
     * @return the lookupEntityCodeAttributeType
     */
    public ArrayDataType getLookupEntityCodeAttributeType() {
        return lookupEntityCodeAttributeType;
    }
    /**
     * @param lookupEntityCodeAttributeType the lookupEntityCodeAttributeType to set
     */
    public void setLookupEntityCodeAttributeType(ArrayDataType lookupEntityCodeAttributeType) {
        this.lookupEntityCodeAttributeType = lookupEntityCodeAttributeType;
    }
    /**
     * @return the exchangeSeparator
     */
    public String getExchangeSeparator() {
        return exchangeSeparator;
    }
    /**
     * @param exchangeSeparator the exchangeSeparator to set
     */
    public void setExchangeSeparator(String exchangeSeparator) {
        this.exchangeSeparator = exchangeSeparator;
    }

    public List<String> getLookupEntityDisplayAttributes() {
        return lookupEntityDisplayAttributes;
    }

    public void setLookupEntityDisplayAttributes(List<String> lookupEntityDisplayAttributes) {
        this.lookupEntityDisplayAttributes = lookupEntityDisplayAttributes;
    }

    public List<String> getLookupEntitySearchAttributes() {
        return lookupEntitySearchAttributes;
    }

    public void setLookupEntitySearchAttributes(List<String> lookupEntitySearchAttributes) {
        this.lookupEntitySearchAttributes = lookupEntitySearchAttributes;
    }

    public String getDictionaryDataType() {
        return dictionaryDataType;
    }

    public void setDictionaryDataType(String dictionaryDataType) {
        this.dictionaryDataType = dictionaryDataType;
    }

    /**
     * @return the useAttributeNameForDisplay
     */
    public boolean isUseAttributeNameForDisplay() {
        return useAttributeNameForDisplay;
    }
    /**
     * @param useAttributeNameForDisplay the useAttributeNameForDisplay to set
     */
    public void setUseAttributeNameForDisplay(boolean useAttributeNameForDisplay) {
        this.useAttributeNameForDisplay = useAttributeNameForDisplay;
    }

    /**
     * This _STRING_ attribute supports case insensitive search.
     */
    public boolean isSearchCaseInsensitive() {
        return searchCaseInsensitive;
    }

    public void setSearchCaseInsensitive(boolean searchCaseInsensitive) {
        this.searchCaseInsensitive = searchCaseInsensitive;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }

    public boolean isMainDisplayable() {
        return mainDisplayable;
    }

    public void setMainDisplayable(boolean mainDisplayable) {
        this.mainDisplayable = mainDisplayable;
    }
}
