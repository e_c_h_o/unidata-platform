package org.unidata.mdm.rest.v1.meta.ro.model;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Publish Model Result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class PublishModelResultRO extends DetailedOutputRO {

}
