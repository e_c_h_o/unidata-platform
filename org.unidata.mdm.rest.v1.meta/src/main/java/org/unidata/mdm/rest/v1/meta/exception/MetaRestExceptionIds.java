/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.exception;

import org.unidata.mdm.system.exception.ExceptionId;

/**
 * @author Alexander Malyshev
 */
public final class MetaRestExceptionIds {
    private MetaRestExceptionIds() {
    }

    public static final ExceptionId EX_META_DATA_GROUP_NAME_OR_TITLE_ABSENT =
        new ExceptionId("EX_META_DATA_GROUP_NAME_OR_TITLE_ABSENT", "app.meta.data.group.name.or.title.absent");

    public static final ExceptionId EX_META_DATA_ENTITY_NOT_FOUND =
        new ExceptionId("EX_META_DATA_ENTITY_NOT_FOUND", "app.meta.data.entity.not.found");

    public static final ExceptionId EX_META_DATA_LOOKUP_NOT_FOUND =
        new ExceptionId("EX_META_DATA_LOOKUP_NOT_FOUND", "app.meta.data.lookup.not.found");

    public static final ExceptionId EX_META_DATA_IMPORT_UNSUPPORTED =
        new ExceptionId("EX_META_DATA_IMPORT_UNSUPPORTED", "app.meta.data.import.unsupported");

    public static final ExceptionId EX_META_DATA_MARSHALING_FAILED =
        new ExceptionId("EX_META_DATA_MARSHALING_FAILED", "app.meta.data.marshaling.failed");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_ALREADY_EXISTS =
        new ExceptionId("EX_META_SOURCE_SYSTEMS_ALREADY_EXISTS", "app.meta.source.systems.already.exists");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_IMPORT_UNSUPPORTED =
        new ExceptionId("EX_META_SOURCE_SYSTEMS_IMPORT_UNSUPPORTED", "app.meta.source.systems.import.unsupported");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_IMPORT_EMPTY =
        new ExceptionId("EX_META_SOURCE_SYSTEMS_IMPORT_EMPTY", "app.meta.source.systems.import.empty");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_MARSHALING_FAILED =
        new ExceptionId("EX_META_SOURCE_SYSTEMS_MARSHALING_FAILED", "app.meta.source.systems.marshaling.failed");

    public static final ExceptionId EX_META_SOURCE_SYSTEMS_NOT_FOUND =
        new ExceptionId("EX_META_SOURCE_SYSTEMS_NOT_FOUND", "app.meta.source.systems.not.found");

    public static final ExceptionId EX_META_MEASUREMENT_CATEGORY_ALREADY_EXISTS =
        new ExceptionId("EX_META_MEASUREMENT_CATEGORY_ALREADY_EXISTS", "app.meta.measurement.category.already.exists");

    public static final ExceptionId EX_META_MEASUREMENT_CATEGORY_NOT_FOUND =
        new ExceptionId("EX_META_MEASUREMENT_CATEGORY_NOT_FOUND", "app.meta.measurement.category.not.found");

    public static final ExceptionId EX_META_MEASUREMENT_IMPORT_UNSUPPORTED =
        new ExceptionId("EX_META_MEASUREMENT_IMPORT_UNSUPPORTED", "app.meta.measurement.import.unsupported");

    public static final ExceptionId EX_META_MEASUREMENT_IMPORT_EMPTY =
        new ExceptionId("EX_META_MEASUREMENT_IMPORT_EMPTY", "app.meta.measurement.import.empty");

    public static final ExceptionId EX_META_MEASUREMENT_MARSHALING_FAILED =
        new ExceptionId("EX_META_MEASUREMENT_MARSHALING_FAILED", "app.meta.measurement.marshaling.failed");

    public static final ExceptionId EX_META_ENUMERATION_ALREADY_EXISTS =
        new ExceptionId("EX_META_ENUMERATION_ALREADY_EXISTS", "app.meta.enumerationAlreadyExists");

    public static final ExceptionId EX_META_ENUMERATION_NOT_FOUND =
        new ExceptionId("EX_META_ENUMERATION_NOT_FOUND", "app.meta.enumeration.not.found");

    public static final ExceptionId EX_META_ENUMERATION_IMPORT_UNSUPPORTED =
        new ExceptionId("EX_META_ENUMERATION_IMPORT_UNSUPPORTED", "app.meta.enumeration.import.unsupported");

    public static final ExceptionId EX_META_ENUMERATION_IMPORT_EMPTY =
        new ExceptionId("EX_META_ENUMERATION_IMPORT_EMPTY", "app.meta.enumeration.import.empty");

    public static final ExceptionId EX_META_MARSHALING_FAILED = new ExceptionId("EX_META_MARSHALING_FAILED", "app.meta.marshaling.failed");


    public static final ExceptionId EX_META_ENUMERATION_MARSHALING_FAILED =
        new ExceptionId("EX_META_ENUMERATION_MARSHALING_FAILED", "app.meta.enumeration.marshaling.failed");

    public static final ExceptionId EX_META_IMPORT_MODEL_INVALID_FILE_FORMAT =
        new ExceptionId("EX_META_IMPORT_MODEL_INVALID_FILE_FORMAT", "app.meta.enum.unrecognized");

    public static final ExceptionId EX_ENUM_WRONG_VALUE =
        new ExceptionId("EX_ENUM_WRONG_VALUE", "app.meta.importModelInvalidFileFormat");
}
