package org.unidata.mdm.rest.v1.meta.ro.entities.register;

import org.unidata.mdm.rest.v1.meta.ro.entities.RegisterEntityRO;

/**
 * Upsert register request
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class UpsertRegisterRequestRO {

    private RegisterEntityRO registerEntity;
    private Long draftId;

    public RegisterEntityRO getRegisterEntity() {
        return registerEntity;
    }

    public void setRegisterEntity(RegisterEntityRO registerEntity) {
        this.registerEntity = registerEntity;
    }

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }
}
