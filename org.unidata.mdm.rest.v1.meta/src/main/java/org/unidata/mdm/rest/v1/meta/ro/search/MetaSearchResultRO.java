package org.unidata.mdm.rest.v1.meta.ro.search;

import java.util.List;

import org.unidata.mdm.rest.search.ro.SearchResultHitRO;
import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Search result
 *
 * @author Alexandr Serov
 * @since 26.11.2020
 **/
public class MetaSearchResultRO extends DetailedOutputRO {

    /**
     * Fields, participating in a query, if any.
     * Null means '_all'. If set, the same fields will be filled in the 'this.preview' field.
     */
    private List<String> fields;
    /**
     * Search hits.
     */
    private List<SearchResultHitRO> hits;
    /**
     * Optional total count limit (max window size).
     */
    private long totalCountLimit;
    /**
     * Max score for search
     */
    private Float maxScore;
    /**
     * Optional number of all potential hits.
     */
    private long totalCount;

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<SearchResultHitRO> getHits() {
        return hits;
    }

    public void setHits(List<SearchResultHitRO> hits) {
        this.hits = hits;
    }

    public long getTotalCountLimit() {
        return totalCountLimit;
    }

    public void setTotalCountLimit(long totalCountLimit) {
        this.totalCountLimit = totalCountLimit;
    }

    public Float getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Float maxScore) {
        this.maxScore = maxScore;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
