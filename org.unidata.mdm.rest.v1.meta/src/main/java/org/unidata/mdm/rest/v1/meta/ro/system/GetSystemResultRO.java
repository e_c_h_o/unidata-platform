package org.unidata.mdm.rest.v1.meta.ro.system;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get system result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetSystemResultRO extends DetailedOutputRO {

    private SourceSystemRO sourceSystem;

    public SourceSystemRO getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(SourceSystemRO sourceSystem) {
        this.sourceSystem = sourceSystem;
    }
}
