package org.unidata.mdm.rest.v1.meta.ro.groups;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Upsert entity group result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class UpsertEntityGroupResultRO extends DetailedOutputRO {
}
