/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.converter.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.unidata.mdm.meta.type.input.meta.MetaMessage;
import org.unidata.mdm.meta.type.input.meta.MetaPropKey;
import org.unidata.mdm.meta.type.input.meta.MetaVertex;
import org.unidata.mdm.rest.v1.meta.ro.MetaCustomPropRO;
import org.unidata.mdm.rest.v1.meta.ro.MetaMessageRO;
import org.unidata.mdm.rest.v1.meta.ro.graph.MetaVertexRO;

/**
 * The Class MetaVertexDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaVertexDTOToROConverter {

	private MetaVertexDTOToROConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the list
	 */
	public static List<MetaVertexRO> convert(Collection<MetaVertex> source) {
		if (source == null) {
			return null;
		}
		List<MetaVertexRO> target = new ArrayList<>();
		for (MetaVertex s : source) {
			target.add(convert(s));
		}
		return target;

	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta vertex RO
	 */
	public static MetaVertexRO convert(MetaVertex source) {
		if (source == null) {
			return null;
		}
		MetaVertexRO target = new MetaVertexRO(source.getId(), source.getDisplayName(),
				MetaActionDTOToROConverter.convert(source.getAction()),
				MetaTypeDTOToROConverter.convert(source.getType()),
				MetaExistenceDTOToROConverter.convert(source.getStatus()));
		target.setStatuses(convertMessages(source.getMessages()));
		target.setCustomProps(convertCustomProps(source.getCustomProps()));
		return target;
	}

	/**
	 * Convert custom props.
	 *
	 * @param source the source
	 * @return the list
	 */
	private static List<MetaCustomPropRO> convertCustomProps(Map<MetaPropKey, String> source) {
		if (source == null) {
			return null;
		}
		List<MetaCustomPropRO> target = new ArrayList<>();
		for (Entry<MetaPropKey, String> entry : source.entrySet()) {
			MetaCustomPropRO elem = new MetaCustomPropRO();
			elem.setKey(entry.getKey().name());
			elem.setValue(entry.getValue());
			target.add(elem);
		}
		return target;
	}

	/**
	 * Convert messages.
	 *
	 * @param source
	 *            the source
	 * @return the list
	 */
	private static List<MetaMessageRO> convertMessages(List<MetaMessage> source) {
		if (source == null) {
			return null;
		}
		List<MetaMessageRO> target = new ArrayList<>();
		for (MetaMessage metaMessage : source) {
			target.add(convertMessages(metaMessage));
		}
		return target;
	}

	/**
	 * Convert messages.
	 *
	 * @param source
	 *            the source
	 * @return the meta message RO
	 */
	private static MetaMessageRO convertMessages(MetaMessage source) {
		if (source == null) {
			return null;
		}
		MetaMessageRO target = new MetaMessageRO();
		target.setStatus(MetaStatusDTOToROConverter.convert(source.getStatus()));
		target.setMessages(source.getMessages());
		return target;
	}
}
