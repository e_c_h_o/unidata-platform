/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.converter;

import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.unidata.mdm.core.type.model.MeasurementCategoryElement;
import org.unidata.mdm.core.type.model.MeasurementUnitElement;
import org.unidata.mdm.meta.type.model.measurement.MeasurementCategory;
import org.unidata.mdm.meta.type.model.measurement.MeasurementUnit;
import org.unidata.mdm.rest.v1.meta.ro.measurement.MeasurementCategoryRO;
import org.unidata.mdm.rest.v1.meta.ro.measurement.MeasurementUnitRO;

import static java.lang.String.CASE_INSENSITIVE_ORDER;

public class MeasurementUnitsConverter {

    private static final Comparator<MeasurementUnitRO> UNITS_COMPARATOR = (o1, o2) -> CASE_INSENSITIVE_ORDER
            .compare(o1.getName(), o2.getName());

    private MeasurementUnitsConverter() {
        super();
    }

    public static MeasurementCategoryRO to(MeasurementCategoryElement category) {

        if (Objects.isNull(category)) {
            return null;
        }

        MeasurementCategoryRO result = new MeasurementCategoryRO();
        result.setDisplayName(category.getDisplayName());
        result.setDescription(category.getDescription());
        result.setName(category.getName());
        result.setCustomProperties(CustomPropertiesConverter.to(category.getCustomProperties()));
        result.setMeasurementUnits(category.getUnits()
                .stream()
                .map(MeasurementUnitsConverter::to)
                .sorted(UNITS_COMPARATOR)
                .collect(Collectors.toList()));

        return result;
    }

    public static MeasurementUnitRO to(@Nonnull MeasurementUnitElement unit) {

        MeasurementUnitRO ro = new MeasurementUnitRO();
        ro.setName(unit.getName());
        ro.setDisplayName(unit.getDisplayName());
        ro.setDescription(unit.getDescription());
        ro.setBase(unit.isBase());
        ro.setConversionFunction(unit.getConversionFunction());

        return ro;
    }

    public static MeasurementCategory from(MeasurementCategoryRO category) {

        if (Objects.isNull(category)) {
            return null;
        }

        return new MeasurementCategory()
                .withDisplayName(category.getDisplayName())
                .withDescription(category.getDescription())
                .withName(category.getName())
                .withCustomProperties(CustomPropertiesConverter.from(category.getCustomProperties()))
                .withUnits(category.getMeasurementUnits()
                    .stream()
                    .filter(Objects::nonNull)
                    .map(MeasurementUnitsConverter::from)
                    .collect(Collectors.toList()));
    }

    public static MeasurementUnit from(MeasurementUnitRO ro) {

        if (Objects.isNull(ro)) {
            return null;
        }

        return new MeasurementUnit()
                .withName(ro.getName())
                .withDisplayName(ro.getDisplayName())
                .withDescription(ro.getDescription())
                .withBase(ro.isBase())
                .withConversionFunction(ro.getConversionFunction());
    }
}
