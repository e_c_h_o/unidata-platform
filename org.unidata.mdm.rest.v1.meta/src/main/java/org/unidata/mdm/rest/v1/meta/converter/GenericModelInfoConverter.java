/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.converter;

import org.unidata.mdm.meta.dto.AbstractModelResult;
import org.unidata.mdm.rest.v1.meta.ro.GetGenericModelInfoResultRO;

/**
 * The Class EnumerationConverter.
 */
public final class GenericModelInfoConverter {

    private GenericModelInfoConverter() { }

    /**
     * Convert from {@link AbstractModelResult} to
     * {@link GetGenericModelInfoResultRO}.
     *
     * @param source
     *            convert from
     * @return converted value
     */
    public static GetGenericModelInfoResultRO to(AbstractModelResult source) {

        if (source == null) {
            return null;
        }

        GetGenericModelInfoResultRO target = new GetGenericModelInfoResultRO();
        target.setName(source.getName());
        target.setDisplayName(source.getDisplayName());
        target.setDescription(source.getDescription());
        target.setVersion(source.getVersion());
        target.setTypeId(source.getTypeId());
        target.setInstanceId(source.getInstanceId());

        return target;
    }
}
