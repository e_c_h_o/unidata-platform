/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.measurement;

import java.util.Collection;

import org.unidata.mdm.rest.v1.meta.ro.CustomPropertyRO;

public class MeasurementCategoryRO {

    private String name;

    private String displayName;

    private String description;

    private Collection<MeasurementUnitRO> measurementUnits;

    private Collection<CustomPropertyRO> customProperties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String shortName) {
        this.displayName = shortName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<MeasurementUnitRO> getMeasurementUnits() {
        return measurementUnits;
    }

    public void setMeasurementUnits(Collection<MeasurementUnitRO> measurementUnitDtoList) {
        this.measurementUnits = measurementUnitDtoList;
    }

    /**
     * @return the customProperties
     */
    public Collection<CustomPropertyRO> getCustomProperties() {
        return customProperties;
    }

    /**
     * @param customProperties the customProperties to set
     */
    public void setCustomProperties(Collection<CustomPropertyRO> customProperties) {
        this.customProperties = customProperties;
    }
}