package org.unidata.mdm.rest.v1.meta.ro.measurement;

import java.util.List;

/**
 * Get measurements result
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
public class GetMeasurementsResultRO {

    private List<MeasurementCategoryRO> measurements;

    public List<MeasurementCategoryRO> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<MeasurementCategoryRO> measurements) {
        this.measurements = measurements;
    }
}

