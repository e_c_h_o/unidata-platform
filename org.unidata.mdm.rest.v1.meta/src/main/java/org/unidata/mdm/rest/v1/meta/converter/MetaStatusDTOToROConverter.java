package org.unidata.mdm.rest.v1.meta.converter;


import org.unidata.mdm.meta.type.input.meta.MetaStatus;
import org.unidata.mdm.rest.v1.meta.ro.MetaStatusRO;

/**
 * The Class MetaStatusDTOToROConverter.
 * 
 * @author ilya.bykov
 */
public class MetaStatusDTOToROConverter {

	private MetaStatusDTOToROConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source
	 *            the source
	 * @return the meta status RO
	 */
	public static MetaStatusRO convert(MetaStatus source) {
		if (source == null) {
			return null;
		}
		return MetaStatusRO.valueOf(source.name());
	}
}
