package org.unidata.mdm.rest.v1.meta.ro.groups;

import java.util.List;

/**
 * Get flat group result
 *
 * @author Alexandr Serov
 * @since 27.11.2020
 **/
public class GetFlatGroupsResultRO {

    private List<LightweightEntityGroupRO> flatEntityGroups;

    public List<LightweightEntityGroupRO> getFlatEntityGroups() {
        return flatEntityGroups;
    }

    public void setFlatEntityGroups(List<LightweightEntityGroupRO> flatEntityGroups) {
        this.flatEntityGroups = flatEntityGroups;
    }
}
