/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.ro.attributes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.rest.system.ro.security.RightRO;
import org.unidata.mdm.rest.v1.meta.ro.CustomPropertyRO;

/**
 * @author Michael Yashin. Created on 25.05.2015.
 */
public abstract class AbstractAttributeRO {
    /**
     * Name - [_a-zA-Z].
     */
    protected String name;
    /**
     * Display name - any char sequence.
     */
    protected String displayName;
    /**
     * Description - any char sequence.
     */
    protected String description;
    /**
     * Attribute is read only.
     */
    protected boolean readOnly;
    /**
     * Attribute is hidden.
     */
    protected boolean hidden;
    /**
     * Rights object.
     */
    protected RightRO rights;

    protected final List<CustomPropertyRO> customProperties = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the readOnly
     */
    public boolean isReadOnly() {
        return readOnly;
    }

    /**
     * @param readOnly the readOnly to set
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * @return the hidden
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * @param hidden the hidden to set
     */
    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return the rights
     */
    public RightRO getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(RightRO rights) {
        this.rights = rights;
    }

    public List<CustomPropertyRO> getCustomProperties() {
        return Collections.unmodifiableList(customProperties);
    }

    public void setCustomProperties(final Collection<CustomPropertyRO> customProperties) {
        if (CollectionUtils.isEmpty(customProperties)) {
            return;
        }
        this.customProperties.clear();
        this.customProperties.addAll(customProperties);
    }

    public void addCustomProperties(final Collection<CustomPropertyRO> customProperties) {
        if (CollectionUtils.isEmpty(customProperties)) {
            return;
        }
        this.customProperties.addAll(customProperties);
    }

    public void addCustomProperty(CustomPropertyRO customProperty) {
        if (customProperty == null) {
            return;
        }
        this.customProperties.add(customProperty);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractAttributeRO)) return false;

        AbstractAttributeRO that = (AbstractAttributeRO) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
