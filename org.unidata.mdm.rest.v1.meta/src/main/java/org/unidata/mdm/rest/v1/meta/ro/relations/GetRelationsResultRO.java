package org.unidata.mdm.rest.v1.meta.ro.relations;

import java.util.List;

import org.unidata.mdm.rest.system.ro.DetailedOutputRO;

/**
 * Get Relations Result
 *
 * @author Alexandr Serov
 * @since 25.11.2020
 **/
public class GetRelationsResultRO extends DetailedOutputRO {

    private List<RelationRO> relations;

    public List<RelationRO> getRelations() {
        return relations;
    }

    public void setRelations(List<RelationRO> relations) {
        this.relations = relations;
    }
}
