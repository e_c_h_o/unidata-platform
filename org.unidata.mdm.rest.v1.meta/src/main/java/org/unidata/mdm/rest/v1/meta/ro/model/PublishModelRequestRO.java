package org.unidata.mdm.rest.v1.meta.ro.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov on Oct 26, 2020
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublishModelRequestRO {

    private Long draftId;

    private Boolean force;

    private Boolean delete;

    public Long getDraftId() {
        return draftId;
    }

    public void setDraftId(Long draftId) {
        this.draftId = draftId;
    }

    public Boolean getForce() {
        return force;
    }

    public void setForce(Boolean force) {
        this.force = force;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }
}
