package org.unidata.mdm.rest.v1.meta.converter;


import org.unidata.mdm.meta.type.input.meta.MetaStatus;
import org.unidata.mdm.rest.v1.meta.ro.MetaStatusRO;

/**
 * The Class MetaStatusROToDTOConverter.
 * @author ilya.bykovF
 */
public class MetaStatusROToDTOConverter {

	private MetaStatusROToDTOConverter() {
	}

	/**
	 * Convert.
	 *
	 * @param source the source
	 * @return the meta status
	 */
	public static MetaStatus convert(MetaStatusRO source) {
		if (source == null) {
			return null;
		}
		return MetaStatus.valueOf(source.name());
	}
}
