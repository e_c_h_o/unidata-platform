package org.unidata.mdm.rest.v1.meta.service.relations;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.apache.commons.lang3.ObjectUtils;
import org.unidata.mdm.meta.context.GetDataModelContext;
import org.unidata.mdm.meta.context.UpsertDataModelContext;
import org.unidata.mdm.meta.dto.GetEntitiesWithRelationsDTO;
import org.unidata.mdm.meta.dto.GetModelDTO;
import org.unidata.mdm.meta.dto.GetModelRelationDTO;
import org.unidata.mdm.rest.v1.meta.converter.RelationDefConverter;
import org.unidata.mdm.rest.v1.meta.converter.RelationDefinitionConverter;
import org.unidata.mdm.rest.v1.meta.converter.entities.EntitiesDefFilteredByRelationSideConverter;
import org.unidata.mdm.rest.v1.meta.ro.relations.DeleteRelationsResultRO;
import org.unidata.mdm.rest.v1.meta.ro.relations.GetRelatedEntitiesResultRO;
import org.unidata.mdm.rest.v1.meta.ro.relations.GetRelationsResultRO;
import org.unidata.mdm.rest.v1.meta.ro.relations.UpsertRelationsRequestRO;
import org.unidata.mdm.rest.v1.meta.ro.relations.UpsertRelationsResultRO;
import org.unidata.mdm.rest.v1.meta.service.AbstractMetaModelRestService;

/**
 * Relations rest controller
 *
 * @author Alexandr Serov
 * @since 24.11.2020
 **/
@Path(RelationRestService.SERVICE_PATH)
@Consumes({"application/json"})
@Produces({"application/json"})
public class RelationRestService extends AbstractMetaModelRestService {

    /**
     * Service path.
     */
    public static final String SERVICE_PATH = "relations";

    /**
     * Service tag
     */
    public static final String SERVICE_TAG = "relations";

    @GET
    @Operation(
        description = "Gets relations list.",
        method = HttpMethod.GET,
        tags = SERVICE_TAG)
    public GetRelationsResultRO find(
        @Parameter(description = "The draft id for drafts. Optional.", in = ParameterIn.QUERY)
        @QueryParam("draftId") @DefaultValue("0") Long draftId) {
        GetModelDTO model = metaModelService.get(GetDataModelContext.builder().allRelations(true)
            .draftId(resolveDraftId(draftId))
            .build());
        GetRelationsResultRO result = new GetRelationsResultRO();
        List<GetModelRelationDTO> relations = ObjectUtils.defaultIfNull(model.getRelations(), Collections.emptyList());
        result.setRelations(relations.stream()
            .map(GetModelRelationDTO::getRelation)
            .map(RelationDefConverter::convert)
            .collect(Collectors.toList())
        );
        return result;
    }

    @GET
    @Path("from/{name}")
    @Operation(
        description = "List of relations, where - 'name' is the left side (from)",
        method = HttpMethod.GET,
        tags = SERVICE_TAG)
    public GetRelatedEntitiesResultRO findEntitiesFrom(@Parameter(in = ParameterIn.PATH)
                                                       @PathParam("name") String entityName,
                                                       @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY)
                                                       @QueryParam("draftId") @DefaultValue("0") Long draftId) {
        return findEntities(entityName, draftId, true);

    }

    @GET
    @Path("to/{name}")
    @Operation(
        description = "List of relations, where - 'name' is the left side (from)",
        method = HttpMethod.GET,
        tags = SERVICE_TAG)
    public GetRelatedEntitiesResultRO findEntitiesTo(@Parameter(in = ParameterIn.PATH)
                                                     @PathParam("name") String entityName,
                                                     @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY)
                                                     @QueryParam("draftId") @DefaultValue("0") Long draftId) {
        return findEntities(entityName, draftId, false);
    }

    /**
     * Upsert.
     */
    @POST
    @Operation(
        description = "Add new/update existing relations.",
        method = HttpMethod.POST,
        tags = SERVICE_TAG)
    public UpsertRelationsResultRO upsert(UpsertRelationsRequestRO req) {
        Objects.requireNonNull(req, "Request can't be null");
        UpsertRelationsResultRO result = new UpsertRelationsResultRO();
        metaModelService.upsert(UpsertDataModelContext.builder()
            .name(req.getName())
            .relationsUpdate(RelationDefinitionConverter.convert(req.getRelations()))
            .draftId(resolveDraftId(req.getDraftId()))
            .build());
        result.setName(req.getName());
        return result;
    }

    /**
     * Delete.
     *
     * @param name the name
     * @return the response
     */
    @DELETE
    @Path("{name}")
    @Operation(
        description = "Remove relation.",
        method = HttpMethod.DELETE,
        tags = SERVICE_TAG)
    public DeleteRelationsResultRO delete(@Parameter(in = ParameterIn.PATH) @PathParam("name") String name,
                                          @Parameter(description = "Draft id. Optional.", in = ParameterIn.QUERY)
                                          @QueryParam("draftId") @DefaultValue("0") Long draftId) {
        DeleteRelationsResultRO result = new DeleteRelationsResultRO();
        metaModelService.upsert(UpsertDataModelContext.builder()
            .relationsDelete(name)
            .draftId(resolveDraftId(draftId))
            .build());
        result.setId(name);
        return result;
    }

    private GetRelatedEntitiesResultRO findEntities(String entityName, Long draftId, boolean fromDirection) {
        GetModelDTO model = metaModelService.get(GetDataModelContext.builder()
            .entityIds(Collections.singletonList(entityName))
            .allFromRelations(fromDirection)
            .allToRelations(!fromDirection)
            .draftId(resolveDraftId(draftId))
            .build());
        GetEntitiesWithRelationsDTO rels = model.getRelationsBySide();
        GetRelatedEntitiesResultRO result = new GetRelatedEntitiesResultRO();
        if (rels != null) {
            result.setEntities(EntitiesDefFilteredByRelationSideConverter.convert(rels));
        } else {
            result.setEntities(Collections.emptyList());
        }
        return result;
    }

}
