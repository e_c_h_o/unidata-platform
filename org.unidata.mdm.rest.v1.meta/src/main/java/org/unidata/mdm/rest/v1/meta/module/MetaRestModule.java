/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.v1.meta.module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.unidata.mdm.rest.v1.meta.type.rendering.MetaModelInputRenderingAction;
import org.unidata.mdm.rest.v1.meta.type.rendering.MetaModelOutputRenderingAction;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.module.Module;
import org.unidata.mdm.system.type.rendering.RenderingAction;

/**
 * Meta REST module.
 * @author Mikhail Mikhailov on Oct 31, 2019
 */
public class MetaRestModule implements Module {
    /**
     * This module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.rest.v1.meta";

    private static final List<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.meta", "6.0"),
            new Dependency("org.unidata.mdm.rest.core", "6.0"),
            new Dependency("org.unidata.mdm.rest.system", "6.0")
    );

    /**
     * Constructor.
     */
    public MetaRestModule() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.0";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Unidata REST meta";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Unidata REST interface to meta model";
    }

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{ "rest_meta_messages" };
    }

    @Override
    public Collection<RenderingAction> getRenderingActions() {
        List<RenderingAction> actions = new ArrayList<>(Arrays.asList(MetaModelInputRenderingAction.values()));
        actions.addAll(Arrays.asList(MetaModelOutputRenderingAction.values()));

        return actions;
    }
}
