/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.unidata.mdm.core.service.job.JobEnumParamExtractor;
import org.unidata.mdm.core.type.job.JobEnumType;
import org.unidata.mdm.core.type.job.JobParameterType;
import org.unidata.mdm.core.type.model.EntityElement;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.MetaModelService;

public class ReindexDataJobTypesEnumParamExtractor implements JobEnumParamExtractor {
    /**
     * All entities.
     */
    public static final String ALL = "ALL";

    private MetaModelService metaModelService;

    public ReindexDataJobTypesEnumParamExtractor(MetaModelService metaModelService) {
        super();
        this.metaModelService = metaModelService;
    }

    @Override
    public JobEnumType extractParameters() {

        final JobEnumType enumration = new JobEnumType();

        List<String> entities = new ArrayList<>();
        entities.addAll(metaModelService.instance(Descriptors.DATA).getRegisters().stream().map(EntityElement::getName).collect(Collectors.toList()));
        entities.addAll(metaModelService.instance(Descriptors.DATA).getRegisters().stream().map(EntityElement::getName).collect(Collectors.toList()));
        entities.sort(String::compareTo);
        entities.add(0, ALL);

        enumration.setParameterType(JobParameterType.STRING);
        enumration.setParameters(entities);
        enumration.setMultiSelect(true);

        return enumration;
    }

}
