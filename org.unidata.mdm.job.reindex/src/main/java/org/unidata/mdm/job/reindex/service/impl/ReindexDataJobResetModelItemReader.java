/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.Collection;
import java.util.Collections;

import org.springframework.batch.item.support.IteratorItemReader;

/**
 * @author Mikhail Mikhailov
 * Simple one cycle reader
 */
public class ReindexDataJobResetModelItemReader extends IteratorItemReader<Boolean> {
    /**
     * One cycle step.
     */
    private static final Collection<Boolean> CYCLE = Collections.singletonList(Boolean.TRUE);
    /**
     * Constructor.
     */
    public ReindexDataJobResetModelItemReader() {
        super(CYCLE);
    }
}
