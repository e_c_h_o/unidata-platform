/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import org.unidata.mdm.core.type.job.StepExecutionState;

/**
 * @author Mikhail Mikhailov
 *
 */
public class ReindexDataJobStepExecutionState implements StepExecutionState {
    /**
     * Records.
     */
    private long reindexedRecords = 0L;
    /**
     * Classified records.
     */
    private long classifiedRecords = 0L;
    /**
     * Classifier data records.
     */
    private long reindexedClassifiers = 0L;
    /**
     * Inserted.
     */
    private long reindexedRelations = 0L;
    /**
     * Constructor.
     */
    public ReindexDataJobStepExecutionState() {
        super();
    }
    /**
     * @param reindexedRecords the reindexedRecords to set
     */
    public void incrementReindexedRecords(long reindexedRecords) {
        this.reindexedRecords += reindexedRecords;
    }
    /**
     * @param reindexedClassifiers the reindexedClassifiers to set
     */
    public void incrementReindexedClassifiers(long reindexedClassifiers) {
        this.reindexedClassifiers += reindexedClassifiers;
    }
    /**
     * @param classifiedRecords the classifiedRecords to set
     */
    public void incrementClassifiedRecords(long classifiedRecords) {
        this.classifiedRecords += classifiedRecords;
    }
    /**
     * @param reindexedRelations the reindexedRelations to set
     */
    public void incrementReindexedRelations(long reindexedRelations) {
        this.reindexedRelations += reindexedRelations;
    }
    /**
     * @return the failed
     */
    public long getReindexedRecords() {
        return reindexedRecords;
    }
    /**
     * @return the skept
     */
    public long getReindexedClassifiers() {
        return reindexedClassifiers;
    }
    /**
     * @return the updated
     */
    public long getClassifiedRecords() {
        return classifiedRecords;
    }
    /**
     * @return the inserted
     */
    public long getReindexedRelations() {
        return reindexedRelations;
    }
}
