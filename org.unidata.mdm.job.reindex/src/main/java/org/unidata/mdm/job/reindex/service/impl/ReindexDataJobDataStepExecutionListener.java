/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.job.step.AbstractJobStepExecutionListener;
import org.unidata.mdm.core.util.JobUtils;

import com.hazelcast.core.HazelcastInstance;

/**
 * @author Mikhail Mikhailov
 * Slave step listener.
 */
@StepScope
public class ReindexDataJobDataStepExecutionListener extends AbstractJobStepExecutionListener  {
    /**
     * HZ innstance.
     */
    @Autowired
    private HazelcastInstance hazelcastInstance;
    /**
     * Constructor.
     */
    public ReindexDataJobDataStepExecutionListener() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeStep(StepExecution stepExecution) {

        if (JobUtils.getStepState() == null) {
            JobUtils.setStepState(new ReindexDataJobStepExecutionState());
        }

        super.authenticateIfNeeded();
        super.beforeStep(stepExecution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {

        super.afterStep(stepExecution);

        /* ReindexDataJobStepExecutionState state = */
        JobUtils.removeStepState();
        /*
        if (state.getReindexedRecords() > 0) {
            IAtomicLong fCounter = hazelcastInstance.getAtomicLong(
                    JobRuntimeUtils.getObjectReferenceName(runId, ReindexDataJobConstants.REINDEX_JOB_REINDEXED_RECORDS_COUNTER));
            fCounter.addAndGet(state.getReindexedRecords());
        }

        if (state.getReindexedClassifiers() > 0) {
            IAtomicLong iCounter = hazelcastInstance.getAtomicLong(
                        JobRuntimeUtils.getObjectReferenceName(runId, ReindexDataJobConstants.REINDEX_JOB_REINDEXED_CLASSIFIERS_COUNTER));
            iCounter.addAndGet(state.getReindexedClassifiers());
        }

        if (state.getClassifiedRecords() > 0) {
            IAtomicLong uCounter = hazelcastInstance.getAtomicLong(
                    JobRuntimeUtils.getObjectReferenceName(runId, ReindexDataJobConstants.REINDEX_JOB_CLASSIFIED_RECORDS_COUNTER));
            uCounter.addAndGet(state.getClassifiedRecords());
        }

        if (state.getReindexedRelations() > 0) {
            IAtomicLong sCounter = hazelcastInstance.getAtomicLong(
                    JobRuntimeUtils.getObjectReferenceName(runId, ReindexDataJobConstants.REINDEX_JOB_REINDEXED_RELATIONS_COUNTER));
            sCounter.addAndGet(state.getReindexedRelations());
        }
        */
        super.clearAuthentication();
        return stepExecution.getExitStatus();
    }

}
