/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.module;

import java.util.Collection;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.service.job.JobStepExecutionRequestHandler;
import org.unidata.mdm.system.type.module.AbstractModule;
import org.unidata.mdm.system.type.module.Dependency;

/**
 * @author Mikhail Mikhailov on Dec 17, 2019
 */
public class ReindexJobModule extends AbstractModule {
    /**
     * Module logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexJobModule.class);
    /**
     * This module id.
     */
    public static final String MODULE_ID = "org.unidata.mdm.job.reindex";
    /**
     * This module dependencies.
     */
    private static final Collection<Dependency> DEPENDENCIES = Set.of(
            new Dependency("org.unidata.mdm.core", "6.0"),
            new Dependency("org.unidata.mdm.search", "6.0"),
            new Dependency("org.unidata.mdm.meta", "6.0"),
            new Dependency("org.unidata.mdm.data", "6.0"),
            new Dependency("org.unidata.mdm.draft", "6.0"));
    /**
     * Stoppable resource.
     */
    @Autowired
    private JobStepExecutionRequestHandler reindexRemoteStepExecutionRequestHandler;
    /**
     * {@inheritDoc}
     */
    @Override
    public String getId() {
        return MODULE_ID;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersion() {
        return "6.0";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "Reindex";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "Reindex Job Module";
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        try {
            reindexRemoteStepExecutionRequestHandler.destroy();
        } catch (Exception e) {
            LOGGER.warn("Exception caught while stopping Reindex Step Request Handler.", e);
        }
    }
}
