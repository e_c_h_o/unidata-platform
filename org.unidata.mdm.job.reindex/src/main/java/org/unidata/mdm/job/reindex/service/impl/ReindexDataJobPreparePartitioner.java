/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.unidata.mdm.job.reindex.configuration.ReindexJobConfigurationConstants;

/**
 * @author Mikhail Mikhailov
 * Update data mapping partitioner.
 */
@JobScope
public class ReindexDataJobPreparePartitioner extends ReindexDataJobAbstractMappingPartitioner {
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexDataJobPreparePartitioner.class);
    /**
     * If true, record's data will be reindexed
     */
    @Value("#{jobParameters[" + ReindexJobConfigurationConstants.PARAM_REINDEX_RECORDS + "] ?: false}")
    private boolean reindexRecords;
    /**
     * If true, rels will be reindexed
     */
    @Value("#{jobParameters[" + ReindexJobConfigurationConstants.PARAM_REINDEX_RELATIONS + "] ?: false}")
    private boolean reindexRelations;
    /**
     * If true, maching data will be reindexed
     */
    @Value("#{jobParameters[" + ReindexJobConfigurationConstants.PARAM_REINDEX_MATCHING + "] ?: false}")
    private boolean reindexMatching;
    /**
     * If true, classifiers data will be reindexed
     */
    @Value("#{jobParameters[" + ReindexJobConfigurationConstants.PARAM_REINDEX_CLASSIFIERS + "] ?: false}")
    private boolean reindexClassifiers;
    /**
     * Constructor.
     */
    public ReindexDataJobPreparePartitioner() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {

        if (!reindexRecords && !reindexRelations && !reindexMatching && !reindexClassifiers) {
            LOGGER.info("No data kind specified for reindexing [reindexRecords, reindexRelations, reindexMatching, reindexClassifiers are all false]. No prepare executed.");
            return Collections.emptyMap();
        }

        return super.partition(gridSize);
    }
}
