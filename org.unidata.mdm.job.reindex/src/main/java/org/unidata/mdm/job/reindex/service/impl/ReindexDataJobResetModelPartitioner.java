/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.unidata.mdm.core.util.JobUtils;
import org.unidata.mdm.job.reindex.configuration.ReindexJobConfigurationConstants;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.Member;

/**
 * @author Mikhail Mikhailov
 * Partitioner, creating a single execution context for each cluster member node,
 *  which is to be run on that node.
 */
@JobScope
public class ReindexDataJobResetModelPartitioner implements Partitioner {
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexDataJobResetModelPartitioner.class);
    /**
     * Force refresh. Skip this step, if false.
     */
    @Value("#{jobParameters[" + ReindexJobConfigurationConstants.PARAM_FORCE_MODEL_REFRESH + "] ?: false}")
    private boolean forceModelRefresh;
    /**
     * HZ instance.
     */
    @Autowired
    private HazelcastInstance hazelcastInstance;
    /**
     * Constructor.
     */
    public ReindexDataJobResetModelPartitioner() {
        super();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {

        if (!forceModelRefresh) {
            LOGGER.info("Model refresh is disabled. Skip step");
            return Collections.emptyMap();
        }

        int number = 0;
        Map<String, ExecutionContext> result = new HashMap<>();
        Set<Member> members = hazelcastInstance.getCluster().getMembers();
        for (Member m : members) {
            result.put(JobUtils.targetedPartitionName(number++, m.getUuid()), new ExecutionContext());
        }

        LOGGER.info("Collected {} partitions (cluster nodes for model reset execution).", result.size());
        return result;
    }
}
