/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.core.util.SecurityUtils;
import org.unidata.mdm.meta.configuration.Descriptors;
import org.unidata.mdm.meta.service.MetaModelService;
import org.unidata.mdm.search.context.MappingRequestContext;
import org.unidata.mdm.search.service.SearchService;

/**
 * @author Mikhail Mikhailov
 * Simple update data mapping writer.
 */
public class ReindexDataJobResetItemWriter implements ItemWriter<String> {
    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexDataJobResetItemWriter.class);
    /**
     * Prepare per index params.
     * ("index.refresh_interval", "1s"); // Enable refresh
     * ("index.warmer.enabled", Boolean.TRUE); // Enable warmers
     */
    private static final Map<String, Object> RESET_INDEX_PARAMS = Collections.singletonMap("index.refresh_interval", "1s");
    /**
     * Search service
     */
    @Autowired
    private SearchService searchService;
    /**
     * Meta model service
     */
    @Autowired
    private MetaModelService metaModelService;

//    @Autowired
//    private PreclusteringService preclusteringService;
    /**
     * Constructor.
     */
    public ReindexDataJobResetItemWriter() {
        super();
    }

    @Override
    public void write(List<? extends String> items) throws Exception {

        for (String entityName : items) {

            boolean isEntity = metaModelService.instance(Descriptors.DATA).isRegister(entityName);
            LOGGER.info(isEntity
                    ? "Re-Setting bulk-optimized options to index for {} (entity)."
                    : "Not re-setting bulk-optimized options to index for {} (lookup).", entityName);
            if (isEntity) {

                MappingRequestContext ctx = MappingRequestContext.builder()
                        .storageId(SecurityUtils.getCurrentUserStorageId())
                        .entity(entityName)
                        .build();

                // searchService.closeIndex(entity, SecurityUtils.getCurrentUserStorageId());
                searchService.setIndexSettings(ctx, RESET_INDEX_PARAMS);
                // searchService.openIndex(entity, SecurityUtils.getCurrentUserStorageId());
                searchService.refreshIndex(ctx, true);
            }
//            preclusteringService.prepareAfterBatchLoad(entityName);
        }
    }
}
