/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.meta.service.MetaModelService;

/**
 * @author Mikhail Mikhailov
 * Simple one time item writer.
 */
public class ReindexDataJobResetModelItemWriter implements ItemWriter<Boolean> {
    /**
     * This logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReindexDataJobResetModelItemWriter.class);
    /**
     * MMSE.
     */
    @Autowired
    private MetaModelService metaModelServiceExt;
    /**
     * Constructor.
     */
    public ReindexDataJobResetModelItemWriter() {
        super();
    }

    @Override
    public void write(List<? extends Boolean> items) throws Exception {
        LOGGER.info("Forcing re-reading model on this node.");
        metaModelServiceExt.afterModuleStartup();
    }
}
