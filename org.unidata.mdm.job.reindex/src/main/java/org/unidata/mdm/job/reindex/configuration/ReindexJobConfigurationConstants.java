/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.job.reindex.configuration;

import org.unidata.mdm.core.service.job.JobCommonParameters;
import org.unidata.mdm.job.reindex.module.ReindexJobModule;

/**
 * @author Mikhail Mikhailov
 * Parameters.
 */
public class ReindexJobConfigurationConstants extends JobCommonParameters {
    /**
     * Parameter 'reindexTypes'.
     */
    public static final String PARAM_REINDEX_TYPES = "reindexTypes";
    /**
     * Param updateMappings.
     */
    public static final String PARAM_UPDATE_MAPPINGS = "updateMappings";
    /**
     * Param forceModelRefresh.
     */
    public static final String PARAM_FORCE_MODEL_REFRESH = "forceModelRefresh";
    /**
     * Parameter 'cleanTypes'.
     */
    public static final String PARAM_CLEAN_INDEXES = "cleanIndexes";
    /**
     * Parameter 'reindexRecords'.
     */
    public static final String PARAM_REINDEX_RECORDS = "reindexRecords";
    /**
     * Parameter 'reindexRelations'.
     */
    public static final String PARAM_REINDEX_RELATIONS = "reindexRelations";
    /**
     * Parameter 'reindexClassifiers'.
     */
    public static final String PARAM_REINDEX_CLASSIFIERS = "reindexClassifiers";
    /**
     * Parameter 'reindexMatching'.
     */
    public static final String PARAM_REINDEX_MATCHING = "reindexMatching";
    /**
     * Parameter 'logFailedIds'.
     */
    public static final String PARAM_WRITE_ID_LOG = "writeIdLog";
    /**
     * Parameter 'processIdLog'. Continue from failure table, not from storage.
     */
    public static final String PARAM_PROCESS_ID_LOG = "processIdLog";
    /**
     * Parameter 'skipDq'.
     */
    public static final String PARAM_SKIP_DQ = "skipDq";
    /**
     * Suppress consistency check, performed by system DQ rules.
     */
    public static final String PARAM_SKIP_CONSISTENCY_CHECK = "skipConsistencyCheck";
    /**
     * Suppress default report. Needed for classifier updates primarily.
     */
    public static final String PARAM_SKIP_DEFAULT_REPORT = "skipDefaultReport";
    /**
     * Parameter 'jmsEnabled'.
     */
    public static final String PARAM_SKIP_NOTIFICATIONS = "skipNotifications";
    /**
     * Records result mark.
     */
    public static final String REINDEX_JOB_REINDEXED_RECORDS_COUNTER = "#REINDEXED_RECORDS";
    /**
     * Classifier data result mark.
     */
    public static final String REINDEX_JOB_REINDEXED_CLASSIFIERS_COUNTER = "#REINDEXED_CLASSIFIERS";
    /**
     * Classified records result mark.
     */
    public static final String REINDEX_JOB_CLASSIFIED_RECORDS_COUNTER = "#CLASSIFIED_RECORDS";
    /**
     * Relations result mark.
     */
    public static final String REINDEX_JOB_REINDEXED_RELATIONS_COUNTER = "#REINDEXED_RELATIONS";

    public static final String USER_REPORT_PARAM = "userReport";

    public static final String USER_REPORT_MESSAGE_PARAM = "message";

    public static final String USER_REPORT_FAIL_MESSAGE_PARAM = "failMessage";

    public static final String OPERATION_ID_FILTER = "operationId";

    // Job name
    public static final String JOB_NAME = "reindexDataJob";

    // Bean names
    public static final String BEAN_NAME_DATA_QUEUE = "reindexDataJobDataQueue";

    public static final String BEAN_NAME_MODEL_TOPIC = "reindexDataJobResetModelTopic";

    public static final String BEAN_NAME_MAPPPING_EXECUTOR = "reindexDataJobMappingTaskExecutor";

    public static final String BEAN_NAME_PARAMETERS_TEMPLATE = "reindexDataJobParameters";

    public static final String BEAN_NAME_STEP_EXEC_HANDLER = "reindexRemoteStepExecutionRequestHandler";

    public static final String BEAN_NAME_DATA_STEP_EXEC_LISTENER = "reindexDataJobDataStepExecutionListener";

    public static final String BEAN_NAME_MAPPING_STEP_EXEC_LISTENER = "reindexDataJobMappingStepExecutionListener";

    public static final String BEAN_NAME_JOB_EXEC_LISTENER = "reindexDataJobListener";

    // Reset model
    public static final String BEAN_NAME_RESET_MODEL_PARTITION_HANDLER = "reindexDataJobResetModelPartitionHandler";

    public static final String BEAN_NAME_RESET_MODEL_PARTITIONER = "reindexDataJobResetModelPartitioner";

    public static final String BEAN_NAME_RESET_MODEL_ITEM_READER = "reindexDataJobResetModelItemReader";

    public static final String BEAN_NAME_RESET_MODEL_ITEM_PROCESSOR = "reindexDataJobResetModelItemProcessor";

    public static final String BEAN_NAME_RESET_MODEL_ITEM_WRITER = "reindexDataJobResetModelItemWriter";

    // Mapping
    public static final String BEAN_NAME_PREPARE_PARTITIONER = "reindexDataJobPreparePartitioner";

    public static final String BEAN_NAME_RESET_PARTITIONER = "reindexDataJobResetPartitioner";

    public static final String BEAN_NAME_MAPPING_PARTITIONER = "reindexDataJobMappingPartitioner";

    public static final String BEAN_NAME_MAPPING_ITEM_READER = "reindexDataJobMappingItemReader";

    public static final String BEAN_NAME_MAPPING_ITEM_PROCESSOR = "reindexDataJobMappingItemProcessor";

    public static final String BEAN_NAME_MAPPING_ITEM_WRITER = "reindexDataJobMappingItemWriter";

    public static final String BEAN_NAME_PREPARE_ITEM_WRITER = "reindexDataJobPrepareItemWriter";

    public static final String BEAN_NAME_RESET_ITEM_WRITER = "reindexDataJobResetItemWriter";

    // Data
    public static final String BEAN_NAME_DATA_PARTITION_HANDLER = "reindexDataJobDataPartitionHandler";

    public static final String BEAN_NAME_DATA_PARTITIONER = "reindexDataJobDataPartitioner";

    public static final String BEAN_NAME_DATA_ITEM_READER = "reindexDataJobDataItemReader";

    public static final String BEAN_NAME_DATA_ITEM_PROCESSOR = "reindexDataJobDataItemProcessor";

    public static final String BEAN_NAME_DATA_ITEM_WRITER = "reindexDataJobDataItemWriter";

    // Steps
    // 1. Force model refresh, if requested.
    public static final String BEAN_NAME_RESET_MODEL_STEP = "reindexDataJobResetModelStep";
    // 2. Ensure mappings exist. Create, if necessary.
    public static final String BEAN_NAME_MAPPING_STEP = "reindexDataJobMappingStep";
    // 3. Apply various index settings (throttling, refresh interval, etc.)
    public static final String BEAN_NAME_PREPARE_STEP = "reindexDataJobPrepareStep";
    // 4. Reindex data actually
    public static final String BEAN_NAME_DATA_STEP = "reindexDataJobDataStep";
    // 5. Put indexes back into normal state (reset throttling, refresh interval, etc.)
    public static final String BEAN_NAME_RESET_STEP = "reindexDataJobResetStep";

    // Prop values
    public static final String PROP_NAME_MAPPING_CONCURRENCY = ReindexJobModule.MODULE_ID + ".mapping.concurrency";

    public static final String PROP_NAME_MAPPING_BLOCK_SIZE = ReindexJobModule.MODULE_ID + ".mapping.block";

    public static final String PROP_NAME_DATA_REINDEX_THREADS = ReindexJobModule.MODULE_ID + ".data.reindex.threads";

    public static final String PROP_NAME_DATA_COMMIT_INTERVAL = ReindexJobModule.MODULE_ID + ".data.commit.interval";

    public static final String PROP_NAME_JOB_DESCRIPTION_CODE = ReindexJobModule.MODULE_ID + ".description.code";

    /**
     * Constructor.
     */
    private ReindexJobConfigurationConstants() {
        super();
    }
}
