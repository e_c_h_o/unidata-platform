/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro.serializer;

import java.io.IOException;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.unidata.mdm.rest.data.ro.CodeAttributeRO;

public class CodeAttributeSerializer extends JsonSerializer<CodeAttributeRO> {

    @Override
    public void serialize(CodeAttributeRO value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {

        jgen.writeStartObject();
        jgen.writeStringField("name", value.getName());
        jgen.writeStringField("type", value.getType().value());
        jgen.writeFieldName("value");

        if (Objects.nonNull(value.getValue())) {
            jgen.writeObject(value.getValue());
        } else {
            jgen.writeNull();
        }

        jgen.writeArrayFieldStart("supplementary");

        if (Objects.nonNull(value.getSupplementary()) && !value.getSupplementary().isEmpty()) {
            for (Object val : value.getSupplementary()) {
                jgen.writeObject(val);
            }
        }

        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}
