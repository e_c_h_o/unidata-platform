/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.data.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.unidata.mdm.core.type.data.Attribute;
import org.unidata.mdm.core.type.data.ComplexAttribute;
import org.unidata.mdm.core.type.data.DataRecord;
import org.unidata.mdm.core.type.data.extended.WinnerInformationComplexAttribute;
import org.unidata.mdm.core.type.data.impl.ComplexAttributeImpl;
import org.unidata.mdm.core.type.data.impl.SerializableDataRecord;
import org.unidata.mdm.data.type.data.EtalonRecord;
import org.unidata.mdm.data.type.keys.RecordOriginKey;
import org.unidata.mdm.rest.data.ro.ComplexAttributeRO;
import org.unidata.mdm.rest.data.ro.NestedRecordRO;
import org.unidata.mdm.rest.data.ro.extended.ExtendedComplexAttributeRO;

/**
 * @author Mikhail Mikhailov
 * REST complex attribute to complex attribute.
 */
public class ComplexAttributeConverter {

    /**
     * Constructor.
     */
    private ComplexAttributeConverter() {
        super();
    }

    /**
     * Copy a list of complex attributes.
     * @param source the source
     * @return collection
     */
    public static Collection<Attribute> from(List<ComplexAttributeRO> source) {

        if (CollectionUtils.isEmpty(source)) {
            return Collections.emptyList();
        }

        List<Attribute> destination = new ArrayList<>(source.size());
        for (ComplexAttributeRO a : source) {
            destination.add(from(a));
        }

        return destination;
    }

    /**
     * Copy a single complex attribute.
     * @param source the source
     * @return target the target
     */
    public static ComplexAttribute from(ComplexAttributeRO source) {

        if (source == null) {
            return null;
        }

        ComplexAttribute target = new ComplexAttributeImpl(source.getName());
        for (NestedRecordRO record : source.getNestedRecords()) {

            SerializableDataRecord nested = new SerializableDataRecord(
                    record.getSimpleAttributes().size() +
                    record.getComplexAttributes().size() +
                    record.getArrayAttributes().size() + 1);

            nested.addAll(SimpleAttributeConverter.from(record.getSimpleAttributes()));
            nested.addAll(from(record.getComplexAttributes()));
            nested.addAll(ArrayAttributeConverter.from(record.getArrayAttributes()));

            target.add(nested);
        }

        return target;
    }

    /**
     * Copy a list of complex attributes.
     * @param source the source
     * @param target the target
     */
    public static void to(Collection<ComplexAttribute> source, List<ComplexAttributeRO> target) {
        if (CollectionUtils.isEmpty(source)) {
            return;
        }

        for (ComplexAttribute sourceAttribute : source) {
            target.add(to(sourceAttribute));
        }
    }

    /**
     * Copy a list of complex attributes.
     * @param source the source
     * @param target the target
     */
    public static void to(Collection<ComplexAttribute> source, List<ComplexAttributeRO> target, EtalonRecord etalonRecord, RecordOriginKey originKey) {
        if (CollectionUtils.isEmpty(source)) {
            return;
        }

        for (ComplexAttribute sourceAttribute : source) {
            target.add(to(sourceAttribute, etalonRecord, originKey));
        }
    }

    /**
     * Copy a single complex attribute.
     * @param source the source
     */
    public static ComplexAttributeRO to(ComplexAttribute source) {

        if (source == null) {
            return null;
        }

        ComplexAttributeRO target = new ComplexAttributeRO();
        target.setName(source.getName());
        for (DataRecord record : source) {
            NestedRecordRO attr = new NestedRecordRO();

            SimpleAttributeConverter.to(record.getSimpleAttributes(), attr.getSimpleAttributes());
            to(record.getComplexAttributes(), attr.getComplexAttributes());
            attr.getArrayAttributes().addAll(ArrayAttributeConverter.to(record.getArrayAttributes()));

            target.getNestedRecords().add(attr);
        }

        return target;
    }

    /**
     * Copy a single complex attribute.
     * @param source the source
     * @param etalonRecord etalon record
     * @param originKey origin Key
     */
    public static ComplexAttributeRO to(ComplexAttribute source, EtalonRecord etalonRecord, RecordOriginKey originKey) {

        if (source == null) {
            return null;
        }

        ExtendedComplexAttributeRO target = new ExtendedComplexAttributeRO();
        target.setName(source.getName());

        ComplexAttribute winnerAttribute = etalonRecord.getComplexAttribute(source.getName());
        target.setWinner(winnerAttribute instanceof WinnerInformationComplexAttribute
                && originKey.getExternalId().equals(((WinnerInformationComplexAttribute) winnerAttribute).getWinnerExternalId())
                && originKey.getSourceSystem().equals(((WinnerInformationComplexAttribute) winnerAttribute).getWinnerSourceSystem()));


        for (DataRecord record : source) {
            NestedRecordRO attr = new NestedRecordRO();

            SimpleAttributeConverter.to(record.getSimpleAttributes(), attr.getSimpleAttributes());
            to(record.getComplexAttributes(), attr.getComplexAttributes());
            attr.getArrayAttributes().addAll(ArrayAttributeConverter.to(record.getArrayAttributes()));

            target.getNestedRecords().add(attr);
        }

        return target;
    }

}
