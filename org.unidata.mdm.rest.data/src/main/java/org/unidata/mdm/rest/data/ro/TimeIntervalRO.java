/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.data.ro;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov
 * Time interval REST object.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeIntervalRO {
    /**
     * Date from.
     */
    private LocalDateTime dateFrom;

    /**
     * Date to.
     */
    private LocalDateTime dateTo;

    /**
     * Is this interval active or not.
     */
    private boolean active;

    /**
     * Contributors.
     */
    private List<ContributorRO> contributors;

    /**
     * Constructor.
     */
    public TimeIntervalRO() {
        super();
    }


    /**
     * @return the dateFrom
     */
    public LocalDateTime getDateFrom() {
        return dateFrom;
    }


    /**
     * @param dateFrom the dateFrom to set
     */
    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }


    /**
     * @return the dateTo
     */
    public LocalDateTime getDateTo() {
        return dateTo;
    }


    /**
     * @param dateTo the dateTo to set
     */
    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }


    /**
     * @return the contributors
     */
    public List<ContributorRO> getContributors() {
        return contributors;
    }


    /**
     * @param contributors the contributors to set
     */
    public void setContributors(List<ContributorRO> contributors) {
        this.contributors = contributors;
    }



    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }



    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

}
