/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class MergePreviewResponseRO.
 *
 * @author ibykov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MergePreviewResponseRO {

    /** The data record. */
    private EtalonRecordRO dataRecord;

    /** The winners. */
    private List<WinnerRO> winners;

    /** The new data record. */
    private EtalonRecordRO manualDataRecord;

    /** The winners. */
    private List<WinnerRO> manualWinners;

    /**
     * Winner etalon id.
     */
    private String winnerEtalonId;

    /**
     * Gets the data record.
     *
     * @return the dataRecord
     */
    public EtalonRecordRO getDataRecord() {
        return dataRecord;
    }

    /**
     * Sets the data record.
     *
     * @param dataRecord
     *            the dataRecord to set
     */
    public void setDataRecord(EtalonRecordRO dataRecord) {
        this.dataRecord = dataRecord;
    }

    /**
     * Gets the winners.
     *
     * @return the winners
     */
    public List<WinnerRO> getWinners() {
        return winners;
    }

    /**
     * Sets the winners.
     *
     * @param winners
     *            the winners to set
     */
    public void setWinners(List<WinnerRO> winners) {
        this.winners = winners;
    }

    /**
     * Gets the new data record.
     *
     * @return the newDataRecord
     */
    public EtalonRecordRO getManualDataRecord() {
        return manualDataRecord;
    }

    /**
     * Sets the new data record.
     *
     * @param manualDataRecord the new manual data record
     */
    public void setManualDataRecord(EtalonRecordRO manualDataRecord) {
        this.manualDataRecord = manualDataRecord;
    }

    /**
     * Gets the manual winners.
     *
     * @return the newWinners
     */
    public List<WinnerRO> getManualWinners() {
        return manualWinners;
    }

    /**
     * Sets the manual winners.
     *
     * @param manualWinners the new manual winners
     */
    public void setManualWinners(List<WinnerRO> manualWinners) {
        this.manualWinners = manualWinners;
    }

    /**
     * Gets the winner etalon id.
     *
     * @return the winner etalon id
     */
    public String getWinnerEtalonId() {
        return winnerEtalonId;
    }

    /**
     * Sets the winner etalon id.
     *
     * @param winnerEtalonId the new winner etalon id
     */
    public void setWinnerEtalonId(String winnerEtalonId) {
        this.winnerEtalonId = winnerEtalonId;
    }
}
