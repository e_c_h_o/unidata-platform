/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.unidata.mdm.rest.data.ro.SimpleAttributeRO;
import org.unidata.mdm.rest.system.serializer.AbstractJsonSerializer;

public class SimpleAttributeSerializer extends AbstractJsonSerializer<SimpleAttributeRO> {

    @Override
    public void serialize(SimpleAttributeRO value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("name", value.getName());
        jgen.writeStringField("type", value.getType()==null?null:value.getType().value());
        jgen.writeStringField("displayValue", value.getDisplayValue());
        jgen.writeStringField("targetEtalonId", value.getTargetEtalonId());
        jgen.writeStringField("valueId", value.getValueId());
        jgen.writeStringField("unitId", value.getUnitId());

        writeValue(value.getType(), value.getValue(), jgen);

        jgen.writeEndObject();
    }
}
