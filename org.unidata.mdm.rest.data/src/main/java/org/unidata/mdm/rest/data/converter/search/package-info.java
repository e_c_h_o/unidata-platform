/**
 * Serach types converters.
 * @author Mikhail Mikhailov on Feb 27, 2020
 */
package org.unidata.mdm.rest.data.converter.search;