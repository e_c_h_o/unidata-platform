/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.converter;

import org.unidata.mdm.data.dto.ErrorInfoDTO;
import org.unidata.mdm.rest.system.ro.ErrorInfo;

/**
 * @author Dmitrii Kopin
 */
public class ErrorInfoToRestErrorInfoConverter {

    /**
     * Constructor.
     */
    private ErrorInfoToRestErrorInfoConverter() {
        super();
    }

    /**
     * Converts error info dto to rest
     *
     * @param source error info dto object
     * @return REST object
     */
    public static ErrorInfo convert(ErrorInfoDTO source) {

        if (source == null) {
            return null;
        }

        ErrorInfo target;
        if(source.getType() != null){
            target = new ErrorInfo(ErrorInfo.Type.valueOf(source.getType()));
        } else {
            target = new ErrorInfo();
        }

        target.setErrorCode(source.getErrorCode());
        target.setUserMessage(source.getUserMessage());
        target.setUserMessageDetails(source.getUserMessageDetails());
        if(source.getSeverity() != null){
            target.setSeverity(ErrorInfo.Severity.valueOf(source.getSeverity().name()));
        }
        return target;
    }
}
