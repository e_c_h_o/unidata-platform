/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.module;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.unidata.mdm.rest.data.service.impl.DataRenderingProvider;
import org.unidata.mdm.rest.data.service.impl.SearchDataRenderingProvider;
import org.unidata.mdm.rest.data.type.rendering.DataRestInputRenderingAction;
import org.unidata.mdm.rest.data.type.rendering.DataRestOutputRenderingAction;
import org.unidata.mdm.system.type.module.Dependency;
import org.unidata.mdm.system.type.module.Module;
import org.unidata.mdm.system.type.rendering.RenderingAction;
import org.unidata.mdm.system.type.rendering.RenderingProvider;

/**
 * @author Alexander Malyshev
 */
public class DataRestModule implements Module {

    private static final List<RenderingAction> RENDERING_ACTIONS
        = ListUtils.union(
                Arrays.asList(DataRestInputRenderingAction.values()),
                Arrays.asList(DataRestOutputRenderingAction.values()));

    private static final List<Dependency> DEPENDENCIES = Arrays.asList(
            new Dependency("org.unidata.mdm.data", "6.0"),
            new Dependency("org.unidata.mdm.search", "6.0"),
            new Dependency("org.unidata.mdm.rest.core", "6.0")
    );

    @Autowired
    private DataRenderingProvider dataRenderingProvider;

    @Autowired
    private SearchDataRenderingProvider searchRenderingProvider;

    @Override
    public String getId() {
        return "org.unidata.mdm.rest.data";
    }

    @Override
    public String getVersion() {
        return "6.0";
    }

    @Override
    public String getName() {
        return "Data Rest Module";
    }

    @Override
    public String getDescription() {
        return "Data Rest Module";
    }

    @Override
    public Collection<Dependency> getDependencies() {
        return DEPENDENCIES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingAction> getRenderingActions() {
        return RENDERING_ACTIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<RenderingProvider> getRenderingProviders() {
        return Arrays.asList(dataRenderingProvider, searchRenderingProvider);
    }

    @Override
    public String[] getResourceBundleBasenames() {
        return new String[]{ "rest_data_messages" };
    }
}
