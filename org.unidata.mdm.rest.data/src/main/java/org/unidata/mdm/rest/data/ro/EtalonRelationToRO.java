/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
package org.unidata.mdm.rest.data.ro;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Mikhail Mikhailov
 * Etalon relation REST definition.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EtalonRelationToRO extends AbstractRelationToRO {
    /**
     * Etalon id of the RELATION.
     */
    private String etalonId;
    /**
     * Etalon id of the TO side.
     */
    private String etalonIdTo;
    /**
     * Etalon display name for 'To side'.
     */
    private String etalonDisplayNameTo;

    /**
     * Constructor.
     */
    public EtalonRelationToRO() {
        super();
    }

    /**
     * @return the etalonId
     */
    public String getEtalonId() {
        return etalonId;
    }

    /**
     * @param etalonId the etalonId to set
     */
    public void setEtalonId(String etalonId) {
        this.etalonId = etalonId;
    }

    /**
     * @return the etalonIdTo
     */
    public String getEtalonIdTo() {
        return etalonIdTo;
    }

    /**
     * @param etalonIdTo the etalonIdTo to set
     */
    public void setEtalonIdTo(String etalonIdTo) {
        this.etalonIdTo = etalonIdTo;
    }


    public String getEtalonDisplayNameTo() {
        return etalonDisplayNameTo;
    }

    public void setEtalonDisplayNameTo(String etalonDisplayNameTo) {
        this.etalonDisplayNameTo = etalonDisplayNameTo;
    }
}
