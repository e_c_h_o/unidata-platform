/*
 * Unidata Platform
 * Copyright (c) 2013-2020, UNIDATA LLC, All rights reserved.
 *
 * Commercial License
 * This version of Unidata Platform is licensed commercially and is the appropriate option for the vast majority of use cases.
 *
 * Please see the Unidata Licensing page at: https://unidata-platform.com/license/
 * For clarification or additional options, please contact: info@unidata-platform.com
 * -------
 * Disclaimer:
 * -------
 * THIS SOFTWARE IS DISTRIBUTED "AS-IS" WITHOUT ANY WARRANTIES, CONDITIONS AND
 * REPRESENTATIONS WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
 * IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, MERCHANTABLE QUALITY,
 * FITNESS FOR A PARTICULAR PURPOSE, DURABILITY, NON-INFRINGEMENT, PERFORMANCE AND
 * THOSE ARISING BY STATUTE OR FROM CUSTOM OR USAGE OF TRADE OR COURSE OF DEALING.
 */
/**
 *
 */
package org.unidata.mdm.rest.data.ro;

import java.util.Date;

/**
 * @author Mikhail Mikhailov
 * Base REST interface for all types of relations.
 */
public interface BaseRelationRO {

    /**
     * @return the relName
     */
    public String getRelName();

    /**
     * @param relName the relName to set
     */
    public void setRelName(String relName);

    /**
     * @return the status
     */
    public String getStatus();

    /**
     * @param status the status to set
     */
    public void setStatus(String status);
    /**
     * @return the createDate
     */
    public Date getCreateDate();

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate);

    /**
     * @return the createdBy
     */
    public String getCreatedBy();

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy);

    /**
     * @return the updateDate
     */
    public Date getUpdateDate();

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate);

    /**
     * @return the updatedBy
     */
    public String getUpdatedBy();

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(String updatedBy);
}
